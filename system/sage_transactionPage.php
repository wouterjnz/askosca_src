<?php
include_once('../includes/config.php');
include_once('../includes/system.php');
include_once('../lib/pdowrapper/class.pdowrapper.php');
$dbConfig = array("host" => $dbhost, "dbname" => $dbname, "username" => $dbuser, "password" => $dbpass);
// get instance of PDO Wrapper object
$db = new PdoWrapper($dbConfig);
include_once('../includes/globalcf.php');
global $db;
$payNowKey = $db->pdoQuery('SELECT service_name, service_key, service_id FROM sage_services WHERE service_id = 14')->result();
$vendorKey = $db->pdoQuery("SELECT name,value,enabled FROM sage_settings WHERE name = 'software_vendor_key'")->result();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ABC Security | Payments</title>

		<!-- FavIcon -->
		<link rel="shortcut icon" href="assets/img/favIcon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="assets/img/favIcon/favicon.ico" type="image/x-icon">

		<link rel="stylesheet" href="../lib/css/bootstrap.min.css">
		<link rel="stylesheet" href="packages/font-awesome-4.2.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="../paynow.css">
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,500' rel='stylesheet' type='text/css'>
	</head>
	<body>
	<div id="headerimg">
    		<div class="row">
        		<div class="col-md-6 col-md-offset-3">
                    <img src="../images/logo.jpg" alt="logo" width="150px">
                </div>
            </div>
	</div>
		<div class="container">
    		<div class="row">
        		<div class="col-md-4 col-md-offset-4">
        			<div class="form-box">
					
<form id='FORMID' method='POST' action='https://paynow.sagepay.co.za/site/paynow.aspx'>
	<input type='hidden' id="m1" name="m1" value='<?php echo $payNowKey['service_key']; ?>'>
	<input type='hidden' id='m2' name='m2' value='<?php echo $vendorKey['value']; ?>'>
	<input type='hidden' id='p2' name='p2' value='<?php echo substr(str_shuffle(md5(microtime())), 0, 15);?>'>
	<input type='hidden' id='Budget' name='Budget' value='N'>
    Payment
	<div class="form-group">
		<label for="p3">Name / Account Number</label>
		<input type='text' id='p3' name='p3' value='services' class="form-control input-lg" required>
		<div class="help-block">
			Please include your name or acc nr after 'services' for reference
		</div>
	</div>

	<div class="form-group">
		<label for="p4">Amount</label>
		<input type='text' id='p4' name='p4' placeholder='eg. 22.50' class="form-control input-lg" required>
		<div class="help-block">
			Please enter the amount you wish to pay
		</div>
	</div>

	<div class="form-group" style="margin-bottom: 50px">
		<label for="m9">Email Address</label>
		<input type='text' id='m9' name='m9' placeholder='steve@example.com' class="form-control input-lg">
	</div>

	<button type="submit" name="submit" class="btn btn-info btn-block btn-lg">
		<i class="fa fa-lock fa-fw"></i> Continue
	</button>
</form>

</div>
        		</div>
    		</div>
		</div>
        <footer class="text-center">
            &copy; <?php date ('Y');?> ABC Security.
        </footer>
	</body>
</html>
<!--
<form id='FORMID' method='POST' action='https://paynow.sagepay.co.za/site/paynow.aspx'> This
is the url
you post to<br/>
<input type='text' id="m1" name="m1" value='2006ce8f-e7c9-4b7a-846d-03d087faa8f2' /> This
is your unique account
service key<br/>
<input type='text' id='m2' name='m2' value='24ade73c-98cf-47b3-99be-cc7b867b3080'
/> This
is a
default value software vendor key<br/>
<input type='text' id='p2' name='p2' value='10121j' /> This
is unique reference number supplied by
you<br/>
<input type='text' id='p3' name='p3' value='Test goods and services' /> This
is the description of good or
products sold & add an purchaser�s name/acc nr to field, if you want to view buyer�s name on your statement<br/>
<input type='text' id='p4' name='p4' value='0.10' /> This
is the amount payable<br/>
<input type='text' id='Budget' name='Budget' value='N' /> This
is budget option<br/>
<input type='text' id='m4' name='m4' value='9005248747847' /> This
is extra fields you can post with info you need to
be sent back(ex: ID nr)<br/>
<input type='text' id='m5' name='m5' value='123' /> This
is extra fields you can post with info you need to
be sent back(ex: cell nr)<br/>
<input type='text' id='m6' name='m6' value='321' /> This
is extra fields you can post with info you need to
be sent back(ex: city/town)<br/>
<input type='text' id='m9' name='m9' value='jamie@gmail.com' /> This
is an email field should you
wish us to mail the card holder<br/>
<input type='text' id='m10' name='m10' value='jamie@gmail.com' /> This
is an email field should you
wish us to mail the card holder<br/>
<input type="submit" name="submit" value="submit"/>
</form>
-->
