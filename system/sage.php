<?php
global $db;

$settings = [];
$db_settings = $db->pdoQuery('SELECT name,value,enabled FROM sage_settings')->results();

foreach ($db_settings as $setting) {
    $settings[$setting['name']] = $setting['value'];
}

$services = [];
$db_services = $db->pdoQuery('SELECT service_name,service_id,service_key,enabled FROM sage_services')->results();

foreach ($db_services as $service) {
    $services[$service['service_name']] = [
        'id' => $service['service_id'],
        'key' => $service['service_key'],
        'enable' => $service['enabled']
    ];
}
?>

<div id="notify" class="alert alert-success" style="display:none;">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <div class="message"></div>
</div>
<div id="bankValidation" class="alert alert-success" style="display:none;">
<a href="#" class="close" data-dismiss="alert">&times;</a>
    <div class="message"></div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <a href="#" class="btn btn-primary" id="action_Validate">Connect & Validate</a>
            <a data-toggle="modal" data-target="#validateBankAccount" class="btn btn-primary">Validate Bank Account</a>
            <a href="#" class="btn btn-primary" target="_blank" id="action_listBanks">List Banks</a>
            <!--<a href="/request/sage.php?action=listbanksbranches" class="btn btn-primary" target="_blank">List Banks &amp; Branches</a>-->
            <a href="#" class="btn btn-primary" id="action_listBB">List Banks &amp; Branches</a>
            <a href="index.php?rdp=sage_batches" class="btn btn-primary" target="_blank">Debit Orders</a>
			<!--<a href="index.php?rdp=sage_outbound_batches" class="btn btn-primary" target="_blank">Outbound Payments</a>-->
            <!--<a href="index.php?rdp=sage_transactionPage" class="btn btn-primary" target="_blank">Pay Now</a>-->
            <a href="paynow/sage_transactionPage.php" class="btn btn-primary" target="_blank">Pay Now</a>
        </div>

        <form method="post" id="validate_sageconnect">
            <input type="hidden" name="act" value="save_settings">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>SagePay Settings</h4>
                    <div class="clear"></div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label margin-bottom">Merchant Account Key</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control margin-bottom" name="merchant_account" value="<?php echo !empty($settings) ? $settings['merchant_account'] : null; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label margin-bottom">Software Vendor Key</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control margin-bottom" name="software_vendor_key" value="<?php echo !empty($settings) ? $settings['software_vendor_key'] : null; ?>"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>SagePay Services</h4>
                    <div class="clear"></div>
                </div>
                <div class="panel-body">
					<div class="form-group row">
					<label class="control-label margin-bottom">Debit Orders</label>
					<hr/>
						<div class="form-group">
							<label class="col-sm-2">Service Key</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="services[debit_orders][key]" value="<?php echo !empty($services) && isset($services['debit_orders']) ? $services['debit_orders']['key'] : null; ?>"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2">Service ID</label>
							<div class="col-sm-10">
							<input type="text" class="form-control" name="services[debit_orders][id]" value="<?php echo !empty($services) && isset($services['debit_orders']) ? $services['debit_orders']['id'] : null; ?>" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2">Enable Service</label>
							<div class="col-sm-10">
						<input type="checkbox" name="services[debit_orders][enable]" <?php echo !empty($services) && isset($services['debit_orders']) && $services['debit_orders']['enable'] ? "checked" : null; ?>/>
						</div>
						</div>
					</div>					
                    <div class="form-group row">
                        <label class="control-label margin-bottom">Creditors Payments</label>
						<hr/>
                       <div class="form-group">
								<label class="col-sm-2">Service Key</label>
								<div class="col-sm-10">
                            <input type="text" class="form-control" name="services[creditors_payments][key]" value="<?php echo !empty($services) && isset($services['creditors_payments']) ? $services['creditors_payments']['key'] : null; ?>"/>
						</div>
						</div>
						<div class="form-group">
								<label class="col-sm-2">Service ID</label>
								<div class="col-sm-10">
                            <input type="text" class="form-control" name="services[creditors_payments][id]" value="<?php echo !empty($services) && isset($services['creditors_payments']) ? $services['creditors_payments']['id'] : null; ?>" />
							</div>
							</div>
						<div class="form-group">
								<label class="col-sm-2">Enable</label>
								<div class="col-sm-10">
                            <input type="checkbox"  name="services[creditors_payments][enable]"  <?php echo !empty($services) && isset($services['creditors_payments']) && $services['creditors_payments']['enable'] ? "checked" : null; ?>/>
                        </div>
                        </div>
					</div>
                    <div class="form-group row">
                        <label class="control-label margin-bottom">Rist Reports</label>
						<hr/>
                        <div class="form-group">
							<label class="col-sm-2">Service Key</label>
							<div class="col-sm-10">
                            <input type="text" class="form-control" name="services[risk_reports][key]" value="<?php echo !empty($services) && isset($services['risk_reports']) ? $services['risk_reports']['key'] : null; ?>"/>
						</div>
						</div>
						 <div class="form-group">
								<label class="col-sm-2">Service ID</label>
								<div class="col-sm-10">
                            <input type="text" class="form-control" name="services[risk_reports][id]" value="<?php echo !empty($services) && isset($services['risk_reports']) ? $services['risk_reports']['id'] : null; ?>" />
						</div>
						</div>
						 <div class="form-group">
								<label class="col-sm-2">Enable</label>
							<div class="col-sm-10">
                            <input type="checkbox" name="services[risk_reports][enable]" <?php echo !empty($services) && isset($services['risk_reports']) && $services['risk_reports']['enable'] ? "checked" : null; ?> />
                        </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label margin-bottom">Account Service</label>
						<hr/>
                       	 <div class="form-group">
								<label class="col-sm-2">Service Key</label>
							<div class="col-sm-10">
                            <input type="text" class="form-control" name="services[account_service][key]" value="<?php echo !empty($services) && isset($services['account_service']) ? $services['account_service']['key'] : null; ?>"/>
					</div>
                        </div>		
						 <div class="form-group">
								<label class="col-sm-2">Service ID</label>
							<div class="col-sm-10">
                            <input type="text" class="form-control" name="services[account_service][id]" value="<?php echo !empty($services) && isset($services['account_service']) ? $services['account_service']['id'] : null; ?>" />
						</div>
                        </div>
						<div class="form-group">
								<label class="col-sm-2">Enable</label>
							<div class="col-sm-10">
                            <input type="checkbox"  name="services[account_service][enable]" <?php echo !empty($services) && isset($services['account_service']) && $services['account_service']['enable'] ? "checked" : null; ?> />
                        </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label margin-bottom">Salary Payments</label>
						<hr/>
                        <div class="form-group">
							<label class="col-sm-2">Service Key</label>
							<div class="col-sm-10">
                            <input type="text" class="form-control" name="services[salary_payments][key]" value="<?php echo !empty($services) && isset($services['salary_payments']) ? $services['salary_payments']['key'] : null; ?>"/>
							</div>
                        </div>
						<div class="form-group">
							<label class="col-sm-2">Service ID</label>
							<div class="col-sm-10">
                            <input type="text" class="form-control" name="services[salary_payments][id]" value="<?php echo !empty($services) && isset($services['salary_payments']) ? $services['salary_payments']['id'] : null; ?>" />
							</div>
                        </div>
						<div class="form-group">
							<label class="col-sm-2">Enable</label>
							<div class="col-sm-10">
                            <input type="checkbox"  name="services[salary_payments][enable]" <?php echo !empty($services) && isset($services['salary_payments']) && $services['salary_payments']['enable'] ? "checked" : null; ?> />
							</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label margin-bottom">Pay Now</label>
						<hr/>
                        <div class="form-group">
							<label class="col-sm-2">Service Key</label>
							<div class="col-sm-10">
                            <input type="text" class="form-control" name="services[pay_now][key]" value="<?php echo !empty($services) && isset($services['pay_now']) ? $services['pay_now']['key'] : null; ?>" />
							</div>
                        </div>
						 <div class="form-group">
							<label class="col-sm-2">Service ID</label>
							<div class="col-sm-10">	
                            <input type="text" class="form-control" name="services[pay_now][id]" value="<?php echo !empty($services) && isset($services['pay_now']) ? $services['pay_now']['id'] : null; ?>" />
							</div>
                        </div>
						 <div class="form-group">
							<label class="col-sm-2">Enable</label>
							<div class="col-sm-10">
								<input type="checkbox"  name="services[pay_now][enable]" <?php echo !empty($services) && isset($services['pay_now']) && $services['pay_now']['enable'] ? "checked" : null; ?> />
							</div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-sm-5 margin-bottom">
                            <input type="submit" id="action_validate_sageconnect"
                            class="btn btn-success float-left" value="Submit"
                            data-loading-text="Creating...">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="validateBankAccount" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Validate Bank Account</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
				<label>Account Number</label>
				<input type="text" name="ac" value="62469490206" class="form-control margin-bottom" />
				</div>
				<div class="form-group">
			<label>Branch Code</label>
				<input type="text" name="bc" value="250655" class="form-control margin-bottom" />
			<label>TP</label>
				<input type="text" name="tp" value="1" class="form-control margin-bottom" />
			</div>
			</div>
			<div class="form-group">
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" id="action_validateBankAccount">Submit</button>
				<button type="butotn" data-dismiss="modal" class="btn">Cancel</button>
			</div>
			</div>
		</div>
	</div>
</div>

<div id="listBanks" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Validate Bank Account</h4>
			</div>
			<div class="modal-body">
				 <div id="listbanksdata"></div>
			</div>
			<div class="form-group">
			<div class="modal-footer">
				<button type="butotn" data-dismiss="modal" class="btn">Close</button>
			</div>
			</div>
		</div>
	</div>
</div>

<div id="validateDetails" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Connect & Validate</h4>
			</div>
			<div class="modal-body">
				 <div id="validateDetailsData"></div>
			</div>
			<div class="form-group">
			<div class="modal-footer">
				<button type="butotn" data-dismiss="modal" class="btn">Close</button>
			</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $("#action_validate_sageconnect").click(function (e) {
        e.preventDefault();
        actionValidateSageConnection();
    });

    function actionValidateSageConnection() {
        var errorNum = farmCheck();

        if (errorNum > 0) {
            $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
            $("#notify .message").html("<strong>Error</strong>: It appears you have forgotten to complete something!");
            $("html, body").animate({scrollTop: $('#notify').offset().top}, 1000);
        } else {
            $(".required").parent().removeClass("has-error");

            var $btn = $("#action_validate_sageconnect").button("loading");

            $.ajax({
                url: 'request/sage.php',
                type: 'POST',
                data: $("#validate_sageconnect").serialize(),
                dataType: 'json',
                success: function (data) {
                    $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#notify").removeClass("alert-warning").addClass("alert-success").fadeIn();
                    $("html, body").animate({scrollTop: $('#notify').offset().top}, 1000);
                    $btn.button("reset");
                },
                error: function (data) {
                    $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
                    $("html, body").animate({scrollTop: $('#notify').offset().top}, 1000);
                    $btn.button("reset");
                }

            });
        }

    }
</script>

<script type="text/javascript">
    $("#action_validateBankAccount").click(function (e) {
        e.preventDefault();
        action_validateBankAccount();
    });

    function action_validateBankAccount() {
        var errorNum = farmCheck();

        if (errorNum > 0) {
            $("#v").removeClass("alert-success").addClass("alert-warning").fadeIn();
            $("#bankValidation .message").html("<strong>Error</strong>: It appears you have forgotten to complete something!");
            $("html, body").animate({scrollTop: $('#bankValidation').offset().top - 100 }, 1000);
        } else {
            $(".required").parent().removeClass("has-error");

            var $btn = $("#action_validate_sageconnect").button("loading");
				
			var ac = $("input[name='ac']").val();	
			var bc = $("input[name='bc']").val();	
			var tp = $("input[name='tp']").val();	
				
            $.ajax({
                url: 'request/sage.php?action=bankaccount&ac='+ac+'&bc='+bc+'&tp='+tp,
                type: 'POST',
                /* data: $("#validate_sageconnect").serialize(),*/
                dataType: 'json',
                success: function (data) {
                    $("#bankValidation .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#bankValidation").removeClass("alert-warning").addClass("alert-success").fadeIn();
                    $("html, body").animate({scrollTop: $('#bankValidation').offset().top -100 }, 1000);
					$('#validateBankAccount').modal('hide');
                    // $btn.button("reset");
                },
                error: function (data) {
                    $("#bankValidation .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#bankValidation").removeClass("alert-success").addClass("alert-warning").fadeIn();
                    $("html, body").animate({scrollTop: $('#bankValidation').offset().top - 100}, 1000);
                    // $btn.button("reset");
                }

            });
        }

    }
</script>

<script type="text/javascript">
    $("#action_listBanks").click(function (e) {
        e.preventDefault();
        action_listBanks();
    });

    function action_listBanks() {
        var errorNum = farmCheck();

        if (errorNum > 0) {
            $("#v").removeClass("alert-success").addClass("alert-warning").fadeIn();
            $("#bankValidation .message").html("<strong>Error</strong>: It appears you have forgotten to complete something!");
            $("html, body").animate({scrollTop: $('#bankValidation').offset().top - 100 }, 1000);
        } else {
            $(".required").parent().removeClass("has-error");
				
            $.ajax({
                url: 'request/sage.php?action=listbanks',
                type: 'POST',
                data: '',
                dataType: 'json',
                success: function (data) {
                    $("#bankValidation .message").html("<strong>" + data.status + "</strong>");
                    $("#bankValidation").removeClass("alert-warning").addClass("alert-success").fadeIn();
                    $("html, body").animate({scrollTop: $('#bankValidation').offset().top -100 }, 1000);
			$('#listBanks').modal('show');
			$('#listbanksdata').html(data.message);
                    // $btn.button("reset");
                },
                error: function (data) {
                    $("#bankValidation .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#bankValidation").removeClass("alert-success").addClass("alert-warning").fadeIn();
                    $("html, body").animate({scrollTop: $('#bankValidation').offset().top - 100}, 1000);
                    // $btn.button("reset");
                }

            });
        }

    }
</script>

<script type="text/javascript">
    $("#action_listBB").click(function (e) {
        e.preventDefault();
        action_listBB();
    });

    function action_listBB() {
        var errorNum = farmCheck();

        if (errorNum > 0) {
            $("#v").removeClass("alert-success").addClass("alert-warning").fadeIn();
            $("#bankValidation .message").html("<strong>Error</strong>: It appears you have forgotten to complete something!");
            $("html, body").animate({scrollTop: $('#bankValidation').offset().top - 100 }, 1000);
        } else {
            $(".required").parent().removeClass("has-error");
				
            $.ajax({
                url: 'request/sage.php?action=listbanksbranches',
                type: 'POST',
                data: '',
                dataType: 'json',
                success: function (data) {
                    $("#bankValidation .message").html("<strong>" + data.status + "</strong>");
                    $("#bankValidation").removeClass("alert-warning").addClass("alert-success").fadeIn();
                    $("html, body").animate({scrollTop: $('#bankValidation').offset().top -100 }, 1000);
			$('#listBB').modal('show');
			$('#listbbdata').html(data.message);
                    // $btn.button("reset");
                },
                error: function (data) {
                    $("#bankValidation .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#bankValidation").removeClass("alert-success").addClass("alert-warning").fadeIn();
                    $("html, body").animate({scrollTop: $('#bankValidation').offset().top - 100}, 1000);
                    // $btn.button("reset");
                }

            });
        }

    }
</script>

<script type="text/javascript">
    $("#action_Validate").click(function (e) {
        e.preventDefault();
        action_Validate();
    });

    function action_Validate() {
        var errorNum = farmCheck();

        if (errorNum > 0) {
            $("#v").removeClass("alert-success").addClass("alert-warning").fadeIn();
            $("#bankValidation .message").html("<strong>Error</strong>: It appears you have forgotten to complete something!");
            $("html, body").animate({scrollTop: $('#bankValidation').offset().top - 100 }, 1000);
        } else {
            $(".required").parent().removeClass("has-error");
				
            $.ajax({
                url: 'request/sage.php?action=services',
                type: 'POST',
                data: '',
                dataType: 'json',
                success: function (data) {
                    $("#bankValidation .message").html("<strong>" + data.status + "</strong>");
                    $("#bankValidation").removeClass("alert-warning").addClass("alert-success").fadeIn();
                    $("html, body").animate({scrollTop: $('#bankValidation').offset().top -100 }, 1000);
			$('#validateDetails').modal('show');
			$('#validateDetailsData').html(data.message);
                    // $btn.button("reset");
                },
                error: function (data) {
                    $("#bankValidation .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#bankValidation").removeClass("alert-success").addClass("alert-warning").fadeIn();
                    $("html, body").animate({scrollTop: $('#bankValidation').offset().top - 100}, 1000);
                    // $btn.button("reset");
                }

            });
        }

    }
</script>