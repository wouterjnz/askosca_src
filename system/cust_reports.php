<?php
// print_r($siteConfig);
// echo $siteConfig['curr'];
if (stristr(htmlentities($_SERVER['PHP_SELF']), "reports.php")) {
    die("Internal Server Error!");
}

if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}
if (isset($_GET['id'])) {
    $id = intval($_GET['id']);
} else {
    $id = 0;
}

switch ($op) {
	case "vat":
        vat_report();
        break;
	case "client_statement":
	    client_statement();
        break;
	case "supplier_statement":
	    supplier_statement();
        break;
	case "outstanding_invoices":
	    outstanding_invoices();
        break;
	case "stock_report":
	    stock_report();
        break;
	case "ledger":
	    ledger();
        break;
    default:
        overview();
        break;
}


function vat_report()
{
		global $siteConfig;
		global $db;
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#mlist').DataTable({
                stateSave: true
            });
        });
    </script>
	
	
	   <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Vat Reports
                    <form method="post" action="index.php?rdp=manager">
                        <input type="hidden" name="update" value="true"><input type="submit"
                                                                               class="btn btn-success btn-sm"
                                                                               value="Refresh Reports">
                    </form>
                </div>

                <div class="panel-body tbl">
				<form method="post" action="index.php?rdp=cust_reports&op=client_statement">
					
					<!--
					<div class="row">
						<div class="col-sm-2">
							<label class="control-label margin-bottom"> Select Customer</label>
						</div>
						<div class="col-sm-8">
							<select name="customer" class="form-control margin-bottom" />
								<option value="--">--</option>
								<?php foreach($results1 as $res){ ?>
								<option value="<?php echo $res['id'];?>" <?php echo ($cid == $res['id'])? 'selected':''; ?>><?php echo $res['name'];?></option>
								<?php } ?>
							</select>
						</div>
						</div>
					
					<div class="row">
					<div class="col-sm-2">
					<label class="control-label margin-bottom"> Date From </label>
					</div>
                    <div class="col-sm-8">
					<div class="input-group date" id="tsn_due">
						<input type="text" name="date_from"  class="form-control margin-bottom required"  
						value="<?php  echo $fdate; ?>" 
						data-date-format="YYYY-MM-DD" />	
						<span class="input-group-addon margin-bottom"><span class="icon-calendar margin-bottom"></span></span>
					</div>
					</div>
					</div>
					
				<div class="row">
					<div class="col-sm-2">
					<label class="control-label margin-bottom"> Date To </label>
					</div>
                    <div class="col-sm-8">
					<div class="input-group date" id="tsn_due">
						<input type="text" name="date_to"  class="form-control margin-bottom required"  
						value="<?php  echo $tdate; ?>" 
						data-date-format="YYYY-MM-DD" />	
						<span class="input-group-addon margin-bottom"><span class="icon-calendar margin-bottom"></span></span>
					</div>					
					</div>					
				</div>
				<div class="form-group row">
				<input type="submit" value="submit" name="submit" class="btn btn-success float-left margin-bottom" />
				</div>
				</form>
				-->
				
				<?php
					
					$day = [];
                    global $db;
					
					$months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
					$monthM = ['Jan', 'Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Aug','Nov','Dec'];
					
					$combined = array_combine($months,$monthM);
				
					for($i = 0; $i < count($months); $i++){
						$query2[$i] = "SELECT sum(tax) as vat FROM invoices WHERE status = 'paid' AND (tsn_due BETWEEN '".date('Y-'.$months[$i].'-01')."' AND '".date('Y-'.$months[$i].'-30')."') ORDER BY id DESC";
						$rdata[$months[$i]] = $db->pdoQuery($query2[$i])->result();
					}
					
                    ?>
					
					<table class="table cell-border" cellspacing="0">
					<tr>
					<?php foreach($months as $month){ ?>	
						<td>
						<?php echo $combined["$month"];  ?>
						</td>
					<?php } ?>
					</tr>
					<?php 
						foreach($rdata as $key => $value){
							$data[$key] = ''; 
							foreach($months as $month){ 
								if($key == $month){ 
									$data[$month] = $value['vat'];	 
								}
							}
						}
					 ?>
					<tr>
					<?php
					foreach($months as $month){ 
						foreach($data as $dat => $kk){
							if($dat == $month){
					?>	
					<td><?php echo ($kk) ? $siteConfig['curr'].$kk : ' -- '; ?></td>
					<?php } } } ?>
					</tr>
					</table>
                </div>
            </div>
        </div>
    </div>
	
	
	
	
	<?php
}
function client_statement()
{
	global $siteConfig;
	global $db;
	?>
	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Client Statement
                    <form method="post" action="index.php?rdp=manager">
                        <!--<input type="hidden" name="update" value="true"><input type="submit" class="btn btn-success btn-sm" value="Refresh Reports">-->
                    </form>
						    <!--<a href="#" class="btn btn-success btn-xs grahk-select">Select Client</a>-->
                </div>
                
                <div class="panel-body tbl">
				<?php 
					$query1 = "SELECT * FROM reg_customers";
                    $results1 = $db->pdoQuery($query1)->results();
					$selected = '';
					
						$fdate = date('Y-m-d', strtotime($siteConfig['date']));
						$tdate = date('Y-m-d', strtotime($siteConfig['date']));
						
					if(isset($_POST['customer']))
					{
						$cid = $_POST['customer'];
						$fdate = date('Y-m-d', strtotime($_POST['date_from']));
						$tdate = date('Y-m-d', strtotime($_POST['date_to']));
					}
				?>
				<form method="post" action="index.php?rdp=cust_reports&op=client_statement">
					
					
					<div class="row">
						<div class="col-sm-2">
							<label class="control-label margin-bottom"> Select Customer</label>
						</div>
						<div class="col-sm-8">
							<select name="customer" class="form-control margin-bottom" />
								<option value="--">--</option>
								<?php foreach($results1 as $res){ ?>
								<option value="<?php echo $res['id'];?>" <?php echo ($cid == $res['id'])? 'selected':''; ?>><?php echo $res['name'];?></option>
								<?php } ?>
							</select>
						</div>
						</div>
					
					<div class="row">
					<div class="col-sm-2">
					<label class="control-label margin-bottom"> Date From </label>
					</div>
                    <div class="col-sm-8">
					<div class="input-group date" id="tsn_due">
						<input type="text" name="date_from"  class="form-control margin-bottom required"  
						value="<?php  echo $fdate; ?>" 
						data-date-format="YYYY-MM-DD" />	
						<span class="input-group-addon margin-bottom"><span class="icon-calendar margin-bottom"></span></span>
					</div>
					</div>
					</div>
					
				<div class="row">
					<div class="col-sm-2">
					<label class="control-label margin-bottom"> Date To </label>
					</div>
                    <div class="col-sm-8">
					<div class="input-group date" id="tsn_due">
						<input type="text" name="date_to"  class="form-control margin-bottom required"  
						value="<?php  echo $tdate; ?>" 
						data-date-format="YYYY-MM-DD" />	
						<span class="input-group-addon margin-bottom"><span class="icon-calendar margin-bottom"></span></span>
					</div>					
					</div>					
				</div>
				<div class="form-group row">
				<input type="submit" value="submit" name="submit" class="btn btn-success float-left margin-bottom" />
				</div>
				</form>
				<?php
					$day = [];
					$results = '';
					if(isset($_POST['customer']))
					{
						
						$id = $_POST['customer'];
						$query = "SELECT 
									i.total, i.status, c.name, c.custCode, i.tid, i.tsn_due, i.tsn_date, i.status
									FROM reg_customers c JOIN invoices i ON i.csd = c.id
									WHERE c.id = $id AND (DATE(i.tsn_date) BETWEEN '$fdate' AND '$tdate')";
						$results = $db->pdoQuery($query)->results();
						if(empty($results)){ echo  '<hr/><center><h3>Sorry. No invoice data for client found.</h3></center>'; exit; }
					}
                    // $query = "SELECT * FROM invoices WHERE status != 'paid' ORDER BY id DESC";
                    // $query = "SELECT total, tid, tsn_due FROM invoices ORDER BY id DESC";
					// print_r($results);
					if(empty($results)){
						exit;
					}
					
					foreach($results as $result)
					{
						if($result['status'] != 'paid'){
							if($result['tsn_due']  <= date('Y-m-d', strtotime("-120 Days", strtotime(date('Y-m-d'))))){
								$day['120'][$result['tid']]['total'][] = $result['total'];
							}elseif($result['tsn_due']  <= date('Y-m-d', strtotime("-90 Days", strtotime(date('Y-m-d')))) && date('Y-m-d', strtotime("+120 Days", strtotime(date('Y-m-d')))) > $result['tsn_due']){
								$day['90'][$result['tid']]['total'][] = $result['total'];
							}elseif($result['tsn_due']  <= date('Y-m-d', strtotime("-60 Days", strtotime(date('Y-m-d')))) && date('Y-m-d', strtotime("+90 Days", strtotime(date('Y-m-d')))) > $result['tsn_due']){
								$day['60'][$result['tid']]['total'][] = $result['total'];
							}elseif($result['tsn_due']  <= date('Y-m-d', strtotime("-30 Days", strtotime(date('Y-m-d')))) && date('Y-m-d', strtotime("+60 Days", strtotime(date('Y-m-d')))) > $result['tsn_due']){
								$day['30'][$result['tid']]['total'][] = $result['total'];
							}else{
								$day['current'][$result['tid']]['total'][] = $result['total'];
							}
						}
					}	
					
                    ?>
					<hr/>
				<h2>Statement For : <?php echo $results[0]['name'];?></h2>
				<!--<input type="text" name="grahak_name" id="grahak_name" />-->
					<table class="table cell-border" cellspacing="0">	
						<tr>
							<td><strong>Date</strong></td>
							<td><strong>Inv No</strong></td>
							<td><strong>Debit</strong></td>
							<td><strong>Credit</strong></td>
						</tr>
					<?php foreach($results as $result){ ?>	
						<tr>
							<td><?php echo $result['tsn_date']; ?></td>
							<td><?php echo $result['tid']; ?></td>
							<td><?php echo ($result['status'] == 'paid')? $result['total']: ''; ?></td>
							<td><?php echo ($result['status'] != 'paid')? $result['total'] : '' ; ?></td>
						</tr>
					<?php } ?>
					</table>
					
					<table class="table cell-border" cellspacing="0">
						<tr>
							<td><strong>120+ Days</strong></td>
							<td><strong>90 Days</strong></td>
							<td><strong>60 Days</strong></td>
							<td><strong>30 Days</strong></td>
							<td><strong>Current</strong></td>
							<td><strong>Total</strong></td>
						</tr>
						<tr>
						<?php 
								$fulltotal = 0;
								foreach($day as $iday => $val){ 
									foreach($val as $v => $k){
									$fulltotal += $k['total'][0];	
							?>
							<td><?php if($iday == '120'){ echo $siteConfig['curr'].$k['total'][0]; }else{ echo '--'; }  ?></td>
							<td><?php if($iday == '90'){ echo $siteConfig['curr'].$k['total'][0]; }else{ echo '--'; }  ?></td>
							<td><?php if($iday == '60'){ echo $siteConfig['curr'].$k['total'][0]; }else{ echo '--'; }  ?></td>
							<td><?php if($iday == '30'){ echo $siteConfig['curr'].$k['total'][0]; }else{ echo '--'; }  ?></td>
							<td><?php if($iday == 'current'){ echo $siteConfig['curr'].$k['total'][0]; }else{ echo '--'; }  ?></td>
						<?php }} ?>
							<td><?php echo ($fulltotal > 0)? $fulltotal:'';?></td>
						</tr>
					</table>
                </div>
            </div>
        </div>
    </div>
	<?php unset($results); unset($day); ?>
	
	<div id="insert_grahk" class="modal fade">
    <div class="modal-dialog lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select an existing customer</h4>
            </div>
            <div class="modal-body">
                <?php

                cstPL();

                ?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn">Cancel</button>
            </div>
        </div>
    </div>
</div>


<?php 
	}
	
	function ledger(){
		
	
	?>
	
		<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">General Ledger
                    <form method="post" action="index.php?rdp=manager">
                        <input type="hidden" name="update" value="true"><input type="submit"
                                                                               class="btn btn-success btn-sm"
                                                                               value="Refresh Reports">
                    </form>
                </div>
                <br><br>

							
				  <div class="panel-body tbl">
				  <?php
					global $siteConfig;
                    global $db;
					$day = [];
					
					//GET INCOMES & EXPENSES
                    $query = "	SELECT * FROM ac_balance";
                    $results = $db->pdoQuery($query)->results();
					
					// GET INVOICE SALES
					$queryinv = "	SELECT * FROM invoices i Join invoice_items ii ON i.tid = ii.tid where i.status = 'paid' ";
                    $resultsinv = $db->pdoQuery($queryinv)->results();
					
					// GET BANKING TRANSACTIONS
					$qbankInc = "	SELECT * FROM ac_transactions";
                    $rbankInc = $db->pdoQuery($qbankInc)->results();
					
					// GET STOCK MANAGEMENT RECEIPTS
					$qstock = "	SELECT * FROM receipts r JOIN receipts_items ri ON ri.tid = r.tid WHERE r.status = 'paid' ";
                    $rstock = $db->pdoQuery($qstock)->results();
					
					//GET RETURNS
                    $returnsq = "	SELECT * FROM returnc r JOIN return_items ri ON ri.tid = r.tid WHERE r.status = 'refunded' ";
                    $returnsr = $db->pdoQuery($returnsq)->results();
					
					//GET DAMAGES
                    $damagesq = "	SELECT * FROM damage d JOIN damage_items di ON di.tid = d.tid WHERE d.status = 'refunded' ";
                    $damagesr = $db->pdoQuery($damagesq)->results();
					
					foreach($results as $result){
						if($result['stat'] == 1){		
							$data['incomes'][] = $result; 
						}else{
							$data['expenses'][] = $result; 
						}
					}
					
					foreach($resultsinv as $resultinv){
							$data['incomes'][] = $resultinv; 
					}
					
					foreach($rbankInc as $resultb){
						if($resultb['stat'] == 'Cr'){
							$data['incomes'][] = $resultb; 
						}else{
							$data['expenses'][] = $resultb; 
						}
					}
					
					foreach($rstock as $stock){
							$data['expenses'][] = $stock; 
					}
					
					foreach($returnsr as $rreturns){
							$data['expenses'][] = $rreturns; 
					}
					
					foreach($damagesr as $rdamages){
							$data['expenses'][] = $rdamages; 
					}
					
					
					//GET INCOMES & EXPENSES
                    $queryt = "	SELECT * FROM ac_transactions ORDER BY tdate DESC	";
                    $resultst = $db->pdoQuery($queryt)->results();
					
					
					// echo "<pre/>";
					// print_r($resultst);
                    ?>
				<table class="table cell-border" cellspacing="0">
					<tr>
						<td>Transaction No</td>
						<td>Debit</td>
						<td>Credit</td>
						<td>Note</td>
						<td>Date</td>
					</tr>
					<?php foreach($resultst as $trans){ ?>
					<tr>
						<td><?php echo $trans['tno']; ?></td>
						<td><?php echo ($trans['stat'] == 'Dr')? $trans['amount']:''; ?></td>
						<td><?php echo ($trans['stat'] == 'Cr')? $trans['amount']:''; ?></td>
						<td><?php echo $trans['note']; ?></td>
						<td><?php echo $trans['tdate']; ?></td>
					</tr>
					<?php } ?>
				</table>
				
                </div>
            </div>
        </div>
    </div>
	
	<?php 
		unset($results);
		unset($data); 	
	?>
	
	<?php } 
	
	function supplier_statement()
	{
	?>
<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Supplier Statement
                    <form method="post" action="index.php?rdp=manager">
                        <input type="hidden" name="update" value="true"><input type="submit"
                                                                               class="btn btn-success btn-sm"
                                                                               value="Refresh Reports">
                    </form>
                </div>
                <br><br>

							
				  <div class="panel-body tbl">
				  <?php
					global $siteConfig;
                    global $db;
					$day = [];
					
                    $query = "	SELECT 
										*
										FROM
											reg_vendors v
										JOIN
											receipts r ON r.csd = v.id		
										";
										
                    $results = $db->pdoQuery($query)->results();
					
					foreach($results as $result){
						$data[$result['name']][] = $result; 
					}
                    ?>
				
					<table class="table cell-border" cellspacing="0">
					<tr>
						<td>Vendor</td>
						<td>Transaction Date</td>
						<td>Status</td>
						<td>Total</td>
					</tr>
					<?php 
					$grandtotal = 0;
					$totalowed = 0;
					foreach($data as $dat => $val){ 
					$name = 0;
					$total = 0;
							foreach($val as $value){ 
							$name += 1;
							$total += $value['total'];
							$grandtotal += $value['total'];
							if($value['status'] != 'paid'){
								$totalowed += $value['total'];
							}
					?>
					<tr style="<?php echo ($value['status'] != 'paid')? 'color: red': ''; ?>">
						<td><?php echo ($name <= 1)? $value['name']:''; ?></td>
						<td><?php echo $value['tsn_date']; ?></td>
						<td><?php echo $value['status']; ?></td>
						<td><?php  echo $value['total']; ?></td>
					</tr>
					<?php if($name > 1){ ?>
					<tr bgcolor="#eee">
					<td></td>
					<td></td>
					<td><strong>Total</strong></td>
					<td><strong><?php echo $total; ?></strong></td>
					</tr>
					<?php } ?>
					<?php } } ?>
					<tr bgcolor="#eee">
					<td></td>
					<td></td>
					<td><strong>Total Paid</strong></td>
					<td><strong><?php echo $grandtotal; ?></strong></td>
					</tr>
					<tr bgcolor="#f99">
					<td></td>
					<td></td>
					<td><strong>Total Unpaid</strong></td>
					<td><strong><?php echo $totalowed; ?></strong></td>
					</tr>
					</table>
                </div>
            </div>
        </div>
    </div>
	
	<?php 
		unset($results);
		unset($data); 
		unset($all); 
	?>
	

	<?php } 
	function outstanding_invoices()
	{
	?>
<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Invoices Outstanding
                    <form method="post" action="index.php?rdp=manager">
                        <input type="hidden" name="update" value="true"><input type="submit"
                                                                               class="btn btn-success btn-sm"
                                                                               value="Refresh Reports">
                    </form>
                </div>
                <br><br>
                <div class="panel-body tbl"><?php
					global $siteConfig;
					$day = [];
                    global $db;
                    $query = "SELECT * FROM invoices WHERE status != 'paid' ORDER BY id DESC";
                    // $query = "SELECT total, tid, tsn_due FROM invoices ORDER BY id DESC";
                    $results = $db->pdoQuery($query)->results();
					
					foreach($results as $result)
					{
						if($result['tsn_due']  <= date('Y-m-d', strtotime("-120 Days", strtotime(date('Y-m-d'))))){
							$day['120'][$result['tid']]['total'][] = $result['total'];
						}elseif($result['tsn_due']  <= date('Y-m-d', strtotime("-90 Days", strtotime(date('Y-m-d')))) && date('Y-m-d', strtotime("+120 Days", strtotime(date('Y-m-d')))) > $result['tsn_due']){
							$day['90'][$result['tid']]['total'][] = $result['total'];
						}elseif($result['tsn_due']  <= date('Y-m-d', strtotime("-60 Days", strtotime(date('Y-m-d')))) && date('Y-m-d', strtotime("+90 Days", strtotime(date('Y-m-d')))) > $result['tsn_due']){
							$day['60'][$result['tid']]['total'][] = $result['total'];
						}elseif($result['tsn_due']  <= date('Y-m-d', strtotime("-30 Days", strtotime(date('Y-m-d')))) && date('Y-m-d', strtotime("+60 Days", strtotime(date('Y-m-d')))) > $result['tsn_due']){
							$day['30'][$result['tid']]['total'][] = $result['total'];
						}else{
							$day['current'][$result['tid']]['total'][] = $result['total'];
						}	
					}			
                    ?>
				
					<table class="table cell-border" cellspacing="0">
					<tr>
						<td>Inv #</td>
						<td>Current</td>
						<td>30 Days</td>
						<td>60 Days</td>
						<td>90 Days</td>
						<td>120 Days</td>
					</tr>
					<?php foreach($day as $iday => $val){ 
						foreach($val as $v => $k){
					?>
					<tr>
					<td>
					<?php echo $v; ?>
					</td>
						<td>
						<?php 
							if($iday == 'current'){ echo $siteConfig['curr'].$k['total'][0]; }else{ echo '--'; } 
						?>
						</td>
					<td>
						<?php 
							if($iday == '30'){ echo $siteConfig['curr'].$k['total'][0]; }else{ echo '--'; }  
						?>
						</td>
						<td>
						<?php 
							if($iday == '60'){ echo $siteConfig['curr'].$k['total'][0]; }else{ echo '--'; }  
						?>
						</td>
						<td>
						<?php 
							if($iday == '90'){ echo $siteConfig['curr'].$k['total'][0]; }else{ echo '--'; }  
						?>
						</td>
						<td>
						<?php 
							if($iday == '120'){ echo $siteConfig['curr'].$k['total'][0]; }else{ echo '--'; }  
						?>
						</td>	
					</tr>
					<?php } } ?>
					</table>
                </div>
            </div>
        </div>
    </div>

	<?php } 
	
	function stock_report()
	{
		
	?>
<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Stock Report
                    <form method="post" action="index.php?rdp=manager">
                        <input type="hidden" name="update" value="true"><input type="submit"
                                                                               class="btn btn-success btn-sm"
                                                                               value="Refresh Reports">
                    </form>
                </div>
                <br><br>

				
				
				  <div class="panel-body tbl">
				  <?php
					global $siteConfig;
                    global $db;
					$day = [];
					
					// echo "<pre/>";
					
                    $query = "SELECT 
										p.qty as totalqty,
										ii.qty as sold,
										p.pid as pid,
										p.product_name,
										p.product_code,
										p.product_price,
										p.purchase_date,
										ii.subtotal
									FROM 
										products p
									LEFT JOIN 
										invoice_items ii ON ii.pid = p.pid 
									WHERE
										(p.purchase_date BETWEEN '2017-01-01' AND '2017-12-30')
									ORDER BY 
										p.pid 
									DESC";
										
                    $results = $db->pdoQuery($query)->results();
					
					// print_r($results);
					foreach($results as $result){
						$data[$result['pid']][] = $result; 
					}
					
					foreach($data as $dat => $value)
					{
						$totalstock = 0;
						$totalsold = 0;
						$soldprice = 0;
						foreach($value as $val){
							if($dat == $val['pid']){
								$totalstock += $val['totalqty']; 
								$totalsold += $val['sold']; 
								$soldprice += $val['subtotal']; 
								$all[$dat]['totalqty'] = $totalstock;
								$all[$dat]['sold'] = $totalsold;
								$all[$dat]['product_name'] = $val['product_name'];
								$all[$dat]['purchase_date'] = $val['purchase_date'];		
								$all[$dat]['soldprice'] = $soldprice;		
							}
						}
					}
					
                    ?>
				
					<table class="table cell-border" cellspacing="0">
					<tr>
						<td>Product</td>
						<td>Purchase Date</td>
						<td>Sold</td>
						<td>In Stock</td>
						<td>Sales</td>
					</tr>
					<?php foreach($all as $products){ ?>
					<tr>
						<td><?php echo $products['product_name']; ?></td>
						<td><?php echo $products['purchase_date']; ?></td>
						<td><?php echo $products['sold']; ?></td>
						<td><?php echo $products['totalqty']; ?></td>
						<td><?php echo $products['soldprice']; ?></td>
					</tr>
					<?php } ?>
					</table>
                </div>
            </div>
        </div>
    </div>
	
	<?php 
		unset($results);
		unset($data); 
	?>

	<?php } ?>	