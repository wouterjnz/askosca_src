<?php

if (stristr(htmlentities($_SERVER['PHP_SELF']), "customer.php")) {
    die("Internal Server Error!");
}
//customer management
if ($user->group_id > 2) {
    die('<div class="panel-heading alert-danger"><h4>You are not authorized!</h4></div>');
}
if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}
if (isset($_GET['id'])) {
    $id = intval($_GET['id']);
} else {
    $id = 0;
}

switch ($op) {
    case "add":
        add();
        break;
    case "edit":
        editc($id);
        break;
    case "invoices":

        break;
    default:
        clist();
        break;
}
function add()
{
	include_once('customer_custom2.php');
/*
    ?>

    <div id="notify" class="alert alert-success" style="display:none;">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <div class="message"></div>
    </div>

    <form method="post" id="create_customer">
        <input type="hidden" name="act" value="create_customer">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>New Customer Details</h4>

                        <div class="clear"></div>
                    </div>
                    <div class="panel-body form-group form-group-sm">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Customer Name</label>

                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Customer Name"
                                               class="form-control margin-bottom  required" name="grahak_name"
                                               id="grahak_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Address</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control margin-bottom required"
                                               name="grahak_adrs1" id="grahak_adrs1" placeholder="Address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Address Line 2</label>

                                    <div class="col-sm-10">

                                        <input type="text" class="form-control  margin-bottom required"
                                               name="grahak_adrs2" id="grahak_adrs2"
                                               placeholder="City, Country, Postal Code">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Contact Number</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control margin-bottom required"
                                               name="grahak_phone" id="grahak_phone" placeholder="Phone number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Email</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control margin-bottom" name="grahak_email"
                                               id="grahak_email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">TAX ID</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control margin-bottom" name="grahak_tax"
                                               id="grahak_tax" placeholder="TAX ID">
                                    </div>
                                </div>
                                <div class="form-group ">

                                    <div class="col-sm-5 margin-bottom">
                                        <input type="submit" id="action_create_customer"
                                               class="btn btn-success float-left" value="Create Customer"
                                               data-loading-text="Creating...">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

    </form><?php
*/
}






function editc($id)
{
    global $db;

    $whereConditions = array('id' => $id);
    $row = $db->select('reg_customers', null, $whereConditions)->results();
    if ($row) {


        // $grahak_name = $row['name'];
        // $grahak_adrs1 = $row['address1'];
        // $grahak_adrs2 = $row['address2'];
        // $grahak_phone = $row['phone'];
        // $grahak_email = $row['email'];
        // $grahak_tax = $row['taxid'];
	
		
    } else {
        die();
    }
	// include_once('customer_custom.php');
	
    ?>
<div id="notify" class="alert alert-success" style="display:none;">
        <a href="#" class="close" data-dismiss="alert">&times;</a>

        <div class="message"></div>
    </div>
    <form method="post" id="update_customer">
        <!--<input type="hidden" name="act" value="update_customer">-->
        <input type="hidden" name="act" value="update_customer_dynamic">
        <input type="hidden" name="id" value="<?php

        // echo $id';
        echo $row['id'];

        ?>">

        <div class="row">
            <div class="col-xs-12">
			<a href="#" data-target="#bankingDetails" data-toggle="modal" class="btn btn-primary " >Banking Details</a>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Editing Customer (<?php
                            // echo $grahak_name;
								echo $row['name'];
                            ?>)
							
							</h4>
					
                        <div class="clear"></div>
                    </div>	
	
                    <div class="panel-body form-group form-group-sm">
                        <div class="row">
                            <div class="col-xs-12">
							
							<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">name </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="name" class="form-control margin-bottom " name="name"
                                               id="name" value="<?php echo $row['name'] ;?>"></div>
                                </div>
								<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">address1 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="address1" class="form-control margin-bottom " name="address1"
                                               id="address1" value="<?php echo $row['address1'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">address2 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="address2" class="form-control margin-bottom " name="address2"
                                               id="address2" value="<?php echo $row['address2'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">phone </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="phone" class="form-control margin-bottom " name="phone"
                                               id="phone" value="<?php echo $row['phone'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">email </label>
                                    <div class="col-sm-10">
									<input type="email" placeholder="email" class="form-control margin-bottom " name="email"
                                               id="email" value="<?php echo $row['email'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">taxid </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="taxid" class="form-control margin-bottom " name="taxid"
                                               id="taxid" value="<?php echo $row['taxid'] ;?>"></div>
                                </div>
								<div class="form-group">
								<label class="col-sm-4 control-label margin-bottom">Business Registration Number</label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="business reg number" class="form-control margin-bottom required" name="business_reg_number"
                                               id="business_reg_number"   value="<?php echo $row['business_reg_number'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Cust Code </label>
                                    <div class="col-sm-10">
									<input type="text" class="form-control margin-bottom " name="CustCode"
                                               id="CustCode" value="<?php echo $row['CustCode']; ?>" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Cust Desc </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="CustDesc" class="form-control margin-bottom " name="CustDesc"
                                               id="CustDesc" value="<?php echo $row['CustDesc'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Contact Title </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="ContactTitle"
                                               id="ContactTitle">
											<option value="">--</option>
											<option value="MR">MR</option>
											<option value="MS">MS</option>
											<option value="MRS">MRS</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Contact First </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="ContactFirst" class="form-control margin-bottom " name="ContactFirst"
                                               id="ContactFirst" value="<?php echo $row['ContactFirst'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Contact Last </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="ContactLast" class="form-control margin-bottom " name="ContactLast"
                                               id="ContactLast" value="<?php echo $row['ContactLast'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Premises Ph </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PremisesPh" class="form-control margin-bottom " name="PremisesPh"
                                               id="PremisesPh" value="<?php echo $row['PremisesPh'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Contact Ph </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="ContactPh" class="form-control margin-bottom " name="ContactPh"
                                               id="ContactPh" value="<?php echo $row['ContactPh'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Cell </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="Cell" class="form-control margin-bottom " name="Cell"
                                               id="Cell" value="<?php echo $row['Cell'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Fax </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="Fax" class="form-control margin-bottom " name="Fax"
                                               id="Fax" value="<?php echo $row['Fax'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Creation Date </label>
                                    <div class="col-sm-10">
									<div class="input-group date" id="CreationDate">
										<input type="text" class="form-control required margin-bottom" name="CreationDate"   value="22-02-2017" data-date-format="DD-MM-YYYY" />
										 <span class="input-group-addon margin-bottom">
                                            <span class="icon-calendar margin-bottom"></span>
                                         </span>
									</div></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Residential Flag </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="ResidentialFlag"
                                               id="ResidentialFlag"><option value="">--</option><option value="Y" <?php echo($row['ResidentialFlag'] == 'Y')? "selected":""; ?>>Y</option><option value="N" <?php echo($row['ResidentialFlag'] == 'N')? "selected":""; ?>>N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> User Password </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="UserPassword" class="form-control margin-bottom " name="UserPassword"
                                               id="UserPassword" value="<?php echo $row['UserPassword'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Physical Address </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PhysicalAddress" class="form-control margin-bottom " name="PhysicalAddress"
                                               id="PhysicalAddress" value="<?php echo $row['PhysicalAddress'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Phys Postal Code </label>
                                    <div class="col-sm-10">
									<input type="text" class="form-control margin-bottom " name="PhysPostalCode"
                                               id="PhysPostalCode" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Building No </label>
                                    <div class="col-sm-10">
									<input type="text"  class="form-control margin-bottom " name="BuildingNo"
                                               id="BuildingNo" value="<?php echo $row['BuildingNo']; ?>" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Building </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="Building" class="form-control margin-bottom " name="Building"
                                               id="Building" value="<?php echo $row['Building'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Street No </label>
                                    <div class="col-sm-10">
								<input type="number" class="form-control margin-bottom " name="StreetNo"
                                               id="StreetNo" value="<?php echo $row['StreetNo']; ?>" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Street Name </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="StreetName" class="form-control margin-bottom " name="StreetName"
                                               id="StreetName" value="<?php echo $row['StreetName'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Postal Code </label>
                                    <div class="col-sm-10">
										<input type="number" class="form-control margin-bottom " name="PostalCode"
                                               id="PostalCode" value="<?php echo $row['PostalCode'] ;?>" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Sub Area </label>
                                    <div class="col-sm-10">
									<input type="text" class="form-control margin-bottom " name="SubArea"
                                               id="SubArea" maxlength="3" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Suburb </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="Suburb" class="form-control margin-bottom " name="Suburb"
                                               id="Suburb" value="<?php echo $row['Suburb'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Suburb Ext </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="SuburbExt" class="form-control margin-bottom " name="SuburbExt"
                                               id="SuburbExt" value="<?php echo $row['SuburbExt'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Monitoring Flag </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="MonitoringFlag"
                                               id="MonitoringFlag"><option value="">--</option><option value="Y" <?php echo($row['MonitoringFlag'] == 'Y')? "selected":""; ?>>Y</option><option value="N" <?php echo($row['MonitoringFlag'] == 'N')? "selected":""; ?>>N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Page Message </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PageMessage" class="form-control margin-bottom " name="PageMessage"
                                               id="PageMessage" value="<?php echo $row['PageMessage'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Premises Ph Search </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PremisesPhSearch" class="form-control margin-bottom " name="PremisesPhSearch"
                                               id="PremisesPhSearch" value="<?php echo $row['PremisesPhSearch'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Contact Ph Search </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="ContactPhSearch" class="form-control margin-bottom " name="ContactPhSearch"
                                               id="ContactPhSearch" value="<?php echo $row['ContactPhSearch'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Cell Search </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="CellSearch" class="form-control margin-bottom " name="CellSearch"
                                               id="CellSearch" value="<?php echo $row['CellSearch'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Postal Address1 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PostalAddress1" class="form-control margin-bottom " name="PostalAddress1"
                                               id="PostalAddress1" value="<?php echo $row['PostalAddress1'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Postal Address2 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PostalAddress2" class="form-control margin-bottom " name="PostalAddress2"
                                               id="PostalAddress2" value="<?php echo $row['PostalAddress2'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Postal Address3 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PostalAddress3" class="form-control margin-bottom " name="PostalAddress3"
                                               id="PostalAddress3" value="<?php echo $row['PostalAddress3'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Postal Address4 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PostalAddress4" class="form-control margin-bottom " name="PostalAddress4"
                                               id="PostalAddress4" value="<?php echo $row['PostalAddress4'] ;?>"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Instructions </label>
                                    <div class="col-sm-10">
									<textarea class="form-control margin-bottom " name="Instructions"
                                               id="Instructions"><?php echo $row['Instructions'];?></textarea></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Medical Inst </label>
                                    <div class="col-sm-10">
									<textarea class="form-control margin-bottom " name="MedicalInst"
                                               id="MedicalInst"><?php echo $row['MedicalInst'];?></textarea></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Spec Inst </label>
                                    <div class="col-sm-10">
									<textarea class="form-control margin-bottom " name="SpecInst"
                                               id="SpecInst"><?php echo $row['SpecInst'];?></textarea></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> O C Flag </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="OCFlag"
                                               id="OCFlag"><option value="">--</option><option value="Y" <?php echo($row['OCFlag'] == 'Y')? "selected":""; ?>>Y</option><option value="N" <?php echo($row['OCFlag'] == 'N')? "selected":""; ?>>N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Schedule Flag </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="ScheduleFlag"
                                               id="ScheduleFlag"><option value="">--</option><option value="Y" <?php echo($row['ScheduleFlag'] == 'Y')? "selected":""; ?>>Y</option><option value="N" <?php echo($row['ScheduleFlag'] == 'N')? "selected":""; ?>>N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Inst Flag </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="InstFlag"
                                               id="InstFlag"><option value="">--</option><option value="Y" <?php echo($row['InstFlag'] == 'Y')? "selected":""; ?>>Y</option><option value="N" <?php echo($row['InstFlag'] == 'N')? "selected":""; ?>>N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Inst Beg </label>
                                    <div class="col-sm-10">
									<div class="input-group date" id="InstBeg">
										<input type="text" class="form-control required margin-bottom" name="InstBeg"   value="22-02-2017" data-date-format="DD-MM-YYYY" />
										 <span class="input-group-addon margin-bottom">
                                            <span class="icon-calendar margin-bottom"></span>
                                         </span>
									</div></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Inst End </label>
                                    <div class="col-sm-10">
									<div class="input-group date" id="InstEnd">
										<input type="text" class="form-control required margin-bottom" name="InstEnd"   value="22-02-2017" data-date-format="DD-MM-YYYY" />
										 <span class="input-group-addon margin-bottom">
                                            <span class="icon-calendar margin-bottom"></span>
                                         </span>
									</div></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Suspend Mode </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="SuspendMode"
                                               id="SuspendMode"><option value="">--</option><option value="Y" <?php echo($row['SuspendMode'] == 'Y')? "selected":""; ?>>Y</option><option value="N" <?php echo($row['SuspendMode'] == 'N')? "selected":""; ?>>N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Suspend Date </label>
                                    <div class="col-sm-10">
									<div class="input-group date" id="SuspendDate">
										<input type="text" class="form-control required margin-bottom" name="SuspendDate"   value="22-02-2017" data-date-format="DD-MM-YYYY" />
										 <span class="input-group-addon margin-bottom">
                                            <span class="icon-calendar margin-bottom"></span>
                                         </span>
									</div></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Suspend Reason </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="SuspendReason" class="form-control margin-bottom " name="SuspendReason"
                                               id="SuspendReason" value="<?php echo $row['SuspendReason'] ;?>"></div>
                                </div>	
							
							<?php /*
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Customer Name</label>

                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Customer Name"
                                               class="form-control margin-bottom  required" name="grahak_name"
                                               id="grahak_name" value="<?php

                                        echo $grahak_name;

                                        ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Address</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control margin-bottom required"
                                               name="grahak_adrs1" id="grahak_adrs1" placeholder="Address" value="<?php

                                        echo $grahak_adrs1;

                                        ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Address Line 2</label>

                                    <div class="col-sm-10">

                                        <input type="text" class="form-control  margin-bottom required"
                                               name="grahak_adrs2" id="grahak_adrs2"
                                               placeholder="City, Country, Postal Code" value="<?php

                                        echo $grahak_adrs2;

                                        ?>">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Contact Number</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control margin-bottom required"
                                               name="grahak_phone" id="grahak_phone" placeholder="Phone number"
                                               value="<?php

                                               echo $grahak_phone;

                                               ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Email</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control margin-bottom" name="grahak_email"
                                               id="grahak_email" placeholder="Email" value="<?php

                                        echo $grahak_email;

                                        ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">TAX ID</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control margin-bottom" name="grahak_tax"
                                               id="grahak_tax" placeholder="TAX ID" value="<?php

                                        echo $grahak_tax;

                                        ?>">
                                    </div>
                                </div>
							*/ ?>
                                <div class="form-group ">

                                    <div class="col-sm-5 margin-bottom">
                                        <input type="submit" id="action_update_customer"
                                               class="btn btn-success float-left" value="Update Customer"
                                               data-loading-text="Creating...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </form>
    <div id="bankingDetails" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Banking Details</h4>
                </div>
                <div class="modal-body">
                    <?php	
					$value = 'update_banking_details';
					$action = 'update_bankingdetails';
					$id = $row['id'];
					include_once('banking_details.php'); ?>
                </div>
                <!--<div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="udpate_banking_data">Submit</button>
                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                </div>-->
            </div>
        </div>
    </div>
    <?php

}

function clist()
{

    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#cust').DataTable({
                stateSave: true,
                "processing": true,
                "serverSide": true,
                "ajax": "request/listcustomer.php"
            });
        });</script>
    <div class="row">

        <div class="col-xs-12">
            <div id="notify" class="alert alert-success" style="display:none;">
                <a href="#" class="close" data-dismiss="alert">&times;</a>

                <div class="message"></div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Customer List</h4>
                </div>
                <div class="panel-body form-group form-group-sm tbl">
                    <table id="cust" class="table cell-border" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Customer Code</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Suspended</th>
                            <th>Settings</th>

                        </tr>

                        </thead>

                    </table>

                </div>
            </div>
        </div>
        <div>

            <div id="delete_customer" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Delete Customer</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to delete this customer?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete
                            </button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                </div>
            </div><?php

}

?>