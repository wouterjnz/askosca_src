   <div id="notify" class="alert alert-success" style="display:none;">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <div class="message"></div>
    </div>
<!--<a href="#" data-target="#bankingDetails" data-toggle="modal" >Banking Details</a>-->
    <form method="post" id="create_customer">
        <input type="hidden" name="act" value="create_customer_dynamic">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>New Customer Details</h4>

                        <div class="clear"></div>
                    </div>
<div class="panel-body form-group form-group-sm">
                        <div class="row">
                            <div class="col-xs-12">
						
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">name </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="name" class="form-control margin-bottom required" name="name"
                                               id="name" required ></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">address1 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="address1" class="form-control margin-bottom required" name="address1"
                                               id="address1"  required ></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">address2 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="address2" class="form-control margin-bottom " name="address2"
                                               id="address2"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">phone </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="phone" class="form-control margin-bottom required" name="phone"
                                               id="phone"  required ></div>
                                </div>
								<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">email </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="email" class="form-control margin-bottom required" name="email"
                                               id="email"  required ></div>
                                </div>
								<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">taxid </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="taxid" class="form-control margin-bottom required" name="taxid"
                                               id="taxid"  required ></div>
                                </div>
								<div class="form-group">
								<label class="col-sm-4 control-label margin-bottom">Business Registration Number</label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="business reg number" class="form-control margin-bottom required" name="business_reg_number"
                                               id="business_reg_number"  ></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Cust Code </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="Customer Code" class="form-control margin-bottom " name="CustCode" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Cust Desc </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="CustDesc" class="form-control margin-bottom " name="CustDesc"
                                               id="CustDesc"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Contact Title </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="ContactTitle" id="ContactTitle">
										   <option value="">--</option>
										   <option value="MR">MR</option>
										   <option value="MS">MS</option>
										   <option value="MRS">MRS</option>
								   </select>
								</div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Contact First </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="ContactFirst" class="form-control margin-bottom " name="ContactFirst"
                                               id="ContactFirst"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Contact Last </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="ContactLast" class="form-control margin-bottom " name="ContactLast"
                                               id="ContactLast"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Premises Ph </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PremisesPh" class="form-control margin-bottom " name="PremisesPh"
                                               id="PremisesPh"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Contact Ph </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="ContactPh" class="form-control margin-bottom " name="ContactPh"
                                               id="ContactPh"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Cell </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="Cell" class="form-control margin-bottom " name="Cell"
                                               id="Cell"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Fax </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="Fax" class="form-control margin-bottom " name="Fax"
                                               id="Fax"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Creation Date </label>
                                    <div class="col-sm-10">
									<div class="input-group date" id="CreationDate">
										<input type="text" class="form-control required margin-bottom" name="CreationDate"   value="22-02-2017" data-date-format="DD-MM-YYYY" />
										 <span class="input-group-addon margin-bottom">
                                            <span class="icon-calendar margin-bottom"></span>
                                         </span>
									</div></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Residential Flag </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="ResidentialFlag"
                                               id="ResidentialFlag"><option value="">--</option><option value="Y">Y</option><option value="N">N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> User Password </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="UserPassword" class="form-control margin-bottom " name="UserPassword"
                                               id="UserPassword"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Physical Address </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PhysicalAddress" class="form-control margin-bottom " name="PhysicalAddress"
                                               id="PhysicalAddress"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Phys Postal Code </label>
                                    <div class="col-sm-10">
									<input type="text" class="form-control margin-bottom"  name="PhysPostalCode" />
                                </div>
								<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Building No </label>
                                    <div class="col-sm-10">
									<input type="text" class="form-control margin-bottom" name="BuildingNo" maxlength="10" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Building </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="Building" class="form-control margin-bottom " name="Building"
                                               id="Building" maxlength="30"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Street No </label>
                                    <div class="col-sm-10">
									<input type="text" class="form-control margin-bottom "  name="StreetNo" maxlength="10" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Street Name </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="StreetName" class="form-control margin-bottom " name="StreetName"
                                               id="StreetName"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Postal Code </label>
                                    <div class="col-sm-10">
									<input type="text" class="form-control margin-bottom "  name="PostalCode" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Sub Area </label>
                                    <div class="col-sm-10">
									<input type="text" class="form-control margin-bottom"  name="SubArea" />
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Suburb </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="Suburb" class="form-control margin-bottom " name="Suburb"
                                               id="Suburb"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Suburb Ext </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="SuburbExt" class="form-control margin-bottom " name="SuburbExt"
                                               id="SuburbExt"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Monitoring Flag </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="MonitoringFlag"
                                               id="MonitoringFlag"><option value="">--</option><option value="Y">Y</option><option value="N">N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Page Message </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PageMessage" class="form-control margin-bottom " name="PageMessage"
                                               id="PageMessage"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Premises Ph Search </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PremisesPhSearch" class="form-control margin-bottom " name="PremisesPhSearch"
                                               id="PremisesPhSearch"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Contact Ph Search </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="ContactPhSearch" class="form-control margin-bottom " name="ContactPhSearch"
                                               id="ContactPhSearch"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Cell Search </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="CellSearch" class="form-control margin-bottom " name="CellSearch"
                                               id="CellSearch"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Postal Address1 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PostalAddress1" class="form-control margin-bottom " name="PostalAddress1"
                                               id="PostalAddress1"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Postal Address2 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PostalAddress2" class="form-control margin-bottom " name="PostalAddress2"
                                               id="PostalAddress2"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Postal Address3 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PostalAddress3" class="form-control margin-bottom " name="PostalAddress3"
                                               id="PostalAddress3"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Postal Address4 </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="PostalAddress4" class="form-control margin-bottom " name="PostalAddress4"
                                               id="PostalAddress4"></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Instructions </label>
                                    <div class="col-sm-10">
									<textarea class="form-control margin-bottom " name="Instructions"
                                               id="Instructions"></textarea></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Medical Inst </label>
                                    <div class="col-sm-10">
									<textarea class="form-control margin-bottom " name="MedicalInst"
                                               id="MedicalInst"></textarea></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Spec Inst </label>
                                    <div class="col-sm-10">
									<textarea class="form-control margin-bottom " name="SpecInst"
                                               id="SpecInst"></textarea></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> O C Flag </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="OCFlag"
                                               id="OCFlag"><option value="">--</option><option value="Y">Y</option><option value="N">N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Schedule Flag </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="ScheduleFlag"
                                               id="ScheduleFlag"><option value="">--</option><option value="Y">Y</option><option value="N">N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Inst Flag </label>
                                    <div class="col-sm-10">
									<select class="form-control margin-bottom " name="InstFlag"
                                               id="InstFlag"><option value="">--</option><option value="Y">Y</option><option value="N">N</option></select></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Inst Beg </label>
                                    <div class="col-sm-10">
									<div class="input-group date" id="InstBeg">
										<input type="text" class="form-control required margin-bottom" name="InstBeg"   value="22-02-2017" data-date-format="DD-MM-YYYY" />
										 <span class="input-group-addon margin-bottom">
                                            <span class="icon-calendar margin-bottom"></span>
                                         </span>
									</div></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Inst End </label>
                                    <div class="col-sm-10">
									<div class="input-group date" id="InstEnd">
										<input type="text" class="form-control required margin-bottom" name="InstEnd"   value="22-02-2017" data-date-format="DD-MM-YYYY" />
										 <span class="input-group-addon margin-bottom">
                                            <span class="icon-calendar margin-bottom"></span>
                                         </span>
									</div></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Suspend Mode </label>
                                    <div class="col-sm-10">
										<select class="form-control margin-bottom " name="SuspendMode" id="SuspendMode">
											<option value="">--</option>
											<option value="Y">Y</option>
											<option value="N">N</option>
										</select>
									</div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Suspend Date </label>
                                    <div class="col-sm-10">
									<div class="input-group date" id="SuspendDate">
										<input type="text" class="form-control required margin-bottom" name="SuspendDate"   value="22-02-2017" data-date-format="DD-MM-YYYY" />
										 <span class="input-group-addon margin-bottom">
                                            <span class="icon-calendar margin-bottom"></span>
                                         </span>
									</div></div>
                                </div>
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom"> Suspend Reason </label>
                                    <div class="col-sm-10">
									<input type="text" placeholder="SuspendReason" class="form-control margin-bottom " name="SuspendReason"
                                               id="SuspendReason"></div>
                                </div>
							
								<div class="form-group ">
                                    <div class="col-sm-5 margin-bottom">
                                        <input type="submit" id="action_create_customer"
                                               class="btn btn-success float-left" value="Create Customer"
                                               data-loading-text="Creating...">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
    </form>	



  <div id="bankingDetails" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Banking Details</h4>
                </div>
                <div class="modal-body">
                    <?php	
					$value = 'create_banking_details';
					$action = 'create_bankingdetails';
					include_once('banking_details.php'); ?>
                </div>
             <!--   <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="udpate_banking_data">Submit</button>
                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                </div>-->
            </div>
        </div>
    </div>	