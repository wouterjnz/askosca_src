<?php
	if(isset($id)){
		$bquery = "SELECT * FROM banking_details where clientID = " .$id; 
		$bresult = $db->pdoQuery($bquery)->result();
		$populate =  1;
	}else{
		$id = 0;
		$populate =  0;
	}
?>

<form method="post" id="<?php echo $value; ?>">
					<input type="hidden" name="act" value="<?php echo $value; ?>">
					<input type="hidden" name="clientID" value="<?php echo $id; ?>">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Banking Details</h4>

					<div class="clear"></div>
				</div>
					<div class="panel-body form-group form-group-sm">
					<div class="row">
						<div class="col-xs-12">
						
							<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">account_ref *</label>
								<div class="col-sm-10">
								<input type="text" placeholder="account_ref" class="form-control margin-bottom " name="account_ref" id="account_ref" maxlength="25" 
								value="<?php if($populate > 0){ echo ($bresult['account_ref'])?:''; } ?>" required ></div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">account_name *</label>
								<div class="col-sm-10">
								<input type="text" placeholder="account_name" class="form-control margin-bottom " name="account_name" id="account_name"maxlength="25" value="<?php if($populate > 0){ echo ($bresult['account_name'])?:''; } ?>" required ></div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">Account Active</label>
								<div class="col-sm-10">
								<select class="form-control margin-bottom " name="account_active" id="account_active">
								<option value="0" <?php if($populate > 0){ echo ($bresult['account_active'] !=0 )?:'selected'; } ?>>N</option>
								<option value="1" <?php if($populate > 0){ echo ($bresult['account_active'] !=1 )?:'selected'; } ?>>Y</option>
								</select>
								</div>
							</div>	
								<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">Delete Account</label>
								<div class="col-sm-10">
								<select class="form-control margin-bottom " name="delete_account" id="delete_account">
								<option value="0" <?php if($populate > 0){ echo ($bresult['delete_account'] != 0)?:'selected'; } ?>>N</option>
								<option value="1" <?php if($populate > 0){ echo ($bresult['delete_account'] != 1)?:'selected'; } ?>>Y</option>
								</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">banking_detail_type *</label>
								<div class="col-sm-10">
								<select class="form-control margin-bottom " name="banking_detail_type" id="banking_detail_type" required  >
								<option value="1" <?php if($populate > 0){ echo ($bresult['banking_detail_type'] != 1)?:'selected'; } ?>>Bank</option>
								<option value="2" <?php if($populate > 0){ echo ($bresult['banking_detail_type'] != 2)?:'selected'; } ?>>Credit Card</option>
								</select>
								</div>
							</div>	
							
							<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">bank_account_name *</label>
								<div class="col-sm-10">
								<input type="text" placeholder="bank_account_name" class="form-control margin-bottom " name="bank_account_name" id="bank_account_name" value="<?php if($populate > 0){ echo ($bresult['bank_account_name'])?:''; } ?>" maxlength="30" required ></div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">bank_account_number * </label>
								<div class="col-sm-10">
								<input type="text" placeholder="bank_account_number" class="form-control margin-bottom " name="bank_account_number" id="bank_account_number" maxlength="11" value="<?php if($populate > 0){ echo ($bresult['bank_account_number'])?:''; } ?>" required ></div>
							</div>		
							
							<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">Branch Code *</label>
								<div class="col-sm-10">
								<input type="text" placeholder="branch_code" class="form-control margin-bottom " name="branch_code" id="branch_code" maxlength="6" value="<?php if($populate > 0){ echo ($bresult['branch_code'])?:''; } ?>" required  ></div>
							</div>	
							
							<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">Default Debit Amount * </label>
								<div class="col-sm-10">
								<input type="text" placeholder="default_debit_amount" class="form-control margin-bottom " name="default_debit_amount" id="default_debit_amount" value="<?php if($populate > 0){ echo ($bresult['default_debit_amount'])?:''; } ?>" required ></div>
							</div>		
							<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">Amount *</label>
								<div class="col-sm-10">
								<input type="text" placeholder="amount" class="form-control margin-bottom " name="amount" id="amount" value="<?php if($populate > 0){ echo ($bresult['amount'])?:''; } ?>" required ></div>
							</div>
								<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">bank_account_type * </label>
								<div class="col-sm-10">
								<select class="form-control margin-bottom " name="bank_account_type" id="bank_account_type" required >
								<option value="1" <?php if($populate > 0){ echo ($bresult['bank_account_type'] != 1)?:'selected'; } ?>>Current / Checking</option>
								<option value="2" <?php if($populate > 0){ echo ($bresult['bank_account_type'] != 2)?:'selected'; } ?>>Savings</option>
								<option value="3" <?php if($populate > 0){ echo ($bresult['bank_account_type'] != 3)?:'selected'; } ?>>Transmission</option>
								<option value="4" <?php if($populate > 0){ echo ($bresult['bank_account_type'] != 4)?:'selected'; } ?>>Bond</option>
								</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label margin-bottom">debit_master_group</label>
								<div class="col-sm-10">
								<input type="text" placeholder="debit_master_group" class="form-control margin-bottom " name="debit_master_group" id="debit_master_group"></div>
							</div>
						
							
								
							
							
							<div class="form-group ">
								<div class="col-sm-5 margin-bottom">
									<input type="submit" id="action_<?php echo $action; ?>"
										   class="btn btn-success float-left" value="Update Banking Details"
										   data-loading-text="Creating...">
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		</form>