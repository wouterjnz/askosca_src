<?php

if (stristr(htmlentities($_SERVER['PHP_SELF']), "jobcard.php")) {
    die("Internal Server Error!");
}
//customer management
$allowed_ids = [1,2,4];
if (!in_array($user->group_id, $allowed_ids)) {
    die('<div class="panel-heading alert-danger"><h4>You are not authorized!</h4></div>');
}
if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}
if (isset($_GET['id'])) {
    $id = intval($_GET['id']);
} else {
    $id = 0;
}

switch ($op) {
    case "add":
        add();
        break;
    case "edit":
        editc($id);
        break;
	// case "closed":
        // clistc();
        // break;
    default:
        clist();
        break;
}
?>


<?php 
function clist() 
{
	$action = '';
	if(isset($_GET['op']) && $_GET['op'] == 'closed')
	{
		$action = '?op=closed';
	}
?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#cust').DataTable({
                stateSave: true,
                "processing": true,
                "serverSide": true,
                "ajax": "request/listjobcards.php<?php echo $action;?>"
            });
        });</script>
    <div class="row">

        <div class="col-xs-12">
            <div id="notify" class="alert alert-success" style="display:none;">
                <a href="#" class="close" data-dismiss="alert">&times;</a>

                <div class="message"></div>
            </div>

			
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Jobcard List</h4>
                </div>	
                <div class="panel-body form-group form-group-sm tbl">
							<a href="index.php?rdp=jobcard&op=add" class="btn btn-info btn-danger"><span class="icon-plus"></span>Add New Jobcard</a>
			<?php if(isset($_GET['op']) && $_GET['op'] == 'closed'){ ?>
				<a href="index.php?rdp=jobcard" class="btn btn-info btn-primary"><span class="icon-plus"></span>Open Jobcards</a>
			<?php }else{ ?>
				<a href="index.php?rdp=jobcard&op=closed" class="btn btn-info btn-primary"><span class="icon-plus"></span>Closed Jobcards</a>
			<?php } ?>
				<br/><br/>
					
                    <table id="cust" class="table cell-border" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Startdate</th>
                            <th>Due Date</th>
                            <th>Status</th>
                            <th>Settings</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div>

            <div id="delete_jobcard" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Delete Jobcard</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to delete this jobcard?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete
                            </button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                </div>
            </div><?php

}


function clistc() 
{
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#cust').DataTable({
                stateSave: true,
                "processing": true,
                "serverSide": true,
                "ajax": "request/listjobcards.php?op=closed"
            });
        });</script>
    <div class="row">

        <div class="col-xs-12">
            <div id="notify" class="alert alert-success" style="display:none;">
                <a href="#" class="close" data-dismiss="alert">&times;</a>

                <div class="message"></div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Jobcard List</h4>
                </div>
				
				
                <div class="panel-body form-group form-group-sm tbl">
					<a href="index.php?rdp=jobcard&op=add" class="btn btn-info btn-danger"><span class="icon-plus"></span>Add New Jobcard</a>
					<br><br>
                    <table id="cust" class="table cell-border" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Startdate</th>
                            <th>Due Date</th>
                            <th>Status</th>
                            <th>Settings</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div>

            <div id="delete_jobcard" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Delete Jobcard</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to delete this jobcard?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete
                            </button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                </div>
            </div><?php

}




function add()
{
	global $siteConfig;
   ?>

<div id="notify" class="alert alert-success" style="display:none;">
	<a href="#" class="close" data-dismiss="alert"></a>
	<div class="message"></div>
</div>

    <form method="post" id="create_jobcard">
        <input type="hidden" name="act" value="create_jobcard">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Add New Jobcard </h4>

                        <div class="clear"></div>
                    </div>
                    <div class="panel-body form-group form-group-sm">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Subject</label>

                                    <div class="col-sm-10">
                                        <input type="text" placeholder="subject"
                                               class="form-control margin-bottom  required" name="subject"
                                               id="grahak_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Priority</label>

                                    <div class="col-sm-10">
                                        <select class="form-control margin-bottom required"
                                               name="priority" id="grahak_priority">
											   <option value="">--</option>
											   <option value="Low">Low</option>
											   <option value="Medium">Medium</option>
											   <option value="High">High</option>
										</select>	   
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Start Date</label>
                                    <div class="col-sm-10">
										<div class="input-group date" id="tsn_date">
                                        <input type="text" class="form-control  margin-bottom required"
                                               name="startdate" id="grahak_startd"
                                               placeholder="start date" value="<?php  echo $siteConfig['date']; ?>" data-date-format="<?php echo $siteConfig['dformat2']; ?>">
										 	<span class="input-group-addon margin-bottom"><span class="icon-calendar margin-bottom"></span></span>
								   </div>
                                    </div>
                                </div>
								
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Due Date</label>
                                    <div class="col-sm-10">
									<div class="input-group date" id="tsn_due">
                                        <input type="text" class="form-control margin-bottom required"
                                               name="duedate" id="grahak_dued" value="<?php  echo $siteConfig['date']; ?>" data-date-format="<?php echo $siteConfig['dformat2']; ?>">
											 	<span class="input-group-addon margin-bottom"><span class="icon-calendar margin-bottom"></span></span>
                                    </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Description</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control margin-bottom" name="description"
                                               id="grahak_description" placeholder="Description" value="">
                                    </div>
                                </div>
                                
								 <div class="form-group row">
										<label class="col-sm-3 control-label margin-bottom">Show on calendar</label>

                                    <div class="col-sm-2">
									<br/>
                                        <input type="checkbox" name="show_on_calendar" value="1" />
                                    </div>
                                </div>
								
								<!--
								<div class="form-group row">
                                    <label class="col-sm-3 control-label margin-bottom">Signature</label>
									<br/>
									   
                                    <div class="col-sm-4">
										<div class="wrapper">
											<center><mute>SIGN HERE</mute></center>
											<canvas id="signature-pad" class="signature-pad" width="400px" height="200px" ></canvas>
										</div>
										<div>
											<a id="save" class="btn btn-primary">Save</a>
											<a id="clear" class="btn btn-primary">Clear</a>
										</div>
                                    </div>
									<div class="col-sm-5">
									   </div>
                                </div>-->

                                <div class="form-group ">
                                    <div class="col-sm-5 margin-bottom">
                                       <input type="submit" id="action_create_jobcard"
                                               class="btn btn-success float-left" value="Create Jobcard"
                                               data-loading-text="Creating...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </form>
  

	<script>
var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
  backgroundColor: 'rgba(200, 200, 200, 25)',
  penColor: 'rgb(0, 0, 0)',
    minWidth: 1,
   maxWidth: 2
});
var saveButton = document.getElementById('save');
var cancelButton = document.getElementById('clear');

saveButton.addEventListener('click', function (event) {
  var signature_data = signaturePad.toDataURL('image/png');

  // var d = 'signature';
  // $(this).attr("href", data).attr("download", "file-" + d + ".png");
});



cancelButton.addEventListener('click', function (event) {
  signaturePad.clear();
});
</script>

<script>
	$("#action_create_jobcard").click(function (e) {
        e.preventDefault();
        actionCreateJobcard();
    });
    function actionCreateJobcard() {	
        var $btn;
        var errorNum = farmCheck();

        if (errorNum > 0) {
            $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
            $("#notify .message").html("<strong>Error</strong>: It appears you have forgotten to complete something!");
            $("html, body").animate({scrollTop: $('#notify').offset().top}, 1000);
        } else {

            var $btn = $("#action_create_jobcard").button("loading");

            $(".required").parent().removeClass("has-error");

            $.ajax({

                url: 'request/jobcard.php',
                type: 'POST',
                data: $("#create_jobcard").serialize(),
                dataType: 'json',
                success: function (data) {
                    $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#notify").removeClass("alert-warning").addClass("alert-success").fadeIn();
                    $("html, body").animate({scrollTop: $('#notify').offset().top - 100 }, 1000);
                    /* $("#create_jobcard").before().html("");
                     $("#create_jobcard").remove(); */
                    $btn.button("reset");
                },
                error: function (data) {
                    $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
                    $("html, body").animate({scrollTop: $('#notify').offset().top - 100 }, 1000);
                    $btn.button("reset");
                }

            });
        }

    }
	</script>



<?php 
}











function editc($id)
{
    global $db;
	global $siteConfig;
    $whereConditions = array('id' => $id);
    $row = $db->select('jobcards', null, $whereConditions)->results();
    if ($row) {
        $grahak_name = $row['name'];
        $grahak_priority = $row['priority'];
        $grahak_startd = $row['startdate'];
        $grahak_dued = $row['duedate'];
        $grahak_description = $row['description'];
        $grahak_signature = $row['signature'];
        $show_on_calendar = $row['show_on_calendar'];
        $grahak_finished = $row['finished'];
        // $grahak_tax = $row['taxid'];
    } else {
        die();
    }
?>
<div id="notify" class="alert alert-success" style="display:none;">
	<a href="#" class="close" data-dismiss="alert"></a>
	<div class="message"></div>
</div>

    <form method="post" id="update_jobcard">
        <input type="hidden" name="act" value="update_jobcard">
        <input type="hidden" name="id" value="<?php

        echo $id;

        ?>">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Editing Jobcard (<?php

                            echo $grahak_name;

                            ?>)</h4>

                        <div class="clear"></div>
                    </div>
                    <div class="panel-body form-group form-group-sm">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Subject</label>

                                    <div class="col-sm-10">
                                        <input type="text" placeholder="priority"
                                               class="form-control margin-bottom  required" name="subject"
                                               id="grahak_name" value="<?php

                                        echo $grahak_name;

                                        ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Priority</label>

                                    <div class="col-sm-10">
                                        <select class="form-control margin-bottom required"
                                               name="priority" id="grahak_priority">
											   <option value="">--</option>
											   <option value="Low" <?php echo ($grahak_priority == "Low")? 'selected':''; ?>>Low</option>
											   <option value="Medium" <?php echo ($grahak_priority == "Medium")? 'selected':''; ?>>Medium</option>
											   <option value="High"<?php echo ($grahak_priority == "High")? 'selected':''; ?>>High</option>
										</select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Start Date</label>

                                    <div class="col-sm-10">
									<div class="input-group date" id="tsn_date">
                                        <input type="text" class="form-control  margin-bottom required"
                                               name="startdate" id="grahak_startd"
                                               placeholder="start date" value="<?php echo $grahak_startd; ?>" data-date-format="YYYY-MM-DD">
											   <span class="input-group-addon margin-bottom"><span class="icon-calendar margin-bottom"></span></span>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Due Date</label>

                                    <div class="col-sm-10">
									<div class="input-group date" id="tsn_due">
                                        <input type="text" class="form-control margin-bottom required"
                                               name="duedate" id="grahak_dued" placeholder="due date"
                                               value="<?php echo $grahak_dued; ?>" data-date-format="YYYY-MM-DD" >
											<span class="input-group-addon margin-bottom"><span class="icon-calendar margin-bottom"></span></span>   
                                    </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">Description</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control margin-bottom" name="description"
                                               id="grahak_description" placeholder="Description" value="<?php

                                        echo nl2br($grahak_description);

                                        ?>">
                                    </div>
                                </div>
                                
								<div class="form-group row">
										<label class="col-sm-3 control-label margin-bottom">Show on calendar</label>

                                    <div class="col-sm-2">
									<br/>
                                        <input type="checkbox" name="show_on_calendar" value="1" <?php if($show_on_calendar == 1){  echo "checked"; } ?> />
                                    </div>
                                </div>
								
								<div class="form-group row <?php echo ($grahak_signature)? '':'hidden'; ?>">
                                   <label class="col-sm-3 control-label margin-bottom">Signature</label>
									<br/>
                                    <div class="col-sm-4">
										<div class="wrapper">
									<img src="<?php echo "data:image/png;base64, $grahak_signature" ;?>" />
                                    </div>
                                    </div>
									<div class="col-sm-5">
									   </div>
                                </div>
								
								<div class="form-group row <?php echo ($grahak_signature)? 'hidden':''; ?>">
                                    <label class="col-sm-3 control-label margin-bottom">Signature</label>
									<br/>
									   
                                    <div class="col-sm-4">
										<div class="wrapper"  style="color: #fff">
											<center><mute>SIGN HERE</mute></center>
											<canvas id="signature-pad" class="signature-pad" width="400px" height="200px"></canvas>
										</div>
										<div>
											<a id="save" class="btn btn-primary hidden">Save</a>
											<a id="clear" class="btn btn-primary">Clear</a>
										</div>
                                    </div>
									<div class="col-sm-5">
									   </div>
                                </div>
								
								<div class="form-group row">
										<label class="col-sm-3 control-label margin-bottom">Status</label>
                                    <div class="col-sm-2">
                                       <select class="form-control margin-bottom required"
                                               name="finished" id="grahak_finished">
											   <option value="0">--</option>
											   <option value="0" <?php echo ($grahak_finished == "0")? 'selected':''; ?>>Open</option>
											   <option value="1" <?php echo ($grahak_finished == "1")? 'selected':''; ?>>Closed</option>
										</select>
                                    </div>
                                </div>
								
                                <div class="form-group ">
                                    <div class="col-sm-5 margin-bottom">
                                        <input type="submit" id="action_update_jobcard"
                                               class="btn btn-success float-left" value="Update Jobcard"
                                               data-loading-text="Creating...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </form>
  
	
	<script>
var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
  backgroundColor: 'rgba(200, 200, 200, 25)',
  penColor: 'rgb(0, 0, 0)',
   minWidth: 1,
   maxWidth: 2
});

//signaturePad.fromDataURL('<?php echo 'data:image/png;base64,' . $grahak_signature; ?>');
// Returns signature image as an array of point groups
// const data = signaturePad.toData();

// Draws signature image from an array of point groups
// signaturePad.fromData(data);

var saveButton = document.getElementById('save');
var cancelButton = document.getElementById('clear');

saveButton.addEventListener('click', function (event) {
  var signature_data = signaturePad.toDataURL('image/png');

  
  jQuery.ajax({
	type: "POST",
	url: 'request/jobcard.php',
	data: {'signature_image': signature_data, 'act': 'signature'},
	success: function (data) {
		$("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
		$("#notify").removeClass("alert-warning").addClass("alert-success").fadeIn();
		$("html, body").animate({scrollTop: $('#notify').offset().top - 100 }, 1000);
		$btn.button("reset");
	},
	error: function (data) {
		$("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
		$("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
		$("html, body").animate({scrollTop: $('#notify').offset().top - 100 }, 1000);
		$btn.button("reset");
	}
  });

		
  // var d = 'signature';
  // $(this).attr("href", data).attr("download", "file-" + d + ".png");
});



cancelButton.addEventListener('click', function (event) {
  signaturePad.clear();
});

function resizeCanvas() {
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
    signaturePad.clear(); // otherwise isEmpty() might return incorrect value
}

window.addEventListener("resize", resizeCanvas);
resizeCanvas();
</script>

<script>
$(document).on('click', "#action_update_jobcard", function (e) {
        e.preventDefault();
        updateJobcard();
    });
    function updateJobcard() {

        var $btn = $("#action_update_jobcard").button("loading");
		// console.log($("#update_jobcard").serialize());
		
		// var alldata = { 'signature_image' : signaturePad.toDataURL('image/png') };
		 var alldata =  {
				'id': $('input[name="id"]').val(),
				'act': $('input[name="act"]').val(),
				'subject': $('input[name="subject"]').val(),
				'priority': $('select[name="priority"]').val(),
				'startdate': $('input[name="startdate"]').val(),
				'duedate': $('input[name="duedate"]').val(),
				'description': $('input[name="description"]').val(),
				'show_on_calendar': $('input[name="show_on_calendar"]').prop('checked'),
				'signature': signaturePad.toDataURL('image/png'),
				'finished': $('select[name="finished"]').val()
			}
	
        jQuery.ajax({
            url: 'request/jobcard.php',
            type: 'POST',
            data: alldata,
            // data:  $("#update_jobcard").serialize(),
            dataType: 'json',
            success: function (data) {
                $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                $("#notify").removeClass("alert-warning").addClass("alert-success").fadeIn();
                $("html, body").animate({scrollTop: $('#notify').offset().top - 100 }, 1000);
                $btn.button("reset");
            },
            error: function (data) {
                $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
                $("html, body").animate({scrollTop: $('#notify').offset().top - 100 }, 1000);
                $btn.button("reset");
            }
        });

    }
</script>
    <?php

}
?>
