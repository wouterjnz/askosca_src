<div id="notify" class="alert alert-success" style="display:none;">
    <a href="#" class="close" data-dismiss="alert">&times;</a>

    <div class="message"></div>
</div>

<div class="row">
    <div class="col-xs-12">
          <a data-target="#processingDeadlines" data-toggle="modal" class="btn btn-primary">View Processing Deadlines</a>
        <form method="post" id="create_sage_batch">
            <input type="hidden" name="act" value="create_sage_batch">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>SagePay Settings</h4>
                    <div class="clear"></div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label margin-bottom">Batch Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control margin-bottom" name="batch_name" value="<?php echo !empty($settings) ? $settings['merchant_account'] : null; ?>"  />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label margin-bottom">Instruction</label>
                        <div class="col-sm-10">
                            <select class="form-control margin-bottom" name="instruction" value="<?php echo !empty($settings) ? $settings['software_vendor_key'] : null; ?>" >
							<option value="">--</option>
							<option value="SameDay">Same Day</option>
							<option value="TwoDay">Two Day</option>
							<option value="Update">Update</option>
							</select>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-2 control-label margin-bottom">Action Date</label>
                        <div class="col-sm-10">
						<div class="input-group date" id="action_date">
                            <input type="text" class="form-control margin-bottom" name="action_date"  value="<?php echo date('Y-m-d');?>" data-date-format="YYYY-MM-DD" />
							<span class="input-group-addon margin-bottom">
                                            <span class="icon-calendar margin-bottom"></span>
                                         </span>
						</div>
                        </div>
                    </div>
					
					<div class="form-group ">
                        <div class="col-sm-5 margin-bottom">
                            <input type="submit" id="action_create_sage_batch"
                            class="btn btn-success float-left" value="Submit"
                            data-loading-text="Creating...">
                        </div>
                    </div>
					
                </div>
            </div>
		</form>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>All Batches</h4>
                    <div class="clear"></div>
                </div>
                <div class="panel-body">
                 
				<?php  $batches = $db->pdoQuery("SELECT * FROM sage_debit_batch")->results(); ?>
                    
					<table class="table">
						<thead>
							<th>Batch</th>
							<th>Instruction</th>
							<th>Action Date</th>
							<th>Date Added</th>
							<th>Action</th>
						</thead>
						
					<?php foreach($batches as $batch){ ?>	
						<tr>
							<td><?php echo $batch['batch_name']; ?></td>
							<td><?php echo $batch['instruction']; ?></td>
							<td><?php echo $batch['action_date']; ?></td>
							<td><?php echo $batch['rdate']; ?></td>
							<td>
								<!--<a href="#" data-toggle="modal" data-target="#addBatchClients" class="btn btn-primary btn-xs">Add Clients</a>
								<a href="index.php?rdp=sage_customers&batchID=<?php echo $batch['id'];?>" class="btn btn-primary btn-xs">Add Clients</a>
								<!--<button class="btn btn-primary btn-xs">Send batch</button></td>-->
								<?php if($batch['sent'] == 1){ ?>
									<button href="#" class="btn btn-success btn-xs">&nbsp;&nbsp;&nbsp;&nbsp;[ Sent ]&nbsp;&nbsp;&nbsp;&nbsp;</button>
								<?php	}else{ ?>
								<a href="index.php?rdp=sage_customers&action=debit_orders&id=<?php echo $batch['id'];?>" class="btn btn-primary btn-xs">Send batch</a>
								<?php } ?>
								<a href="index.php?rdp=sage_batch_reports&id=<?php echo $batch['id'];?>" class="btn btn-primary btn-xs">Get Report</a></td>
						</tr>
					<?php } ?>
					</table>
					
                </div>
            </div>
    </div>
</div>

<div id="processingDeadlines" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Processing Deadlines</h4>
			</div>
			
			<div class="modal-body">
		<table class="default">
<tbody>
<tr class="default">
<th class="default" scope="col"></th>
<th class="default" scope="col"><strong>Same day debit orders</strong></th>
<th class="default" scope="col"><strong>Dated debit orders</strong></th>
</tr>
<tr class="default">
<td class="default"><strong>Processing days<br />
</strong>(excludes any public holidays)</td>
<td class="default">Monday to Saturday</td>
<td class="default">Monday to Friday</td>
</tr>
<tr class="default">
<td class="default"><strong>Must be loaded -and authorized</strong><br />
on Sage Pay before</td>
<td class="default">10:59 on the action date (Mon-Fri) -or<br />
23:59 one (1) business day prior<br />
to the action date for Saturday service</td>
<td class="default">23:59 two (2) business days prior to action date</td>
</tr>
<tr class="default">
<td class="default"><strong>Debits processed by banks</strong><br />
(as a general rule but might<br />
differ from bank to bank)</td>
<td class="default">usually after 16h00 on the action date</td>
<td class="default">usually 00h01 on the action date</td>
</tr>
<tr class="default">
<td class="default"><b>Payment of collection value </b><br />
less retentions and fees as available balance<br />
in the Sage Pay merchant account</td>
<td class="default">One (1) business day after the action date.</td>
<td class="default">On the action date.</td>
</tr>
</tbody>
</table>
			</div>
			<!--<div class="form-group">
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" id="action_validateBankAccount">Submit</button>
				<button type="butotn" data-dismiss="modal" class="btn">Cancel</button>
			</div>
			</div>-->
		</div>
		
	</div>
</div>

<script type="text/javascript">
    $("#action_create_sage_batch").click(function (e) {
        e.preventDefault();
        actionValidateSageConnection();
    });

    function actionValidateSageConnection() {
        var errorNum = farmCheck();

        if (errorNum > 0) {
            $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
            $("#notify .message").html("<strong>Error</strong>: It appears you have forgotten to complete something!");
            $("html, body").animate({scrollTop: $('#notify').offset().top - 100}, 1000);
        } else {
            $(".required").parent().removeClass("has-error");

            var $btn = $("#action_create_sage_batch").button("loading");

            $.ajax({
                url: 'request/sage.php',
                type: 'POST',
                data: $("#create_sage_batch").serialize(),
                dataType: 'json',
                success: function (data) {
                    $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#notify").removeClass("alert-warning").addClass("alert-success").fadeIn();
                    $("html, body").animate({scrollTop: $('#notify').offset().top - 100}, 1000);
                    $btn.button("reset");
                },
                error: function (data) {
                    $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                    $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
                    $("html, body").animate({scrollTop: $('#notify').offset().top - 100}, 1000);
                    $btn.button("reset");
                }

            });
        }

    }
</script>
