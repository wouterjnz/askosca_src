   <div id="notify" class="alert alert-success" style="display:none;">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <div class="message"></div>
    </div>

    <form method="post" id="create_customer">
        <input type="hidden" name="act" value="create_customer">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>New Customer Details</h4>

                        <div class="clear"></div>
                    </div>
                    <div class="panel-body form-group form-group-sm">
                        <div class="row">
                            <div class="col-xs-12">
						<?php 
								global $db;
								// echo "<pre/>";
								
								$qry = "SHOW COLUMNS FROM reg_customers";
								// $qry = "DESCRIBE reg_customers";
								// $qry = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'express_invoice' AND TABLE_NAME = 'reg_customers'";
								
								$results = $db->pdoQuery($qry)->results();
								
								// print_r($results);
								// exit;
								$exept = array('id','tid');
								foreach($results as $result){
									
									$type = preg_replace('/\(.*\)/','',$result['Type']);
									// GET data between paranthesis
									$value = preg_replace('#[^()]*\((([^()]+|(?R))*)\)[^()]*#','\1', $result['Type']);
									// print_r($inter);
									// echo $value;
									// exit;
									if(in_array($result['Field'], $exept)){ continue; }
									switch($type){
										case 'int':  
											echo get_record($type, $result['Field'], $value);
										break;
										case 'varchar':  
											echo get_record($type, $result['Field'], $value);
										break;
										case 'tinyint':  
											echo get_record($type, $result['Field'], $value);
										break;
										case 'char':  
											echo get_record($type, $result['Field'], $value);
										break;
										case 'datetime':  
											echo get_record($type, $result['Field'], $value);
										break;
										case 'text':  
											echo get_record('text', $result['Field'], $value);
										break;
									}
									// break;
								}
							
								
								
								function get_record($type, $name, $value){
									global $siteConfig;
									
									$label = preg_split('/(?=[A-Z])/', $name);
									
									$newlable = '';
									foreach($label as $splitlabel)
									{
										$newlable .= $splitlabel.' ';
									}
									
									$record = '
									<div class="form-group">
                                    <label class="col-sm-2 control-label margin-bottom">'.$newlable.'</label>
                                    <div class="col-sm-10">
									';
									$ignore = array('text', 'datetime', 'char', 'tinyint');
									if($type == 'varchar'){
										
                                       $record .= '<input type="'.$type.'" placeholder="'.$name.'" class="form-control margin-bottom " name="'.$name.'"
                                               id="'.$name.'" value="<?php echo $row[\''.$name.'\'] ;?>">';
									}
									if($type == 'char'){
										
                                       $record .= '<select class="form-control margin-bottom " name="'.$name.'"
                                               id="'.$name.'">';
											  
											 if($value == 1){
												$record .=  '<option value="">--</option><option value="Y" <?php echo($row[\''.$name.'\'] == Y)? "selected":""; ?>>Y</option><option value="N" <?php echo($row[\''.$name.'\'] == N)? "selected":""; ?>>N</option>'; 
											 }
											 if($value > 1){
												$record .=  '<option value="">--</option>
																	<option value="">0</option>
																	<option value="0">0</option>'; 
											 } 
										$record	.=   '</select>';
									}
									if($type == 'text'){
										$record .= '<textarea class="form-control margin-bottom " name="'.$name.'"
                                               id="'.$name.'"><?php echo $row[\''.$name.'\'];?></textarea>';
									}
                                    
									if($type == 'datetime'){
									$record .= '<div class="input-group date" id="'.$name.'">
										<input type="text" class="form-control required margin-bottom" name="'.$name.'"   value="'.$siteConfig['date'].'" data-date-format="'.$siteConfig['dformat2'].'" />
										 <span class="input-group-addon margin-bottom">
                                            <span class="icon-calendar margin-bottom"></span>
                                         </span>
									</div>';
									}
									$record .= '</div>
                                </div>';
									return $record;
								}
								?>
							
                                <div class="form-group ">

                                    <div class="col-sm-5 margin-bottom">
                                        <input type="submit" id="action_create_customer"
                                               class="btn btn-success float-left" value="Create Customer"
                                               data-loading-text="Creating...">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

    </form>