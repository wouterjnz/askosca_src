<?php

if (stristr(htmlentities($_SERVER['PHP_SELF']), "product.php")) {
    die("Internal Server Error!");
}
if ($user->group_id > 2) {
    die('<div class="panel-heading alert-danger"><h4>You are not authorized!</h4></div>');
}

    global $db, $siteConfig;
?>


		<?php 
		$message = '';
		if(isset($_GET['status']) && $_GET['status'] == 'success'){ 
			$alert_css = 'alert-success';
			
			if(isset($_GET['message'])){ 
				$message = $_GET['message'];
			}else{
				$message = 'Data has been imported.';
			}
		}
		if(isset($_GET['status']) && $_GET['status'] == 'duplicates'){
			$alert_css = 'alert-warning';
			$message = "Data processed, but duplicates were found.";
		}
		if(isset($_GET['status']) && $_GET['status'] == 'warning'){
			$alert_css = 'alert-error';
			$message = $_GET['message'];
		}
		if(isset($_GET['status']) && $_GET['status'] == 'error'){
			$alert_css = 'alert-error';
			$message = $_GET['message'];
		}
		  ?>
    <div id="notify" class="alert <?php echo $alert_css;?>" style="display: <?php echo (!empty($message))? "block":"none";?>">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <div class="message">
		<?php  echo (!empty($message))? $message:'';?>
		</div>
    </div>

    <div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Import Products</h4>
            </div>
            <div class="panel-body form-group form-group-sm">
				
				
				<?php 
			
				if(isset($_POST['vrdata']) && !empty($_POST['vrdata']))
				{
					echo "<h2>Results (please import file again)</h2>";
					echo "<hr/>";
					echo $_POST['vrdata'];
					echo "<hr/>";
				}
				if(isset($_POST['data'])){
					$data = json_decode($_POST['data']);
					// echo "<pre/>";
					// print_r($data);
					$duplicates = count($data->exists);
					$imported = count($data->newdata);
					 echo 'Duplicates -- '. $duplicates. ' <br/>Imported  -- '.$imported."<br/><br/>";
					if($duplicates > 0){
						view_results($data->exists, $duplicates, $imported);
					}	
				}

				?>
				
				
				
                <form action="request/import_prod.php" method="post" id="add_products" enctype="multipart/form-data">
                    <input type="hidden" name="act" value="add_product">

                    <div class="row">
                        <div class="col-xs-12">
                            
							<div class="form-group">
                                <label class="col-sm-4 control-label margin-bottom">Select List</label>
                                <div class="col-sm-8">
								<input type="file" name="fileToImport" class="form-control margin-bottom" required />
								</div>
							</div>
							
							<div class="form-group">
                                <label class="col-sm-4 control-label margin-bottom">Select Supplier</label>
                                <div class="col-sm-8">
                                    <select name="supplier" class="form-control margin-bottom">
										<option value="">--</option>
                                        <?php $query = "SELECT * FROM reg_vendors";
                                        $result = $db->pdoQuery($query)->results();
                                        foreach ($result as $row) {
                                            $cid = $row['id'];
                                            $title = $row['name'];
                                            echo "<option value='$cid'>$title</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
							
							<div class="form-group">
                                <label class="col-sm-4 control-label margin-bottom">Import With Categories?</label>
                                <div class="col-sm-8">
								<input type="checkbox" name="with_categories" value="1" onclick="showCategories()" id="tickedCategories"/>
								</div>
							</div>
							
                            <div class="form-group hidden" id="categories">
                                <label class="col-sm-8 control-label margin-bottom">Product Category (*this will overwrite the csv categories and will add the category to every record)</label>
                                <div class="col-sm-8">
                                    <select name="product_cat" class="form-control margin-bottom">
										<option value="">--</option>
                                        <?php $query = "SELECT * FROM product_cat";
                                        $result = $db->pdoQuery($query)->results();
                                        foreach ($result as $row) {
                                            $cid = $row['id'];
                                            $title = $row['title'];
                                            echo "<option value='$cid'>$title</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
							
							<div class="form-group">
                                <label class="col-sm-4 control-label margin-bottom">Markup to add (%)?</label>
                                <div class="col-sm-8">
								<input type="number" name="markup" class="form-control margin-bottom" value="10" />
								</div>
							</div>
							
							
							
							<div class="form-group">
                                <label class="col-sm-4 control-label margin-bottom">Run Simulation / View Input ?</label>
                                <div class="col-sm-8">
								<input type="checkbox" name="view_results" value="1" />
								</div>
							</div>
							
                            <div class="form-group">
                                <label class="col-sm-4 control-label margin-bottom"></label>
                                <div class="col-sm-8">
                                    <input type="submit" id="action_add_products" class="btn btn-success margin-bottom"
                                           value="Import" data-loading-text="Importing...">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
				
				
            </div>
        </div>
    </div>
    <div>
	
	
	<?php
						function view_results($data, $duplicates, $imported)
						{ ?>
						<form action="request/import_prod.php" method="post">
						<?php 
							echo "<h2>DUPLICATE PRODUCTS FOUND. -- ($duplicates) </h2>";
							echo "<hr/>";
							$tble = "<table class='table cell-border' cellspacing='0'>
								<tr>
									<td>CATEGORY</td>
									<td>Product Code</td>
									<td>Product Description</td>
									<td>Excl Vat</td>
									<td>Incl Vat</td>
									<td>MY WHOLESALE PRICE</td>
									<td>MY PRICE</td>
									<td>CHECK</td>
								</tr>";
								$category = '';
								$style = '';
							foreach($data as $va)
							{
									if(format_vat($va->import_inclvat) <  format_vat($va->systemwholesale)){
										$style = 'color: #F00';
									}
									if(format_vat($va->import_inclvat) > format_vat($va->systemwholesale)){
										$style = 'color: #00F';
									}
									$tble .= "
										<tr style='$style'>
											<td>".$va->category."</td>
											<td>".$va->product."</td>
											<td>".$va->product_code."</td>
											<td>".str_replace('R','',$va->import_exclvat)."</td>
											<td>".str_replace('R','',$va->import_inclvat)."</td>
											<td>".number_format($va->systemwholesale, 2,'.',',')."</td>
											<td>".number_format($va->systemprice, 2, '.',',')."</td>
											<td><input type='checkbox' name='product_checked[]' value='".$va->pid."' /></td>
										</tr>";
							}	
								
							$tble .= "</table>";
							echo $tble;
							
						?>
						<input type="submit" value="Import & overwrite" name="submit" class="pull-right" />
				</form>						
						<?php echo "<hr/>";}
						function format_vat($price){
							if(is_numeric($price)){
							$replaced_string = str_replace('R','',$price);
							$replaced_string2 = str_replace(',','',$replaced_string);
							$formatted_number = number_format($replaced_string2,2,'.','');
							return $formatted_number;
							}
						}
						
						
				
	?>
	
    <script type="text/javascript">
        $("#action_add_product").click(function (e) {
            e.preventDefault();
            actionAddProduct();
        });
        function actionAddProduct() {

            var errorNum = farmCheck();
            if (errorNum > 0) {
                $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
                $("#notify .message").html("<strong>Error</strong>: It appears you have forgotten to complete something!");
                $("html, body").animate({scrollTop: $('#notify').offset().top}, 1000);
            } else {

                $(".required").parent().removeClass("has-error");

                var $btn = $("#action_add_product").button("loading");

                $.ajax({
                    url: 'request/e_control.php',
                    type: 'POST',
                    data: $("#add_product").serialize(),
                    dataType: 'json',
                    success: function (data) {
                        $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                        $("#notify").removeClass("alert-warning").addClass("alert-success").fadeIn();
                        $("html, body").animate({scrollTop: $('#notify').offset().top}, 1000);
                        $btn.button("reset");
                    },
                    error: function (data) {
                        $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                        $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
                        $("html, body").animate({scrollTop: $('#notify').offset().top}, 1000);
                        $btn.button("reset");
                    }
                });
            }
        }
		
	function showCategories()
	{
		var checked = document.getElementById('tickedCategories').checked;
		if(checked){
			$('#categories').removeClass('hidden');
		}else{
			$('#categories').addClass('hidden');
		}
	}
    </script>