<div class="row">
	<div class="col-xs-12">
		<div id="notify" class="alert alert-success" style="display:none;">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			<div class="message"></div>
		</div>
		
		<div class="panel panel-default"   style="overflow: scroll; width:100%">
			<div class="panel-heading">
				<h4>Customer List</h4>
				<?php $url = "request/sage.php?action=debit_orders&id=".$_GET['id'].""; $url = "#"?>
				 <center>VERIFY BATCH DATA BELOW AND <a href="<?php echo $url; ?>" onclick="submitBatch('<?php echo $_GET['id'];?>')"  class="btn btn-success btn-sm" >CONTINUE HERE</a> TO SUBMIT BATCH.</center>
			</div>
			<div class="panel-body form-group form-group-sm tbl">
				<form>
				<?php 
				$batchid = $_GET['id'];
				$bquery = "SELECT 
						* 
						FROM 
							sage_debit_batch
						WHERE 
							id = $batchid
						";
														
				$batches = $db->pdoQuery($bquery)->result();

			if($batches['processed'] == 0){
				$client_query = "SELECT 
						c.email, c.Cell, b.*, ri.*, ri.id as invoiceID, c.id as clientID
						FROM rec_invoices ri
						JOIN  reg_customers c ON ri.csd = c.id
						LEFT JOIN banking_details b ON b.clientID = c.id
						WHERE ri.active = 0
						";
				$clients = $db->pdoQuery($client_query)->results();
				
				if(empty($clients)){
					echo "<center><h2>No recurring invoices found.  Please set up invoices and try again.</h2><center/>";
					exit;
				}
						
				// save batch client data
				foreach($clients as $client){
					$status = $db->insert('sage_debit_batch_clients', array('batchID' => $batchid, 'clientID' => $client['clientID'], 'invoiceID' => $client['invoiceID']))->rStatus();
				}
				if(isset($status) && $status == true)
				{
					$db->update('sage_debit_batch', array('processed' => 1), array('id' => $batchid));
				}
			}elseif($batches['processed'] == 1){
				$batchdata = "SELECT sb.*, c.email, c.Cell, ri.total, b.* FROM sage_debit_batch_clients sb 
						JOIN reg_customers c ON c.id = sb.clientID 
						JOIN rec_invoices ri ON ri.id = sb.invoiceID
						LEFT JOIN banking_details b ON b.clientID = c.id
						WHERE batchID = $batchid";
				$clients = $db->pdoQuery($batchdata)->results();
			}
			// echo "<pre/>";
			// print_r($clients);
			// exit;
			
			
				$fields = [
				'account_ref',
				'account_name',				
				'account_active',
				'delete_account',	
				'banking_detail_type',
				'bank_account_name',	
				'bank_account_type',
				'branch_code',
				'filler',
				'bank_account_number',
				'default_debit_amount',
				'total', // amount
				'email',
				'Cell', // phone number for notifications
				'debit_master_group',
				'clientID', //extra 1 - client ID
				'invoiceID',  // extra 2 - rec invoiceID
				'extra3' // extra 3 - batchID
			];

			
			?>
					<table class="table cell-border dataTable" cellspacing="0">		
					<tr>
					<td>Remove</td>
				<?php
					foreach($fields as $key => $value){	 ?>
					<td><strong><?php echo str_replace("_", " ", $value); ?></strong></td>
				<?php	} ?>
					</tr>
				<?php foreach($clients as $client){
					echo "<tr  id='".$client['invoiceID']."'>";
					echo "<td><input type='checkbox' onclick='remove(".$batchid.",".$client['invoiceID'].")' /></td>";
				if(!empty($client)){
					foreach($fields as $key => $value){
						$content = $client[$value];
							if($value == 'extra3'){
								$content = $batchid;
							}
							if($value == 'invoiceID'){
								$content = '<a href="index.php?rdp=viewinvoice&id='.$client['invoiceID'].'" target="_blank" class="btn btn-success btn-xs">'.$client['invoiceID'].'</a>';
							}
						echo "<td>";
						echo $content ;
						echo "</td>";
						}
					}
					echo "</tr>";
				} ?>
					</table>
					<!--<input type="submit" value="submit" name="submit" class="btn btn-primary block pull-right" />-->
				</form>
			</div>
		</div>
		
	</div>
<div>

<div id="delete_customer" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Delete Customer</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete this customer?</p>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete
				</button>
				<button type="button" data-dismiss="modal" class="btn">Cancel</button>
			</div>
		</div>
	</div>
</div>

<?php /*

$batchid = $_GET['id'];
				$bquery = "SELECT 
						* 
						FROM 
							sage_debit_batch
						WHERE 
							id = $batchid
						AND 
							processed = 0
						";
														
				$batches = $db->pdoQuery($bquery)->results();
			
				$client_query = "SELECT 
						c.email, c.Cell, b.*, ri.*, ri.id as invoiceID, c.id as clientID
						FROM rec_invoices ri
						JOIN  reg_customers c ON ri.csd = c.id
						LEFT JOIN banking_details b ON b.clientID = c.id
						WHERE ri.active = 0
						";
				$clients = $db->pdoQuery($client_query)->results();
				
				$fields = [
				'account_ref',
				'account_name',				
				'account_active',
				'delete_account',	
				'banking_detail_type',
				'bank_account_name',	
				'bank_account_type',
				'branch_code',
				'filler',
				'bank_account_number',
				'default_debit_amount',
				'total', // amount
				'email',
				'Cell', // phone number for notifications
				'debit_master_group',
				'clientID', //extra 1 - client ID
				'invoiceID',  // extra 2 - rec invoiceID
				'extra3' // extra 3 - batchID
			];

<table class="table cell-border dataTable" cellspacing="0">		
					<tr>
				<?php
					foreach($fields as $key => $value){	 ?>
					<td><strong><?php echo str_replace("_", " ", $value); ?></strong></td>
				<?php	} ?>
					</tr>
				<?php foreach($clients as $client){
					echo "<tr>";
				if(!empty($client)){
					foreach($fields as $key => $value){
						$content = $client[$value];
							if($value == 'extra3'){
								$content = $batchid;
							}
							if($value == 'invoiceID'){
								$content = '<a href="index.php?rdp=viewinvoice&id='.$client['invoiceID'].'" target="_blank" class="btn btn-success btn-xs">'.$client['invoiceID'].'</a>';
							}
						echo "<td>";
						echo $content ;
						echo "</td>";
						}
					}
					echo "</tr>";
				} ?>
					</table>
*/ ?>

<script>
function submitBatch(id){
	            // var errorNum = farmCheck();
            

                $(".required").parent().removeClass("has-error");

                var $btn = $("#action_add_product").button("loading");

                $.ajax({
                    url: 'request/sage.php?action=debit_orders&id='+id,
                    type: 'POST',
                    data: '',
                    dataType: 'json',
                    success: function (data) {
                        $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                        $("#notify").removeClass("alert-warning").addClass("alert-success").fadeIn();
                        $("html, body").animate({scrollTop: $('#notify').offset().top -100 }, 1000);
                        $btn.button("reset");
                    },
                    error: function (data) {
                        $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                        $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
                        $("html, body").animate({scrollTop: $('#notify').offset().top -100 }, 1000);
                        $btn.button("reset");
                    }
                });
           
}

function remove(batchid, invoiceid){
	            // var errorNum = farmCheck();
                // $(".required").parent().removeClass("has-error");
                // var $btn = $("#action_add_product").button("loading");

                $.ajax({
                    url: 'request/sage.php?action=remove_batch_clients',
                    type: 'POST',
                    data: {'invoiceID': invoiceid, 'batchID': batchid},
                    dataType: 'json',
                    success: function (data) {
                        // $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                        // $("#notify").removeClass("alert-warning").addClass("alert-success").fadeIn();
                        // $("html, body").animate({scrollTop: $('#notify').offset().top}, 1000);
                        // $btn.button("reset");
						$('#'+invoiceid).remove();
                    },
                    error: function (data) {
                        // $("#notify .message").html("<strong>" + data.status + "</strong>: " + data.message);
                        // $("#notify").removeClass("alert-success").addClass("alert-warning").fadeIn();
                        // $("html, body").animate({scrollTop: $('#notify').offset().top}, 1000);
                        // $btn.button("reset");
                    }
                });
           
}
</script>