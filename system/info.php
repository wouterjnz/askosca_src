<?php
/**
 * Express Invoice & Stock Manager
 * Copyright (c) Rajesh Dukiya. All Rights Reserved
 * ***********************************************************************
 *
 *  Email: admin@jnz.co.za
 *  Website: https://www.ultimatekode.com
 *
 *  ************************************************************************
 *  * This software is furnished under a license and may be used and copied
 *  * only  in  accordance  with  the  terms  of such  license and with the
 *  * inclusion of the above copyright notice.
 *  * If you Purchased from Codecanyon, Please read the full License from
 *  * here- http://codecanyon.net/licenses/standard/
 * ***********************************************************************
 */
if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}
//about
switch ($op) {
    case "support":
        support();
        break;
    case "tips":
        tips();
        break;
    default:
        info();
        break;
}


function support()
{

    ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Express Invoice Support</h4>

                    <div class="clear"></div>
                </div>
                <div class="panel-body form-group form-group-sm">
                    <div class="row">
                        <div class="col-sm-12">Hi,<p>Are you facing and software reated issue? There are two ways to get
                                support.</p>

                            <p><strong>With Codecanyon</strong>, You can write us on <a
                                    href="http://codecanyon.net/user/ultimatecode" target="_blank">codecanyon.net/user/ultimatecode</a>
                            </p>

                            <p>Additionally , you can write us on <a href="mailto:admin@jnz.co.za">admin@jnz.co.za</a>
                                for additional information.</p></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php

}

function tips()
{

    ?>
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Express Invoice Support</h4>

                <div class="clear"></div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">Dear Software user,<br>We made this software with months of continue work.
                        However We tried to to make this software bug free and good user interface and easy to set up.
                        Even these things still you are facing any issue or need any support feel free to write us on
                        admin@jnz.co.za
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php

}

function info()
{

    ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Express Invoice Info</h4>

            <div class="clear"></div>
        </div>
        <div class="panel-body form-group form-group-sm">
            <div class="row">
                <div class="col-sm-12">
                    <div style="text-align: center;"><h1>EXPRESS INVOICE</h1>

                        <h2>version: 7.0</h2>

                        <h3>&copy; 2016 <a href="https://www.ultimatekode.com">www.ultimatekode.com</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php

}

?>