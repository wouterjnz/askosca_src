CREATE TABLE `tbl_campaigns_archived` (
	`id` INT(11) UNSIGNED NOT NULL,
	`campaign_name` VARCHAR(40) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`type` VARCHAR(40) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`campaign_description` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`campaign_id` VARCHAR(8) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`region_id` INT(11) NOT NULL DEFAULT '0',
	`permission` VARCHAR(10) NULL DEFAULT 'all' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `campaign_name` (`campaign_name`) USING BTREE,
	INDEX `campaign_id` (`campaign_id`) USING BTREE,
	INDEX `region_id` (`region_id`) USING BTREE,
	INDEX `permission` (`permission`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `campaign_leads_archived` (
	`id` INT(9) UNSIGNED NOT NULL,
	`filename` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`campaign_id` INT(11) NULL DEFAULT '0',
	`company_name` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`first_name` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`last_name` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`jobtitle` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`email` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`phone_number` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`address1` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`city` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`zip` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`country` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`comments` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`company_size` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`industry_sector` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`time_called` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`lead_source` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`permission` TEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`list_id` INT(11) NOT NULL DEFAULT '0',
	`lead_status_id` INT(11) NULL DEFAULT '0',
	`type` VARCHAR(25) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`referral_lead_id` INT(11) NOT NULL DEFAULT '0',
	`user` INT(11) NOT NULL DEFAULT '0',
	`updated` TIMESTAMP NULL DEFAULT NULL,
	`starttime` TIMESTAMP NULL DEFAULT NULL,
	`endtime` TIMESTAMP NULL DEFAULT NULL,
	`qa_status` INT(11) NULL DEFAULT '0',
	`deleted` INT(1) NOT NULL DEFAULT '0',
	`deleted_by` INT(11) NOT NULL DEFAULT '0',
	`deleted_time` DATETIME NULL DEFAULT NULL,
	`qa_status_export` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`da_status_export` INT(1) NOT NULL DEFAULT '0',
	`dispo_date` DATETIME NULL DEFAULT NULL,
	`qa_date` DATETIME NULL DEFAULT NULL,
	`da_date` DATETIME NULL DEFAULT NULL,
	`title` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`effective_date` VARCHAR(16) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`policy_number` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`broker_channel` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`product` VARCHAR(25) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`inception_date` VARCHAR(16) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`policy_status` VARCHAR(25) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`policy_status_per_history` VARCHAR(25) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`relationship` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`id_number` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`date_of_birth` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`age` VARCHAR(3) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`gender` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`work_number` VARCHAR(25) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`cell_number` VARCHAR(25) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`address2` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`address3` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`address4` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`postal_code` VARCHAR(6) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`payment_method` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`agent_code` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`deduction_day` VARCHAR(2) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`debit_order_account_holder` VARCHAR(35) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`debit_order_bank_name` VARCHAR(25) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`debit_order_account_number` VARCHAR(25) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`debit_order_branch_code` VARCHAR(8) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`debit_order_account_type` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`sum_assured_amount` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`total_premium` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`cover_amount` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`total_cover` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`state` VARCHAR(25) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`warm_lead_indicator` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`linkedin_link` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`alternate_number` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`alternative_number_a` VARCHAR(15) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`alternative_number_b` VARCHAR(15) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`alternative_number_c` VARCHAR(15) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`alternative_number_d` VARCHAR(15) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`pool_id` VARCHAR(15) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`campaign_code` VARCHAR(15) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `campaign_id` (`campaign_id`) USING BTREE,
	INDEX `company_name` (`company_name`) USING BTREE,
	INDEX `email` (`email`) USING BTREE,
	INDEX `phone_number` (`phone_number`) USING BTREE,
	INDEX `country` (`country`) USING BTREE,
	INDEX `time_called` (`time_called`) USING BTREE,
	INDEX `lead_source` (`lead_source`) USING BTREE,
	INDEX `list_id` (`list_id`) USING BTREE,
	INDEX `lead_status_id` (`lead_status_id`) USING BTREE,
	INDEX `referral_lead_id` (`referral_lead_id`) USING BTREE,
	INDEX `user` (`user`) USING BTREE,
	INDEX `updated` (`updated`) USING BTREE,
	INDEX `starttime` (`starttime`) USING BTREE,
	INDEX `endtime` (`endtime`) USING BTREE,
	INDEX `qa_status` (`qa_status`) USING BTREE,
	INDEX `dispo_date` (`dispo_date`) USING BTREE,
	INDEX `qa_date` (`qa_date`) USING BTREE,
	INDEX `da_date` (`da_date`) USING BTREE,
	INDEX `effective_date` (`effective_date`) USING BTREE,
	INDEX `policy_number` (`policy_number`) USING BTREE,
	INDEX `product` (`product`) USING BTREE,
	INDEX `inception_date` (`inception_date`) USING BTREE,
	INDEX `policy_status` (`policy_status`) USING BTREE,
	INDEX `id_number` (`id_number`) USING BTREE,
	INDEX `work_number` (`work_number`) USING BTREE,
	INDEX `agent_code` (`agent_code`) USING BTREE,
	INDEX `warm_lead_indicator` (`warm_lead_indicator`) USING BTREE,
	INDEX `linkedin_link` (`linkedin_link`) USING BTREE,
	INDEX `alternate_number` (`alternate_number`) USING BTREE,
	INDEX `state` (`state`) USING BTREE,
	INDEX `ix_alternative_number_a` (`alternative_number_a`) USING BTREE,
	INDEX `ix_alternative_number_b` (`alternative_number_b`) USING BTREE,
	INDEX `ix_alternative_number_c` (`alternative_number_c`) USING BTREE,
	INDEX `ix_alternative_number_d` (`alternative_number_d`) USING BTREE,
	INDEX `ix_pool_id` (`pool_id`) USING BTREE,
	INDEX `ix_campaign_code` (`campaign_code`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


CREATE TABLE `lead_list_archived` (
	`id` INT(11) UNSIGNED NOT NULL,
	`list_name` TEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`added` TIMESTAMP NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	`campaign_id` INT(11) NOT NULL DEFAULT '0',
	`template_id` INT(11) NOT NULL,
	`active` INT(1) NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `list_name` (`list_name`(1024)) USING BTREE,
	INDEX `campaign_id` (`campaign_id`) USING BTREE,
	INDEX `template_id` (`template_id`) USING BTREE,
	INDEX `active` (`active`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `view_campaign_leads_archived` (
`id` int(9) unsigned
,`filename` varchar(100)
,`campaign_id` int(11)
,`company_name` varchar(200)
,`first_name` varchar(200)
,`last_name` varchar(200)
,`jobtitle` varchar(200)
,`email` varchar(200)
,`phone_number` varchar(200)
,`address1` varchar(200)
,`city` varchar(200)
,`zip` varchar(200)
,`country` varchar(200)
,`comments` varchar(200)
,`company_size` varchar(50)
,`industry_sector` varchar(50)
,`time_called` varchar(50)
,`lead_source` varchar(100)
,`permission` text
,`list_id` int(11)
,`lead_status_id` int(11)
,`type` varchar(25)
,`referral_lead_id` int(11)
,`user` int(11)
,`updated` timestamp
,`starttime` timestamp
,`endtime` timestamp
,`qa_status` int(11)
,`deleted` int(1)
,`deleted_by` int(11)
,`deleted_time` datetime
,`qa_status_export` varchar(50)
,`da_status_export` int(1)
,`dispo_date` datetime
,`qa_date` datetime
,`da_date` datetime
,`title` varchar(10)
,`effective_date` varchar(16)
,`policy_number` varchar(30)
,`broker_channel` varchar(20)
,`product` varchar(25)
,`inception_date` varchar(16)
,`policy_status` varchar(25)
,`policy_status_per_history` varchar(25)
,`relationship` varchar(20)
,`id_number` varchar(20)
,`date_of_birth` varchar(10)
,`age` varchar(3)
,`gender` varchar(10)
,`work_number` varchar(25)
,`cell_number` varchar(25)
,`address2` varchar(100)
,`address3` varchar(100)
,`address4` varchar(100)
,`postal_code` varchar(6)
,`payment_method` varchar(20)
,`agent_code` varchar(10)
,`deduction_day` varchar(2)
,`debit_order_account_holder` varchar(35)
,`debit_order_bank_name` varchar(25)
,`debit_order_account_number` varchar(25)
,`debit_order_branch_code` varchar(8)
,`debit_order_account_type` varchar(10)
,`sum_assured_amount` varchar(10)
,`total_premium` varchar(10)
,`cover_amount` varchar(10)
,`total_cover` varchar(10)
,`state` varchar(25)
,`warm_lead_indicator` varchar(50)
,`linkedin_link` varchar(100)
,`alternate_number` varchar(20)
,`campaign_name` varchar(40)
,`username` varchar(50)
,`lead_status` varchar(50)
,`campaign_code` varchar(15)
,`pool_id` varchar(15)
);

--

DROP TABLE IF EXISTS `view_campaign_leads_archived`;

CREATE ALGORITHM=UNDEFINED DEFINER=`jnzsoftw_askosca`@`localhost` SQL SECURITY DEFINER VIEW `view_campaign_leads_archived`  AS SELECT `u`.`id` AS `id`, `u`.`filename` AS `filename`, `u`.`campaign_id` AS `campaign_id`, `u`.`company_name` AS `company_name`, `u`.`first_name` AS `first_name`, `u`.`last_name` AS `last_name`, `u`.`jobtitle` AS `jobtitle`, `u`.`email` AS `email`, `u`.`phone_number` AS `phone_number`, `u`.`address1` AS `address1`, `u`.`city` AS `city`, `u`.`zip` AS `zip`, `u`.`country` AS `country`, `u`.`comments` AS `comments`, `u`.`company_size` AS `company_size`, `u`.`industry_sector` AS `industry_sector`, `u`.`time_called` AS `time_called`, `u`.`lead_source` AS `lead_source`, `u`.`permission` AS `permission`, `u`.`list_id` AS `list_id`, `u`.`lead_status_id` AS `lead_status_id`, `u`.`type` AS `type`, `u`.`referral_lead_id` AS `referral_lead_id`, `u`.`user` AS `user`, `u`.`updated` AS `updated`, `u`.`starttime` AS `starttime`, `u`.`endtime` AS `endtime`, `u`.`qa_status` AS `qa_status`, `u`.`deleted` AS `deleted`, `u`.`deleted_by` AS `deleted_by`, `u`.`deleted_time` AS `deleted_time`, `u`.`qa_status_export` AS `qa_status_export`, `u`.`da_status_export` AS `da_status_export`, `u`.`dispo_date` AS `dispo_date`, `u`.`qa_date` AS `qa_date`, `u`.`da_date` AS `da_date`, `u`.`title` AS `title`, `u`.`effective_date` AS `effective_date`, `u`.`policy_number` AS `policy_number`, `u`.`broker_channel` AS `broker_channel`, `u`.`product` AS `product`, `u`.`inception_date` AS `inception_date`, `u`.`policy_status` AS `policy_status`, `u`.`policy_status_per_history` AS `policy_status_per_history`, `u`.`relationship` AS `relationship`, `u`.`id_number` AS `id_number`, `u`.`date_of_birth` AS `date_of_birth`, `u`.`age` AS `age`, `u`.`gender` AS `gender`, `u`.`work_number` AS `work_number`, `u`.`cell_number` AS `cell_number`, `u`.`address2` AS `address2`, `u`.`address3` AS `address3`, `u`.`address4` AS `address4`, `u`.`postal_code` AS `postal_code`, `u`.`payment_method` AS `payment_method`, `u`.`agent_code` AS `agent_code`, `u`.`deduction_day` AS `deduction_day`, `u`.`debit_order_account_holder` AS `debit_order_account_holder`, `u`.`debit_order_bank_name` AS `debit_order_bank_name`, `u`.`debit_order_account_number` AS `debit_order_account_number`, `u`.`debit_order_branch_code` AS `debit_order_branch_code`, `u`.`debit_order_account_type` AS `debit_order_account_type`, `u`.`sum_assured_amount` AS `sum_assured_amount`, `u`.`total_premium` AS `total_premium`, `u`.`cover_amount` AS `cover_amount`, `u`.`total_cover` AS `total_cover`, `u`.`state` AS `state`, `u`.`warm_lead_indicator` AS `warm_lead_indicator`, `u`.`linkedin_link` AS `linkedin_link`, `u`.`alternate_number` AS `alternate_number`, `c`.`campaign_name` AS `campaign_name`, `e`.`username` AS `username`, `s`.`lead_status` AS `lead_status`, `u`.`pool_id` AS `pool_id`, `u`.`campaign_code` AS `campaign_code` FROM (((`campaign_leads_archived` `u` left join `tbl_campaigns_archived` `c` on(`c`.`id` = `u`.`campaign_id`)) left join `tbl_users` `e` on(`e`.`user_id` = `u`.`user`)) left join `tbl_lead_status` `s` on(`s`.`lead_status_id` = `u`.`lead_status_id`));
COMMIT;

-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 22, 2021 at 12:40 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jnzsoftw_askosca`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_lead_list`
-- (See below for the actual view)
--
CREATE TABLE `view_lead_list_archived` (
`id` int(11) unsigned
,`list_name` text
,`added` timestamp
,`campaign_id` int(11)
,`template_id` int(11)
,`active` int(1)
,`dispositioned` decimal(22,0)
,`total_leads` bigint(21)
);

-- --------------------------------------------------------

--
-- Structure for view `view_lead_list`
--
DROP TABLE IF EXISTS `view_lead_list_archived`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lead_list_archived`  AS SELECT `l`.`id` AS `id`, `l`.`list_name` AS `list_name`, `l`.`added` AS `added`, `l`.`campaign_id` AS `campaign_id`, `l`.`template_id` AS `template_id`, `l`.`active` AS `active`, sum(if(`c`.`lead_status_id` > 0,1,0)) AS `dispositioned`, count(`c`.`id`) AS `total_leads` FROM (`lead_list_archived` `l` join `campaign_leads_archived` `c` on(`c`.`list_id` = `l`.`id`)) GROUP BY `l`.`id` ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
