-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 21, 2021 at 01:54 PM
-- Server version: 10.2.38-MariaDB-cll-lve
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jnzsoftw_askosca`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_campaign_leads`
-- (See below for the actual view)
--
DROP TABLE IF EXISTS `view_campaign_leads`;

CREATE TABLE `view_campaign_leads` (
`id` int(9) unsigned
,`filename` varchar(100)
,`campaign_id` int(11)
,`tblcampaignid` int(11)
,`company_name` varchar(200)
,`first_name` varchar(200)
,`last_name` varchar(200)
,`jobtitle` varchar(200)
,`email` varchar(200)
,`phone_number` varchar(200)
,`address1` varchar(200)
,`city` varchar(200)
,`zip` varchar(200)
,`country` varchar(200)
,`comments` varchar(200)
,`company_size` varchar(50)
,`industry_sector` varchar(50)
,`time_called` varchar(50)
,`lead_source` varchar(100)
,`permission` text
,`list_id` int(11)
,`lead_status_id` int(11)
,`type` varchar(25)
,`referral_lead_id` int(11)
,`user` int(11)
,`updated` timestamp
,`starttime` timestamp
,`endtime` timestamp
,`qa_status` int(11)
,`deleted` int(1)
,`deleted_by` int(11)
,`deleted_time` datetime
,`qa_status_export` varchar(50)
,`da_status_export` int(1)
,`dispo_date` datetime
,`qa_date` datetime
,`da_date` datetime
,`title` varchar(10)
,`effective_date` varchar(16)
,`policy_number` varchar(30)
,`broker_channel` varchar(20)
,`product` varchar(25)
,`inception_date` varchar(16)
,`policy_status` varchar(25)
,`policy_status_per_history` varchar(25)
,`relationship` varchar(20)
,`id_number` varchar(20)
,`date_of_birth` varchar(10)
,`age` varchar(3)
,`gender` varchar(10)
,`work_number` varchar(25)
,`cell_number` varchar(25)
,`address2` varchar(100)
,`address3` varchar(100)
,`address4` varchar(100)
,`postal_code` varchar(6)
,`payment_method` varchar(20)
,`agent_code` varchar(10)
,`deduction_day` varchar(2)
,`debit_order_account_holder` varchar(35)
,`debit_order_bank_name` varchar(25)
,`debit_order_account_number` varchar(25)
,`debit_order_branch_code` varchar(8)
,`debit_order_account_type` varchar(10)
,`sum_assured_amount` varchar(10)
,`total_premium` varchar(10)
,`cover_amount` varchar(10)
,`total_cover` varchar(10)
,`state` varchar(25)
,`warm_lead_indicator` varchar(50)
,`linkedin_link` varchar(100)
,`alternate_number` varchar(20)
,`campaign_name` varchar(40)
,`username` varchar(50)
,`lead_status` varchar(50)
,`campaign_code` varchar(50)
,`pool_id` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `view_campaign_leads`
--
DROP TABLE IF EXISTS `view_campaign_leads`;

CREATE ALGORITHM=UNDEFINED DEFINER=`jnzsoftw_askosca`@`localhost` SQL SECURITY DEFINER VIEW `view_campaign_leads`  AS SELECT `u`.`id` AS `id`, `u`.`filename` AS `filename`, `u`.`campaign_id` AS `campaign_id`, `c`.`campaign_id` AS `tblcampaignid`,`u`.`company_name` AS `company_name`, `u`.`first_name` AS `first_name`, `u`.`last_name` AS `last_name`, `u`.`jobtitle` AS `jobtitle`, `u`.`email` AS `email`, `u`.`phone_number` AS `phone_number`, `u`.`address1` AS `address1`, `u`.`city` AS `city`, `u`.`zip` AS `zip`, `u`.`country` AS `country`, `u`.`comments` AS `comments`, `u`.`company_size` AS `company_size`, `u`.`industry_sector` AS `industry_sector`, `u`.`time_called` AS `time_called`, `u`.`lead_source` AS `lead_source`, `u`.`permission` AS `permission`, `u`.`list_id` AS `list_id`, `u`.`lead_status_id` AS `lead_status_id`, `u`.`type` AS `type`, `u`.`referral_lead_id` AS `referral_lead_id`, `u`.`user` AS `user`, `u`.`updated` AS `updated`, `u`.`starttime` AS `starttime`, `u`.`endtime` AS `endtime`, `u`.`qa_status` AS `qa_status`, `u`.`deleted` AS `deleted`, `u`.`deleted_by` AS `deleted_by`, `u`.`deleted_time` AS `deleted_time`, `u`.`qa_status_export` AS `qa_status_export`, `u`.`da_status_export` AS `da_status_export`, `u`.`dispo_date` AS `dispo_date`, `u`.`qa_date` AS `qa_date`, `u`.`da_date` AS `da_date`, `u`.`title` AS `title`, `u`.`effective_date` AS `effective_date`, `u`.`policy_number` AS `policy_number`, `u`.`broker_channel` AS `broker_channel`, `u`.`product` AS `product`, `u`.`inception_date` AS `inception_date`, `u`.`policy_status` AS `policy_status`, `u`.`policy_status_per_history` AS `policy_status_per_history`, `u`.`relationship` AS `relationship`, `u`.`id_number` AS `id_number`, `u`.`date_of_birth` AS `date_of_birth`, `u`.`age` AS `age`, `u`.`gender` AS `gender`, `u`.`work_number` AS `work_number`, `u`.`cell_number` AS `cell_number`, `u`.`address2` AS `address2`, `u`.`address3` AS `address3`, `u`.`address4` AS `address4`, `u`.`postal_code` AS `postal_code`, `u`.`payment_method` AS `payment_method`, `u`.`agent_code` AS `agent_code`, `u`.`deduction_day` AS `deduction_day`, `u`.`debit_order_account_holder` AS `debit_order_account_holder`, `u`.`debit_order_bank_name` AS `debit_order_bank_name`, `u`.`debit_order_account_number` AS `debit_order_account_number`, `u`.`debit_order_branch_code` AS `debit_order_branch_code`, `u`.`debit_order_account_type` AS `debit_order_account_type`, `u`.`sum_assured_amount` AS `sum_assured_amount`, `u`.`total_premium` AS `total_premium`, `u`.`cover_amount` AS `cover_amount`, `u`.`total_cover` AS `total_cover`, `u`.`state` AS `state`, `u`.`warm_lead_indicator` AS `warm_lead_indicator`, `u`.`linkedin_link` AS `linkedin_link`, `u`.`alternate_number` AS `alternate_number`, `c`.`campaign_name` AS `campaign_name`, `e`.`username` AS `username`, `s`.`lead_status` AS `lead_status`, `u`.`pool_id` AS `pool_id`, `u`.`campaign_code` AS `campaign_code`  FROM (((`campaign_leads` `u` left join `tbl_campaigns` `c` on(`c`.`id` = `u`.`campaign_id`)) left join `tbl_users` `e` on(`e`.`user_id` = `u`.`user`)) left join `tbl_lead_status` `s` on(`s`.`lead_status_id` = `u`.`lead_status_id`)) ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
