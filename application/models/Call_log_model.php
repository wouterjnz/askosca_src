<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Call_log_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
		
    }
	

    /**
     * Get lead
     * @param  string $id Optional - leadid
     * @return mixed
     */
	protected $table = '';
	
	   public static function getAudioRecordsByPhoneNumber($number) {
		$CI = &get_instance();
		$this->db2 = $CI->load->database('vicidial', TRUE);
		// $qry = $this->db2->query("SELECT * FROM employee");
		// print_r($qry->result());

		
	
		$qry = $this->db2->query("SELECT *, SUBSTRING_INDEX(filename, '_', -1) AS contact_number FROM recording_log WHERE SUBSTRING_INDEX(filename, '_', -1) = '$number'");
		return $qry;
        // return DB::connection('asterisk')->select(DB::raw("SELECT *, SUBSTRING_INDEX(filename, '_', -1) AS contact_number FROM recording_log WHERE SUBSTRING_INDEX(filename, '_', -1) = '$number'"));
        //return Array('one', 'two', 'three');
    }

	public static function getAudioRecordsByPhoneNumberArrayAPI($number){
	    
	$url = 'http://askosca.voipcloud.co.za/vicidial/non_agent_api_crm.php?source=test&function=phone_number_log&stage=pipe&user=6665&pass=159357pocALOCapi&phone_number='.$number;
	
	$ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, $url); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch); 
		
		return $output;
	}
	
	public static function getAudioRecordingFiles($lead_id){

		$url = 'http://askosca.voipcloud.co.za/vicidial/non_agent_api_crm.php?source=test&function=recording_lookup&stage=pipe&user=6665&pass=159357pocALOCapi&lead_id='.$lead_id;
		
		$ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch); 
		
		return $output;
		
	}
    public static function getAudioRecordsByPhoneNumberArray($numbers) {
        $numbersCondition = "'" . implode("','", array_filter($numbers)) . "'";
		
		// $CI = &get_instance();
		// $this->vicidial = $CI->load->database('vicidial', TRUE);
		$db2 = new stdClass();
		$CI = &get_instance();
		$db2 = $CI->load->database('vicidial', TRUE);
		
		$qry = $db2->query("SELECT *, SUBSTRING_INDEX(filename, '_', -1) AS contact_number FROM recording_log WHERE SUBSTRING_INDEX(filename, '_', -1) IN ($numbersCondition)");
		// $qry = $db2->query("SELECT * from recording_log");
		return $qry->result();
        // return DB::connection('asterisk')->select(DB::raw("SELECT *, SUBSTRING_INDEX(filename, '_', -1) AS contact_number FROM recording_log WHERE SUBSTRING_INDEX(filename, '_', -1) IN ($numbersCondition)"));
        //return Array('one', 'two', 'three');
    }


    public function get($id = '',$where = array())
    {
        $this->db->where($where);
        if (is_numeric($id)) {
            $this->db->where('id', $id);
            return $this->db->get('tblleadlists')->row();
        }
        return $this->db->get($this->table)->result_array();
    }
	
	/**
	* Create batch list
	* @param array $param name, type, note
	* @return int batchID
	*/
	public function create_list($params)
	{
		$this->db->select_max('batchID');
		$batchID = $this->db->get($this->table)->row('batchID');
		if($batchID == ''){ $newbatchID = 1000; }else{ $newbatchID = $batchID+1; }
		
		$data["batchID"] = $newbatchID;
		if(isset($params['name'])){
			$data["name"] = $params['name'];
		}
		if(isset($params['type'])){
			$data["type"] = $params['type'];
		}
		if(isset($params['note'])){
			$data["note"] = $params['note'];
		}
		$data["created_at"] = date('Y-m-d H:i:s');

		$create = $this->db->insert($this->table, $data);;
		if($create)
		{
			return $newbatchID;
		}else{
			return false;
		}
	}
	
	// Batch list
    /**
     * Get batch lists
     * @param integer Batch ID
     * @return mixed object if id passed else array
     */
    public function get_batch_list($id = false)
    {
        if (is_numeric($id)) {
            $this->db->where('batchID', $id);
            return $this->db->get($this->table)->result_array();
        }
        return $this->db->get($this->table)->result_array();
    }
	
	public function update($column, $id, $data)
	{
		$this->db->where($column, $id);
		$this->db->update($this->table, $data);
	}
}
?>