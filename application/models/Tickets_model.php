<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of client_model
 *
 * @author NaYeM
 */
class Tickets_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;
   
	/* 
	* PIPE TICKETS
	*  @params
	*/
	 public function pipe_decode_string($string)
    {
        if ($pos = strpos($string, "=?") === false) {
            return $string;
        }
        $newresult = NULL;
        while (!($pos === false)) {
            $newresult .= substr($string, 0, $pos);
            $string   = substr($string, $pos + 2, strlen($string));
            $intpos   = strpos($string, "?");
            $charset  = substr($string, 0, $intpos);
            $enctype  = strtolower(substr($string, $intpos + 1, 1));
            $string   = substr($string, $intpos + 3, strlen($string));
            $endpos   = strpos($string, "?=");
            $mystring = substr($string, 0, $endpos);
            $string   = substr($string, $endpos + 2, strlen($string));
            if ($enctype == "q") {
                $mystring = quoted_printable_decode(str_replace("_", " ", $mystring));
            } else {
                if ($enctype == "b") {
                    $mystring = base64_decode($mystring);
                }
            }
            $newresult .= $mystring;
            $pos = strpos($string, "=?");
        }
        $result = $newresult . $string;
        return $result;
    }
    public function insert_piped_ticket($data)
    {
   	
      $blacklisted_emails = ['info@neotel.co.za','channelsupport@neotel.co.za','Channelsupport@neotel.co.za'];
        // if (get_option('email_piping_enabled') == 0) {
            // return false;
        // }
        $attachments  = $data['attachments'];
        $subject      = $data['subject'];
        $message      = $data['body'];
        $name         = $data['fromname'];
        $email        = $data['email'];
        $to           = $data['to'];
        
        
        $decodestring = $subject . "##||-MESSAGESPLIT-||##" . $message;
        $decodestring = $this->pipe_decode_string($decodestring);
        $decodestring = explode("##||-MESSAGESPLIT-||##", $decodestring);
        $subject      = $decodestring[0];
        $message      = $decodestring[1];
        $raw_message  = $message;
        $mailstatus   = false;
		$insert_ar = array(
			'ticket_code' => 'external',
			'subject' => $subject,
			'body' => $message,
			'status' => 'new',
			'priority' => 1,
			'external' => 1,
			'from_email' => $email,
			'to_email' => $to	
		);
	$pos = strpos($subject, "[Ticket ID: ");	
	 if ($pos === false) {	
		$this->db->insert('tbl_tickets',$insert_ar);
		$id = $this->db->insert_id();
		
		$ref = ""; 
		$refs = "0";
	
		for($i=0; $i <= 4; $i++){
			if(strlen($id) < $i){
				$ref .= "$refs";
			}
		}
		$newid = $ref.$id;
		//$message = 'we received your email.';
		//echo 'before template\r\n';
		
			// GET Template
			$email_template = $this->check_by(array('email_group' => 'ticket_entry_reply'), 'tbl_email_templates');
		//echo 'template body \r\n';	
			$tmessage = $email_template->template_body;
		//echo 'template subject\r\n';
			$tsubject = $email_template->subject;
			//$data['body'] = $rdata['body'];
		//echo 'parsing\r\n';	
			$clientName = str_replace("{CLIENT_NAME}", $email, $tmessage);
			$tsubjectm = str_replace("{TICKET_SUBJECT}", $tsubject, $clientName);
			// $message = str_replace("{TICKET_REPLY}", $rdata['body'], $ticket_subject);

			// $Link = str_replace("{TICKET_LINK}", base_url() . 'admin/tickets/ticket_details/' . $id, $ticket_reply);
			$tmessage = str_replace("{SITE_NAME}", 'askOsca', $tsubjectm);
		//echo 'loading view\r\n';
			//$instance = new CI_Controller();
		
			//$data['message'] = $tmessage;
			//$tmessage = $instance->load->view('email_template', $data, TRUE);
			
			//echo $message;
		// reply email
		
		//echo $message . ' - id '.$id;
		//echo 'sending.... \r\n';
		
		// do not sent an autoresponder to blacklisted emails
        if(!in_array($email, $blacklisted_emails)){
		    $this->send_pipe_email(['recipient' => $email, 'subject' => $subject . " [Ticket ID: $newid] ", 'message' => $tmessage, 'resourceed_file' => '']);
        }
		//if($is_sent){ echo 'sent'; }else{ echo 'error occured: '; print_r($is_sent); }
		//print_r($this->email->print_debugger());
		$path = '../../uploads1/'; 	
		foreach ($attachments as $attachment) {
			 $filename      = $attachment["filename"];
                        $filenameparts = explode(".", $filename);
                        $extension     = end($filenameparts);
                          $filename = implode(array_slice($filenameparts, 0, 0 - 1));
                            $filename = trim(preg_replace("/[^a-zA-Z0-9-_ ]/", "", $filename));
                            if (!$filename) {
                                $filename = "attachment";
                            }
                            if (!file_exists($path)) {
                                mkdir($path);
                                fopen($path . 'index.html', 'w');
                            }
                            $filename = unique_filename($path, $filename . "." . $extension);
                            
				//$mul_val = $this->tickets_model->multi_uploadAllType('upload_file');
				//$data_up['upload_file'] = json_encode($mul_val);
				$fp       = fopen($path .$filename, "w");
                           	 fwrite($fp, $attachment["data"]);
                         	   fclose($fp);
		}
		//$data_up['upload_file'] = 'test';
			$this->db->where('tickets_id', $id);
			$this->db->update('tbl_tickets', array('upload_file'=> $data_up['upload_file']));
            } else {
                $tid = substr($subject, $pos + 12);
                $tid = substr($tid, 0, strpos($tid, "]"));
                //$this->db->where('ticketid', $tid);
                //$data = $this->db->get('tbltickets')->row();
                //$tid  = $data->ticketid;
                //$this->_table_name = 'tbl_tickets_replies';
          	$this->db->insert('tbl_tickets_replies',['tickets_id' => $tid, 'replierid' => 1, 'body' => $message]);
            }
            
		 return true;
		//exit;
		/*
        $spam_filters = $this->db->get('tblticketsspamcontrol')->result_array();
        foreach ($spam_filters as $filter) {
            $type  = $filter['type'];
            $value = $filter['value'];
            if ($type == "sender") {
                if (strtolower($value) == strtolower($email)) {
                    $mailstatus = "Blocked Sender";
                }
            }
            if ($type == "subject") {
                if (strpos("x" . strtolower($subject), strtolower($value))) {
                    $mailstatus = "Blocked Subject";
                }
            }
            if ($type == "phrase") {
                if (strpos("x" . strtolower($message), strtolower($value))) {
                    $mailstatus = "Blocked Phrase";
                }
            }
        }
		*/
        // No spam found
        if (!$mailstatus) {
            $pos = strpos($subject, "[Ticket ID: ");
            if ($pos === false) {
            } else {
                $tid = substr($subject, $pos + 12);
                $tid = substr($tid, 0, strpos($tid, "]"));
                $this->db->where('ticketid', $tid);
                $data = $this->db->get('tbltickets')->row();
                $tid  = $data->ticketid;
            }
            $to       = trim($to);
            $toemails = explode(",", $to);
            $deptid   = false;
            $userid   = false;
            foreach ($toemails as $toemail) {
                if (!$deptid) {
                    $this->db->where('email', $toemail);
                    $data = $this->db->get('tbldepartments')->row();
                    if ($data) {
                        $deptid = $data->departmentid;
                        $to     = $data->email;
                    }
                }
            }
            if (!$deptid) {
                $mailstatus = "Department Not Found";
            } else {
                if ($to == $email) {
                    $mailstatus = "Blocked Potential Email Loop";
                } else {
                    $message = trim($message);
                    $this->db->where('active', 1);
                    $this->db->where('email', $email);
                    $result = $this->db->get('tblstaff')->row();
                    if ($result) {
                        if ($tid) {
                            $data            = array();
                            $data['message'] = $message;
                            $data['status']  = 1;
                            if ($userid == false) {
                                $data['name']  = $name;
                                $data['email'] = $email;
                            }
                            $reply_id = $this->add_reply($data, $tid, $result->staffid);
                            if ($reply_id) {
                                $mailstatus = "Ticket Reply Imported Successfully";
                            }
                        } else {
                            $mailstatus = "Ticket ID Not Found";
                        }
                    } else {
                        $this->db->where('email', $email);
                        $result = $this->db->get('tblcontacts')->row();
                        if ($result) {
                            $userid    = $result->userid;
                            $contactid = $result->id;
                        }
                        if ($userid == false && get_option('email_piping_only_registered') == '1') {
                            $mailstatus = "Unregistered Email Address";
                        } else {
                            $filterdate = date("YmdHis", mktime(date("H"), date("i") - 15, date("s"), date("m"), date("d"), date("Y")));
                            $query      = 'SELECT count(*) as total FROM tbltickets WHERE date > "' . $filterdate . '" AND (email="' . $this->db->escape($email) . '"';
                            if ($userid) {
                                $query .= " OR userid=" . (int) $userid;
                            }
                            $query .= ")";
                            $result = $this->db->query($query)->row();
                            if (10 < $result->total) {
                                $mailstatus = "Exceeded Limit of 10 Tickets within 15 Minutes";
                            } else {
                                if (isset($tid)) {
                                    $data            = array();
                                    $data['message'] = $message;
                                    $data['status']  = 1;
                                    if ($userid == false) {
                                        $data['name']  = $name;
                                        $data['email'] = $email;
                                    } else {
                                        $data['userid']    = $userid;
                                        $data['contactid'] = $contactid;
                                    }
                                    $reply_id = $this->add_reply($data, $tid);
                                    if ($reply_id) {
                                        $mailstatus = "Ticket Reply Imported Successfully";
                                    }
                                } else {
                                    if (get_option('email_piping_only_registered') == 1 && !$userid) {
                                        $mailstatus = "Blocked Ticket Opening from Unregistered User";
                                    } else {
                                        if (get_option('email_piping_only_replies') == '1') {
                                            $mailstatus = "Only Replies Allowed by Email";
                                        } else {
                                            $data               = array();
                                            $data['department'] = $deptid;
                                            $data['subject']    = $subject;
                                            $data['message']    = $message;
                                            $data['contactid']  = $contactid;
                                            // $data['priority']   = get_option('email_piping_default_priority');
                                            if ($userid == false) {
                                                $data['name']  = $name;
                                                $data['email'] = $email;
                                            } else {
                                                $data['userid'] = $userid;
                                            }
                                            $tid        = $this->add($data);
                                            $mailstatus = "Ticket Imported Successfully";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($mailstatus == "") {
            $mailstatus = "Ticket Import Failed";
        } else {
			/*
            if (isset($tid)) {
                if (!empty($attachments)) {
                    $ticket_attachments = array();
                    $allowed_extensions = explode('|', get_option('ticket_attachments_file_extensions'));
                    $path               = TICKET_ATTACHMENTS_FOLDER . $tid . '/';
                    foreach ($attachments as $attachment) {
                        $filename      = $attachment["filename"];
                        $filenameparts = explode(".", $filename);
                        $extension     = end($filenameparts);
                        if (in_array($extension, $allowed_extensions)) {
                            $filename = implode(array_slice($filenameparts, 0, 0 - 1));
                            $filename = trim(preg_replace("/[^a-zA-Z0-9-_ ]/", "", $filename));
                            if (!$filename) {
                                $filename = "attachment";
                            }
                            if (!file_exists($path)) {
                                mkdir($path);
                                fopen($path . 'index.html', 'w');
                            }
                            $filename = unique_filename($path, $filename . "." . $extension);
                            $fp       = fopen($path . $filename, "w");
                            fwrite($fp, $attachment["data"]);
                            fclose($fp);
                            array_push($ticket_attachments, array(
                                'file_name' => $filename,
                                'filetype' => get_mime_by_extension($filename)
                            ));
                        }
                    }
                    // $this->insert_ticket_attachments_to_database($ticket_attachments, $tid, $reply_id);
                }
            }
			*/
        }
		/*
        $this->db->insert('tblticketpipelog', array(
            'date' => date('Y-m-d H:i:s'),
            'email_to' => $to,
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'message' => $message,
            'status' => $mailstatus
        ));
		*/
    }
}
