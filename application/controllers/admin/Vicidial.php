<?php

/**
 * Description of Tasks
 *
 * @author Jamie
 */
class Vicidial extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->load->model('tickets_model');
    }
    
    
    public function index(){
        $data['title'] = 'Agent Login';
        $subview  = 'agent_login.php';
		
		$id = $this->session->userdata('user_id');
		$data['server'] = config_item('vicidial_server'); //'156.38.193.158';

		$data['vicidial'] = $this->db->where('user_id',$id)->get('tbl_users')->row();
		
        $data['subview'] = $this->load->view('admin/vicidial/' . $subview, $data, TRUE);
	
		// print_r($data); exit;
        $this->load->view('admin/_layout_main', $data); //page load
    }   
}