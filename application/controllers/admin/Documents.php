<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');


class Documents extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
		$this->load->model('client_model');
		$this->load->model('invoice_model');
        $this->load->model('account_model');
    }


    public function index()
    {
        $data['title'] = 'Documents';
	     // get all client
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'client_id';
        $data['all_client'] = $this->invoice_model->get();
        $data['subview'] = $this->load->view('admin/documents/documents', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

	public function edit_form($table, $id=0, $active_tab) 
	{
		$data['title'] = 'Documents';
			     // get all client
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'client_id';
        $data['all_client'] = $this->invoice_model->get();
		//get data
		$this->invoice_model->_table_name = 'tbl_docs_'.$table;
        $this->invoice_model->_order_by = 'id';
        $data['all_'.$table] = $this->invoice_model->get_by(array('id' => $id), TRUE);
		$data['active'] = $active_tab;
		// print_r($data);
		
		// if ($id == 0) {
		$data['subview'] = 	$this->load->view('admin/documents/documents', $data, TRUE);
		 $this->load->view('admin/_layout_main', $data); //page load
		// } else {
			// $this->load->view('admin/document/edit_form/$id', $data);
		// }
	}
	
	public function save($table, $additional = false)
	{
		$exclude_fields = array('submit','barcode');
		
		$data = $this->filter_posts($this->input->post(), $exclude_fields, false);
		
		if (empty($data)) {
			return false;
		}
	
		$query = $this->db->insert($table, $data);
		
		if ($additional) {
			
			$id = $this->db->insert_id();
			$additional_data = $this->filter_posts($this->input->post(), $exclude_fields, true, $data);
			$table = $table.'_data';
			
			unset($data);
			if (!empty($additional_data)) {
				foreach ($additional_data as $add => $val) {
					for ($i = 0; $i <= count($add); $i++) {
						$data[$i]['installation_id'] = $id;
						$data[$i][$add] = $val[$i];
					}
				}
				$this->db->insert_batch($table, $data);
			}
		}
		
		if ($query) {
			set_message("success","The form has been saved.");
		    redirect('admin/documents');
			return true;
		}
	}
    
	public function update($table, $id, $additional = 0)
	{
		$exclude_fields = array('submit','barcode');
		
		$data = $this->filter_posts($this->input->post(), $exclude_fields, false);
		
		if (empty($data)) {
			return false;
		}
		
		$this->db->where('id', $id);
		$query = $this->db->update($table, $data);
		
		if ($additional) {
			$additional_data = $this->filter_posts($this->input->post(), $exclude_fields, true, $data);
			$table = $table.'_data';
			print_r($additional_data);
			if (!empty($additional_data)) {
				foreach ($additional_data as $add => $val) {
					for ($i = 0; $i <= count($add); $i++) {
						if ($add == 'id') {
							$idx[] = $val[$i];	
						} 
						$datas[$i][$add] = $val[$i];
					}
				}
					
				foreach($idx as $ids){
					foreach($datas as $idata){
						if($ids == $idata['id']){
							$this->db->where('id', $ids)->update($table, $idata);
						}
					}
				}
			}
		}
		// print_r($datas);
		if ($query) {
			set_message("success","The form has been updated.");
		    redirect('admin/documents');
			return true;
		}
		
	}
	
	function filter_posts($posts, $exclude, $extra, $additional_data = array())
	{
		// print_r($additional_data);
		// return;
		if (!empty($posts)) {
			foreach ($posts as $post => $val) {
				if (!in_array($post, $exclude)) {
						$data[$post] = $val;
				}
			}
			
				unset($data['id']);
				unset($data['l_desc']);
				unset($data['l_qty']);
				unset($data['l_unitprice']);
				unset($data['l_total']);
				unset($data['m_desc']);
				unset($data['m_qty']);
				unset($data['m_unitprice']);
				unset($data['m_total']);
				
				// unset($data['m_vat']);
				// unset($data['m_vat_percentage']);
				// unset($data['l_vat']);
				// unset($data['l_vat_percentage']);
				// unset($data['m_grand_total']);
				// unset($data['l_grand_total']);
			
			if(!empty($additional_data)){
				$diff =  array_diff_assoc($posts, $additional_data);
				unset($diff['submit']);
				return $diff;
			}
			
			return $data;
		}
	}
	
}
