<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author pc mart ltd
 */
class Pos extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('invoice_model');
        $this->load->model('estimates_model');
    }
	
    public function index($action = NULL)
    {
		
        $data['title'] = config_item('company_name');
        $data['page'] = lang('point of sale');
		
		
        //total expense count
        $this->admin_model->_table_name = "tbl_transactions"; //table name
        $this->admin_model->_order_by = "transactions_id"; // order by 
        $total_income_expense = $this->admin_model->get(); // get result
        $today_income_expense = $this->admin_model->get_by(array('date' => date('Y-m-d'))); // get result
		   // get all client
        $this->invoice_model->_table_name = 'tbl_client';
        $this->invoice_model->_order_by = 'client_id';
		$data['all_client'] = $this->invoice_model->get();
		
		
		
		
        $today_income = 0;
        $today_expense = 0;
        foreach ($today_income_expense as $t_income_expense) {
            if ($t_income_expense->type == 'Income') {
                $today_income += $t_income_expense->amount;

            } elseif ($t_income_expense->type == 'Expense') {
                $today_expense += $t_income_expense->amount;
            }
        }
        $data['today_income'] = $today_income;

        $data['today_expense'] = $today_expense;

        $total_income = 0;
        $total_expense = 0;
        foreach ($total_income_expense as $v_income_expense) {
            if ($v_income_expense->type == 'Income') {
                $total_income += $v_income_expense->amount;

            } elseif ($v_income_expense->type == 'Expense') {
                $total_expense += $v_income_expense->amount;
            }
        }
        $data['total_income'] = $total_income;

        $data['total_expense'] = $total_expense;

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->admin_model->check_by(array('user_id' => $user_id), 'tbl_users');
        $data['role'] = $user_info->role_id;

        $data['invoce_total'] = $this->invoice_totals_per_currency();
        if (!empty($action) && $action == 'payments') {
            $data['yearly'] = $this->input->post('yearly', TRUE);
        } else {
            $data['yearly'] = date('Y'); // get current year
        }
        if (!empty($action) && $action == 'Income') {
            $data['Income'] = $this->input->post('Income', TRUE);
        } else {
            $data['Income'] = date('Y'); // get current year
        }
        if ($this->input->post('year', TRUE)) { // if input year 
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
        // get all expense list by year and month
        $data['all_income'] = $this->get_transactions_list($data['Income'], 'Income');

        $data['all_expense'] = $this->get_transactions_list($data['year'], 'Expense');

        $data['yearly_overview'] = $this->get_yearly_overview($data['yearly']);

        if ($this->input->post('month', TRUE)) { // if input year 
            $data['month'] = $this->input->post('month', TRUE);
        } else { // else current year
            $data['month'] = date('Y-m'); // get current year
        }
        $data['income_expense'] = $this->get_income_expense($data['month']);

        // goal tracking
        if ($this->input->post('goal_month', TRUE)) { // if input year
            $data['goal_month'] = $this->input->post('goal_month', TRUE);
        } else { // else current year
            $data['goal_month'] = date('Y-m'); // get current year
        }
        $data['goal_report'] = $this->get_goal_report($data['goal_month']);

		
		// Check if action is a numeric (selected warehouse)
		if( ! preg_match('/^\d+$/', $action) ){

			
			$data['subview'] = $this->load->view('admin/pos/pos_warehouse', $data, TRUE);
			$this->load->view('admin/pos/pos', $data);
			
		}else{
			$this->db->where('id', $action);
			$query = $this->db->get('tbl_warehouse')->result_array();
			$data['warehouse_id'] = $action;
			$data['page'] = '&nbsp;&nbsp;&nbsp; ---- &nbsp;&nbsp;&nbsp;&nbsp; ' . $query[0]['warehouse'] ;
			
        $data['subview'] = $this->load->view('admin/pos/pos_view', $data, TRUE);
        $this->load->view('admin/pos/pos', $data);
		}
    }

	public function receipt($id)
	{

		 $data['title'] = config_item('company_name');
        $data['page_title'] = lang('point of sale receipt');
		
		$this->db->where('id',$id);
		$query = $this->db->get('tbl_pos');
		foreach($query->result() as $row){
			$data['pos'] = $row;
		}
		
		// $this->db->select('*');
		$this->db->where('posId',$id);
		if(date('Y-d-m') < '2018-01-21'){
		$this->db->join('tbl_saved_items','tbl_saved_items On tbl_saved_items.saved_items_id = tbl_pos_items.itemId','left');
		}else{
		    $this->db->join('tbl_saved_items_pos','tbl_saved_items_pos On tbl_saved_items_pos.saved_items_id = tbl_pos_items.itemId','left');
		}
		$query_pos = $this->db->get('tbl_pos_items');
		foreach($query_pos->result() as $row){
			$data['items'][] = $row;
		}
		
		// get all client
		$this->db->where('client_id',$data['pos']->clientId);
		$data['all_client'] = $this->db->get('tbl_client')->result();
		
		 $this->load->view('admin/pos/pos_receipt', $data);
	}
	
	public function save_items()
	{
		// print_r($_POST);
		// exit;
		$user_id = $this->session->userdata('user_id');
		
		if(empty($_POST)){
			echo json_encode(array("status" => "FAIL", "reason"=>"NO POST DATA"));
			exit;
		}
		$data = [];
		foreach($_POST['items'] as $item)
		{
		
			$itm = explode(",", $item);
			
			$data[] = ['item' => $itm[0], 'cost' => $itm[1], 'qty' => $itm[2]];
			
		}
		
		$totalItems = $_POST['totalItemsf'];
		$totalAmount = $_POST['totalAmount'];
		$totalVat = $_POST['totalVat'];
		$totalPayable = $_POST['totalPayablef'];
		$totalPaid = $_POST['paidVal'];
		$totalChange = $_POST['changeVal'];
		$clientId = $_POST['client_id'];
		$warehouseId = $_POST['warehouse_id'];
		// exit;
		$insertPOS = array(
			'clientId'	=> $clientId,
			'totalItems'	=> $totalItems,
			'totalAmount'	=> $totalAmount,
			'totalVat'	=> $totalVat,
			'totalPayable'	=> $totalPayable,
			'totalPaid'	=> $totalPaid,
			'totalChange'	=> $totalChange,
			'warehouse_id' => $warehouseId,
			'user_id' => $user_id
		);
		$query = $this->db->insert('tbl_pos', $insertPOS);
		$posId = $this->db->insert_id();
		foreach($data as $dat){
			$insertItems = array(
			'posId' => $posId,
			'itemId' => $dat['item'],
			'price' => $dat['cost'],
			'qty' => $dat['qty'],
			);
			$query = $this->db->insert('tbl_pos_items', $insertItems);
			if($query){
				// update inventory
				$this->update_inventory($dat['item'],$dat['qty']);
			}
		}
		echo json_encode(array('success'=>true,'result'=>$posId));
	}
	
	public function update_inventory($id, $qty){
		$this->db->set('quantity', 'quantity -' . $qty, FALSE);
        $this->db->where('saved_items_id', $id);
        $this->db->update('tbl_saved_items');
	}
	
    public function get_goal_report($month)
    {// this function is to create get monthy recap report

        //m = date('m', strtotime($month));
        $m = date('m', strtotime($month));
        $year = date('Y', strtotime($month));
        $start_date = $year . "-" . $m . '-' . '01';
        $end_date = $year . "-" . $m . '-' . '31';

        $get_goal_report = $this->admin_model->get_goal_report_by_month($start_date, $end_date); // get all report by start date and in date

        return $get_goal_report; // return the result
    }

    function invoice_totals_per_currency()
    {
        $invoices_info = $this->db->where('inv_deleted', 'No')->get('tbl_invoices')->result();
        $paid = $due = array();
        $currency = 'USD';
        $symbol = array();
        foreach ($invoices_info as $v_invoices) {
            if (!isset($paid[$v_invoices->currency])) {
                $paid[$v_invoices->currency] = 0;
            }
            if (!isset($due[$v_invoices->currency])) {
                $due[$v_invoices->currency] = 0;
            }
            $paid[$v_invoices->currency] += $this->invoice_model->get_invoice_paid_amount($v_invoices->invoices_id);
            $due[$v_invoices->currency] += $this->invoice_model->get_invoice_due_amount($v_invoices->invoices_id);
            $currency = $this->admin_model->check_by(array('code' => $v_invoices->currency), 'tbl_currencies');
            $symbol[$v_invoices->currency] = $currency->symbol;
        }
        return array("paid" => $paid, "due" => $due, "symbol" => $symbol);
    }

    public function get_yearly_overview($year)
    {// this function is to create get monthy recap report
        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $month = '0' . $i;
            } else {
                $month = $i;
            }
            $yearly_report[$i] = $this->admin_model->calculate_amount($year, $month); // get all report by start date and in date 
        }
        return $yearly_report; // return the result
    }

    public function get_income_expense($month)
    {// this function is to create get monthy recap report
        //m = date('m', strtotime($month));
        $m = date('m', strtotime($month));
        $year = date('Y', strtotime($month));
        if ($m >= 1 && $m <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
            $start_date = $year . "-" . '0' . $m . '-' . '01';
            $end_date = $year . "-" . '0' . $m . '-' . '31';
        } else {
            $start_date = $year . "-" . $m . '-' . '01';
            $end_date = $year . "-" . $m . '-' . '31';
        }
        $get_expense_list = $this->admin_model->get_transactions_list_by_month($start_date, $end_date); // get all report by start date and in date 

        return $get_expense_list; // return the result
    }

    public function get_transactions_list($year, $type)
    {// this function is to create get monthy recap report
        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i . '-' . '01';
                $end_date = $year . "-" . '0' . $i . '-' . '31';
            } else {
                $start_date = $year . "-" . $i . '-' . '01';
                $end_date = $year . "-" . $i . '-' . '31';
            }
            $get_expense_list[$i] = $this->admin_model->get_transactions_list_by_date($type, $start_date, $end_date); // get all report by start date and in date
        }
        return $get_expense_list; // return the result
    }

    public function set_language($lang)
    {
        $this->session->set_userdata('lang', $lang);
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function set_clocking($id = NULL)
    {

// sate into attendance table
        $adata['user_id'] = $this->session->userdata('user_id');
        $clocktime = $this->input->post('clocktime', TRUE);
        $date = $this->input->post('clock_date', TRUE);
        if (empty($date)) {
            $date = date('Y-m-d');
        }
        $time = $this->input->post('clock_time', TRUE);
        if (empty($time)) {
            $time = date('h:i:s');;
        }

        if ($clocktime == 1) {
            $adata['date_in'] = $date;
        } else {
            $adata['date_out'] = $date;
        }
        if (!empty($adata['date_in'])) {
            // chck existing date is here or not
            $check_date = $this->admin_model->check_by(array('user_id' => $adata['user_id'], 'date_in' => $adata['date_in']), 'tbl_attendance');
        }
        if (!empty($check_date)) { // if exis do not save date and return id
            if ($check_date->attendance_status != '1') {
                $udata['attendance_status'] = 1;
                $this->admin_model->_table_name = "tbl_attendance"; // table name
                $this->admin_model->_primary_key = "attendance_id"; // $id
                $this->admin_model->save($udata, $check_date->attendance_id);
            }
            $data['attendance_id'] = $check_date->attendance_id;

        } else { // else save data into tbl attendance
            // get attendance id by clock id into tbl clock
            // if attendance id exist that means he/she clock in
            // return the id
            // and update the day out time
            $check_existing_data = $this->admin_model->check_by(array('clock_id' => $id), 'tbl_clock');

            $this->admin_model->_table_name = "tbl_attendance"; // table name
            $this->admin_model->_primary_key = "attendance_id"; // $id
            if (!empty($check_existing_data)) {
                $this->admin_model->save($adata, $check_existing_data->attendance_id);
            } else {
                $adata['attendance_status'] = 1;
                //save data into attendance table
                $data['attendance_id'] = $this->admin_model->save($adata);
            }
        }
        // save data into clock table
        if ($clocktime == 1) {
            $data['clockin_time'] = $time;
        } else {
            $data['clockout_time'] = $time;
            $data['comments'] = $this->input->post('comments', TRUE);
        }

        //save data in database
        $this->admin_model->_table_name = "tbl_clock"; // table name
        $this->admin_model->_primary_key = "clock_id"; // $id
        if (!empty($id)) {
            $data['clocking_status'] = 0;
            $this->admin_model->save($data, $id);
        } else {
            $data['clocking_status'] = 1;
            $this->admin_model->save($data);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

	public function get_item($item = '')
	{
		if(empty($item)){
			echo json_encode(['success'=>'error', 'message'=>'item empty']);
			exit;
		}
		
		$this->db->where('barcode_string',$item);
		$query = $this->db->get('tbl_saved_items')->result_array();
		if(empty($query)){
			echo json_encode(['success'=>'error', 'message'=>'query empty']);
			exit;
		}
		unset($query[0]['barcode']);
		$data = $query[0];
		$data['success'] = 'success';
		
		echo json_encode($data);

	}
}
