<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Leads extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('items_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

	public function agent_leads($id = null){
		   
		 $client = 'askosca';
		if($client == 'askosca'){
			$table = 'lead_list';
			$table_id = 'id';
		}else{
			$table = 'lead_list';
			$table_id = 'id';
		}
		
		$data['title'] = lang('all_leads');
		$data['leads_info'] = $this->items_model->check_by(array('id' => $id), $table);
		$data['active'] = 2;

		$data['all_leads'] = $this->items_model->get_permission($table);
	}
	
    public function index($id = NULL)
    {
		if($this->session->userdata('user_type') == 4){
			redirect('admin/leads/index_leads');
		}
		//QA
		if($this->session->userdata('user_type') == 5){
			redirect('admin/leads/sale_leads');
		}
		//DA
		if($this->session->userdata('user_type') == 6){
			redirect('admin/leads/dba_leads');
		}
		// ini_set('display_errors',0);
       $client = 'askosca';
		if($client == 'askosca'){
			$table = 'lead_list';
			$table_id = 'id';
		}else{
			$table = 'lead_list';
			$table_id = 'id';
		}
        $data['title'] = lang('all_leads');
        if (!empty($id)) {
            $data['active'] = 2;
            $can_edit = $this->items_model->can_action($table, 'edit', array('id' => $table_id));
            if (!empty($can_edit)) {
                $data['leads_info'] = $this->items_model->check_by(array('id' => $id), $table);
            }
        } else {
            $data['active'] = 1;
        }
        // get all leads status
        $status_info = $this->db->get('tbl_lead_status')->result();
        if (!empty($status_info)) {
            foreach ($status_info as $v_status) {
                $data['status_info'][$v_status->lead_type][] = $v_status;
            }
        }
        $data['assign_user'] = $this->items_model->allowad_user('55');
        // $data['all_leads'] = $this->items_model->get_permission($table,'',20);
        $data['all_leads'] = $this->items_model->get_permission($table);
		// print_r($data);
		// exit;
        $data['subview'] = $this->load->view('admin/leads/list_leads', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }
	
	public function index_leads($list_id = null, $id = NULL)
    {
      $client = 'askosca';
		if($client == 'askosca'){
			$table = 'campaign_leads';
			$table_id = 'id';
		}else{
			$table = 'tbl_leads';
			$table_id = 'lead_id';
		}
        $data['title'] = lang('all_leads');
        if (!empty($id)) {
            $data['active'] = 2;
            $can_edit = $this->items_model->can_action($table, 'edit', array('id' => $table_id));
            if (!empty($can_edit)) {
                $data['leads_info'] = $this->items_model->check_by(array('id' => $id), $table);
            }
        } else {
            $data['active'] = 1;
        }
        
         $template_id = $this->db->where('id',$list_id)->get('lead_list')->row('template_id');
		$data['template_id'] = $template_id;
		
		
        // get all leads status
        
        if($template_id == 2){
        $status_info = $this->db->where('template_id',$template_id)->get('tbl_lead_status')->result();
        }else{
            $status_info = $this->db->where('template_id',1)->get('tbl_lead_status')->result();
        }
        if (!empty($status_info)) {
            foreach ($status_info as $v_status) {
                $data['status_info'][$v_status->lead_type][] = $v_status;
            }
        }
        $data['assign_user'] = $this->items_model->allowad_user('55');
        $data['all_leads'] = $this->items_model->get_permission($table,null,5000, $list_id);

        // $data['subview'] = $this->load->view('admin/leads/all_leads', $data, TRUE);
       
		
		if($template_id == 2){
			$data['subview'] = $this->load->view('admin/leads/all_leads_template', $data, TRUE);
		}else{
			$data['subview'] = $this->load->view('admin/leads/all_leads', $data, TRUE);
		}
		
        $this->load->view('admin/_layout_main', $data); //page load
    }
	
	public function sale_leads($id = NULL)
    {
	  if (!empty($this->input->post('daterange'))) {
		  $daterange = explode('-',$this->input->post('daterange'));
		
	      $date_from = $daterange[0].'-'.$daterange[1].'-'.$daterange[2];
	      $date_to = $daterange[3].'-'.$daterange[4].'-'.$daterange[5];
		  if ($date_from < '2018-12-03 23:59:59') {
			  $data['search_array'] = array('lead_status_id' => 6, 'qa_status !=' => 1);
		  } else {
			  $data['search_array'] = array('lead_status_id' => 6, 'qa_status !=' => 1, 'dispo_date >= ' => $date_from, 'dispo_date <= ' => $date_to);
		  }
	  } else {
		  $date_from = date('Y-m-d 00:00:00');
	      $date_to = date('Y-m-d 23:59:59');
	      $date_from_query = date('Y-m-d 00:00:00');
	      $date_to_query = date('Y-m-d 23:59:59');
		  $data['search_array'] = array('lead_status_id' => 6, 'qa_status !=' => 1, 'dispo_date >= ' => $date_from_query, 'dispo_date <= ' => $date_to_query);
	  }
	  
	  $data['date_from'] = $date_from;
	  $data['date_to'] = $date_to;
	  
      $client = 'askosca';
		if($client == 'askosca'){
			$table = 'campaign_leads';
			$table_id = 'id';
		}else{
			$table = 'tbl_leads';
			$table_id = 'lead_id';
		}
        $data['title'] = lang('all_leads');
        if (!empty($id)) {
            $data['active'] = 2;
            $can_edit = $this->items_model->can_action($table, 'edit', array('id' => $table_id));
            if (!empty($can_edit)) {
                $data['leads_info'] = $this->items_model->check_by(array('id' => $id), $table);
            }
        } else {
            $data['active'] = 1;
        }
        // get all leads status
        $status_info = $this->db->get('tbl_lead_status')->result();
        if (!empty($status_info)) {
            foreach ($status_info as $v_status) {
                $data['status_info'][$v_status->lead_type][] = $v_status;
            }
        }
        $data['assign_user'] = $this->items_model->allowad_user('55');
        $data['all_leads'] = $this->db->where($data['search_array'])->get('view_campaign_leads')->result();
        //$data['all_leads'] = $this->db->where(array('lead_status_id' => 6, 'qa_status !=' => 1, 'deleted' => 0))->get('view_campaign_leads')->result();
		
        $data['subview'] = $this->load->view('admin/leads/qa_leads', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }
	
	public function dba_leads($id = NULL)
    {
        
     if (!empty($this->input->post('daterange'))) {	
		  $daterange = explode('-',$this->input->post('daterange'));
		
	      $date_from = $daterange[0].'-'.$daterange[1].'-'.$daterange[2];
	      $date_to = $daterange[3].'-'.$daterange[4].'-'.$daterange[5];
		  
		  if($date_from < '2018-12-03 23:59:59') {
			  $data['search_array'] = array();
		  }else{
			  $data['search_array'] = array('qa_date >= ' => $date_from, 'qa_date <= ' => $date_to);
		  }
		  
	  } else {
		  $date_from = date('Y-m-d 00:00:00');
	      $date_to = date('Y-m-d 23:59:59');
	      $date_from_query = date('Y-m-d 00:00:00');
	      $date_to_query = date('Y-m-d 23:59:59');
		  $data['search_array'] = array('qa_date >= ' => $date_from_query, 'qa_date <= ' => $date_to_query);
	  }
	  
	  $data['date_from'] = $date_from;
	  $data['date_to'] = $date_to;
	  
      $client = 'askosca';
		if($client == 'askosca'){
			$table = 'view_campaign_leads';
			$table_id = 'id';
		}else{
			$table = 'tbl_leads';
			$table_id = 'lead_id';
		}
        $data['title'] = lang('all_leads');
        if (!empty($id)) {
            $data['active'] = 2;
            $can_edit = $this->items_model->can_action($table, 'edit', array('id' => $table_id));
            if (!empty($can_edit)) {
                $data['leads_info'] = $this->items_model->check_by(array('id' => $id), $table);
            }
        } else {
            $data['active'] = 1;
        }
        // get all leads status
        $status_info = $this->db->get('tbl_lead_status')->result();
        if (!empty($status_info)) {
            foreach ($status_info as $v_status) {
                $data['status_info'][$v_status->lead_type][] = $v_status;
            }
        }
        $data['assign_user'] = $this->items_model->allowad_user('55');
        //$data['all_leads'] = $this->db->where(array('qa_status' => 1))->get('view_campaign_leads')->result();
         $data['all_leads'] = $this->db->where($data['search_array'])->get('data_admin_queue')->result();
		
        $data['subview'] = $this->load->view('admin/leads/dba_leads', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }
    
		public function campaign_list($campaign_id = NULL)
    {
		if($this->session->userdata('user_type') == 4){
			redirect('admin/leads/index_leads');
		}
		
		// ini_set('display_errors',0);
       $client = 'askosca';
		if($client == 'askosca'){
			$table = 'lead_list';
			$table_id = 'id';
		}else{
			$table = 'lead_list';
			$table_id = 'id';
		}
        $data['title'] = lang('all_leads');
        if (!empty($id)) {
            $data['active'] = 2;
            $can_edit = $this->items_model->can_action($table, 'edit', array('id' => $table_id));
            if (!empty($can_edit)) {
                $data['leads_info'] = $this->items_model->check_by(array('campaign_id' => $campaign_id), $table);
            }
        } else {
            $data['active'] = 1;
        }
        // get all leads status
        $status_info = $this->db->get('tbl_lead_status')->result();
        if (!empty($status_info)) {
            foreach ($status_info as $v_status) {
                $data['status_info'][$v_status->lead_type][] = $v_status;
            }
        }
        $data['assign_user'] = $this->items_model->allowad_user('55');
        // $data['all_leads'] = $this->items_model->get_permission($table,'',20);
        $data['all_leads'] = $this->db->where('campaign_id',$campaign_id)->get($table)->result();
		// print_r($data);
		// exit;
        $data['subview'] = $this->load->view('admin/leads/list_leads', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }
    
    public function import_leads()
    {
        $data['title'] = lang('import_leads');
        $data['assign_user'] = $this->items_model->allowad_user('55');
        $data['templates'] = $this->db->get('list_template')->result();
        // get all leads status
        $status_info = $this->db->get('tbl_lead_status')->result();
        if (!empty($status_info)) {
            foreach ($status_info as $v_status) {
                $data['status_info'][$v_status->lead_type][] = $v_status;
            }
        }

        $data['subview'] = $this->load->view('admin/leads/import_leads', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

	public function save_imported()
    {
        $mappings = $this->db->where('template_id', $this->input->post('template_id'))->get('list_template_mapping')->result();
        $filename = $_FILES["upload_file"]["name"];
        //load the excel library
        $this->load->library('excel');
        ob_start();
        $file = $_FILES["upload_file"]["tmp_name"];
        if (!empty($file)) {
            $valid = false;
            $types = array('Excel2007', 'Excel5');
            foreach ($types as $type) {
                $reader = PHPExcel_IOFactory::createReader($type);
                if ($reader->canRead($file)) {
                    $valid = true;
                }
            }
            if (!empty($valid)) {
                try {
                    $objPHPExcel = PHPExcel_IOFactory::load($file);
                } catch (Exception $e) {
                    die("Error loading file :" . $e->getMessage());
                }
				$this->db->insert('lead_list', array('list_name' => $filename, 'campaign_id' => $this->input->post('campaign_id'), 'added' => date('Y-m-d h:i:s'), 'active' => 1, 'template_id' => $this->input->post('template_id')));
				$list_id = $this->db->insert_id();
				$data2 = [];
				
                //All data from excel
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                
                for ($x = 2; $x <= count($sheetData); $x++) {
                    // **********************
                    // Save Into leads table
                    // **********************
                    if($this->input->post('template_id') == 1){
        				if(!empty(trim($sheetData[$x]["A"])) && !empty(trim($sheetData[$x]["B"])) && !empty(trim($sheetData[$x]["C"])) && !empty(trim($sheetData[$x]["D"])) && !empty(trim($sheetData[$x]["E"])) && !empty(trim($sheetData[$x]["F"]))){
                            $data = $this->items_model->array_from_post(array('campaign_id'));
        					$data['filename'] = $filename;
        					$data['list_id'] = $list_id;
        					// $data['campaign_id'] = '12131';
                            $data['company_name'] = trim($sheetData[$x]["A"]);
                            $data['first_name'] = trim($sheetData[$x]["B"]);
                            $data['last_name'] = trim($sheetData[$x]["C"]);
                            $data['jobtitle'] = trim($sheetData[$x]["D"]);
                            $data['email'] = trim($sheetData[$x]["E"]);
                            $data['phone_number'] = trim($sheetData[$x]["F"]);
                            $data['address1'] = trim($sheetData[$x]["G"]);
                            $data['city'] = trim($sheetData[$x]["H"]);
                            $data['zip'] = trim($sheetData[$x]["I"]);
                            $data['country'] = trim($sheetData[$x]["J"]);
                            $data['company_size'] = trim($sheetData[$x]["K"]);
                            $data['industry_sector'] = trim($sheetData[$x]["L"]);
                            $data['time_called'] = trim($sheetData[$x]["M"]);
                            $data['comments'] = trim($sheetData[$x]["N"]);
                            $data['lead_source'] = trim($sheetData[$x]["O"]);
                          /*
                            $data['alternate_number'] = trim($sheetData[$x]["K"]);
                            $data['alternate_dm'] = trim($sheetData[$x]["L"]);
                            $data['alternate_project'] = trim($sheetData[$x]["M"]);
                            $data['outcome'] = trim($sheetData[$x]["N"]);
                            $data['comments'] = trim($sheetData[$x]["O"]);
                            $data['date_worked'] = trim($sheetData[$x]["P"]);
                            $data['time_worked'] = trim($sheetData[$x]["Q"]);
                            $data['esl'] = trim($sheetData[$x]["R"]);
                        */
                            $data['permission'] = 'all';
                            // save to tbl_leads
                            $this->items_model->_table_name = 'campaign_leads';
                            $this->items_model->_primary_key = 'id';
                            $this->items_model->save($data);
        				}
                    }else{
                    	foreach($mappings as $map){
        					$data2['filename'] = $filename;
        					$data2['campaign_id'] = $this->input->post('campaign_id');
        					$data2['list_id'] = $list_id;
        					$data2['permission'] = 'all';
        					$data2[$map->map_column] =  $sheetData[$x][$map->map_field];
        				}
        				$this->db->insert('campaign_leads', $data2);
                    }	
                }
                $type = 'success';
                $message = lang('save_leads');
            } else {
                $type = 'error';
                $message = "Sorry your uploaded file type not allowed ! please upload XLS/CSV File ";
            }
        } else {
            $type = 'error';
            $message = "You did not Select File! please upload XLS/CSV File ";
        }
		$this->create_vicidial_list($list_id);
		$this->send_leads($list_id);
         set_message($type, $message);
         redirect('admin/leads');
    }
	
    public function save_imported_old()
    {
        //load the excel library
        $this->load->library('excel');
        ob_start();
        $file = $_FILES["upload_file"]["tmp_name"];
        if (!empty($file)) {
            $valid = false;
            $types = array('Excel2007', 'Excel5');
            foreach ($types as $type) {
                $reader = PHPExcel_IOFactory::createReader($type);
                if ($reader->canRead($file)) {
                    $valid = true;
                }
            }
            if (!empty($valid)) {
                try {
                    $objPHPExcel = PHPExcel_IOFactory::load($file);
                } catch (Exception $e) {
                    die("Error loading file :" . $e->getMessage());
                }
                //All data from excel
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                for ($x = 2; $x <= count($sheetData); $x++) {
                    // **********************
                    // Save Into leads table
                    // **********************
                    $data = $this->items_model->array_from_post(array('client_id', 'lead_status_id', 'lead_source_id'));
                    $data['lead_name'] = trim($sheetData[$x]["A"]);
                    $data['organization'] = trim($sheetData[$x]["B"]);
                    $data['contact_name'] = trim($sheetData[$x]["C"]);
                    $data['email'] = trim($sheetData[$x]["D"]);
                    $data['phone'] = trim($sheetData[$x]["E"]);
                    $data['mobile'] = trim($sheetData[$x]["F"]);
                    $data['address'] = trim($sheetData[$x]["G"]);
                    $data['city'] = trim($sheetData[$x]["H"]);
                    $data['country'] = trim($sheetData[$x]["I"]);
                    $data['facebook'] = trim($sheetData[$x]["J"]);
                    $data['skype'] = trim($sheetData[$x]["K"]);
                    $data['twitter'] = trim($sheetData[$x]["L"]);
                    $data['notes'] = trim($sheetData[$x]["M"]);

                    $data['permission'] = 'all';
                    // save to tbl_leads
                    $this->items_model->_table_name = 'tbl_leads';
                    $this->items_model->_primary_key = 'leads_id';
                    $this->items_model->save($data);
                }
                $type = 'success';
                $message = lang('save_leads');
            } else {
                $type = 'error';
                $message = "Sorry your uploaded file type not allowed ! please upload XLS/CSV File ";
            }
        } else {
            $type = 'error';
            $message = "You did not Select File! please upload XLS/CSV File ";
        }
        set_message($type, $message);
        redirect('admin/leads');
    }

    public
    function saved_leads($list_id = null, $id = NULL)
    {
		 $client = 'askosca';
		if($client == 'askosca'){
			$table = 'campaign_leads';
			$table_id = 'id';
		}else{
			$table = 'tbl_leads';
			$table_id = 'lead_id';
		}
		
        $this->items_model->_table_name = $table;
        $this->items_model->_primary_key = $table_id;

       // $data = $this->items_model->array_from_post(array('company_name', 'first_name', 'last_name', 'jobtitle', 'email', 'phone_number', 'address1',
		//'city', 'zip', 'country', 'alternate_number', 'alternate_dm', 'alternate_project', 'outcome', 'comments', 'date_worked', 'time_worked','esl','lead_status_id'));
		
		$data = $this->items_model->array_from_post(array('company_name', 'first_name', 'last_name', 'jobtitle', 'email', 'phone_number', 'address1',
		'city', 'zip', 'country','comments','company_size','industry_sector','time_called','lead_status_id'));
		$data['updated'] = date('Y-m-d H:i:s');
		if($this->input->post('lead_status_id') == 6){
		    $data['dispo_date'] = date('Y-m-d H:i:s');
		}
        // update root category
        // $where = array('client_id' => $data['client_id'], 'lead_name' => $data['lead_name']);
        $where = array('company_name' => $data['company_name']);
        // duplicate value check in DB
        if (!empty($id)) { // if id exist in db update data
            $leads_id = array($table_id.' !=' => $id);
        } else { // if id is not exist then set id as null
            $leads_id = null;
        }

        // check whether this input data already exist or not
        $check_leads = $this->items_model->check_update($table, $where, $table_id);
        if (!empty($check_leads)) { // if input data already exist show error alert
            // massage for user
            $type = 'error';
            $msg = "<strong style='color:#000'>" . $data['company_name'] . '</strong>  ' . lang('already_exist');
        } else { // save and update query
            $permission = 'everyone'; //$this->input->post('permission', true);
            if (!empty($permission)) {

                if ($permission == 'everyone') {
                    $assigned = 'all';
                } else {
                    $assigned_to = $this->items_model->array_from_post(array('assigned_to'));
                    if (!empty($assigned_to['assigned_to'])) {
                        foreach ($assigned_to['assigned_to'] as $assign_user) {
                            $assigned[$assign_user] = $this->input->post('action_' . $assign_user, true);
                        }
                    }
                }
                if ($assigned != 'all') {
                    $assigned = json_encode($assigned);
                }
                $data['permission'] = $assigned;
            } else {
                set_message('error', lang('assigned_to') . ' Field is required');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $data['user'] = $this->session->userdata('user_id');
            $return_id = $this->items_model->save($data, $id);
            if (!empty($id)) {
                $id = $id;
                $action = 'activity_update_leads';
                $msg = lang('update_leads');
            } else {
                $id = $return_id;
                $action = 'activity_save_leads';
                $msg = lang('save_leads');
            }
            save_custom_field(5, $id);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'leads',
                'module_field_id' => $id,
                'activity' => $action,
                'icon' => 'fa-circle-o',
                'value1' => $data['lead_name']
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);
            // messages for user
            $type = "success";
        }
        $message = $msg;
        set_message($type, $message);
        redirect('admin/leads/index_leads/'.$list_id);
    }
    
    	public
    function saved_leads_template($list_id = null, $id = NULL)
    {
		 $client = 'askosca';
		if($client == 'askosca'){
			$table = 'campaign_leads';
			$table_id = 'id';
		}else{
			$table = 'tbl_leads';
			$table_id = 'lead_id';
		}
		
        $this->items_model->_table_name = $table;
        $this->items_model->_primary_key = $table_id;

        $data =  $this->items_model->array_from_post(array('title', 
    'gender',  
    'first_name',  
    'last_name',  
    'date_of_birth',  
    'age', 
    'effective_date', 
    'policy_number',  
    'policy_status',  
    'policy_status_per_history',  
    'broker_channel',  
    'product', 
    'inception_date',  
    'relationship',  
    'email',
    'id_number',  
    'work_number',  
    'cell_number',  
    'address1',  
    'address2',  
    'address3',  
    'address4',  
    'postal_code',  
    'agent_code',  
    'payment_method',  
    'deduction_day',  
    'debit_order_account_holder',  
    'debit_order_account_number',  
    'debit_order_bank_name',  
    'debit_order_branch_code',  
    'debit_order_account_type',  
    'sum_assured_amount',  
    'total_premium',  
    'cover_amount',  
    'total_cover',  
    'lead_status_id', 
    'comments'));
	
		$data['updated'] = date('Y-m-d H:i:s');
        // update root category
        // $where = array('client_id' => $data['client_id'], 'lead_name' => $data['lead_name']);
        $where = array('first_name' => $data['first_name'], 'email' => $data['email']);
        // duplicate value check in DB
        if (!empty($id)) { // if id exist in db update data
            $leads_id = array($table_id.' !=' => $id);
        } else { // if id is not exist then set id as null
            $leads_id = null;
        }

        // check whether this input data already exist or not
        $check_leads = $this->items_model->check_update($table, $where, $table_id);
        if (!empty($check_leads)) { // if input data already exist show error alert
            // massage for user
            $type = 'error';
            $msg = "<strong style='color:#000'>" . $data['company_name'] . '</strong>  ' . lang('already_exist');
        } else { // save and update query
            $permission = $this->input->post('permission', true);
            if (!empty($permission)) {

                if ($permission == 'everyone') {
                    $assigned = 'all';
                } else {
                    $assigned_to = $this->items_model->array_from_post(array('assigned_to'));
                    if (!empty($assigned_to['assigned_to'])) {
                        foreach ($assigned_to['assigned_to'] as $assign_user) {
                            $assigned[$assign_user] = $this->input->post('action_' . $assign_user, true);
                        }
                    }
                }
                if ($assigned != 'all') {
                    $assigned = json_encode($assigned);
                }
                $data['permission'] = $assigned;
            } else {
                set_message('error', lang('assigned_to') . ' Field is required');
                redirect($_SERVER['HTTP_REFERER']);
            }
			$data['user'] = $this->session->userdata('user_id');
            $return_id = $this->items_model->save($data, $id);
            if (!empty($id)) {
                $id = $id;
                $action = 'activity_update_leads';
                $msg = lang('update_leads');
            } else {
                $id = $return_id;
                $action = 'activity_save_leads';
                $msg = lang('save_leads');
            }
            save_custom_field(5, $id);
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'leads',
                'module_field_id' => $id,
                'activity' => $action,
                'icon' => 'fa-circle-o',
                'value1' => $data['email']
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);
            // messages for user
            $type = "success";
        }
        $message = $msg;
        set_message($type, $message);
        // redirect('admin/leads/index_leads/'.$list_id);
		redirect($_SERVER['HTTP_REFERER']);
    }
    
    public
    function leads_details($id, $active = NULL, $op_id = NULL)
    {
        $data['title'] = lang('leads_details');
        //get all task information
        $data['leads_details'] = $this->items_model->check_by(array('leads_id' => $id), 'tbl_leads');

        $this->items_model->_table_name = "tbl_task_attachment"; //table name
        $this->items_model->_order_by = "leads_id";
        $data['files_info'] = $this->items_model->get_by(array('leads_id' => $id), FALSE);

        foreach ($data['files_info'] as $key => $v_files) {
            $this->items_model->_table_name = "tbl_task_uploaded_files"; //table name
            $this->items_model->_order_by = "task_attachment_id";
            $data['project_files_info'][$key] = $this->items_model->get_by(array('task_attachment_id' => $v_files->task_attachment_id), FALSE);
        }

        if ($active == 2) {
            $data['active'] = 2;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 1;
        } elseif ($active == 3) {
            $data['active'] = 3;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 1;
        } elseif ($active == 4) {
            $data['active'] = 4;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 1;
        } elseif ($active == 5) {
            $data['active'] = 5;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 1;
        } elseif ($active == 'metting') {
            $data['active'] = 3;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 2;
            $data['mettings_info'] = $this->items_model->check_by(array('mettings_id' => $op_id), 'tbl_mettings');
        } elseif ($active == 'call') {
            $data['active'] = 2;
            $data['sub_active'] = 2;
            $data['call_info'] = $this->items_model->check_by(array('calls_id' => $op_id), 'tbl_calls');
            $data['sub_metting'] = 1;
        } else {
            $data['active'] = 1;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 1;
        }

        $data['subview'] = $this->load->view('admin/leads/leads_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public
    function convert($id)
    {
        $data['title'] = lang('convert_to_client'); //Page title
        $data['person'] = 1;
        // get all country
        $this->items_model->_table_name = "tbl_countries"; //table name
        $this->items_model->_order_by = "id";
        $data['countries'] = $this->items_model->get();

        // get all currencies
        $this->items_model->_table_name = 'tbl_currencies';
        $this->items_model->_order_by = 'name';
        $data['currencies'] = $this->items_model->get();
        // get all language
        $this->items_model->_table_name = 'tbl_languages';
        $this->items_model->_order_by = 'name';
        $data['languages'] = $this->items_model->get();

        $data['leads_info'] = $this->items_model->check_by(array('leads_id' => $id), 'tbl_leads');
        $data['modal_subview'] = $this->load->view('admin/leads/_modal_convert', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public
    function converted($leads_id)
    {
        $data = $this->items_model->array_from_post(array('name', 'email', 'short_note', 'website', 'phone', 'mobile', 'fax', 'address', 'city', 'zipcode', 'currency',
            'skype_id', 'linkedin', 'facebook', 'twitter', 'language', 'country', 'vat', 'hosting_company', 'hostname', 'port', 'password', 'username', 'client_status'));
        if (!empty($_FILES['profile_photo']['name'])) {
            $val = $this->items_model->uploadImage('profile_photo');
            $val == TRUE || redirect('admin/client/manage_client');
            $data['profile_photo'] = $val['path'];
        }
        $data['leads_id'] = $leads_id;

        $this->items_model->_table_name = 'tbl_client';
        $this->items_model->_primary_key = "client_id";
        $return_id = $this->items_model->save($data);
        // update to tbl_leads
        $u_data['converted_client_id'] = $return_id;
        $this->items_model->_table_name = 'tbl_leads';
        $this->items_model->_primary_key = "leads_id";
        $this->items_model->save($u_data, $leads_id);
        $action = ('activity_convert_to_client');
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'client',
            'module_field_id' => $return_id,
            'activity' => $action,
            'icon' => 'fa-user',
            'value1' => $data['name']
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = "activities_id";
        $this->items_model->save($activities);
        // messages for user
        $type = "success";
        $message = lang('convert_to_client_suucess');
        set_message($type, $message);
        redirect('admin/client/client_details/' . $return_id);
    }

    public
    function update_users($id)
    {
        // get all assign_user
        $can_edit = $this->items_model->can_action('tbl_leads', 'edit', array('leads_id' => $id));
        if (!empty($can_edit)) {
            // get permission user by menu id
            $data['assign_user'] = $this->items_model->allowad_user('55');

            $data['leads_info'] = $this->items_model->check_by(array('leads_id' => $id), 'tbl_leads');
            $data['modal_subview'] = $this->load->view('admin/leads/_modal_users', $data, FALSE);
            $this->load->view('admin/_layout_modal', $data);
        } else {
            set_message('error', lang('there_in_no_value'));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public
    function update_member($id)
    {
        $can_edit = $this->items_model->can_action('tbl_leads', 'edit', array('leads_id' => $id));
        if (!empty($can_edit)) {
            $leads_info = $this->items_model->check_by(array('leads_id' => $id), 'tbl_leads');

            $permission = $this->input->post('permission', true);
            if (!empty($permission)) {

                if ($permission == 'everyone') {
                    $assigned = 'all';
                } else {
                    $assigned_to = $this->items_model->array_from_post(array('assigned_to'));
                    if (!empty($assigned_to['assigned_to'])) {
                        foreach ($assigned_to['assigned_to'] as $assign_user) {
                            $assigned[$assign_user] = $this->input->post('action_' . $assign_user, true);
                        }
                    }
                }
                if ($assigned != 'all') {
                    $assigned = json_encode($assigned);
                }
                $data['permission'] = $assigned;
            } else {
                set_message('error', lang('assigned_to') . ' Field is required');
                redirect($_SERVER['HTTP_REFERER']);
            }

//save data into table.
            $this->items_model->_table_name = "tbl_leads"; // table name
            $this->items_model->_primary_key = "leads_id"; // $id
            $this->items_model->save($data, $id);

            $msg = lang('update_leads');
            $activity = 'activity_update_leads';

// save into activities
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'leads',
                'module_field_id' => $id,
                'activity' => $activity,
                'icon' => 'fa-ticket',
                'value1' => $leads_info->lead_name,
            );
// Update into tbl_project
            $this->items_model->_table_name = "tbl_activities"; //table name
            $this->items_model->_primary_key = "activities_id";
            $this->items_model->save($activities);

            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            set_message('error', lang('there_in_no_value'));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public
    function change_status($leads_id, $lead_status_id)
    {
        $can_edit = $this->items_model->can_action('tbl_leads', 'edit', array('leads_id' => $leads_id));
        if (!empty($can_edit)) {
            $data['lead_status_id'] = $lead_status_id;
            $this->items_model->_table_name = 'tbl_leads';
            $this->items_model->_primary_key = 'leads_id';
            $this->items_model->save($data, $leads_id);
            
            if($lead_status_id == 9){
                $this->delete_lead_company($leads_id);
            }
            // messages for user
            $type = "success";
            $message = lang('change_status');
            set_message($type, $message);
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            set_message('error', lang('there_in_no_value'));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public
    function saved_call($leads_id, $id = NULL)
    {
        $data = $this->items_model->array_from_post(array('date', 'call_summary', 'client_id', 'user_id'));
        $data['leads_id'] = $leads_id;
        $this->items_model->_table_name = 'tbl_calls';
        $this->items_model->_primary_key = 'calls_id';
        $return_id = $this->items_model->save($data, $id);
        if (!empty($id)) {
            $id = $id;
            $action = 'activity_update_leads_call';
            $msg = lang('update_leads_call');
        } else {
            $id = $return_id;
            $action = 'activity_save_leads_call';
            $msg = lang('save_leads_call');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $leads_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $data['call_summary']
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        // messages for user
        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '2');
    }

    public
    function delete_leads_call($leads_id, $id)
    {
        $calls_info = $this->items_model->check_by(array('calls_id' => $id), 'tbl_calls');
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $leads_id,
            'activity' => 'activity_leads_call_deleted',
            'icon' => 'fa-circle-o',
            'value1' => $calls_info->call_summary
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        $this->items_model->_table_name = 'tbl_calls';
        $this->items_model->_primary_key = 'calls_id';
        $this->items_model->delete($id);
        $type = 'success';
        $message = lang('leads_call_deleted');
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '2');
    }

    public
    function delete_leads_mettings($leads_id, $id)
    {
        $mettings_info = $this->items_model->check_by(array('mettings_id' => $id), 'tbl_mettings');

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $leads_id,
            'activity' => 'activity_leads_call_deleted',
            'icon' => 'fa-circle-o',
            'value1' => $mettings_info->meeting_subject
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        $this->items_model->_table_name = 'tbl_mettings';
        $this->items_model->_primary_key = 'mettings_id';
        $this->items_model->delete($id);
        $type = 'success';
        $message = lang('leads_mettings_deleted');
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '3');
    }

    public
    function saved_metting($leads_id, $id = NULL)
    {
        $this->items_model->_table_name = 'tbl_mettings';
        $this->items_model->_primary_key = 'mettings_id';

        $data = $this->items_model->array_from_post(array('meeting_subject', 'user_id', 'location', 'description'));
        $data['start_date'] = strtotime($this->input->post('start_date') . ' ' . date('H:i', strtotime($this->input->post('start_time'))));
        $data['end_date'] = strtotime($this->input->post('end_date') . ' ' . date('H:i', strtotime($this->input->post('end_time'))));
        $data['leads_id'] = $leads_id;
        $user_id = serialize($this->items_model->array_from_post(array('attendees')));
        if (!empty($user_id)) {
            $data['attendees'] = $user_id;
        } else {
            $data['attendees'] = '-';
        }
        $return_id = $this->items_model->save($data, $id);

        if (!empty($id)) {
            $id = $id;
            $action = 'activity_update_leads_metting';
            $msg = lang('update_leads_metting');
        } else {
            $id = $return_id;
            $action = 'activity_save_leads_metting';
            $msg = lang('save_leads_metting');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $leads_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $data['meeting_subject']
        );

        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        // messages for user
        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '3');
    }

    public
    function save_comments()
    {

        $data['leads_id'] = $this->input->post('leads_id', TRUE);
        $data['comment'] = $this->input->post('comment', TRUE);
        $data['user_id'] = $this->session->userdata('user_id');

        //save data into table.
        $this->items_model->_table_name = "tbl_task_comment"; // table name
        $this->items_model->_primary_key = "task_comment_id"; // $id
        $this->items_model->save($data);

        // save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $data['leads_id'],
            'activity' => 'activity_new_leads_comment',
            'icon' => 'fa-ticket',
            'value1' => $data['comment'],
        );
        // Update into tbl_project
        $this->items_model->_table_name = "tbl_activities"; //table name
        $this->items_model->_primary_key = "activities_id";
        $this->items_model->save($activities);


        $type = "success";
        $message = lang('leads_comment_save');
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $data['leads_id'] . '/' . '4');
    }

    public
    function delete_comments($leads_id, $task_comment_id)
    {
        //save data into table.
        $this->items_model->_table_name = "tbl_task_comment"; // table name
        $this->items_model->_primary_key = "task_comment_id"; // $id
        $this->items_model->delete($task_comment_id);

        $type = "success";
        $message = lang('task_comment_deleted');
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '4');
    }

    public
    function save_attachment($task_attachment_id = NULL)
    {
        $data = $this->items_model->array_from_post(array('title', 'description', 'leads_id'));
        $data['user_id'] = $this->session->userdata('user_id');

        // save and update into tbl_files
        $this->items_model->_table_name = "tbl_task_attachment"; //table name
        $this->items_model->_primary_key = "task_attachment_id";
        if (!empty($task_attachment_id)) {
            $id = $task_attachment_id;
            $this->items_model->save($data, $id);
            $msg = lang('leads_file_updated');
        } else {
            $id = $this->items_model->save($data);
            $msg = lang('leads_file_added');
        }

        if (!empty($_FILES['task_files']['name']['0'])) {
            $old_path_info = $this->input->post('uploaded_path');
            if (!empty($old_path_info)) {
                foreach ($old_path_info as $old_path) {
                    unlink($old_path);
                }
            }
            $mul_val = $this->items_model->multi_uploadAllType('task_files');

            foreach ($mul_val as $val) {
                $val == TRUE || redirect('admin/leads/leads_details/' . $data['leads_id'] . '/' . '5');
                $fdata['files'] = $val['path'];
                $fdata['file_name'] = $val['fileName'];
                $fdata['uploaded_path'] = $val['fullPath'];
                $fdata['size'] = $val['size'];
                $fdata['ext'] = $val['ext'];
                $fdata['is_image'] = $val['is_image'];
                $fdata['image_width'] = $val['image_width'];
                $fdata['image_height'] = $val['image_height'];
                $fdata['task_attachment_id'] = $id;
                $this->items_model->_table_name = "tbl_task_uploaded_files"; // table name
                $this->items_model->_primary_key = "uploaded_files_id"; // $id
                $this->items_model->save($fdata);
            }
        }
        // save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $data['leads_id'],
            'activity' => 'activity_new_leads_attachment',
            'icon' => 'fa-ticket',
            'value1' => $data['title'],
        );
        // Update into tbl_project
        $this->items_model->_table_name = "tbl_activities"; //table name
        $this->items_model->_primary_key = "activities_id";
        $this->items_model->save($activities);
        // messages for user
        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $data['leads_id'] . '/' . '5');
    }

    public
    function delete_files($leads_id, $task_attachment_id)
    {
        $file_info = $this->items_model->check_by(array('task_attachment_id' => $task_attachment_id), 'tbl_task_attachment');
        // save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $leads_id,
            'activity' => 'activity_leads_attachfile_deleted',
            'icon' => 'fa-ticket',
            'value1' => $file_info->title,
        );
        // Update into tbl_project
        $this->items_model->_table_name = "tbl_activities"; //table name
        $this->items_model->_primary_key = "activities_id";
        $this->items_model->save($activities);

        //save data into table.
        $this->items_model->_table_name = "tbl_task_attachment"; // table name
        $this->items_model->delete_multiple(array('task_attachment_id' => $task_attachment_id));

        $type = "success";
        $message = lang('leads_attachfile_deleted');
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '5');
    }

    public
    function delete_leads($id)
    {
			 $client = 'askosca';
		if($client == 'askosca'){
			$table = 'campaign_leads';
			$table_id = 'id';
		}else{
			$table = 'tbl_leads';
			$table_id = 'lead_id';
		}
		
        $can_delete = $this->items_model->can_action('campaign_leads', 'delete', array('id' => $id));
        if (!empty($can_delete)) {
            $leads_info = $this->items_model->check_by(array('id' => $id), 'campaign_leads');
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'leads',
                'module_field_id' => $id,
                'activity' => 'activity_leads_deleted',
                'icon' => 'fa-circle-o',
                'value1' => $leads_info->lead_name
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);

         

            $this->items_model->_table_name = 'campaign_leads';
            $this->items_model->_primary_key = 'id';
            $this->items_model->delete($id);

            $type = 'success';
            $message = lang('leads_deleted');
            set_message($type, $message);
        } else {
            set_message('error', lang('there_in_no_value'));
        }

        redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function delete_list($id){
        $this->items_model->_table_name = 'lead_list';
        $this->items_model->_primary_key = 'id';
        $this->items_model->delete($id);
        
        if($id > 0){
            $this->db->where('list_id', $id)->delete('campaign_leads');
        }
        
        $activity = array(
		'user' => $this->session->userdata('user_id'),
		'module' => 'lead_list',
		'module_field_id' => $id,
		'activity' => 'activity_list_deleted',
		'icon' => 'fa-circle-o',
		'value1' => 'list '.$id.' has been deleted'
    	);
    	$this->items_model->_table_name = 'tbl_activities';
    	$this->items_model->_primary_key = 'activities_id';
    	$this->items_model->save($activity);
				
        $this->delete_vicidial_list($id);
        
        $type = 'success';
        $message = 'list has been deleted.';
        set_message($type, $message);
        redirect($_SERVER['HTTP_REFERER']);
    }
    
    
	public function create_vicidial_list($list_id) {
		
		$data = $this->db->where('id', $list_id)->get('lead_list')->row();
		$campaign_id = $this->db->where('id',$data->campaign_id)->get('tbl_campaigns')->row('campaign_id');
		// print_r($data);
		// print_r($campaign_id);
		if (!empty($list_id))
		{
			$server = config_item('vicidial_server'); //'156.38.193.158';
			$api_user = '6665';
			$api_pass = '159357pocALOCapi';
			
			if($list_id < 10){
				$list_id = '0'.$list_id;
			}

			$listname = str_replace(" ","_", str_replace("-","",str_replace(" ", "+",$data->list_name)));
			$list_name = substr($listname, 0 , 29);
// 			$listname1 = substr(); 
// 			$campaign_id = str_replace(" ", "+",$campaign_id);
			// echo $list_name;
			$url ="http://$server/vicidial/non_agent_api.php?source=test&function=add_list&user=$api_user&pass=$api_pass&list_id=$list_id&list_name={$list_name}&campaign_id={$campaign_id}&active=Y&local_call_time=12pm-5pm&";
					
// 			echo $url;
			$this->send_to_vicidial($url);
			
			$url2 ="http://$server/vicidial/non_agent_api.php?source=test&function=update_list&user=$api_user&pass=$api_pass&list_id=$list_id&list_name={$list_name}&campaign_id={$campaign_id}&active=Y&reset_list=Y";
			$this->send_to_vicidial($url2);
			
			// webform
			$url3 = "http://$server/vicidial/non_agent_api.php?source=crm&user=$api_user&pass=$api_pass&function=update_list&list_id={$list_id}&campaign_id=".$campaign_id."&web_form_address=VAR".base_url('admin/campaigns/campaign_questions/'.$data->campaign_id)."?entry_list_id=--A--vendor_lead_code--B--";
			$this->send_to_vicidial($url3);
		}
	}
	
	public function send_leads($list_id)
	{
		//$leads = $this->db->where('list_id',$list_id)->limit(500)->get('campaign_leads')->result();
		$leads = $this->db->where('list_id',$list_id)->limit(15000)->get('campaign_leads')->result();
	    if($leads){
	        $campaign_id = $this->db->where('id', $leads[0]->campaign_id)->get('tbl_campaigns')->row('campaign_id');
	    }
		$server = config_item('vicidial_server'); //'156.38.193.158';
		$api_user = '6665';
		$api_pass = '159357pocALOCapi';
		
			// if($list_id < 10){
				// $list_id = '0'.$list_id;
			// }
			
		foreach ($leads as $lead) {
		  //if(!empty($lead->company_name) && !empty($lead->first_name) && !empty($lead->last_name) && !empty($lead->phone_number)){
		if(!empty($lead->first_name) && !empty($lead->last_name)){
		//$phone_number = substr($lead->phone_number,0);
		$phone_number = str_replace(' ','', str_replace('-','',$lead->phone_number?$lead->phone_number:$lead->cell_number));
		$first_name = str_replace(' ', '+',$lead->first_name);
		$last_name = str_replace(' ', '+',$lead->last_name);
		$address1 = str_replace(' ', '+',$lead->address1);
		$city = str_replace(' ', '+',$lead->city?$lead->city:$lead->address2);
		$email = str_replace(' ', '+',$lead->email);
		$url = "http://$server/vicidial/non_agent_api.php?source=test&user=$api_user&pass=$api_pass&function=add_lead&phone_number={$phone_number}&list_id=$list_id&first_name={$first_name}&last_name={$last_name}&address1={$address1}&city={$city}&email={$email}&vendor_lead_code={$lead->id}&add_to_hopper=Y";
		// echo $url."\n";
		//print_r($lead);
	    $this->send_to_vicidial($url);
		  }
		}
		
		$url2 = "http://$server/vicidial/non_agent_api.php?source=testt&user=$api_user&pass=$api_pass&function=update_campaign&campaign_id=$campaign_id&reset_hopper=Y&hopper_level=1000";
		$this->send_to_vicidial($url2);
	}
	
	public function delete_vicidial_lead($lead_id){
    	$server = config_item('vicidial_server'); //'156.38.193.158';
		$api_user = '6665';
		$api_pass = '159357pocALOCapi';
		
    	$url = "http://$server/vicidial/non_agent_api.php?source=test&user=$api_user&pass=$api_pass&function=update_lead&delete_lead=Y&vendor_lead_code=$lead_id";
    	$this->send_to_vicidial($url);
	}
	
	public function send_leads_offset($list_id, $offset = 12000)
	{
        exit;
		$leads = $this->db->where('list_id',$list_id)->limit(2000, $offset)->get('campaign_leads')->result();
      //  print_r($leads); exit;
	    if($leads){
	        $campaign_id = $this->db->where('id', $leads[0]->campaign_id)->get('tbl_campaigns')->row('campaign_id');
	    }
		$server = config_item('vicidial_server'); //'156.38.193.158';
		$api_user = '6665';
		$api_pass = '159357pocALOCapi';
		
//	$total_leads = 0;
			
		foreach ($leads as $lead) {
		  if(!empty($lead->company_name) && !empty($lead->first_name) && !empty($lead->last_name) && !empty($lead->phone_number)){
		//$phone_number = substr($lead->phone_number,0);
		$phone_number = str_replace(' ','', str_replace('-','',$lead->phone_number));
		$first_name = str_replace(' ', '+',$lead->first_name);
		$last_name = str_replace(' ', '+',$lead->last_name);
		$address1 = str_replace(' ', '+',$lead->address1);
		$city = str_replace(' ', '+',$lead->city);
		$email = str_replace(' ', '+',$lead->email);
		$url = "http://$server/vicidial/non_agent_api.php?source=test&user=$api_user&pass=$api_pass&function=add_lead&phone_number={$phone_number}&list_id=$list_id&first_name={$first_name}&last_name={$last_name}&address1={$address1}&city={$city}&email={$email}&vendor_lead_code={$lead->id}&add_to_hopper=Y";
	
	    $this->send_to_vicidial($url);
	  //  $total_leads++;
		  }
		  
		
		}
	//	echo $total_leads;
	//	$url2 = "http://$server/vicidial/non_agent_api.php?source=testt&user=$api_user&pass=$api_pass&function=update_campaign&campaign_id=$campaign_id&reset_hopper=N&hopper_level=1000";
	//	$this->send_to_vicidial($url2);
	}
	
	public function send_to_vicidial($url){
		$ch = curl_init(); 
	
        // set url 
        curl_setopt($ch, CURLOPT_URL, $url); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch); 
		
		print_r($output);
	}
	
	public function export_leads($list_id){
	    //ini_set('memory_limit','1024M');
		$this->load->library('excel');
		//$data = $this->db->where('list_id',$list_id)->limit(1000)->get('campaign_leads')->result();
		
		$template_id = $this->db->where('id', $list_id)->get('lead_list')->row('template_id');
		if($template_id == 2){
		$headers = $this->db->select("id, title, effective_date, policy_number, broker_channel,product,inception_date,policy_status,policy_status_per_history, relationship,id_number,date_of_birth,first_name,last_name,age,gender,work_number ,cell_number ,address1,address2 ,address3 ,address4 ,postal_code ,payment_method,agent_code ,deduction_day ,debit_order_account_holder ,debit_order_bank_name,debit_order_account_number ,debit_order_branch_code ,debit_order_account_type ,sum_assured_amount ,total_premium,cover_amount,total_cover,referral_lead_id,campaign_id,campaign_name,list_id,username,lead_status,lead_source,updated")->where(array('list_id' => $list_id, 'deleted' => 0))->limit(1)->get('view_campaign_leads_loa')->result();
		$data = $this->db->select("id, title, effective_date, policy_number, broker_channel,product,inception_date,policy_status,policy_status_per_history, relationship,id_number,date_of_birth,first_name,last_name,age,gender,work_number ,cell_number ,address1,address2 ,address3 ,address4 ,postal_code ,payment_method,agent_code ,deduction_day ,debit_order_account_holder ,debit_order_bank_name,debit_order_account_number ,debit_order_branch_code ,debit_order_account_type ,sum_assured_amount ,total_premium,cover_amount,total_cover,referral_lead_id,campaign_id,campaign_name,list_id,username,lead_status,lead_source,updated")->where(array('list_id' => $list_id, 'deleted' => 0))->get('view_campaign_leads_loa')->result();
		}else{
		$headers = $this->db->select("id, referral_lead_id, campaign_id, campaign_name, list_id, username, lead_status, lead_source, updated, company_name, first_name, last_name, jobtitle, email, phone_number, address1, city, zip, country, company_size, industry_sector, time_called, comments")->where(array('list_id' => $list_id, 'deleted' => 0))->limit(1)->get('view_campaign_leads')->result();
        $data = $this->db->select("id, referral_lead_id, campaign_id, campaign_name, list_id, username, lead_status, lead_source, updated, company_name, first_name, last_name, jobtitle, email, phone_number, address1, city, zip, country, company_size, industry_sector, time_called, comments")->where(array('list_id' => $list_id, 'deleted' => 0))->get('view_campaign_leads')->result();
		}
		
		if(!empty($data)){
		    $campaign_id = $data[0]->campaign_id;
			$campaign_questions = $this->db->where('campaign_id',$data[0]->campaign_id)->get('campaign_questions')->result();
		}
		
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0);
		
		// HEADINGS
		$rowCount = 1; 
		foreach ($headers as $dat) {
			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$d);	
				$char++;
				$total_chars++;
			}
		$rowCount++;
		break;
		}
		
		$rowCount2 = 1;
		$total_char = 'A';
		for($n = 0; $n <= $total_chars; $n++){
			$total_char++;
		}
		
		if(!empty($campaign_questions)){
			foreach($campaign_questions as $q){				
				#$cdata[$q->question_question] = $a->answer ;
				$objPHPExcel->getActiveSheet()->SetCellValue($total_char.$rowCount2,$q->question_question);	
				$total_char++;
				unset($q);
			}
			$rowCount2++;
		}
		unset($campaign_questions);
		$campaign_questions = null;
					
		
	    // DATA	
	
		$rowCount = 2; 
		foreach ($data as $dat) {
			$campaign_answers[] = $this->db->where(array('lead_id' => $dat->id,'campaign_id' => $campaign_id))->get('campaign_answers')->result();
    	//	$status_info = $this->db->where('lead_status_id', $dat->lead_status_id)->get('tbl_lead_status')->row('lead_status');
			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
		 //   	if($d == 'lead_status_id'){ $v = $status_info; }
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$v);	
				$char++;
				$total_chars++;
			}
		$rowCount++;
		unset($dat);
		}
			
	
		$total_char = 'A';
	//	if(!isset($total_chars)){}
		for($n = 0; $n <= $total_chars; $n++){
			$total_char++;
		}
        $total_answers = count($campaign_answers);
        /*
        if(!empty($campaign_questions)){
			foreach($campaign_questions as $q){
			    if(!empty($q)){
			       
    				foreach($campaign_answers as $a){
    					foreach($a as $answ){
    						if($q->id == $answ->question_id){
    							$objPHPExcel->getActiveSheet()->SetCellValue($total_char.$rowCount2,$answ->answer);	
    							$total_char++;
    							if(($total_answers % 4) == 0){
    						//	 $rowCount2++;
    							}
    						}
    					}
    				}
			    }
			}
		}
		*/
		
	
			       
    				foreach($campaign_answers as $a){
    				    $ntotal_char = $total_char;
    				    $total_qa = 0;
    					foreach($a as $answ){
    							$objPHPExcel->getActiveSheet()->SetCellValue($ntotal_char.$rowCount2,$answ->answer);	
    					        	$ntotal_char++;	
    						//	if(($total_answers % 4) == 0){
    						    
    						//	}
    					}
    						 $rowCount2++;
    						 unset($a);
    				}
			    unset($campaign_answers);	
		
//exit;
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=list_$list_id.xlsx");
		header("Content-Transfer-Encoding: binary ");

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('php://output');	
	}
	
	public function export_lead($lead_id){
		$this->load->library('excel');
	$data = $this->db->select("id, campaign_id, campaign_name, list_id, username, lead_status, lead_source, updated, qa_status, da_status_export, company_name, first_name, last_name, jobtitle, email, phone_number, address1, city, zip, country, company_size, industry_sector, time_called, comments")->where('id',$lead_id)->limit(1)->get('view_campaign_leads')->result();
		
		if(!empty($data)){
		    $campaign_id = $data[0]->campaign_id;
			$campaign_questions = $this->db->where('campaign_id',$data[0]->campaign_id)->get('campaign_questions')->result();
		}
		// print_r($data); exit;
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0);
		
		// HEADINGS
		$rowCount = 1; 
		foreach ($data as $dat) {
			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$d);	
				$char++;
				$total_chars++;
			}
		$rowCount++;
		break;
		}
		
		$rowCount2 = 1;
		$total_char = 'A';
		for($n = 0; $n <= $total_chars; $n++){
			$total_char++;
		}
		
		if(!empty($campaign_questions)){
			foreach($campaign_questions as $q){				
				#$cdata[$q->question_question] = $a->answer ;
				$objPHPExcel->getActiveSheet()->SetCellValue($total_char.$rowCount2,$q->question_question);	
				$total_char++;
			}
			$rowCount2++;
		}
					
					
		
	    // DATA	
	
		$rowCount = 2; 
		foreach ($data as $dat) {
			$lead_status = '';
			$campaign_answers[] = $this->db->where(array('lead_id' => $dat->id,'campaign_id' => $campaign_id))->get('campaign_answers')->result();
			// $status_info = $this->db->where('lead_status_id', $dat->lead_status_id)->get('tbl_lead_status')->row('lead_status');
		
			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				if($d == 'qa_status'){ if($v == 1){ $v = 'Approved'; } if($v == 2){ $v = 'Pending'; } if($v == 3){ $v = 'Declined'; } }
				if($d == 'da_status_export'){ if($v == 1){ $v = 'Successful'; } if($v == 2){ $v = 'Bad Email'; } if($v == 3){ $v = 'Duplicate'; } }
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$v);	
				$char++;
				$total_chars++;
			}
		$rowCount++;
		}
			
		
		$total_char = 'A';
		for($n = 0; $n <= $total_chars; $n++){
			$total_char++;
		}
        $total_answers = count($campaign_answers);
        /*
        if(!empty($campaign_questions)){
			foreach($campaign_questions as $q){
			    if(!empty($q)){
			       
    				foreach($campaign_answers as $a){
    					foreach($a as $answ){
    						if($q->id == $answ->question_id){
    							$objPHPExcel->getActiveSheet()->SetCellValue($total_char.$rowCount2,$answ->answer);	
    							$total_char++;
    							if(($total_answers % 4) == 0){
    						//	 $rowCount2++;
    							}
    						}
    					}
    				}
			    }
			}
		}
		*/
		
	
			       
    				foreach($campaign_answers as $a){
    				    $ntotal_char = $total_char;
    				    $total_qa = 0;
    					foreach($a as $answ){
									$objPHPExcel->getActiveSheet()->SetCellValue($ntotal_char.$rowCount2,$answ->answer);	
    					        	$ntotal_char++;	
    						//	if(($total_answers % 4) == 0){
    						    
    						//	}
    					}
    						 $rowCount2++;
    				}
			    	
		
//exit;
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=list_$lead_id.xlsx");
		header("Content-Transfer-Encoding: binary ");

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('php://output');
	}
	
	 public function disable_list($id){
        $this->db->where('id',$id)->update('lead_list', array('active' => '0'));
        
        if($id > 0){
			$this->disable_vicidial_list($id, 'N');
        }
        
		$activity = array(
					'user' => $this->session->userdata('user_id'),
					'module' => 'lead_list',
					'module_field_id' => $id,
					'activity' => 'activity_list_disabled',
					'icon' => 'fa-circle-o',
					'value1' => 'list '.$id.' has been disabled'
				);
			$this->items_model->_table_name = 'tbl_activities';
			$this->items_model->_primary_key = 'activities_id';
			$this->items_model->save($activity);
				
        $type = 'success';
        $message = 'list has been disabled.';
        set_message($type, $message);
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function activate_list($id){
        $this->db->where('id',$id)->update('lead_list', array('active' => '1'));
        
        if($id > 0){
			$this->disable_vicidial_list($id,'Y');
        }
        
		$activity = array(
			'user' => $this->session->userdata('user_id'),
			'module' => 'lead_list',
			'module_field_id' => $id,
			'activity' => 'activity_list_activated',
			'icon' => 'fa-circle-o',
			'value1' => 'list '.$id.' has been activated'
		);
		
		$this->items_model->_table_name = 'tbl_activities';
		$this->items_model->_primary_key = 'activities_id';
		$this->items_model->save($activity);
				
        $type = 'success';
        $message = 'list has been disabled.';
        set_message($type, $message);
        redirect($_SERVER['HTTP_REFERER']);
    }
    
	public function delete_vicidial_list($list_id){

		if (!empty($list_id))
		{
			$server = config_item('vicidial_server'); //'156.38.193.158';
			$api_user = '6665';
			$api_pass = '159357pocALOCapi';

			// echo $list_name;
			$url ="http://$server/vicidial/non_agent_api.php?source=test&function=update_list&user=$api_user&pass=$api_pass&list_id=$list_id&delete_list=Y&delete_leads=Y";
					
			// echo $url;
			$this->send_to_vicidial($url);
    	}
	}
	
	public function disable_vicidial_list($list_id, $action){
		if (!empty($list_id))
		{
			$server = config_item('vicidial_server'); //'156.38.193.158';
			$api_user = '6665';
			$api_pass = '159357pocALOCapi';

			// echo $list_name;
			$url ="http://$server/vicidial/non_agent_api.php?source=test&function=update_list&user=$api_user&pass=$api_pass&list_id=$list_id&active=$action";
					
			// echo $url;
			$this->send_to_vicidial($url);
		}
	}
	
	public function delete_lead_company($id){
        $data = $this->db->where('id',$id)->get('campaign_leads')->row();
       // print_r($data); exit;
        $company = $data->company_name;
        $phone = $data->phone_number;
        
        //soft delete based on phone number & company name;
        //$records = $this->db->where(array('phone_number' => $phone, 'company_name' => $company))->get('campaign_leads')->result();
        
        //soft delete based on company name only;
        $records = $this->db->where(array('company_name' => $company))->get('campaign_leads')->result();
        
        if(!empty($records)){
            foreach($records as $record){
                $this->db->where('id',$record->id)->update('campaign_leads', array('deleted' => 1, 'deleted_by' => $this->session->userdata('user_id'), 'deleted_time' => date('Y-m-d H:i:s')));
            }
        }
        
        if(!empty($records)){
            foreach($records as $record){
                $this->delete_vicidial_lead($record->id);
            }
        }
	}
	
	public function analysis_report($type, $from_date, $to_date) {
		
		$date_from = urldecode($from_date);
		$date_to = urldecode($to_date);
	
		$da_select = '';
		if ($date_from < '2018-12-03 23:59:59') {
			if ($type == 'data_admin') {
				$da_select = ', da_status_export';
				$search_array = array('qa_status' => 1);
			} else {
				$search_array = array('lead_status_id' => 6);
			}
		} else {
			if ($type == 'data_admin') {
				$da_select = ', da_status_export';
				$search_array = array('qa_status' => 1, 'qa_date >= ' => $date_from, 'qa_date <= ' => $date_to);
			} else {
				$search_array = array('lead_status_id' => 6, 'dispo_date >=' => $date_from, 'dispo_date <= ' => $date_to);
			}
		}
		
		$this->load->library('excel');
		$data = $this->db->select("id, campaign_id, campaign_name, list_id, username, lead_status, lead_source, company_name, first_name, last_name, jobtitle, email, phone_number, address1, city, zip, country, company_size, industry_sector, time_called, comments, updated, qa_status $da_select")->where($search_array)->get('view_campaign_leads')->result();
		#print_r($data); exit;
		if (!empty($data)) {
		    $campaign_id = $data[0]->campaign_id;
			$campaign_questions = $this->db->where('campaign_id',$data[0]->campaign_id)->get('campaign_questions')->result();
		}
		
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0);
			$total_chars = 0;
		// HEADINGS
		$rowCount = 1; 
		foreach ($data as $dat) {
			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$d);	
				$char++;
				$total_chars++;
			}
			$rowCount++;
			break;
		}
		
		$rowCount2 = 1;
		$total_char = 'A';
		for ($n = 0; $n <= $total_chars; $n++) {
			$total_char++;
		}
		
		if (!empty($campaign_questions)) {
			foreach($campaign_questions as $q) {				
				#$cdata[$q->question_question] = $a->answer ;
				$objPHPExcel->getActiveSheet()->SetCellValue($total_char.$rowCount2,$q->question_question);	
				$total_char++;
			}
			$rowCount2++;
		}
		
		$rowCount = 2; 
		foreach ($data as $dat) {
			$lead_status = '';
			$campaign_answers[] = $this->db->where(array('lead_id' => $dat->id,'campaign_id' => $campaign_id))->get('campaign_answers')->result();
			
			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				if($d == 'qa_status'){ if($v == 1){ $v = 'Approved'; } if($v == 2){ $v = 'Pending'; } if($v == 3){ $v = 'Declined'; } }
				if($d == 'da_status'){ if($v == 1){ $v = 'Successfully Exported'; } if($v == 2){ $v = 'Bad Email'; } if($v == 3){ $v = 'Duplicate'; } }
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$v);	
				$char++;
				$total_chars++;
			}
		$rowCount++;
		}
			
		
		$total_char = 'A';
		for($n = 0; $n <= $total_chars; $n++){
			$total_char++;
		}
        $total_answers = count($campaign_answers);
        	       
		foreach($campaign_answers as $a){
			$ntotal_char = $total_char;
			$total_qa = 0;
			foreach($a as $answ){
				$objPHPExcel->getActiveSheet()->SetCellValue($ntotal_char.$rowCount2,$answ->answer);	
				$ntotal_char++;	
			}
			 $rowCount2++;
		}
        $filename = "{$type}_Report_".substr($date_from,0,10)."_".substr($date_to,0,10)."";
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename.xlsx");
		header("Content-Transfer-Encoding: binary ");

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('php://output');
	}
	
	
	public function create_vicidial_list_debug($list_id = '1641') {
		
		$data = $this->db->where('id', $list_id)->get('lead_list')->row();
		$campaign_id = $this->db->where('id',$data->campaign_id)->get('tbl_campaigns')->row('campaign_id');
	
		if (!empty($list_id))
		{
			$server = config_item('vicidial_server'); //'156.38.193.158';
			$api_user = '6665';
			$api_pass = '159357pocALOCapi';
			
			if($list_id < 10){
				$list_id = '0'.$list_id;
			}

			$list_name = str_replace(" ", "+",$data->list_name);
			$campaign_id = str_replace(" ", "+",$campaign_id);
			
			$url ="http://$server/vicidial/non_agent_api.php?source=test&function=add_list&user=$api_user&pass=$api_pass&list_id=$list_id&list_name={$list_name}&campaign_id={$campaign_id}&active=Y&local_call_time=12pm-5pm&";
					
			// echo $url;
        	// $this->send_to_vicidial($url);
			
			$url2 ="http://$server/vicidial/non_agent_api.php?source=test&function=update_list&user=$api_user&pass=$api_pass&list_id=$list_id&list_name={$list_name}&campaign_id={$campaign_id}&active=Y&reset_list=Y";
		    // $this->send_to_vicidial($url2);
			
			// webform
			$url3 = "http://$server/vicidial/non_agent_api.php?source=crm&user=$api_user&pass=$api_pass&function=update_list&list_id={$list_id}&campaign_id=".$campaign_id."&web_form_address=VAR".base_url('admin/campaigns/campaign_questions/'.$data->campaign_id)."?entry_list_id=--A--vendor_lead_code--B--";
		    // $this->send_to_vicidial($url3);
		    
		    echo 'Add URI: ' . $url;
		    echo "\r\n";
		    echo 'Update URI: ' . $url2;
		    echo "\r\n";
		    echo 'Webform URI: ' . $url3;
		}
	}
	
}
