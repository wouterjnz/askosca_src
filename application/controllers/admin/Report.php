<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class report extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('report_model');
        $this->load->model('invoice_model');
        $this->load->model('estimates_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    public function account_statement()
    {
        $data['title'] = lang('account_statement');
        $data['account_id'] = $this->input->post('account_id', TRUE);
        if (!empty($data['account_id'])) {
            $data['report'] = TRUE;
            $data['start_date'] = $this->input->post('start_date', TRUE);
            $data['end_date'] = $this->input->post('end_date', TRUE);
            $data['transaction_type'] = $this->input->post('transaction_type', TRUE);
            $data['all_transaction_info'] = $this->get_account_statement($data['account_id'], $data['start_date'], $data['end_date'], $data['transaction_type']);
        }
        $data['subview'] = $this->load->view('admin/report/account_statement', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    function get_account_statement($account_id, $start_date, $end_date, $transaction_type)
    {
        if ($transaction_type == 'all_transactions') {
            $where = array('account_id' => $account_id, 'date >=' => $start_date, 'date <=' => $end_date);
        } elseif ($transaction_type == 'debit') {
            $where = array('account_id' => $account_id, 'date >=' => $start_date, 'date <=' => $end_date, 'credit' => $transaction_type);
        } else {
            $where = array('account_id' => $account_id, 'date >=' => $start_date, 'date <=' => $end_date, 'debit' => $transaction_type);
        }
        $this->report_model->_table_name = "tbl_transactions"; //table name
        $this->report_model->_order_by = "transactions_id";
        return $this->report_model->get_by($where, FALSE);
    }

    public function account_statement_pdf($account_id, $start_date, $end_date, $transaction_type)
    {

        $data['all_transaction_info'] = $this->get_account_statement($account_id, $start_date, $end_date, $transaction_type);
        $data['title'] = lang('account_statement');
        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/report/account_statement_pdf', $data, TRUE);
        pdf_create($viewfile, lang('account_statement') . ' From:' . $start_date . ' To:', $end_date);
    }

    public function income_report()
    {
        $data['title'] = lang('income_report');
        $data['transactions_report'] = $this->get_transactions_report();
        $data['subview'] = $this->load->view('admin/report/income_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function income_report_pdf()
    {
        $data['title'] = lang('income_report');
        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/report/income_report_pdf', $data, TRUE);
        pdf_create($viewfile, lang('income_report'));
    }

    public function get_transactions_report()
    {// this function is to create get monthy recap report
        $m = date('n');
        $year = date('Y');
        $num = cal_days_in_month(CAL_GREGORIAN, $m, $year);
        for ($i = 1; $i <= $num; $i++) {
            if ($m >= 1 && $m <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $date = $year . "-" . '0' . $m;
            } else {
                $date = $year . "-" . $m;
            }
            $date = $date . '-' . $i;
            $transaction_report[$i] = $this->db->where('date', $date)->order_by('transactions_id', 'DESC')->get('tbl_transactions')->result();
        }
        return $transaction_report; // return the result
    }

    public function expense_report()
    {
        $data['title'] = lang('expense_report');
        $data['transactions_report'] = $this->get_transactions_report();
        $data['subview'] = $this->load->view('admin/report/expense_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function expense_report_pdf()
    {
        $data['title'] = lang('expense_report');
        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/report/expense_report_pdf', $data, TRUE);
        pdf_create($viewfile, lang('expense_report'));
    }

    public function income_expense()
    {
        $data['title'] = lang('income_expense');
        $data['transactions_report'] = $this->get_transactions_report();
        $data['subview'] = $this->load->view('admin/report/income_expense', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function income_expense_pdf()
    {
        $data['title'] = lang('income_expense');
        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/report/income_expense_pdf', $data, TRUE);
        pdf_create($viewfile, lang('income_expense'));
    }

    public function date_wise_report()
    {
        $data['title'] = lang('date_wise_report');
        $data['start_date'] = $this->input->post('start_date', TRUE);
        $data['end_date'] = $this->input->post('end_date', TRUE);
        if (!empty($data['start_date']) && !empty($data['end_date'])) {
            $data['report'] = TRUE;
            $data['all_transaction_info'] = $this->db->where('date >=', $data['start_date'], 'date >=', $data['end_date'])->get('tbl_transactions')->result();
        }
        $data['subview'] = $this->load->view('admin/report/date_wise_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function date_wise_report_pdf($start_date, $end_date)
    {
        $data['title'] = lang('date_wise_report');
        $this->load->helper('dompdf');
        $data['all_transaction_info'] = $this->db->where('date >=', $start_date, 'date <=', $end_date)->get('tbl_transactions')->result();
        $viewfile = $this->load->view('admin/report/date_wise_report_pdf', $data, TRUE);
        pdf_create($viewfile, lang('date_wise_report'));
    }

    public function report_by_month()
    {
        $data['title'] = lang('report_by_month');
        $data['current_month'] = date('m');

        if ($this->input->post('year', TRUE)) { // if input year
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
        // get all expense list by year and month
        $data['report_by_month'] = $this->get_report_by_month($data['year']);

        $data['subview'] = $this->load->view('admin/report/report_by_month', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function get_report_by_month($year, $month = NULL)
    {// this function is to create get monthy recap report
        if (!empty($month)) {
            if ($month >= 1 && $month <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $month . '-' . '01';
                $end_date = $year . "-" . '0' . $month . '-' . '31';
            } else {
                $start_date = $year . "-" . $month . '-' . '01';
                $end_date = $year . "-" . $month . '-' . '31';
            }
            $get_expense_list = $this->report_model->get_report_by_date($start_date, $end_date); // get all report by start date and in date
        } else {
            for ($i = 1; $i <= 12; $i++) { // query for months
                if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                    $start_date = $year . "-" . '0' . $i . '-' . '01';
                    $end_date = $year . "-" . '0' . $i . '-' . '31';
                } else {
                    $start_date = $year . "-" . $i . '-' . '01';
                    $end_date = $year . "-" . $i . '-' . '31';
                }
                $get_expense_list[$i] = $this->report_model->get_report_by_date($start_date, $end_date); // get all report by start date and in date
            }
        }
        return $get_expense_list; // return the result
    }

    public function report_by_month_pdf($year, $month)
    {
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'report/report_by_month',
            'module_field_id' => $year,
            'activity' => lang('activity_report_by_month_pdf'),
            'icon' => 'fa-laptop',
            'value1' => $year,
            'value2' => $month
        );
        $this->report_model->_table_name = 'tbl_activities';
        $this->report_model->_primary_key = 'activities_id';
        $this->report_model->save($activity);

        $data['report_list'] = $this->get_report_by_month($year, $month);
        $month_name = date('F', strtotime($year . '-' . $month)); // get full name of month by date query
        $data['monthyaer'] = $month_name . '  ' . $year;
        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/report/report_by_month_pdf', $data, TRUE);
        pdf_create($viewfile, lang('report_by_month') . '- ' . $data['monthyaer']);
    }

    public function all_income()
    {
        $data['title'] = lang('all_income');
        $data['subview'] = $this->load->view('admin/report/all_income', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function all_expense()
    {
        $data['title'] = lang('all_expense');
        $data['subview'] = $this->load->view('admin/report/all_expense', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function all_transaction()
    {
        $data['title'] = lang('all_transaction');
        $data['transactions_report'] = $this->get_transactions_report();
        $data['subview'] = $this->load->view('admin/report/all_transaction', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function tasks_assignment()
    {
        $data['title'] = lang('tasks_assignment');
        $data['all_project'] = $this->report_model->get_permission('tbl_project');
        // get permission user by menu id
        $permission_user = $this->report_model->all_permission_user('57');
        // get all admin user
        $admin_user = $this->db->where('role_id', 1)->get('tbl_users')->result();
        // if not exist data show empty array.
        if (!empty($permission_user)) {
            $permission_user = $permission_user;
        } else {
            $permission_user = array();
        }
        if (!empty($admin_user)) {
            $admin_user = $admin_user;
        } else {
            $admin_user = array();
        }
        $data['assign_user'] = array_merge($admin_user, $permission_user);

        $data['user_tasks'] = $this->get_tasks_by_user($data['assign_user']);

        $data['subview'] = $this->load->view('admin/report/project_tasks_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }


    function get_tasks_by_user($assign_user, $tasks = null)
    {
        $tasks_info = $this->report_model->get_permission('tbl_task');
        if (!empty($tasks_info)):foreach ($tasks_info as $v_tasks):
            if (!empty($tasks)) {
                if ($v_tasks->permission == 'all') {
                    $permission[$v_tasks->permission][$v_tasks->task_status][] = $v_tasks->task_status;
                } else {
                    $get_permission = json_decode($v_tasks->permission);
                    if (!empty($get_permission)) {
                        foreach ($get_permission as $id => $v_permission) {
                            if (!empty($assign_user)) {
                                foreach ($assign_user as $v_user) {
                                    if ($v_user->user_id == $id) {
                                        $permission[$v_user->user_id][$v_tasks->task_status][] = $v_tasks->task_status;
                                    }
                                }
                            }

                        }
                    }
                }
            } else {
                if (!empty($v_tasks->project_id)) {

                    if ($v_tasks->permission == 'all') {
                        $permission[$v_tasks->permission][$v_tasks->task_status][] = $v_tasks->task_status;
                    } else {
                        $get_permission = json_decode($v_tasks->permission);
                        if (!empty($get_permission)) {
                            foreach ($get_permission as $id => $v_permission) {
                                if (!empty($assign_user)) {
                                    foreach ($assign_user as $v_user) {
                                        if ($v_user->user_id == $id) {
                                            $permission[$v_user->user_id][$v_tasks->task_status][] = $v_tasks->task_status;
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }


        endforeach;
        endif;
        if (empty($permission)) {
            $permission = array();
        }
        return $permission;
    }

    public
    function bugs_assignment()
    {
        $data['title'] = lang('bugs_assignment') . ' ' . lang('report');
        $data['all_project'] = $this->report_model->get_permission('tbl_project');
        // get permission user by menu id
        $permission_user = $this->report_model->all_permission_user('58');
        // get all admin user
        $admin_user = $this->db->where('role_id', 1)->get('tbl_users')->result();
        // if not exist data show empty array.
        if (!empty($permission_user)) {
            $permission_user = $permission_user;
        } else {
            $permission_user = array();
        }
        if (!empty($admin_user)) {
            $admin_user = $admin_user;
        } else {
            $admin_user = array();
        }
        $data['assign_user'] = array_merge($admin_user, $permission_user);

        $data['user_bugs'] = $this->get_bugs_by_user($data['assign_user']);

        $data['yearly_report'] = $this->get_project_report_by_month();
        $data['subview'] = $this->load->view('admin/report/project_bugs_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function get_project_report_by_month($tickets = null)
    {// this function is to create get monthy recap report

        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = date('Y') . "-" . '0' . $i . '-' . '01';
                $end_date = date('Y') . "-" . '0' . $i . '-' . '31';
            } else {
                $start_date = date('Y') . "-" . $i . '-' . '01';
                $end_date = date('Y') . "-" . $i . '-' . '31';
            }
            if (!empty($tickets)) {
                $where = array('created >=' => $start_date . ' 00:00:00', 'created <=' => $end_date . ' 23:59:59');
                $get_result[$i] = $this->db->where($where)->get('tbl_tickets')->result();; // get all report by start date and in date
            } else {
                $where = array('created_time >=' => $start_date, 'created_time <=' => $end_date);
                $get_result[$i] = $this->db->where($where)->get('tbl_bug')->result();; // get all report by start date and in date
            }
        }

        return $get_result; // return the result
    }

    function get_bugs_by_user($assign_user, $bugs = null)
    {
        $bugs_info = $this->report_model->get_permission('tbl_bug');

        if (!empty($bugs_info)):foreach ($bugs_info as $v_bugs):
            if (!empty($bugs)) {
                if ($v_bugs->permission == 'all') {
                    $permission[$v_bugs->permission][$v_bugs->bug_status][] = $v_bugs->bug_status;
                } else {
                    $get_permission = json_decode($v_bugs->permission);
                    if (!empty($get_permission)) {
                        foreach ($get_permission as $id => $v_permission) {
                            if (!empty($assign_user)) {
                                foreach ($assign_user as $v_user) {
                                    if ($v_user->user_id == $id) {
                                        $permission[$v_user->user_id][$v_bugs->bug_status][] = $v_bugs->bug_status;
                                    }
                                }
                            }

                        }
                    }
                }
            } else {
                if (!empty($v_bugs->project_id)) {

                    if ($v_bugs->permission == 'all') {
                        $permission[$v_bugs->permission][$v_bugs->bug_status][] = $v_bugs->bug_status;
                    } else {
                        $get_permission = json_decode($v_bugs->permission);
                        if (!empty($get_permission)) {
                            foreach ($get_permission as $id => $v_permission) {
                                if (!empty($assign_user)) {
                                    foreach ($assign_user as $v_user) {
                                        if ($v_user->user_id == $id) {
                                            $permission[$v_user->user_id][$v_bugs->bug_status][] = $v_bugs->bug_status;
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }

        endforeach;
        endif;
        if (empty($permission)) {
            $permission = array();
        }
        return $permission;
    }

    public function project_report()
    {
        $data['title'] = lang('project_report');
        // get permission user by menu id
        $permission_user = $this->report_model->all_permission_user('57');
        // get all admin user
        $admin_user = $this->db->where('role_id', 1)->get('tbl_users')->result();
        // if not exist data show empty array.
        if (!empty($permission_user)) {
            $permission_user = $permission_user;
        } else {
            $permission_user = array();
        }
        if (!empty($admin_user)) {
            $admin_user = $admin_user;
        } else {
            $admin_user = array();
        }
        $data['assign_user'] = array_merge($admin_user, $permission_user);

        $data['all_project'] = $this->report_model->get_permission('tbl_project');
        $data['user_project'] = $this->get_project_by_user($data['assign_user']);
        $data['subview'] = $this->load->view('admin/report/project_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function tasks_report()
    {
        $data['title'] = lang('project_report');
        // get permission user by menu id
        $permission_user = $this->report_model->all_permission_user('54');
        // get all admin user
        $admin_user = $this->db->where('role_id', 1)->get('tbl_users')->result();
        // if not exist data show empty array.
        if (!empty($permission_user)) {
            $permission_user = $permission_user;
        } else {
            $permission_user = array();
        }
        if (!empty($admin_user)) {
            $admin_user = $admin_user;
        } else {
            $admin_user = array();
        }
        $data['assign_user'] = array_merge($admin_user, $permission_user);

        $data['all_tasks'] = $this->report_model->get_permission('tbl_task');
        $data['user_tasks'] = $this->get_tasks_by_user($data['assign_user'], true);
        $data['subview'] = $this->load->view('admin/report/tasks_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public
    function bugs_report()
    {
        $data['title'] = lang('bugs_assignment') . ' ' . lang('report');
        // get permission user by menu id
        $permission_user = $this->report_model->all_permission_user('58');
        // get all admin user
        $admin_user = $this->db->where('role_id', 1)->get('tbl_users')->result();
        // if not exist data show empty array.
        if (!empty($permission_user)) {
            $permission_user = $permission_user;
        } else {
            $permission_user = array();
        }
        if (!empty($admin_user)) {
            $admin_user = $admin_user;
        } else {
            $admin_user = array();
        }
        $data['assign_user'] = array_merge($admin_user, $permission_user);
        $data['user_bugs'] = $this->get_bugs_by_user($data['assign_user'], true);

        $data['yearly_report'] = $this->get_project_report_by_month();
        $data['subview'] = $this->load->view('admin/report/bugs_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    function get_project_by_user($assign_user)
    {
        $all_project = $this->report_model->get_permission('tbl_project');
        if (!empty($all_project)):foreach ($all_project as $v_project):
            if ($v_project->permission == 'all') {
                $permission[$v_project->permission][$v_project->project_status][] = $v_project->project_status;
            } else {
                $get_permission = json_decode($v_project->permission);
                if (!empty($get_permission)) {
                    foreach ($get_permission as $id => $v_permission) {
                        if (!empty($assign_user)) {
                            foreach ($assign_user as $v_user) {
                                if ($v_user->user_id == $id) {
                                    $permission[$v_user->user_id][$v_project->project_status][] = $v_project->project_status;
                                }
                            }
                        }

                    }
                }
            }
        endforeach;
        endif;
        if (empty($permission)) {
            $permission = array();
        }

        return $permission;
    }

    public function tickets_report()
    {
        $data['title'] = lang('tickets_report');
        // get permission user by menu id
        $permission_user = $this->report_model->all_permission_user('7');
        // get all admin user
        $admin_user = $this->db->where('role_id', 1)->get('tbl_users')->result();
        // if not exist data show empty array.
        if (!empty($permission_user)) {
            $permission_user = $permission_user;
        } else {
            $permission_user = array();
        }
        if (!empty($admin_user)) {
            $admin_user = $admin_user;
        } else {
            $admin_user = array();
        }
        $data['assign_user'] = array_merge($admin_user, $permission_user);
        $data['user_tickets'] = $this->get_tickets_by_user($data['assign_user']);

        $data['yearly_report'] = $this->get_project_report_by_month(true);

        $data['subview'] = $this->load->view('admin/report/tickets_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    function get_tickets_by_user($assign_user)
    {
        $all_ticktes = $this->report_model->get_permission('tbl_tickets');
        if (!empty($all_ticktes)):foreach ($all_ticktes as $v_ticktes):
            if ($v_ticktes->permission == 'all') {
                $permission[$v_ticktes->permission][$v_ticktes->status][] = $v_ticktes->status;
            } else {
                $get_permission = json_decode($v_ticktes->permission);
                if (!empty($get_permission)) {
                    foreach ($get_permission as $id => $v_permission) {
                        if (!empty($assign_user)) {
                            foreach ($assign_user as $v_user) {
                                if ($v_user->user_id == $id) {
                                    $permission[$v_user->user_id][$v_ticktes->status][] = $v_ticktes->status;
                                }
                            }
                        }

                    }
                }
            }
        endforeach;
        endif;
        if (empty($permission)) {
            $permission = array();
        }

        return $permission;
    }

    public function client_report()
    {
        $data['title'] = lang('client_report');
        $data['all_client_info'] = $this->db->get('tbl_client')->result();

        $data['subview'] = $this->load->view('admin/report/client_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    	public function ticket_statement($client_id, $date_from, $date_to, $domain_only, $download = '')
	{
		$client_email = $this->db->where('client_id', $client_id)->get('tbl_client')->row('email');
	if(!empty($client_email)){
		if($domain_only == 'true'){
			$domain = explode('@', $client_email);
			$client_email = $domain[1];

			$data['ticket_info'] = $this->db->like('from_email', $client_email)->get('tbl_tickets')->result();

		}else{
			$data['ticket_info'] = $this->db->where('from_email', $client_email)->get('tbl_tickets')->result();
		}

		// $data['ticket_info'] = $this->db->query("SELECT * FROM tbl_tickets LEFT JOIN tbl_tickets_replies ON tbl_tickets.tickets_id = tbl_tickets_replies.tickets_id WHERE from_email = '$client_email' ")->result();
		if(empty($download)){
		$this->load->view('admin/report/ticket_statement', $data);
		}else{
			   $this->load->helper('dompdf');
				$viewfile = $this->load->view('admin/report/ticket_statement', $data, TRUE);
				pdf_create($viewfile, 'Ticket Statement_' . $client_id);
		}
	}
	}

	public function invoice_statement($client_id, $date_from, $date_to, $download = '')
	{
	    if(!empty($client_id)){
	    $client_name = $this->db->where('client_id', $client_id)->get('tbl_client')->row('name');
		//$invoices = $this->db->where('client_id', $client_id)->get('tbl_invoices')->result();
		$invoices = $this->db->query("SELECT * FROM tbl_invoices WHERE client_id = $client_id AND due_date >= '$date_from' AND due_date <= '$date_to'")->result();
		$payments = [];
		foreach ($invoices as $invoice) {
			$all_payments[] = $this->db->where('invoices_id',$invoice->invoices_id)->get('tbl_payments')->result();
		}
		if(!empty($all_payments)){
		foreach ($all_payments as $payment){
			foreach ($payment as $pmt) {
				$payments[] = $pmt;
			}
		}
		}
		$table = '<center><h4>Invoice Statement for Client:<br/>'.$client_name .'<br/>Date: '.$date_from .' to '. $date_to.'</h4></center>
		<table class="table table-striped" cellspacing="0" width="100%">
						<tr>
							<td><strong>Due Date</strong></td>
							<td><strong>Type</strong></td>
							<td><strong>Reference_no</strong></td>
							<td><strong>Payment</strong></td>
							<td><strong>Amount</strong></td>
							<td><strong>Balance</strong></td>
						</tr>';

		$grand_balance = 0;
		foreach ($invoices as $invoice) {
		    	$amount = 0;
			$tbl_items = $this->db->where('invoices_id',$invoice->invoices_id)->get('tbl_items')->result();
			if (!empty($tbl_items)) {
			    foreach($tbl_items as $tbl_itms){
				    $amount += ($tbl_itms->total_cost + ($tbl_itms->total_cost * ($invoice->tax / 100)));
			    }
			    //$amount += $tbl_itms[0]->total_cost + ($tbl_items[0]->total_cost * ($invoice->tax / 100));
			}
			$table .= '<tr>
						<td>' . $invoice->due_date . '</td>
						<td> Invoice </td>
						<td>' . $invoice->reference_no . '</td>
						<td></td>
						<td>R ' . $this->n_format($amount) . '</td>
						<td>R '.$this->n_format($amount).'</td>
					</tr>';
			$total_balance = 0;
			if(!empty($payments)){

			foreach ($payments as $payment) {
				if ($payment->invoices_id == $invoice->invoices_id) {
					$balance = floatval($amount) - floatval($payment->amount);

					$table .= '<tr>
								<td>' . $payment->payment_date . '</td>
								<td> Payment </td>
								<td>' . $payment->trans_id . '</td>
								<td>R ' . $this->n_format($payment->amount) . '</td>
								<td></td>
								<td>R ' . $this->n_format($balance - $total_balance) . '</td>
							</tr>';
								$total_balance += $this->n_format(floatval($payment->amount));
				}
			}}
			$grand_balance += ($amount - $total_balance);

		}
		$table .= '<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td><strong>TOTAL DUE</strong></td>
								<td> R '.$this->n_format($grand_balance) .'</td>
							</tr>';
		$table .= '</table>';

		if(empty($download)){
			echo $table;
		}else{
					$this->load->helper('dompdf');
					$this->load->helper('download');
					// $viewfile = $this->load->view('admin/report/ticket_statement', $data, TRUE);
				//	pdf_create($table, 'Invoice_statement_' . $client_id);
				$filename = 'Invoice_statement_' . $client_id;
					$output = pdf_create($table, $filename, false);
					file_put_contents($filename. '.pdf', $output);
					return force_download("./".$filename.'.pdf', null);
		}
	    }
	}

	function n_format($n){
		if(is_numeric($n)){
			return number_format($n,2,".","") ;
		}else{
			return $n;
		}
	}


	public function agent_report()
    {
        $data['title'] = lang('account_statement');
        $data['account_id'] = $this->input->post('account_id', TRUE);
        if (!empty($data['account_id'])) {
            $data['report'] = TRUE;
            $data['start_date'] = $this->input->post('start_date', TRUE);
            $data['end_date'] = $this->input->post('end_date', TRUE);
        }
        $data['subview'] = $this->load->view('admin/report/agent_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

	public function agent_report_xls()
	{
		$this->load->library('excel');

		$type = $this->input->post('type', TRUE);
		$agent_id = $this->input->post('agent_id', TRUE);

		$date_from = $this->input->post('type', TRUE);
		$date_from = $this->input->post('date_from', TRUE);
		$date_to = $this->input->post('date_to', TRUE);

		if ($type == 1) {
			$where = array('user' => $agent_id);
		}
		if ($type == 2) {
			$this->agent_clockin_report($agent_id, $date_from, $date_to); exit;
		}
		if ($type == 3) {
			$where = array('user'=> $agent_id, 'referral_lead_id >' => 0);
			$this->agent_pipeline_leads($agent_id, $date_from, $date_to); exit;
		}
		if ($type == 4) {
			$where = array('user'=> $agent_id, 'referral_lead_id >' => 0);
			$this->agent_campaign_leads($date_from, $date_to); exit;
		}
// 		$new_date_from = date('Y-m-d', strtotime("-1 day", strtotime($date_from)));
// 		$new_date_to = date('Y-m-d', strtotime("+1 day", strtotime($date_to)));
		if($agent_id == 'all'){
		$leads = $this->db->query("
					SELECT username, DATE(updated) as date_updated, lead_status, campaign_leads.lead_status_id, count(campaign_leads.lead_status_id) as total_outcomes, starttime, endtime
					FROM campaign_leads
					LEFT JOIN tbl_lead_status ON tbl_lead_status.lead_status_id = campaign_leads.lead_status_id
					LEFT JOIN tbl_users ON tbl_users.user_id = campaign_leads.user
					WHERE campaign_leads.updated >= '".$date_from."' AND campaign_leads.updated <= '".$date_to." 23:59:59'
					Group by date_updated,campaign_leads.lead_status_id")->result();
		}elseif($agent_id > 0){
		    	$leads = $this->db->query("
					SELECT username, DATE(updated) as date_updated, lead_status, campaign_leads.lead_status_id, count(campaign_leads.lead_status_id) as total_outcomes, starttime, endtime
					FROM campaign_leads
					LEFT JOIN tbl_lead_status ON tbl_lead_status.lead_status_id = campaign_leads.lead_status_id
					LEFT JOIN tbl_users ON tbl_users.user_id = campaign_leads.user
					WHERE campaign_leads.user = $agent_id
					AND campaign_leads.updated >= '".$date_from."' AND campaign_leads.updated <= '".$date_to." 23:59:59'
					Group by date_updated,campaign_leads.lead_status_id")->result();
		}

		// echo "<pre/>";
		// print_r($leads); exit;

		$outcomes = $this->db->get('tbl_lead_status')->result();

		foreach($leads as $lead){
			$data[$lead->date_updated][$lead->lead_status][] = $lead->total_outcomes;
			$final[$lead->date_updated]=[];
		}

		foreach($data as $key => $datadate){
			foreach($datadate as $vkey => $value){
				$final[$key][$vkey] = $value[0];
			}
		}

        // 		echo "<pre/>";
        // 		print_r($leads); exit;
		// print_r($data); exit;
		// print_r($final); exit;

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$total_chars = 0;
		// HEADINGS

		$rowCount = 1;
		$total_chars = 0;
		$char  = 'A';
		$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,'DATE');
		foreach ($outcomes as $outcome) {
			// echo $d;
			$char++;
			$total_chars++;
			$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$outcome->lead_status);
		}
		$char++;
		$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,'Total');
		$char++;
		$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,'Conversation Rate');
		$rowCount++;

		$rowCount2 = 1;
		$total_char = 'A';
		for ($n = 0; $n <= $total_chars; $n++) {
			$total_char++;
		}


		$rowCount = 2;
		foreach ($data as $datkey => $dat) {
			$total_chars = 0;
			$char  = 'A';
			$converstion_rate = 0; $interested = 0; $not_interested = 0; $total = 0;
			$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$datkey);
			foreach ($outcomes as $outcome) {
				$new_outcome = 0;
				foreach ($dat as $d => $v) {
					foreach($v as $n){
						if($outcome->lead_status === $d){
							$new_outcome = $n;
							$total+=$n;
							if($outcome->lead_status == "INTERESTED"){  $interested += $n; }
							if($outcome->lead_status == "NOT INTERESTED"){  $not_interested += $n; }
						}
					}
				}
				$char++;
				$total_chars++;
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$new_outcome);
			}
			$char++;
			$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$total);
			$char++;
			$conversion_rate = $interested>0? (($not_interested/$interested) * 100):0;
			$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount, number_format($conversion_rate,2)."%");
		$rowCount++;
		}

		// exit;
        $filename = "Agent_Leads_Report_".$date_from."_".$date_to."";
		header("Pragma: public");
		header("Expires: 0");
        header('Content-Type: application/vnd.ms-excel');
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename.xls");
		header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		//$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('php://output');

	}

	public function agent_pipeline_leads($agent_id, $datefrom, $dateto)
	{
		$this->load->library('excel');

		if($agent_id == 'all'){
		    $where = array('referral_lead_id >' => 0);
		}else{
		    $where = array('user'=> $agent_id, 'referral_lead_id >' => 0);
		}
		$data = $this->db->select("id, campaign_id, campaign_name, list_id, username, lead_status, lead_source, company_name, first_name, last_name, jobtitle, email, phone_number, address1, city, zip, state, country, company_size, industry_sector, comments, warm_lead_indicator, linkedin_link,  referral_lead_id,updated")->where($where)->get('view_campaign_leads')->result();


		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$total_chars = 0;
		// HEADINGS
		$rowCount = 1;
		foreach ($data as $dat) {
			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$d);
				$char++;
				$total_chars++;
			}
			$rowCount++;
			break;
		}

		$rowCount2 = 1;
		$total_char = 'A';
		for ($n = 0; $n <= $total_chars; $n++) {
			$total_char++;
		}

		$rowCount = 2;
		foreach ($data as $dat) {
			// $lead_status = '';
			// $campaign_answers[] = $this->db->where(array('lead_id' => $dat->id,'campaign_id' => $campaign_id))->get('campaign_answers')->result();

			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				// if($d == 'qa_status'){ if($v == 1){ $v = 'Approved'; } if($v == 2){ $v = 'Pending'; } if($v == 3){ $v = 'Does Not Pas'; } if($v == 4){ $v = 'Does Not Qualify'; } }
				// if($d == 'da_status'){ if($v == 1){ $v = 'Successfully Uploaded'; } if($v == 2){ $v = 'Bad Email'; } if($v == 3){ $v = 'Pending'; } if($v == 3){ $v = 'Unsuccessful'; }  }

				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$v);
				$char++;
				$total_chars++;
			}
		$rowCount++;
		}

        $filename = "Agent_Pipeline_Report_".$datefrom."_".$dateto."";
		header("Pragma: public");
		header("Expires: 0");
        header('Content-Type: application/vnd.ms-excel');
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename.xls");
		header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		//$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('php://output');

	}

	public function agent_campaign_leads($datefrom, $dateto)
	{

		$this->load->library('excel');

		$leads = $this->db->query("
					SELECT username, tbl_campaigns.campaign_name, DATE(updated) as date_updated, lead_status, campaign_leads.lead_status_id, count(campaign_leads.lead_status_id) as total_outcomes
					FROM campaign_leads
					JOIN tbl_campaigns on tbl_campaigns.id = campaign_leads.campaign_id
					LEFT JOIN tbl_lead_status ON tbl_lead_status.lead_status_id = campaign_leads.lead_status_id
					LEFT JOIN tbl_users ON tbl_users.user_id = campaign_leads.user
					WHERE campaign_leads.updated >= '".$datefrom."' AND campaign_leads.updated < '".$dateto." 23:59:59'
					Group by username,date_updated,campaign_leads.campaign_id, campaign_leads.lead_status_id")->result();
		// echo "<pre/>";
		// print_r($leads); exit;

		$outcomes = $this->db->get('tbl_lead_status')->result();

		foreach($leads as $lead){
			$data[$lead->username][$lead->campaign_name][$lead->lead_status][] = $lead->total_outcomes;
			$final[$lead->username][$lead->campaign_name]=[];
		}

		// foreach($data as $key => $datadate){
		// 	foreach($datadate as $vkey => $value){
		// 		$final[$key][$vkey] = $value[0];
		// 	}
		// }
		/*
        $line = 'Agent | Campaign | ';

        foreach ($outcomes as $outcome) {
		    $line .= $outcome->lead_status;
            $line .= ' | ';
		}
        $line;
		$line2 = "<Br/><br/>";
        // echo "<pre/>";

        foreach ($data as $datkey => $dat) {
            $line2 .= $datkey . " <Br/> ";
            $converstion_rate = 0; $interested = 0; $not_interested = 0; $total = 0;
            foreach($dat as $campaign_name => $campaign_outcomes) {
                $line2 .=  $campaign_name;
                foreach($outcomes as $outcome){
                    $new_outcome = 0;
                    foreach($campaign_outcomes as $cc => $co){
                        foreach($co as $camp_outcome){
                            if($outcome->lead_status === $cc){
                                $new_outcome = $camp_outcome;
                                $total+=$camp_outcome;
                                if($outcome->lead_status == "INTERESTED"){  $interested += $camp_outcome; }
                                if($outcome->lead_status == "NOT INTERESTED"){  $not_interested += $camp_outcome; }
                            }
                        }
                    }
                    $line2 .= $new_outcome;
                }
                $line2 .= "<br/>";
                // echo $campaign_name; print_r($outcomes);
            }
            $line2 .= "<br/>";
        }

        echo  $line . $line2;
        exit;
        */
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$total_chars = 0;
		// HEADINGS

		$rowCount = 1;
		$total_chars = 0;
		$char  = 'A';
		$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,'AGENT');
		$char++;
		$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,'CAMPAIGN');
		foreach ($outcomes as $outcome) {
			// echo $d;
			$char++;
			$total_chars++;
			$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$outcome->lead_status);
		}
		$char++;
		$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,'Total');
		$char++;
		$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,'Conversation Rate');
		$rowCount++;

		$rowCount2 = 1;
		$total_char = 'A';
		for ($n = 0; $n <= $total_chars; $n++) {
			$total_char++;
		}


		$rowCount = 2;
        // $rowCount2 = 0;
		foreach ($data as $agent => $agent_data) {
			$total_chars = 0;
			$char  = 'A';
			$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount, $agent);
            $char++;
            $rowCount++;
            $rowCount2 = $rowCount;
            foreach($agent_data as $campaign_name => $campaign_outcomes) {
                $char2 = $char;
                $converstion_rate = 0; $interested = 0; $not_interested = 0; $total = 0;
		    	$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount2,$campaign_name);
                foreach ($outcomes as $outcome) {
                    $new_outcome = 0;
                    foreach($campaign_outcomes as $cc => $co){
                        foreach($co as $camp_outcome){
                            if($outcome->lead_status === $cc){
                                $new_outcome = $camp_outcome;
                                $total+=$camp_outcome;
                                if($outcome->lead_status == "INTERESTED"){  $interested += $camp_outcome; }
                                if($outcome->lead_status == "NOT INTERESTED"){  $not_interested += $camp_outcome; }
                            }
                        }
                    }
                    // $total_chars++;
                    $char2++;
                    $objPHPExcel->getActiveSheet()->SetCellValue($char2.$rowCount,$new_outcome);
                }
                $char2++;
                $objPHPExcel->getActiveSheet()->SetCellValue($char2.$rowCount2,$total);
                $char2++;

                $conversion_rate = $interested>0? (($not_interested/$interested) * 100):0;
                $objPHPExcel->getActiveSheet()->SetCellValue($char2.$rowCount2, number_format($conversion_rate,2)."%");
                $rowCount2+=1;
                // $char++;
                $rowCount++;
            }
            // $char2++;
            $rowCount++;
		}

        $filename = "Agent_Campaign_Report_".$datefrom."_".$dateto."";
		header("Pragma: public");
		header("Expires: 0");
        header('Content-Type: application/vnd.ms-excel');
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename.xls");
		header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		//$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('php://output');

	}

	public function agent_clockin_report($agent_id, $datefrom, $dateto)
	{
		$this->load->library('excel');
		$new_date_from = date('Y-m-d', strtotime("-1 day", strtotime($date_from)));

		if($agent_id == 'all'){
            $data = $this->db->query("SELECT
					username, date_in, tbl_attendance.date_out, clockin_time, clockout_time
					FROM tbl_attendance
					JOIN tbl_clock ON tbl_clock.attendance_id = tbl_attendance.attendance_id
					JOIN tbl_users ON tbl_users.user_id = tbl_attendance.user_id
					WHERE date_in >= '".$new_date_from."' and date_out <= '".$dateto."'
					")->result();
		}else{
		    $data = $this->db->query("SELECT
					username, date_in, tbl_attendance.date_out, clockin_time, clockout_time
					FROM tbl_attendance
					JOIN tbl_clock ON tbl_clock.attendance_id = tbl_attendance.attendance_id
					JOIN tbl_users ON tbl_users.user_id = tbl_attendance.user_id
					WHERE tbl_attendance.user_id = $agent_id AND date_in >= '".$new_date_from."' and date_out <= '".$dateto."'
					")->result();
		}

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$total_chars = 0;
		// HEADINGS
		$rowCount = 1;
		foreach ($data as $dat) {
			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$d);
				$char++;
				$total_chars++;
			}
			$rowCount++;
			break;
		}

		$rowCount2 = 1;
		$total_char = 'A';
		for ($n = 0; $n <= $total_chars; $n++) {
			$total_char++;
		}

		$rowCount = 2;
		foreach ($data as $dat) {
			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$v);
				$char++;
				$total_chars++;
			}
		$rowCount++;
		}

        $filename = "Agent_Attendance_Report_".$datefrom."_".$dateto."";
		header("Pragma: public");
		header("Expires: 0");
        header('Content-Type: application/vnd.ms-excel');
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename.xls");
		header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		//$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('php://output');

	}


	public function campaign_report(){
	    //ini_set('display_errors',1);
	    $this->load->library('excel');
		$leads = $this->db->query("
			SELECT
			    tbl_campaigns.campaign_name,
			    campaign_leads.campaign_id,
			    count(campaign_leads.campaign_id) as total_leads,
			    SUM(IF(campaign_leads.user = 0, 1, 0)) as untouched,
			    SUM(IF(campaign_leads.user > 0, 1, 0)) as touched,
			    SUM(IF(campaign_leads.user > 0 and lead_status_id = 20, 1, 0)) as callbacks
			FROM campaign_leads
			JOIN tbl_campaigns on tbl_campaigns.id = campaign_leads.campaign_id
			Group by campaign_leads.campaign_id")->result();

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$total_chars = 0;
		// HEADINGS
		$rowCount = 1;
		foreach ($leads as $dat) {
			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$d);
				$char++;
				$total_chars++;
			}
			$rowCount++;
			break;
		}

		$rowCount2 = 1;
		$total_char = 'A';
		for ($n = 0; $n <= $total_chars; $n++) {
			$total_char++;
		}

		$rowCount = 2;
		foreach ($leads as $dat) {


			$total_chars = 0;
			$char  = 'A';
			foreach ($dat as $d => $v) {
				$objPHPExcel->getActiveSheet()->SetCellValue($char.$rowCount,$v);
				$char++;
				$total_chars++;
			}
		$rowCount++;
		}


        $filename = "Campaign_report";
		header("Pragma: public");
		header("Expires: 0");
        header('Content-Type: application/vnd.ms-excel');
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename.xls");
		header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		//$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('php://output');
	}


}
