<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Warehouse extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('items_model');
        $this->load->model('stock_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }
	
    public function index($id = NULL) {
        $data['title'] = 'Warehouses';
        if (!empty($id)) {
            $data['active'] = 2;
            $data['warehouse_info'] = $this->items_model->check_by(array('id' => $id), 'tbl_warehouse');
        } else {
            $data['active'] = 1;
        }
	
		
		
        $data['subview'] = $this->load->view('admin/warehouses/warehouse_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function saved_warehouse($id = NULL) {

        $this->items_model->_table_name = 'tbl_warehouse';
        $this->items_model->_primary_key = 'id';
		
        $data = $this->items_model->array_from_post(array('warehouse','address'));
		
        // update root category
        $where = array('warehouse' => $data['warehouse']);
        // duplicate value check in DB
        if (!empty($id)) { // if id exist in db update data
            $saved_items_id = array('id !=' => $id);
        } else { // if id is not exist then set id as null
            $saved_items_id = null;
        }

        // check whether this input data already exist or not
        $check_items = $this->items_model->check_update('tbl_warehouse', $where, $saved_items_id);
        if (!empty($check_items)) {	
			// if input data already exist show error alert
            // massage for user
            $type = 'error';
            $msg = "<strong style='color:#000'>" . $data['warehouse'] . '</strong>  ' . lang('already_exist');
        } else { // save and update query                        
            $this->items_model->save($data, $id);
			
            if (!empty($id)) {
                $id = $id;
                $action = 'activity_update_warehouse';
                $msg = 'update warehouse';
            } else {
                $id = $return_id;
                $action = 'activity_save_warehouse';
                $msg = 'save warehouse';
            }
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'warehouse',
                'module_field_id' => $id,
                'activity' => $action,
                'icon' => 'fa-circle-o',
                'value1' => $data['warehouse']
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);
            // messages for user
            $type = "success";
        }
        $message = $msg;
        set_message($type, $message);
        redirect('admin/warehouse/index');
    }

    public function delete_warehouse($id) {
        $items_info = $this->items_model->check_by(array('id' => $id), 'tbl_warehouse');
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'warehouse',
            'module_field_id' => $id,
            'activity' => 'activity_warehouse_deleted',
            'icon' => 'fa-circle-o',
            'value1' => $items_info->warehouse
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        $this->items_model->_table_name = 'tbl_warehouse';
        $this->items_model->_primary_key = 'id';
        $this->items_model->delete($id);
        $type = 'success';
        $message = 'Warehouse Deleted';
        set_message($type, $message);
        redirect('admin/warehouse/index');
    }
	
}
