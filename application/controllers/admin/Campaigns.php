<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Campaigns extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('items_model');
        $this->load->model('call_log_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    public function index($id = NULL)
    {
      $client = 'askosca';
		if($client == 'askosca'){
			$table = 'tbl_campaigns';
			$table_id = 'id';
		}else{
			$table = 'tbl_campaigns';
			$table_id = 'id';
		}
        $data['title'] = 'All Campaigns';
        if (!empty($id)) {
            $data['active'] = 2;
            $can_edit = $this->items_model->can_action($table, 'edit', array('id' => $table_id));
            // if (!empty($can_edit)) {
                $data['leads_info'] = $this->items_model->check_by(array('id' => $id), $table);
            // }
        } else {
            $data['active'] = 1;
        }
        // get all leads status
        $status_info = $this->db->get('tbl_lead_status')->result();
        if (!empty($status_info)) {
            foreach ($status_info as $v_status) {
                $data['status_info'][$v_status->lead_type][] = $v_status;
            }
        }
        $data['assign_user'] = $this->items_model->allowad_user('55');
        $data['all_leads'] = $this->items_model->get_permission($table);
        $data['all_leads_archived'] = $this->items_model->get_permission('tbl_campaigns_archived');

        $data['subview'] = $this->load->view('admin/campaigns/all_campaigns', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

 	public function create_campaign_questions($id){
		$data['page'] = 'Campaigns';
        $data['title'] = 'Campaign Questions'; //Page title
        $data['campaign_id'] = $id;
        $data['subview'] = $this->load->view('admin/campaigns/campaign_questions', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
	}

    public function import_leads()
    {
        $data['title'] = lang('import_leads');
        $data['assign_user'] = $this->items_model->allowad_user('55');
        // get all leads status
        $status_info = $this->db->get('tbl_lead_status')->result();
        if (!empty($status_info)) {
            foreach ($status_info as $v_status) {
                $data['status_info'][$v_status->lead_type][] = $v_status;
            }
        }

        $data['subview'] = $this->load->view('admin/leads/import_leads', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

	public function save_imported()
    {
        //load the excel library
        $this->load->library('excel');
        ob_start();
        $file = $_FILES["upload_file"]["tmp_name"];
        if (!empty($file)) {
            $valid = false;
            $types = array('Excel2007', 'Excel5');
            foreach ($types as $type) {
                $reader = PHPExcel_IOFactory::createReader($type);
                if ($reader->canRead($file)) {
                    $valid = true;
                }
            }
            if (!empty($valid)) {
                try {
                    $objPHPExcel = PHPExcel_IOFactory::load($file);
                } catch (Exception $e) {
                    die("Error loading file :" . $e->getMessage());
                }
				$this->db->insert('lead_list', array('list_name' => $file, 'added' => date('Y-m-d h:i:s')));
				$list_id = $this->db->insert_id();
                //All data from excel
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                for ($x = 2; $x <= count($sheetData); $x++) {
                    // **********************
                    // Save Into leads table
                    // **********************
                    // $data = $this->items_model->array_from_post(array('client_id', 'lead_status_id', 'lead_source_id'));
					$data['filename'] = $file;
					$data['list_id'] = $list_id;
					$data['campaign_id'] = '12131';
                    $data['company_name'] = trim($sheetData[$x]["A"]);
                    $data['first_name'] = trim($sheetData[$x]["B"]);
                    $data['last_name'] = trim($sheetData[$x]["C"]);
                    $data['jobtitle'] = trim($sheetData[$x]["D"]);
                    $data['email'] = trim($sheetData[$x]["E"]);
                    $data['phone_number'] = trim($sheetData[$x]["F"]);
                    $data['address1'] = trim($sheetData[$x]["G"]);
                    $data['city'] = trim($sheetData[$x]["H"]);
                    $data['zip'] = trim($sheetData[$x]["I"]);
                    $data['country'] = trim($sheetData[$x]["J"]);
                    $data['alternate_number'] = trim($sheetData[$x]["K"]);
                    $data['alternate_dm'] = trim($sheetData[$x]["L"]);
                    $data['alternate_project'] = trim($sheetData[$x]["M"]);
                    $data['outcome'] = trim($sheetData[$x]["N"]);
                    $data['comments'] = trim($sheetData[$x]["O"]);
                    $data['date_worked'] = trim($sheetData[$x]["P"]);
                    $data['time_worked'] = trim($sheetData[$x]["Q"]);
                    $data['esl'] = trim($sheetData[$x]["R"]);

                    $data['permission'] = 'all';
                    // save to tbl_leads
                    $this->items_model->_table_name = 'campaign_leads';
                    $this->items_model->_primary_key = 'id';
                    $this->items_model->save($data);
                }
                $type = 'success';
                $message = lang('save_leads');
            } else {
                $type = 'error';
                $message = "Sorry your uploaded file type not allowed ! please upload XLS/CSV File ";
            }
        } else {
            $type = 'error';
            $message = "You did not Select File! please upload XLS/CSV File ";
        }
        set_message($type, $message);
        redirect('admin/leads');
    }

    public function save_imported_old()
    {
        //load the excel library
        $this->load->library('excel');
        ob_start();
        $file = $_FILES["upload_file"]["tmp_name"];
        if (!empty($file)) {
            $valid = false;
            $types = array('Excel2007', 'Excel5');
            foreach ($types as $type) {
                $reader = PHPExcel_IOFactory::createReader($type);
                if ($reader->canRead($file)) {
                    $valid = true;
                }
            }
            if (!empty($valid)) {
                try {
                    $objPHPExcel = PHPExcel_IOFactory::load($file);
                } catch (Exception $e) {
                    die("Error loading file :" . $e->getMessage());
                }
                //All data from excel
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                for ($x = 2; $x <= count($sheetData); $x++) {
                    // **********************
                    // Save Into leads table
                    // **********************
                    $data = $this->items_model->array_from_post(array('client_id', 'lead_status_id', 'lead_source_id'));
                    $data['lead_name'] = trim($sheetData[$x]["A"]);
                    $data['organization'] = trim($sheetData[$x]["B"]);
                    $data['contact_name'] = trim($sheetData[$x]["C"]);
                    $data['email'] = trim($sheetData[$x]["D"]);
                    $data['phone'] = trim($sheetData[$x]["E"]);
                    $data['mobile'] = trim($sheetData[$x]["F"]);
                    $data['address'] = trim($sheetData[$x]["G"]);
                    $data['city'] = trim($sheetData[$x]["H"]);
                    $data['country'] = trim($sheetData[$x]["I"]);
                    $data['facebook'] = trim($sheetData[$x]["J"]);
                    $data['skype'] = trim($sheetData[$x]["K"]);
                    $data['twitter'] = trim($sheetData[$x]["L"]);
                    $data['notes'] = trim($sheetData[$x]["M"]);

                    $data['permission'] = 'all';
                    // save to tbl_leads
                    $this->items_model->_table_name = 'tbl_leads';
                    $this->items_model->_primary_key = 'leads_id';
                    $this->items_model->save($data);
                }
                $type = 'success';
                $message = lang('save_leads');
            } else {
                $type = 'error';
                $message = "Sorry your uploaded file type not allowed ! please upload XLS/CSV File ";
            }
        } else {
            $type = 'error';
            $message = "You did not Select File! please upload XLS/CSV File ";
        }
        set_message($type, $message);
        redirect('admin/leads');
    }

    public
    function saved_campaign($id = NULL)
    {
		 $client = 'askosca';
		if($client == 'askosca'){
			$table = 'tbl_campaigns';
			$table_id = 'id';
		}else{
			$table = 'tbl_campaigns';
			$table_id = 'lead_id';
		}

        $this->items_model->_table_name = $table;
        $this->items_model->_primary_key = $table_id;

        $data = $this->items_model->array_from_post(array('campaign_id', 'campaign_name','campaign_description', 'type', 'region_id'));
        // update root category
        // $where = array('client_id' => $data['client_id'], 'lead_name' => $data['lead_name']);
        $where = array('campaign_id' => $data['campaign_id'],'campaign_name' => $data['campaign_name'],'campaign_description' => $data['campaign_description'], 'region_id' => $data['region']);
        // duplicate value check in DB
        if (!empty($id)) { // if id exist in db update data
            $leads_id = array($table_id.' !=' => $id);
        } else { // if id is not exist then set id as null
            $leads_id = null;
        }

        // check whether this input data already exist or not
        $check_leads = $this->items_model->check_update($table, $where, $table_id);
        if (!empty($check_leads)) { // if input data already exist show error alert
            // massage for user
            $type = 'error';
            $msg = "<strong style='color:#000'>" . $data['campaign_name'] . '</strong>  ' . lang('already_exist');
        } else { // save and update query

            $return_id = $this->items_model->save($data, $id);
            if (!empty($id)) {
                $id = $id;
                $action = 'activity_update_campaign';
                $msg = 'Campaign Updated';
            } else {
                $id = $return_id;
                $action = 'activity_save_campaign';
                $msg = 'Campaign Saved';
            }

            save_custom_field(5, $id);

			$activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'campaigns',
                'module_field_id' => $id,
                'activity' => $action,
                'icon' => 'fa-circle-o',
                'value1' => $data['campaign_id']
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);
            // messages for user
            $type = "success";
			$this->create_vicidial_campaign($id);
			// exit;
        }
        $message = $msg;
        set_message($type, $message);
        redirect('admin/campaigns');
    }

    public
    function leads_details($id, $active = NULL, $op_id = NULL)
    {
        $data['title'] = lang('leads_details');
        //get all task information
        $data['leads_details'] = $this->items_model->check_by(array('leads_id' => $id), 'tbl_leads');

        $this->items_model->_table_name = "tbl_task_attachment"; //table name
        $this->items_model->_order_by = "leads_id";
        $data['files_info'] = $this->items_model->get_by(array('leads_id' => $id), FALSE);

        foreach ($data['files_info'] as $key => $v_files) {
            $this->items_model->_table_name = "tbl_task_uploaded_files"; //table name
            $this->items_model->_order_by = "task_attachment_id";
            $data['project_files_info'][$key] = $this->items_model->get_by(array('task_attachment_id' => $v_files->task_attachment_id), FALSE);
        }

        if ($active == 2) {
            $data['active'] = 2;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 1;
        } elseif ($active == 3) {
            $data['active'] = 3;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 1;
        } elseif ($active == 4) {
            $data['active'] = 4;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 1;
        } elseif ($active == 5) {
            $data['active'] = 5;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 1;
        } elseif ($active == 'metting') {
            $data['active'] = 3;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 2;
            $data['mettings_info'] = $this->items_model->check_by(array('mettings_id' => $op_id), 'tbl_mettings');
        } elseif ($active == 'call') {
            $data['active'] = 2;
            $data['sub_active'] = 2;
            $data['call_info'] = $this->items_model->check_by(array('calls_id' => $op_id), 'tbl_calls');
            $data['sub_metting'] = 1;
        } else {
            $data['active'] = 1;
            $data['sub_active'] = 1;
            $data['sub_metting'] = 1;
        }

        $data['subview'] = $this->load->view('admin/leads/leads_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public
    function convert($id)
    {
        $data['title'] = lang('convert_to_client'); //Page title
        $data['person'] = 1;
        // get all country
        $this->items_model->_table_name = "tbl_countries"; //table name
        $this->items_model->_order_by = "id";
        $data['countries'] = $this->items_model->get();

        // get all currencies
        $this->items_model->_table_name = 'tbl_currencies';
        $this->items_model->_order_by = 'name';
        $data['currencies'] = $this->items_model->get();
        // get all language
        $this->items_model->_table_name = 'tbl_languages';
        $this->items_model->_order_by = 'name';
        $data['languages'] = $this->items_model->get();

        $data['leads_info'] = $this->items_model->check_by(array('leads_id' => $id), 'tbl_leads');
        $data['modal_subview'] = $this->load->view('admin/leads/_modal_convert', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public
    function converted($leads_id)
    {
        $data = $this->items_model->array_from_post(array('name', 'email', 'short_note', 'website', 'phone', 'mobile', 'fax', 'address', 'city', 'zipcode', 'currency',
            'skype_id', 'linkedin', 'facebook', 'twitter', 'language', 'country', 'vat', 'hosting_company', 'hostname', 'port', 'password', 'username', 'client_status'));
        if (!empty($_FILES['profile_photo']['name'])) {
            $val = $this->items_model->uploadImage('profile_photo');
            $val == TRUE || redirect('admin/client/manage_client');
            $data['profile_photo'] = $val['path'];
        }
        $data['leads_id'] = $leads_id;

        $this->items_model->_table_name = 'tbl_client';
        $this->items_model->_primary_key = "client_id";
        $return_id = $this->items_model->save($data);
        // update to tbl_leads
        $u_data['converted_client_id'] = $return_id;
        $this->items_model->_table_name = 'tbl_leads';
        $this->items_model->_primary_key = "leads_id";
        $this->items_model->save($u_data, $leads_id);
        $action = ('activity_convert_to_client');
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'client',
            'module_field_id' => $return_id,
            'activity' => $action,
            'icon' => 'fa-user',
            'value1' => $data['name']
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = "activities_id";
        $this->items_model->save($activities);
        // messages for user
        $type = "success";
        $message = lang('convert_to_client_suucess');
        set_message($type, $message);
        redirect('admin/client/client_details/' . $return_id);
    }

    public
    function update_users($id)
    {
        // get all assign_user
        $can_edit = $this->items_model->can_action('tbl_leads', 'edit', array('leads_id' => $id));
        if (!empty($can_edit)) {
            // get permission user by menu id
            $data['assign_user'] = $this->items_model->allowad_user('55');

            $data['leads_info'] = $this->items_model->check_by(array('leads_id' => $id), 'tbl_leads');
            $data['modal_subview'] = $this->load->view('admin/leads/_modal_users', $data, FALSE);
            $this->load->view('admin/_layout_modal', $data);
        } else {
            set_message('error', lang('there_in_no_value'));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public
    function update_member($id)
    {
        $can_edit = $this->items_model->can_action('tbl_leads', 'edit', array('leads_id' => $id));
        if (!empty($can_edit)) {
            $leads_info = $this->items_model->check_by(array('leads_id' => $id), 'tbl_leads');

            $permission = $this->input->post('permission', true);
            if (!empty($permission)) {

                if ($permission == 'everyone') {
                    $assigned = 'all';
                } else {
                    $assigned_to = $this->items_model->array_from_post(array('assigned_to'));
                    if (!empty($assigned_to['assigned_to'])) {
                        foreach ($assigned_to['assigned_to'] as $assign_user) {
                            $assigned[$assign_user] = $this->input->post('action_' . $assign_user, true);
                        }
                    }
                }
                if ($assigned != 'all') {
                    $assigned = json_encode($assigned);
                }
                $data['permission'] = $assigned;
            } else {
                set_message('error', lang('assigned_to') . ' Field is required');
                redirect($_SERVER['HTTP_REFERER']);
            }

//save data into table.
            $this->items_model->_table_name = "tbl_leads"; // table name
            $this->items_model->_primary_key = "leads_id"; // $id
            $this->items_model->save($data, $id);

            $msg = lang('update_leads');
            $activity = 'activity_update_leads';

// save into activities
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'leads',
                'module_field_id' => $id,
                'activity' => $activity,
                'icon' => 'fa-ticket',
                'value1' => $leads_info->lead_name,
            );
// Update into tbl_project
            $this->items_model->_table_name = "tbl_activities"; //table name
            $this->items_model->_primary_key = "activities_id";
            $this->items_model->save($activities);

            $type = "success";
            $message = $msg;
            set_message($type, $message);
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            set_message('error', lang('there_in_no_value'));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public
    function change_status($leads_id, $lead_status_id)
    {
        $can_edit = $this->items_model->can_action('tbl_leads', 'edit', array('leads_id' => $leads_id));
        if (!empty($can_edit)) {
            $data['lead_status_id'] = $lead_status_id;
            $this->items_model->_table_name = 'tbl_leads';
            $this->items_model->_primary_key = 'leads_id';
            $this->items_model->save($data, $leads_id);
            // messages for user
            $type = "success";
            $message = lang('change_status');
            set_message($type, $message);
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            set_message('error', lang('there_in_no_value'));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public
    function saved_call($leads_id, $id = NULL)
    {
        $data = $this->items_model->array_from_post(array('date', 'call_summary', 'client_id', 'user_id'));
        $data['leads_id'] = $leads_id;
        $this->items_model->_table_name = 'tbl_calls';
        $this->items_model->_primary_key = 'calls_id';
        $return_id = $this->items_model->save($data, $id);
        if (!empty($id)) {
            $id = $id;
            $action = 'activity_update_leads_call';
            $msg = lang('update_leads_call');
        } else {
            $id = $return_id;
            $action = 'activity_save_leads_call';
            $msg = lang('save_leads_call');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $leads_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $data['call_summary']
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        // messages for user
        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '2');
    }

    public
    function delete_leads_call($leads_id, $id)
    {
        $calls_info = $this->items_model->check_by(array('calls_id' => $id), 'tbl_calls');
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $leads_id,
            'activity' => 'activity_leads_call_deleted',
            'icon' => 'fa-circle-o',
            'value1' => $calls_info->call_summary
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        $this->items_model->_table_name = 'tbl_calls';
        $this->items_model->_primary_key = 'calls_id';
        $this->items_model->delete($id);
        $type = 'success';
        $message = lang('leads_call_deleted');
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '2');
    }

    public
    function delete_leads_mettings($leads_id, $id)
    {
        $mettings_info = $this->items_model->check_by(array('mettings_id' => $id), 'tbl_mettings');

        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $leads_id,
            'activity' => 'activity_leads_call_deleted',
            'icon' => 'fa-circle-o',
            'value1' => $mettings_info->meeting_subject
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        $this->items_model->_table_name = 'tbl_mettings';
        $this->items_model->_primary_key = 'mettings_id';
        $this->items_model->delete($id);
        $type = 'success';
        $message = lang('leads_mettings_deleted');
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '3');
    }

    public
    function saved_metting($leads_id, $id = NULL)
    {
        $this->items_model->_table_name = 'tbl_mettings';
        $this->items_model->_primary_key = 'mettings_id';

        $data = $this->items_model->array_from_post(array('meeting_subject', 'user_id', 'location', 'description'));
        $data['start_date'] = strtotime($this->input->post('start_date') . ' ' . date('H:i', strtotime($this->input->post('start_time'))));
        $data['end_date'] = strtotime($this->input->post('end_date') . ' ' . date('H:i', strtotime($this->input->post('end_time'))));
        $data['leads_id'] = $leads_id;
        $user_id = serialize($this->items_model->array_from_post(array('attendees')));
        if (!empty($user_id)) {
            $data['attendees'] = $user_id;
        } else {
            $data['attendees'] = '-';
        }
        $return_id = $this->items_model->save($data, $id);

        if (!empty($id)) {
            $id = $id;
            $action = 'activity_update_leads_metting';
            $msg = lang('update_leads_metting');
        } else {
            $id = $return_id;
            $action = 'activity_save_leads_metting';
            $msg = lang('save_leads_metting');
        }
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $leads_id,
            'activity' => $action,
            'icon' => 'fa-circle-o',
            'value1' => $data['meeting_subject']
        );

        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        // messages for user
        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '3');
    }

    public
    function save_comments()
    {

        $data['leads_id'] = $this->input->post('leads_id', TRUE);
        $data['comment'] = $this->input->post('comment', TRUE);
        $data['user_id'] = $this->session->userdata('user_id');

        //save data into table.
        $this->items_model->_table_name = "tbl_task_comment"; // table name
        $this->items_model->_primary_key = "task_comment_id"; // $id
        $this->items_model->save($data);

        // save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $data['leads_id'],
            'activity' => 'activity_new_leads_comment',
            'icon' => 'fa-ticket',
            'value1' => $data['comment'],
        );
        // Update into tbl_project
        $this->items_model->_table_name = "tbl_activities"; //table name
        $this->items_model->_primary_key = "activities_id";
        $this->items_model->save($activities);


        $type = "success";
        $message = lang('leads_comment_save');
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $data['leads_id'] . '/' . '4');
    }

    public
    function delete_comments($leads_id, $task_comment_id)
    {
        //save data into table.
        $this->items_model->_table_name = "tbl_task_comment"; // table name
        $this->items_model->_primary_key = "task_comment_id"; // $id
        $this->items_model->delete($task_comment_id);

        $type = "success";
        $message = lang('task_comment_deleted');
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '4');
    }

    public
    function save_attachment($task_attachment_id = NULL)
    {
        $data = $this->items_model->array_from_post(array('title', 'description', 'leads_id'));
        $data['user_id'] = $this->session->userdata('user_id');

        // save and update into tbl_files
        $this->items_model->_table_name = "tbl_task_attachment"; //table name
        $this->items_model->_primary_key = "task_attachment_id";
        if (!empty($task_attachment_id)) {
            $id = $task_attachment_id;
            $this->items_model->save($data, $id);
            $msg = lang('leads_file_updated');
        } else {
            $id = $this->items_model->save($data);
            $msg = lang('leads_file_added');
        }

        if (!empty($_FILES['task_files']['name']['0'])) {
            $old_path_info = $this->input->post('uploaded_path');
            if (!empty($old_path_info)) {
                foreach ($old_path_info as $old_path) {
                    unlink($old_path);
                }
            }
            $mul_val = $this->items_model->multi_uploadAllType('task_files');

            foreach ($mul_val as $val) {
                $val == TRUE || redirect('admin/leads/leads_details/' . $data['leads_id'] . '/' . '5');
                $fdata['files'] = $val['path'];
                $fdata['file_name'] = $val['fileName'];
                $fdata['uploaded_path'] = $val['fullPath'];
                $fdata['size'] = $val['size'];
                $fdata['ext'] = $val['ext'];
                $fdata['is_image'] = $val['is_image'];
                $fdata['image_width'] = $val['image_width'];
                $fdata['image_height'] = $val['image_height'];
                $fdata['task_attachment_id'] = $id;
                $this->items_model->_table_name = "tbl_task_uploaded_files"; // table name
                $this->items_model->_primary_key = "uploaded_files_id"; // $id
                $this->items_model->save($fdata);
            }
        }
        // save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $data['leads_id'],
            'activity' => 'activity_new_leads_attachment',
            'icon' => 'fa-ticket',
            'value1' => $data['title'],
        );
        // Update into tbl_project
        $this->items_model->_table_name = "tbl_activities"; //table name
        $this->items_model->_primary_key = "activities_id";
        $this->items_model->save($activities);
        // messages for user
        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $data['leads_id'] . '/' . '5');
    }

    public
    function delete_files($leads_id, $task_attachment_id)
    {
        $file_info = $this->items_model->check_by(array('task_attachment_id' => $task_attachment_id), 'tbl_task_attachment');
        // save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'leads',
            'module_field_id' => $leads_id,
            'activity' => 'activity_leads_attachfile_deleted',
            'icon' => 'fa-ticket',
            'value1' => $file_info->title,
        );
        // Update into tbl_project
        $this->items_model->_table_name = "tbl_activities"; //table name
        $this->items_model->_primary_key = "activities_id";
        $this->items_model->save($activities);

        //save data into table.
        $this->items_model->_table_name = "tbl_task_attachment"; // table name
        $this->items_model->delete_multiple(array('task_attachment_id' => $task_attachment_id));

        $type = "success";
        $message = lang('leads_attachfile_deleted');
        set_message($type, $message);
        redirect('admin/leads/leads_details/' . $leads_id . '/' . '5');
    }

    public
    function delete_campaign($id)
    {

        $can_delete = $this->items_model->can_action('tbl_campaigns', 'delete', array('id' => $id));
        // if (!empty($can_delete)) {

            $leads_info = $this->items_model->check_by(array('id' => $id), 'tbl_campaigns');

            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'campaigns',
                'module_field_id' => $id,
                'activity' => 'activity_campaigns_deleted',
                'icon' => 'fa-circle-o',
                'value1' => $leads_info->campaign_name
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);

            $campaign_to_archive = $this->db->where('id',$id)->get('tbl_campaigns')->row();

            $archived = array(
                'id' => $campaign_to_archive->id,
                'campaign_name' => $campaign_to_archive->campaign_name,
                'type' => $campaign_to_archive->type,
                'campaign_description' => $campaign_to_archive->campaign_description,
                'campaign_id' => $campaign_to_archive->campaign_id,
                'region_id' => $campaign_to_archive->region_id,
                'permission' => $campaign_to_archive->permission
            );

            $this->items_model->_table_name = 'tbl_campaigns_archived';
            $this->items_model->save($archived);

            $leads_to_archive = $this->db->where(array('campaign_id' => $id))->get('campaign_leads')->result_array();

            foreach($leads_to_archive as $lead) {

             $lead_array[] = array(
                   'id' => $lead['id'],
                   'filename' => $lead['filename'],
                   'campaign_id' => $lead['campaign_id'],
                   'company_name' => $lead['company_name'],
                   'first_name' => $lead['first_name'],
                   'last_name' => $lead['last_name'],
                   'jobtitle' => $lead['jobtitle'],
                   'email' => $lead['email'],
                   'phone_number' => $lead['phone_number'],
                   'address1' => $lead['address1'],
                   'city' => $lead['city'],
                   'zip' => $lead['zip'],
                   'country' => $lead['country'],
                   'comments' => $lead['comments'],
                   'company_size' => $lead['company_size'],
                   'industry_sector' => $lead['industry_sector'],
                   'time_called' => $lead['time_called'],
                   'lead_source' => $lead['lead_source'],
                   'permission' => $lead['permission'],
                   'list_id' => $lead['list_id'],
                   'lead_status_id' => $lead['lead_status_id'],
                   'type' => $lead['type'],
                   'referral_lead_id' => $lead['referral_lead_id'],
                   'user' => $lead['user'],
                   'updated' => $lead['updated'],
                   'starttime' => $lead['starttime'],
                   'endtime' => $lead['endtime'],
                   'qa_status' => $lead['qa_status'],
                   'deleted' => $lead['deleted'],
                   'deleted_by' => $lead['deleted_by'],
                   'deleted_time' => $lead['deleted_time'],
                   'qa_status_export' => $lead['qa_status_export'],
                   'da_status_export' => $lead['da_status_export'],
                   'dispo_date' => $lead['dispo_date'],
                   'qa_date' => $lead['qa_date'],
                   'da_date' => $lead['da_date'],
                   'title' => $lead['title'],
                   'effective_date' => $lead['effective_date'],
                   'policy_number' => $lead['policy_number'],
                   'broker_channel' => $lead['broker_channel'],
                   'product' => $lead['product'],
                   'inception_date' => $lead['inception_date'],
                   'policy_status' => $lead['policy_status'],
                   'policy_status_per_history' => $lead['policy_status_per_history'],
                   'relationship' => $lead['relationship'],
                   'id_number' => $lead['id_number'],
                   'date_of_birth' => $lead['date_of_birth'],
                   'age' => $lead['age'],
                   'gender' => $lead['gender'],
                   'work_number' => $lead['work_number'],
                   'cell_number' => $lead['cell_number'],
                   'address2' => $lead['address2'],
                   'address3' => $lead['address3'],
                   'address4' => $lead['address4'],
                   'postal_code' => $lead['postal_code'],
                   'payment_method' => $lead['payment_method'],
                   'agent_code' => $lead['agent_code'],
                   'deduction_day' => $lead['deduction_day'],
                   'debit_order_account_holder' => $lead['debit_order_account_holder'],
                   'debit_order_bank_name' => $lead['debit_order_bank_name'],
                   'debit_order_account_number' => $lead['debit_order_account_number'],
                   'debit_order_branch_code' => $lead['debit_order_branch_code'],
                   'debit_order_account_type' => $lead['debit_order_account_type'],
                   'sum_assured_amount' => $lead['sum_assured_amount'],
                   'total_premium' => $lead['total_premium'],
                   'cover_amount' => $lead['cover_amount'],
                   'total_cover' => $lead['total_cover'],
                   'state' => $lead['state'],
                   'warm_lead_indicator' => $lead['warm_lead_indicator'],
                   'linkedin_link' => $lead['linkedin_link'],
                   'alternate_number' => $lead['alternate_number'],
                   'alternative_number_a' => $lead['alternative_number_a'],
                   'alternative_number_b' => $lead['alternative_number_b'],
                   'alternative_number_c' => $lead['alternative_number_c'],
                   'alternative_number_d' => $lead['alternative_number_d'],
                   'pool_id' => $lead['pool_id'],
                   'campaign_code' => $lead['campaign_code']
             );

            }

            $this->db->insert_batch('campaign_leads_archived', $lead_array);

            $leadlist_to_archive = $this->db->where(array('campaign_id' => $id))->get('lead_list')->result_array();

            foreach($leadlist_to_archive as $llist) {

                $lead_list_array[] = array(
                    'id' => $llist['id'],
                    'list_name' => $llist['list_name'],
                    'added' => $llist['added'],
                    'campaign_id' => $llist['campaign_id'],
                    'template_id' => $llist['template_id'],
                    'active' => $llist['active']
                );
            }

            $this->db->insert_batch('lead_list_archived', $lead_list_array);

            $this->items_model->_table_name = 'tbl_campaigns';
            $this->items_model->_primary_key = 'id';
            $this->items_model->delete($id);

            $this->db->where('campaign_id', $id);
            $this->db->delete('campaign_leads');

            $this->db->where('campaign_id', $id);
            $this->db->delete('lead_list');

            $type = 'success';
            $message = 'Campaign Deleted';
            set_message($type, $message);
        // } else {
            // set_message('error', lang('there_in_no_value'));
        // }
       // $this->vicidial_delete_campaign($id);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function delete_campaign_permanent($id) {

        $this->items_model->_table_name = 'tbl_campaigns_archived';
        $this->items_model->_primary_key = 'id';
        $this->items_model->delete($id);

        $this->db->where('campaign_id', $id);
        $this->db->delete('campaign_leads_archived');

        $this->db->where('campaign_id', $id);
        $this->db->delete('lead_list_archived');

        $type = 'success';
        $message = 'Campaign Deleted';
        set_message($type, $message);

        redirect($_SERVER['HTTP_REFERER']);
    }

	public
	function create_vicidial_campaign($campaign_id) {
        return [];
		$campaign = $this->db->where('id',$campaign_id)->get('tbl_campaigns')->row();

		if(!empty($campaign)){
		$server = config_item('vicidial_server'); //'156.38.193.158';
		$api_user = '6665';
		$api_pass = '159357pocALOCapi';

		$url ="http://$server/vicidial/non_agent_api.php?source=crm&function=add_crm_campaign&user=$api_user&pass=$api_pass&campaign_id={$campaign->campaign_id}&campaign_name=".str_replace(' ','+',$campaign->campaign_name);
		$this->send_to_vicidial($url);
		}
	}

	public
	function send_to_vicidial($url){
		$ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

		print_r($output);
	}

	public
	function generate_form()
    {
        echo '<pre/>';
        $questions = $this->input->post();
		$campaign_id = $this->input->post('campaign_id');
        unset($questions['submit']);
        unset($questions['campaign_id']);
		// sort($questions);

        $total = count($questions);

		$data = array();
		for($i = 1; $i < 21; $i++){
			foreach($questions as $key => $value){
				$new_key = explode('_',$key);
				if($new_key[1] ==  $i){
					$data[$i][$key] = $value;
				}
			}
		}

		$i = 1;
		$required = '';
		$input = '';
		$op = '';
		$textinput = '';

		foreach ($data as $dat) {
				$div = '';
				$this->db->insert('campaign_questions',array('campaign_id' => $campaign_id, 'question_number' => $i, 'question_question' => $dat['question_'.$i], 'question_type' => $dat['type_'.$i], 'question_options' => $dat['options_'.$i], 'question_required' => $dat['required_'.$i]));
			foreach ($dat as $d => $v) {

				switch ($d) {
					case 'question_'.$i :
						 if($d == 'question_'.$i){
							 // echo 'yes';
							$textinput = $v;
						 };
					break;
					case 'options_'.$i :
						$options = explode(",",$v);
						foreach ($options as $option) {
							$op .= '<option value="'.$option.'">'.$option.'</option>';
						};
					break;
					case 'required_'.$i :
						if ($v == 'yes') { $required = ' required '; };
					break;
					case 'type_'.$i :
						if ($d == 'type_'.$i) {
							if (strtolower($v) == 'short text') {
								$input = '<input class="form-control" name="{names}" value="{textinput}" {required} ></input>';
							}
							if (strtolower($v) == 'long text') {
								$input = '<textarea class="form-control" name="{names}" {required} >{textinput}</textarea>';
							}
							if (strtolower($v) == 'select box') {
								$input = '<select class="form-control" name="{names}" {required} >{options}</select>';
							}
						};
					break;
				}
			}
	/*
			if (!empty($dat['question_'.$i])) {
				$div .=
					"<div class=\"row\">
						<div class=\"col-sm-6\">
							$textinput
						</div>
					<div class=\"col-sm-6\">";
						$form = str_replace('{textinput}',"",$input);
						$form2 = str_replace('{required}',$required,$form);
						$form3 = str_replace('{options}',$op,$form2);
						$form4 = str_replace('{names}','me',$form3);
				$div .= $form4.	"</div>
				</div>";
				echo $div;
			}
			// echo $dat['question_'.$i];
			*/
			$i++;
		}
		/*
		$campaign = $this->db->where('id',$campaign_id)->get('tbl_campaigns')->row();
		$lists = $this->db->where('campaign_id',$campaign_id)->get('lead_list')->result();

		// Update Webform
		$server = config_item('vicidial_server'); //'156.38.193.158';
		$api_user = '6665';
		$api_pass = '159357pocALOCapi';

		foreach($lists as $list){
			$url = "http://$server/vicidial/non_agent_api.php?source=crm&user=$api_user&pass=$api_pass&function=update_list&list_id={$list->id}&campaign_id=".$campaign->campaign_id."&web_form_address=VAR".base_url('admin/campaigns/campaign_questions/'.$campaign_id)."?entry_list_id=--A--vendor_lead_code--B--";
			$this->send_to_vicidial($url);
		}
		*/
		redirect('admin/campaigns/campaign_questions/'.$campaign_id);
    }

	public
	function campaign_questions($id)
	{
	    /*
		$data['page'] = 'Campaigns';
        $data['title'] = 'Campaign Questions'; //Page title
        $data['lead_id'] = isset($_GET['lead_id'])? $_GET['lead_id']: '0';
		$data['questions'] = $this->db->where('campaign_id',$id)->get('campaign_questions')->result_array();
		$data['id'] = $id;
        $data['subview'] = $this->load->view('admin/campaigns/campaign_questions_full', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
        */
        //redirect('admin/campaigns/campaign_questions/'.$campaign_id.'?entry_list_id='.$new_id.'&referral_id='.$lead_id);
        $data['duplicate'] = isset($_GET['duplicate'])? $_GET['duplicate']: '0';

        $data['page'] = 'Campaigns';
        $data['title'] = 'Campaign Questions'; //Page title
		$data['id'] = $id;
        $data['lead_id'] = isset($_GET['entry_list_id'])? $_GET['entry_list_id']: '0';
        $data['referral_id'] = isset($_GET['referral_id'])? $_GET['referral_id']: '0';
        $this->db->cache_on();
		$data['questions'] = $this->db->where('campaign_id',$id)->get('campaign_questions')->result_array();
        $this->db->cache_off();

		$data['leads_info'] = array();
		$data['answers'] = array();

		if($data['lead_id'] > 0){
			// $data['leads_info'] = $this->db->where('id',$data['lead_id'])->get('campaign_leads')->row();
			$leads_info = $this->items_model->check_by(array('id' => $data['lead_id']), 'campaign_leads');

			$data['campaign_name'] = $this->db->where('id',$id)->get('tbl_campaigns')->row('campaign_name');

			$data['leads_info'] = ($data['referral_id'] > 0)?$this->items_model->check_by(array('id' => $data['referral_id']), 'campaign_leads'):$leads_info;

			$data['answers'] = $this->db->where(array('lead_id'=>$data['lead_id'], 'campaign_id' => $id))->get('campaign_answers')->result();

		    $data['callRecordsFiles'] = $this->recordings($data['leads_info']->phone_number);

            $data['template'] = $this->db->where('id',$data['leads_info']->list_id)->get('lead_list')->row('template_id');

			 // get all leads status
			$status_info = $this->db->get('tbl_lead_status')->result();
			if (!empty($status_info)) {
				foreach ($status_info as $v_status) {
					$data['status_info'][$v_status->lead_type][] = $v_status;
				}
			}
			$data['assign_user'] = $this->items_model->allowad_user('55');

			$data['subview'] = $this->load->view('admin/campaigns/campaign_questions_both', $data, TRUE);

		}else{
			$data['subview'] = $this->load->view('admin/campaigns/campaign_questions_full', $data, TRUE);
		}


        $this->load->view('admin/_layout_main', $data); //page load

	}

	public
	function save_questions($id){
	    /*
		if($id > 0){
		$questions = $this->input->post();
		$lead_id = $this->input->post('lead_id');
		unset($questions['submit']);
		unset($questions['lead_id']);
		foreach($questions as $key => $value) {
			$ids = explode('_',$key);

			if(is_array($value)){
				foreach($value as $val){
					$this->db->insert('campaign_answers',array('campaign_id'=> $id, 'question_id' => $ids[2], 'question_number' => $ids[1], 'answer' => $val,'lead_id' => $lead_id));
				}
			}else{
				$this->db->insert('campaign_answers',array('campaign_id'=> $id, 'question_id' => $ids[2], 'question_number' => $ids[1], 'answer' => $value, 'lead_id' => $lead_id));
			}
		}

		$type = "success";
        $message = 'Form Saved.';
        set_message($type, $message);
        redirect('admin/campaigns/campaign_questions/' .$id);
		}
		*/

		if($id > 0){
		$questions = $this->input->post();
		$method = $this->input->post('save_method');
		$lead_id = $this->input->post('lead_id');
		// print_r($questions);
		// exit;
		unset($questions['submit']);
		unset($questions['save_method']);
		unset($questions['lead_id']);
		foreach($questions as $key => $value) {
			$ids = explode('_',$key);
			// print_r($ids);
			// break;
			if(is_array($value)){
				foreach($value as $val){
					if(empty($method)){
						$this->db->insert('campaign_answers',array('campaign_id'=> $id, 'question_id' => $ids[2], 'question_number' => $ids[1], 'answer' => $val,'lead_id' => $lead_id));
					}else{
						if(isset($ids[3])){
						$this->db->where('id',$ids[3])->update('campaign_answers', array('answer' => $val));
						}
					}
				}
			}else{
				if(empty($method)){
					$this->db->insert('campaign_answers',array('campaign_id'=> $id, 'question_id' => $ids[2], 'question_number' => $ids[1], 'answer' => $value, 'lead_id' => $lead_id));
				}else{
					if(isset($ids[3])){
						$this->db->where('id',$ids[3])->update('campaign_answers', array('answer' => $value));
					}
				}
			}
		}
	// exit;
		$type = "success";
        $message = 'Form Saved.';
        set_message($type, $message);
		redirect($_SERVER['HTTP_REFERER']);
        // redirect('admin/campaigns/campaign_questions/' .$id);
		}

	}

	public function update_webform($campaign_id){
        return [];
        $campaign = $this->db->where('id',$campaign_id)->get('tbl_campaigns')->row();
		$lists = $this->db->where('campaign_id',$campaign_id)->get('lead_list')->result();

		// Update Webform
		$server = config_item('vicidial_server'); //'156.38.193.158';
		$api_user = '6665';
		$api_pass = '159357pocALOCapi';

		foreach($lists as $list){
			$url = "http://$server/vicidial/non_agent_api.php?source=crm&user=$api_user&pass=$api_pass&function=update_list&list_id={$list->id}&campaign_id=".$campaign->campaign_id."&web_form_address=VAR".base_url('admin/campaigns/campaign_questions/'.$campaign_id)."?entry_list_id=--A--entry_list_id--B--";
			// echo $url;
			$this->send_to_vicidial($url);
		}
	}

	public function edit_form($id){
		if($id > 0){
			$data['page'] = 'Campaigns';
			$data['title'] = 'Campaign Questions'; //Page title
			$data['campaign_id'] = $id;
			$data['questions'] = $this->db->where('campaign_id',$id)->get('campaign_questions')->result();
			// print_r($data);exit;

			$data['subview'] = $this->load->view('admin/campaigns/campaign_questions', $data, TRUE);
			$this->load->view('admin/_layout_main', $data); //page load
		}
	}


	public function update_form()
	{
		 echo '<pre/>';
        $questions = $this->input->post();
		$campaign_id = $this->input->post('campaign_id');
        unset($questions['submit']);
        unset($questions['campaign_id']);
		// print_r($questions);
		$data = array();
		for($i = 1; $i < 15; $i++){
			foreach($questions as $key => $value){
				$new_key = explode('_',$key);
				if($new_key[1] ==  $i){
					$data[$i][$key] = $value;
				}
			}
		}
	//	print_r($data);
		$i = 1;
		foreach($data as $dat){
		// $this->db->insert('campaign_questions',array('campaign_id' => $campaign_id, 'question_number' => $i, 'question_question' => $dat['question_'.$i], 'question_type' => $dat['type_'.$i], 'question_options' => $dat['options_'.$i], 'question_required' => $dat['required_'.$i]));
			$this->db->where('id', $dat['id_'.$i])->update('campaign_questions', array('question_question' => $dat['question_'.$i], 'question_type' => $dat['type_'.$i], 'question_options' => $dat['options_'.$i], 'question_required' => $dat['required_'.$i]));

			$i++;
		}

		$type = "success";
        $message = 'Form Updated.';
        set_message($type, $message);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function add_referral($campaign_id, $lead_id){

		//$list_id = $this->db->where('campaign_id',$campaign_id)->get('lead_list')->row('id');
		$list_id = $this->db->where('id',$lead_id)->get('campaign_leads')->row('list_id');
		$leads_info = $this->items_model->check_by(array('id' => $lead_id), 'campaign_leads');
		$data['campaign_id'] = $campaign_id;
		$data['type'] = 'referral';
		$data['referral_lead_id'] = $lead_id;
		$data['list_id'] = $list_id;

		$this->db->insert('campaign_leads',$data);
		$new_id  =$this->db->insert_id();
		// $data['lead_id']  =$this->db->insert_id();
		// $template = $this->db->where('id',$data['leads_info']->list_id)->get('lead_list')->row('template_id');

		  // $data['subview'] = $this->load->view('admin/campaigns/campaign_questions_both', ['data' => $leads_info,
		  // 'lead_id' => $data['lead_id'],
		  // 'id' => $lead_id,
		  // 'template' => $template
		  // ], TRUE);
		  // $this->load->view('admin/_layout_main', $data);
		  redirect('admin/campaigns/campaign_questions/'.$campaign_id.'?entry_list_id='.$new_id.'&referral_id='.$lead_id);
		  //redirect('admin/campaigns/campaign_questions/'.$campaign_id.'?entry_list_id='.$lead_id.'&referral_id='.$new_id);
	}

	public function recordings($number){
		return [];
	    $callRecords = $this->call_log_model->getAudioRecordsByPhoneNumberArrayAPI($number);

		$data['callRecordsFiles'] = '';
		$callRecords = explode("\n", $callRecords);
		$lead_ids = array();
		foreach($callRecords as $record){
			if(is_numeric(substr($record, 0, 4))){
				$_lead = explode("|", $record);
				$lead_ids[$_lead[3]] = $_lead[3];
			}
		}

    if(empty($lead_ids)){
        unset($lead_ids);
        $callRecords = $this->call_log_model->getAudioRecordsByPhoneNumberArrayAPI($number);
		$data['callRecordsFiles'] = '';
		$callRecords = explode("\n", $callRecords);
		$lead_ids = array();
		foreach($callRecords as $record){
			if(is_numeric(substr($record, 0, 4))){
				$_lead = explode("|", $record);
				$lead_ids[$_lead[3]] = $_lead[3];
			}
		}
    }
		if(!empty($lead_ids)){
			foreach($lead_ids as $leads){
			   $callRecordLogs = $this->call_log_model->getAudioRecordingFiles($leads);
			   $callRecordLog[] = explode("\n", $callRecordLogs);
			 //  $callRecordLog[] = $this->call_log_model->getAudioRecordingFiles($leads);
		    }
	//	 print_r($callRecordLog);
		foreach($callRecordLog as $recordLog){
		    foreach($recordLog as $log){
			    $record_link[] = explode("|", $log);
		    }
		}
	//	 print_r($record_link);
		foreach($record_link as $l){
		    if(isset($l[4]) && trim($l[4]) > ''){
		      $callrecordinglink[] = $l[4];
		    }
		}

		$data['callRecordsFiles'] = $callrecordinglink;
		}
		return $data['callRecordsFiles'];
	}

	 public function change_qa_status($id, $status)
    {
		#1 Approved
		#2 Pending
		#3 Declined
        $data['qa_status'] = $status;
        $data['qa_date'] = date('Y-m-d H:i:s');
        $this->items_model->_table_name = 'campaign_leads';
        $this->items_model->_primary_key = 'id';
        $this->items_model->save($data, $id);
        // messages for user
        $type = "success";
        $message = lang('change_status');
        set_message($type, $message);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function change_qa_status_export($id, $status)
    {
		#1 Successfully Exported
		#2 Bad Email
		#3 Duplicate
        $data['qa_status_export'] = $status;
        //$data['qa_date'] = date('Y-m-d H:i:s');
        $this->items_model->_table_name = 'campaign_leads';
        $this->items_model->_primary_key = 'id';
        $this->items_model->save($data, $id);
        // messages for user
        $type = "success";
        $message = lang('change_status');
        set_message($type, $message);
        redirect($_SERVER['HTTP_REFERER']);
    }

     public function change_da_status_export($id, $status)
    {
		#1 Successfully Exported
		#2 Bad Email
		#3 Duplicate
        $data['da_status_export'] = $status;
        $data['da_date'] = date('Y-m-d H:i:s');
        $this->items_model->_table_name = 'campaign_leads';
        $this->items_model->_primary_key = 'id';
        $this->items_model->save($data, $id);
        // messages for user
        $type = "success";
        $message = lang('change_status');
        set_message($type, $message);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function get_next_lead($date_from, $date_to)
	{

		$date_from = urldecode($date_from);
	    $date_to = urldecode($date_to);

		if ($date_from < '2018-12-03 23:59:59') {
			  $data['search_array'] = array('lead_status_id' => 6, 'qa_status' => 0);
		  } else {
			  $data['search_array'] = array('lead_status_id' => 6, 'qa_status !=' => 0, 'dispo_date' => $date_from, 'dispo_date <= ' => $date_to);
		  }

		$lead = $this->db->where($data['search_array'])->limit(1)->get('view_campaign_leads')->row();

		if(!empty($lead)){
			$type = "success";
			$message = lang('change_status');
			$link = base_url("admin/campaigns/campaign_questions/{$lead->campaign_id}?entry_list_id={$lead->id}&date_from={$date_from}&date_to={$date_to}");
			redirect($link);
		}else{
			$type = "success";
			$message = lang('change_status');
			set_message($type, $message);
			redirect('admin/leads/sale_leads');
		}
	}

}
