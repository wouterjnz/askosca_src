<?php

/**
 * Description of supplier
 *
 * @author NaYeM
 */
class Supplier extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('supplier_model');
        $this->load->model('invoice_model');
        $this->load->model('estimates_model');
    }

    public function manage_supplier($id = NULL)
    {
        if (!empty($id)) {
            $data['active'] = 2;
            // get all supplier info by supplier id
            $this->supplier_model->_table_name = "tbl_supplier"; //table name
            $this->supplier_model->_order_by = "supplier_id";
            $data['supplier_info'] = $this->supplier_model->get_by(array('supplier_id' => $id), TRUE);

        } else {
            $data['active'] = 1;
        }
        if (!empty($data['supplier_info']) && $data['supplier_info']->supplier_status == 2) {
            $data['company'] = 1;
        } else {
            $data['person'] = 1;
        }
        $data['title'] = "Manage supplier"; //Page title
        $data['page'] = lang('supplier');

        // get all country
        $this->supplier_model->_table_name = "tbl_countries"; //table name
        $this->supplier_model->_order_by = "id";
        $data['countries'] = $this->supplier_model->get();

        // get all currencies
        $this->supplier_model->_table_name = 'tbl_currencies';
        $this->supplier_model->_order_by = 'name';
        $data['currencies'] = $this->supplier_model->get();
        // get all language
        $this->supplier_model->_table_name = 'tbl_languages';
        $this->supplier_model->_order_by = 'name';
        $data['languages'] = $this->supplier_model->get();

        $data['all_supplier_info'] = $this->db->get('tbl_supplier')->result();

        $data['subview'] = $this->load->view('admin/supplier/manage_supplier', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function import()
    {
        $data['title'] = lang('import') . ' ' . lang('supplier');
        // get all country
        $this->supplier_model->_table_name = "tbl_countries"; //table name
        $this->supplier_model->_order_by = "id";
        $data['countries'] = $this->supplier_model->get();

        // get all currencies
        $this->supplier_model->_table_name = 'tbl_currencies';
        $this->supplier_model->_order_by = 'name';
        $data['currencies'] = $this->supplier_model->get();
        // get all language
        $this->supplier_model->_table_name = 'tbl_languages';
        $this->supplier_model->_order_by = 'name';
        $data['languages'] = $this->supplier_model->get();

        $data['subview'] = $this->load->view('admin/supplier/import_supplier', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function save_imported()
    {
        //load the excel library
        $this->load->library('excel');
        ob_start();
        $file = $_FILES["upload_file"]["tmp_name"];
        if (!empty($file)) {
            $valid = false;
            $types = array('Excel2007', 'Excel5');
            foreach ($types as $type) {
                $reader = PHPExcel_IOFactory::createReader($type);
                if ($reader->canRead($file)) {
                    $valid = true;
                }
            }
            if (!empty($valid)) {
                try {
                    $objPHPExcel = PHPExcel_IOFactory::load($file);
                } catch (Exception $e) {
                    die("Error loading file :" . $e->getMessage());
                }
                //All data from excel
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                for ($x = 2; $x <= count($sheetData); $x++) {
                    // **********************
                    // Save Into leads table
                    // **********************
                    $data = $this->supplier_model->array_from_post(array('supplier_status', 'vat', 'language', 'currency', 'country'));

                    $data['name'] = trim($sheetData[$x]["A"]);
                    $data['email'] = trim($sheetData[$x]["B"]);
                    $data['short_note'] = trim($sheetData[$x]["C"]);
                    $data['phone'] = trim($sheetData[$x]["D"]);
                    $data['mobile'] = trim($sheetData[$x]["E"]);
                    $data['fax'] = trim($sheetData[$x]["F"]);
                    $data['city'] = trim($sheetData[$x]["G"]);
                    $data['zipcode'] = trim($sheetData[$x]["H"]);
                    $data['address'] = trim($sheetData[$x]["I"]);
                    $data['skype_id'] = trim($sheetData[$x]["J"]);
                    $data['twitter'] = trim($sheetData[$x]["K"]);
                    $data['facebook'] = trim($sheetData[$x]["L"]);
                    $data['linkedin'] = trim($sheetData[$x]["M"]);
                    $data['hosting_company'] = trim($sheetData[$x]["N"]);
                    $data['hostname'] = trim($sheetData[$x]["O"]);
                    $data['username'] = trim($sheetData[$x]["P"]);
                    $data['password'] = trim($sheetData[$x]["Q"]);
                    $data['port'] = trim($sheetData[$x]["R"]);

                    $this->supplier_model->_table_name = 'tbl_supplier';
                    $this->supplier_model->_primary_key = "supplier_id";
                    $id = $this->supplier_model->save($data);

                    $action = ('activity_update_company');
                    $activities = array(
                        'user' => $this->session->userdata('user_id'),
                        'module' => 'supplier',
                        'module_field_id' => $id,
                        'activity' => $action,
                        'icon' => 'fa-user',
                        'value1' => $data['name']
                    );
                    $this->supplier_model->_table_name = 'tbl_activities';
                    $this->supplier_model->_primary_key = "activities_id";
                    $this->supplier_model->save($activities);
                }
            } else {
                $type = 'error';
                $message = "Sorry your uploaded file type not allowed ! please upload XLS/CSV File ";
            }
        } else {
            $type = 'error';
            $message = "You did not Select File! please upload XLS/CSV File ";
        }
        set_message($type, $message);
        redirect('admin/supplier/manage_supplier');

    }


    public function save_supplier($id = NULL)
    {

        $data = $this->supplier_model->array_from_post(array('name', 'email', 'short_note', 'website', 'phone', 'mobile', 'fax', 'address', 'city', 'zipcode', 'currency',
            'skype_id', 'linkedin', 'facebook', 'twitter', 'language', 'country', 'vat', 'hosting_company', 'hostname', 'port', 'password', 'username', 'supplier_status'));
        if (!empty($_FILES['profile_photo']['name'])) {
            $val = $this->supplier_model->uploadImage('profile_photo');
            $val == TRUE || redirect('admin/supplier/manage_supplier');
            $data['profile_photo'] = $val['path'];
        }

        $this->supplier_model->_table_name = 'tbl_supplier';
        $this->supplier_model->_primary_key = "supplier_id";
        $return_id = $this->supplier_model->save($data, $id);
        if (!empty($id)) {
            $id = $id;
            $action = ('activity_added_new_company');
        } else {
            $id = $return_id;
            $action = ('activity_update_company');
        }
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'supplier',
            'module_field_id' => $id,
            'activity' => $action,
            'icon' => 'fa-user',
            'value1' => $data['name']
        );
        $this->supplier_model->_table_name = 'tbl_activities';
        $this->supplier_model->_primary_key = "activities_id";
        $this->supplier_model->save($activities);
        // messages for user
        $type = "success";
        $message = lang('supplier_updated');
        set_message($type, $message);
        redirect('admin/supplier/manage_supplier');
    }


    public function supplier_details($id, $action = null)
    {
        if ($action == 'add_contacts') {
            // get all language
            $this->supplier_model->_table_name = 'tbl_languages';
            $this->supplier_model->_order_by = 'name';
            $data['languages'] = $this->supplier_model->get();
            // get all location
            $this->supplier_model->_table_name = 'tbl_locales';
            $this->supplier_model->_order_by = 'name';
            $data['locales'] = $this->supplier_model->get();
            $data['company'] = $id;
            $user_id = $this->uri->segment(6);
            if (!empty($user_id)) {
                // get all user_info by user id
                $data['account_details'] = $this->supplier_model->check_by(array('user_id' => $user_id), 'tbl_account_details');

                $data['user_info'] = $this->supplier_model->check_by(array('user_id' => $user_id), 'tbl_users');
            }

        }
        $data['title'] = "View supplier Details"; //Page title
        // get all supplier details
        $this->supplier_model->_table_name = "tbl_supplier"; //table name
        $this->supplier_model->_order_by = "supplier_id";
        $data['supplier_details'] = $this->supplier_model->get_by(array('supplier_id' => $id), TRUE);

        // get all invoice by supplier id
        $this->supplier_model->_table_name = "tbl_invoices"; //table name
        $this->supplier_model->_order_by = "supplier_id";
        $data['supplier_invoices'] = $this->supplier_model->get_by(array('supplier_id' => $id), FALSE);

        // get all estimates by supplier id
        $this->supplier_model->_table_name = "tbl_estimates"; //table name
        $this->supplier_model->_order_by = "supplier_id";
        $data['supplier_estimates'] = $this->supplier_model->get_by(array('supplier_id' => $id), FALSE);

        // get supplier contatc by supplier id
        $data['supplier_contacts'] = $this->supplier_model->get_supplier_contacts($id);

        $data['subview'] = $this->load->view('admin/supplier/supplier_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function save_contact($id = NULL)
    {
        $data = $this->supplier_model->array_from_post(array('fullname', 'company', 'phone', 'mobile', 'skype', 'language', 'locale'));

        if (!empty($id)) {
            $u_data['email'] = $this->input->post('email', TRUE);
            $u_data['last_ip'] = $this->input->ip_address();
            $this->supplier_model->_table_name = 'tbl_users';
            $this->supplier_model->_primary_key = 'user_id';
            $user_id = $this->supplier_model->save($u_data, $id);
            $data['user_id'] = $user_id;
            $acount_info = $this->supplier_model->check_by(array('user_id' => $id), 'tbl_account_details');

            $this->supplier_model->_table_name = 'tbl_account_details';
            $this->supplier_model->_primary_key = 'account_details_id';
            $return_id = $this->supplier_model->save($data, $acount_info->account_details_id);

            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'supplier',
                'module_field_id' => $id,
                'activity' => ('activity_update_contact'),
                'icon' => 'fa-user',
                'value1' => $data['fullname']
            );
            $this->supplier_model->_table_name = 'tbl_activities';
            $this->supplier_model->_primary_key = "activities_id";
            $this->supplier_model->save($activities);
        } else {
            $user_data = $this->supplier_model->array_from_post(array('email', 'username', 'password'));
            $u_data['last_ip'] = $this->input->ip_address();
            $check_email = $this->supplier_model->check_by(array('email' => $user_data['email']), 'tbl_users');
            $check_username = $this->supplier_model->check_by(array('username' => $user_data['username']), 'tbl_users');

            if ($user_data['password'] == $this->input->post('confirm_password', TRUE)) {
                $u_data['password'] = $this->hash($user_data['password']);

                if (!empty($check_username)) {
                    $message['error'][] = 'This Username Already Used ! ';
                } else {
                    $u_data['username'] = $user_data['username'];
                }
                if (!empty($check_email)) {
                    $message['error'][] = 'This email Address Already Used ! ';
                } else {
                    $u_data['email'] = $user_data['email'];
                }
            } else {
                $message['error'][] = 'Sorry Your Password and Confirm Password Does not match !';
            }

            if (!empty($u_data['password']) && !empty($u_data['username']) && !empty($u_data['email'])) {
                $u_data['role_id'] = $this->input->post('role_id', true);
                $u_data['activated'] = '1';

                $this->supplier_model->_table_name = 'tbl_users';
                $this->supplier_model->_primary_key = 'user_id';
                $user_id = $this->supplier_model->save($u_data, $id);

                $data['user_id'] = $user_id;

                $this->supplier_model->_table_name = 'tbl_account_details';
                $this->supplier_model->_primary_key = 'account_details_id';
                $return_id = $this->supplier_model->save($data, $id);
                // check primary contact
                $primary_contact = $this->supplier_model->check_by(array('supplier_id' => $data['company']), 'tbl_supplier');

                if ($primary_contact->primary_contact == 0) {
                    $c_data['primary_contact'] = $return_id;
                    $this->supplier_model->_table_name = 'tbl_supplier';
                    $this->supplier_model->_primary_key = 'supplier_id';
                    $this->supplier_model->save($c_data, $data['company']);
                }

                $activities = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'supplier',
                    'module_field_id' => $id,
                    'activity' => ('activity_added_new_contact'),
                    'icon' => 'fa-user',
                    'value1' => $data['fullname']
                );
                $this->supplier_model->_table_name = 'tbl_activities';
                $this->supplier_model->_primary_key = "activities_id";
                $this->supplier_model->save($activities);
            }
        }
        // messages for user        
        $message['success'] = 'Contact Information Successfully Updated !';
        if (!empty($message['error'])) {
            $this->session->set_userdata($message);
        } else {
            $this->session->set_userdata($message);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function make_primary($user_id, $supplier_id)
    {
        $user_info = $this->supplier_model->check_by(array('user_id' => $user_id), 'tbl_account_details');

        $this->db->set('primary_contact', $user_id);
        $this->db->where('supplier_id', $supplier_id)->update('tbl_supplier');
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'supplier',
            'module_field_id' => $supplier_id,
            'activity' => ('activity_primary_contact'),
            'icon' => 'fa-user',
            'value1' => $user_info->fullname
        );
        $this->supplier_model->_table_name = 'tbl_activities';
        $this->supplier_model->_primary_key = "activities_id";
        $this->supplier_model->save($activities);

        // messages for user
        $type = "success";
        $message = lang('primary_contact_set');
        set_message($type, $message);
        redirect('admin/supplier/supplier_details/' . $supplier_id);
    }

    public function delete_contacts($supplier_id, $id)
    {
        $sbtn = $this->input->post('submit', true);
        if (!empty($sbtn)) {
            // delete into user table by user id
            $this->supplier_model->_table_name = 'tbl_supplier';
            $this->supplier_model->_order_by = 'primary_contact';
            $primary_contact = $this->supplier_model->get_by(array('primary_contact' => $id), TRUE);
            if (!empty($primary_contact)) {
                // delete into user table by user id
                $this->supplier_model->_table_name = 'tbl_account_details';
                $this->supplier_model->_order_by = 'company';
                $supplier_info = $this->supplier_model->get_by(array('company' => $supplier_id), FALSE);
                $result = count($supplier_info);
                if ($result != '1') {
                    $data['primary_contact'] = $supplier_info[1]->account_details_id;
                } else {
                    $data['primary_contact'] = 0;
                }
                $this->supplier_model->_table_name = 'tbl_supplier';
                $this->supplier_model->_primary_key = 'primary_contact';
                $this->supplier_model->save($data, $supplier_id);
            }
            $user_info = $this->supplier_model->check_by(array('user_id' => $id), 'tbl_account_details');
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'supplier',
                'module_field_id' => $id,
                'activity' => ('activity_deleted_contact'),
                'icon' => 'fa-user',
                'value1' => $user_info->fullname
            );
            $this->supplier_model->_table_name = 'tbl_account_details';
            $this->supplier_model->delete_multiple(array('user_id' => $id));

            $this->supplier_model->_table_name = "tbl_private_message_send"; //table name
            $this->supplier_model->_order_by = "send_user_id";
            $check_send_id = $this->supplier_model->get_by(array('send_user_id' => $id), FALSE);
            if (!empty($check_send_id)) {
                $where = array('send_user_id' => $id);
            }
            $this->supplier_model->_table_name = "tbl_private_message_send"; //table name
            $this->supplier_model->_order_by = "receive_user_id";
            $check_receive_id = $this->supplier_model->get_by(array('receive_user_id' => $id), FALSE);
            if (!empty($check_receive_id)) {
                $where = array('receive_user_id' => $id);
            }
            if (!empty($check_send_id) || !empty($check_receive_id)) {
                $this->supplier_model->_table_name = 'tbl_private_message_send';
                $this->supplier_model->delete_multiple($where);
            }

            $this->supplier_model->_table_name = 'tbl_activities';
            $this->supplier_model->delete_multiple(array('user' => $id));

            $this->supplier_model->_table_name = 'tbl_expense';
            $this->supplier_model->_order_by = 'user_id';
            $expense_info = $this->supplier_model->get_by(array('user_id' => $id), FALSE);

            foreach ($expense_info as $v_expense) {
                $this->supplier_model->_table_name = 'tbl_expense_bill_copy';
                $this->supplier_model->delete_multiple(array('expense_id' => $v_expense->expense_id));
            }

            $this->supplier_model->_table_name = 'tbl_expense';
            $this->supplier_model->delete_multiple(array('user_id' => $id));

            $this->supplier_model->_table_name = 'tbl_payments';
            $this->supplier_model->delete_multiple(array('paid_by' => $id));

            // delete all tbl_quotations by id
            $this->supplier_model->_table_name = 'tbl_quotations';
            $this->supplier_model->_order_by = 'user_id';
            $quotations_info = $this->supplier_model->get_by(array('user_id' => $id), FALSE);

            if (!empty($quotations_info)) {
                foreach ($quotations_info as $v_quotations) {
                    $this->supplier_model->_table_name = 'tbl_quotation_details';
                    $this->supplier_model->delete_multiple(array('quotations_id' => $v_quotations->quotations_id));
                }
            }
            $this->supplier_model->_table_name = 'tbl_quotations';
            $this->supplier_model->delete_multiple(array('user_id' => $id));

            $this->supplier_model->_table_name = 'tbl_quotationforms';
            $this->supplier_model->delete_multiple(array('quotations_created_by_id' => $id));
            $this->supplier_model->_table_name = 'tbl_users';
            $this->supplier_model->delete_multiple(array('user_id' => $id));

            $this->supplier_model->_table_name = 'tbl_user_role';
            $this->supplier_model->delete_multiple(array('user_id' => $id));

            $this->supplier_model->_table_name = 'tbl_inbox';
            $this->supplier_model->delete_multiple(array('user_id' => $id));

            $this->supplier_model->_table_name = 'tbl_sent';
            $this->supplier_model->delete_multiple(array('user_id' => $id));

            $this->supplier_model->_table_name = 'tbl_draft';
            $this->supplier_model->delete_multiple(array('user_id' => $id));

            $this->supplier_model->_table_name = 'tbl_tickets';
            $this->supplier_model->delete_multiple(array('reporter' => $id));

            $this->supplier_model->_table_name = 'tbl_tickets_replies';
            $this->supplier_model->delete_multiple(array('replierid' => $id));

            // messages for user
            $type = "success";
            $message = lang('delete_contact');
            set_message($type, $message);
            redirect('admin/supplier/supplier_details/' . $supplier_id);
        } else {
            $data['title'] = "Delete supplier Contact"; //Page title
            $data['user_info'] = $this->db->where('user_id', $id)->get('tbl_account_details')->row();
            $data['supplier_id'] = $supplier_id;
            $data['subview'] = $this->load->view('admin/user/delete_user', $data, TRUE);
            $this->load->view('admin/_layout_main', $data); //page load
        }
    }

    public
    function delete_supplier($supplier_id, $yes = null)
    {
        $sbtn = $this->input->post('submit', true);
        if (!empty($sbtn) && !empty($yes)) {
            // delete into user table by user id
            $this->supplier_model->_table_name = 'tbl_account_details';
            $this->supplier_model->_order_by = 'company';
            $supplier_info = $this->supplier_model->get_by(array('company' => $supplier_id), FALSE);
            if (!empty($supplier_info)) {
                foreach ($supplier_info as $v_supplier) {
                    $this->supplier_model->delete_multiple(array('account_details_id' => $v_supplier->account_details_id));
                    $this->supplier_model->_table_name = 'tbl_users';
                    $this->supplier_model->delete_multiple(array('user_id' => $v_supplier->user_id));
                    $this->supplier_model->_table_name = 'tbl_inbox';
                    $this->supplier_model->delete_multiple(array('user_id' => $v_supplier->user_id));

                    $this->supplier_model->_table_name = 'tbl_sent';
                    $this->supplier_model->delete_multiple(array('user_id' => $v_supplier->user_id));

                    $this->supplier_model->_table_name = 'tbl_draft';
                    $this->supplier_model->delete_multiple(array('user_id' => $v_supplier->user_id));

                    $this->supplier_model->_table_name = 'tbl_expense';
                    $this->supplier_model->delete_multiple(array('user_id' => $v_supplier->user_id));

                    $this->supplier_model->_table_name = 'tbl_tickets';
                    $this->supplier_model->delete_multiple(array('reporter' => $v_supplier->user_id));
                    //save data into table.
                    // Bugs
                    $bugs_info = $this->db->where('reporter', $v_supplier->user_id)->get('tbl_bug')->result();
                    if (!empty($bugs_info)) {
                        foreach ($bugs_info as $v_bugs) {
                            $this->supplier_model->_table_name = "tbl_task_attachment"; //table name
                            $this->supplier_model->_order_by = "bug_id";
                            $files_info = $this->supplier_model->get_by(array('bug_id' => $v_bugs->bug_id), FALSE);
                            foreach ($files_info as $v_files) {
                                $this->supplier_model->_table_name = "tbl_task_uploaded_files"; //table name
                                $this->supplier_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                            }
                            //delete into table.
                            $this->supplier_model->_table_name = "tbl_task_attachment"; // table name
                            $this->supplier_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                            //delete data into table.
                            $this->supplier_model->_table_name = "tbl_task_comment"; // table name
                            $this->supplier_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                            //delete data into table.
                            $this->supplier_model->_table_name = "tbl_task"; // table name
                            $this->supplier_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                            $this->supplier_model->_table_name = "tbl_bug"; // table name
                            $this->supplier_model->delete_multiple(array('reporter' => $v_supplier->user_id));
                        }

                    }

                }
            }
            // delete all leads by id
            $this->supplier_model->_table_name = 'tbl_leads';
            $this->supplier_model->_order_by = 'supplier_id';
            $leads_info = $this->supplier_model->get_by(array('supplier_id' => $supplier_id), FALSE);
            if (!empty($leads_info)) {
                foreach ($leads_info as $v_leads) {
                    //delete data into table.
                    $this->supplier_model->_table_name = "tbl_calls"; // table name
                    $this->supplier_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                    //delete data into table.
                    $this->supplier_model->_table_name = "tbl_mettings"; // table name
                    $this->supplier_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                    //delete data into table.
                    $this->supplier_model->_table_name = "tbl_task_comment"; // table name
                    $this->supplier_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                    $this->supplier_model->_table_name = "tbl_task_attachment"; //table name
                    $this->supplier_model->_order_by = "leads_id";
                    $files_info = $this->supplier_model->get_by(array('leads_id' => $v_leads->leads_id), FALSE);

                    if (!empty($files_info)) {
                        foreach ($files_info as $v_files) {
                            //save data into table.
                            $this->supplier_model->_table_name = "tbl_task_uploaded_files"; // table name
                            $this->supplier_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                        }
                    }
                    //save data into table.
                    $this->supplier_model->_table_name = "tbl_task_attachment"; // table name
                    $this->supplier_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                    $this->supplier_model->_table_name = 'tbl_leads';
                    $this->supplier_model->_primary_key = 'leads_id';
                    $this->supplier_model->delete($v_leads->leads_id);
                }
            }

            // project
            // delete all leads by id
            $this->supplier_model->_table_name = 'tbl_project';
            $this->supplier_model->_order_by = 'supplier_id';
            $project_info = $this->supplier_model->get_by(array('supplier_id' => $supplier_id), FALSE);
            if (!empty($project_info)) {
                foreach ($project_info as $v_project) {
                    //delete data into table.
                    $this->supplier_model->_table_name = "tbl_task_comment"; // table name
                    $this->supplier_model->delete_multiple(array('project_id' => $v_project->project_id));

                    $this->supplier_model->_table_name = "tbl_task_attachment"; //table name
                    $this->supplier_model->_order_by = "task_id";
                    $files_info = $this->supplier_model->get_by(array('project_id' => $v_project->project_id), FALSE);
                    if (!empty($files_info)) {
                        foreach ($files_info as $v_files) {
                            //save data into table.
                            $this->supplier_model->_table_name = "tbl_task_uploaded_files"; // table name
                            $this->supplier_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                        }
                    }
                    //save data into table.
                    $this->supplier_model->_table_name = "tbl_task_attachment"; // table name
                    $this->supplier_model->delete_multiple(array('project_id' => $v_project->project_id));

                    //save data into table.
                    $this->supplier_model->_table_name = "tbl_milestones"; // table name
                    $this->supplier_model->delete_multiple(array('project_id' => $v_project->project_id));

                    // tasks
                    $taskss_info = $this->db->where('project_id', $v_project->project_id)->get('tbl_task')->result();
                    if (!empty($taskss_info)) {
                        foreach ($taskss_info as $v_taskss) {

                            $this->supplier_model->_table_name = "tbl_task_attachment"; //table name
                            $this->supplier_model->_order_by = "task_id";
                            $files_info = $this->supplier_model->get_by(array('task_id' => $v_taskss->task_id), FALSE);
                            foreach ($files_info as $v_files) {
                                $this->supplier_model->_table_name = "tbl_task_uploaded_files"; //table name
                                $this->supplier_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                            }
                            //delete into table.
                            $this->supplier_model->_table_name = "tbl_task_attachment"; // table name
                            $this->supplier_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                            //delete data into table.
                            $this->supplier_model->_table_name = "tbl_task_comment"; // table name
                            $this->supplier_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                            $this->supplier_model->_table_name = "tbl_task"; // table name
                            $this->supplier_model->_primary_key = "task_id"; // $id
                            $this->supplier_model->delete($v_taskss->task_id);
                        }

                    }

                    // Bugs
                    $bugs_info = $this->db->where('project_id', $v_project->project_id)->get('tbl_bug')->result();
                    if (!empty($bugs_info)) {
                        foreach ($bugs_info as $v_bugs) {


                            $this->supplier_model->_table_name = "tbl_task_attachment"; //table name
                            $this->supplier_model->_order_by = "bug_id";
                            $files_info = $this->supplier_model->get_by(array('bug_id' => $v_bugs->bug_id), FALSE);
                            foreach ($files_info as $v_files) {
                                $this->supplier_model->_table_name = "tbl_task_uploaded_files"; //table name
                                $this->supplier_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                            }
                            //delete into table.
                            $this->supplier_model->_table_name = "tbl_task_attachment"; // table name
                            $this->supplier_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                            //delete data into table.
                            $this->supplier_model->_table_name = "tbl_task_comment"; // table name
                            $this->supplier_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                            //delete data into table.
                            $this->supplier_model->_table_name = "tbl_task"; // table name
                            $this->supplier_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                            $this->supplier_model->_table_name = "tbl_bug"; // table name
                            $this->supplier_model->_primary_key = "bug_id"; // $id
                            $this->supplier_model->delete($v_bugs->bug_id);
                        }

                    }

                    $this->supplier_model->_table_name = 'tbl_project';
                    $this->supplier_model->_primary_key = 'project_id';
                    $this->supplier_model->delete($v_project->project_id);
                }
            }

            // delete all invoice by id
            $invoice_info = $this->db->where('supplier_id', $supplier_id)->get('tbl_invoices')->result();
            if (!empty($invoice_info)) {
                foreach ($invoice_info as $v_invoice) {
                    // delete all payment info by id
                    $this->supplier_model->_table_name = 'tbl_payments';
                    $this->supplier_model->delete_multiple(array('invoices_id' => $v_invoice->invoices_id));
                }
            }
            $this->supplier_model->_table_name = 'tbl_invoices';
            $this->supplier_model->delete_multiple(array('supplier_id' => $supplier_id));

            // delete all project by id
            $this->supplier_model->_table_name = 'tbl_estimates';
            $this->supplier_model->_order_by = 'supplier_id';
            $estimates_info = $this->supplier_model->get_by(array('supplier_id' => $supplier_id), FALSE);
            if (!empty($estimates_info)) {
                foreach ($estimates_info as $v_estimates) {
                    $this->supplier_model->_table_name = 'tbl_estimate_items';
                    $this->supplier_model->delete_multiple(array('estimates_id' => $v_estimates->estimates_id));

                }
            }
            $this->supplier_model->_table_name = 'tbl_estimates';
            $this->supplier_model->delete_multiple(array('supplier_id' => $supplier_id));
            // delete all tbl_quotations by id
            $this->supplier_model->_table_name = 'tbl_quotations';
            $this->supplier_model->_order_by = 'supplier_id';
            $quotations_info = $this->supplier_model->get_by(array('supplier_id' => $supplier_id), FALSE);

            if (!empty($quotations_info)) {
                foreach ($quotations_info as $v_quotations) {
                    $this->supplier_model->_table_name = 'tbl_quotation_details';
                    $this->supplier_model->delete_multiple(array('quotations_id' => $v_quotations->quotations_id));
                }
            }
            $this->supplier_model->_table_name = 'tbl_quotations';
            $this->supplier_model->delete_multiple(array('supplier_id' => $supplier_id));

            $this->supplier_model->_table_name = 'tbl_transactions';
            $this->supplier_model->delete_multiple(array('paid_by' => $supplier_id));

            $user_info = $this->supplier_model->check_by(array('supplier_id' => $supplier_id), 'tbl_supplier');
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'supplier',
                'module_field_id' => $this->session->userdata('user_id'),
                'activity' => ('activity_deleted_supplier'),
                'icon' => 'fa-user',
                'value1' => $user_info->name
            );
            $this->supplier_model->_table_name = 'tbl_activities';
            $this->supplier_model->_primary_key = "activities_id";
            $this->supplier_model->save($activities);

            // deletre into tbl_account details by user id
            $this->supplier_model->_table_name = 'tbl_supplier';
            $this->supplier_model->_primary_key = 'supplier_id';
            $this->supplier_model->delete($supplier_id);


            // messages for user
            $type = "success";
            $message = lang('delete_supplier');
            set_message($type, $message);
            redirect('admin/supplier/manage_supplier');
        } else {
            $data['title'] = "Delete supplier "; //Page title
            $data['supplier_info'] = $this->db->where('supplier_id', $supplier_id)->get('tbl_supplier')->row();
            $data['subview'] = $this->load->view('admin/supplier/delete_supplier', $data, TRUE);
            $this->load->view('admin/_layout_main', $data); //page load
        }
    }

    function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }

}
