<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Regions extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('items_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    public 
	function index($id = NULL)
    {
        $client = 'askosca';
	  
		if($client == 'askosca'){
			$table = 'tbl_regions';
			$table_id = 'id';
		}else{
			$table = 'tbl_regions';
			$table_id = 'id';
		}
		
        $data['title'] = 'All Regions';
        if (!empty($id)) {
            $data['active'] = 2;
            $can_edit = $this->items_model->can_action($table, 'edit', array('id' => $table_id));
            // if (!empty($can_edit)) {
                $data['region_info'] = $this->items_model->check_by(array('id' => $id), $table);
            // }
        } else {
            $data['active'] = 1;
        }
     
        $data['assign_user'] = $this->items_model->allowad_user('55');
        $data['all_regions'] = $this->items_model->get_permission($table);

        $data['subview'] = $this->load->view('admin/regions/all_regions', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public
    function saved_region($id = NULL)
    {

		$table = 'tbl_regions';
		$table_id = 'id';
		
        $this->items_model->_table_name = $table;
        $this->items_model->_primary_key = $table_id;

        $data = $this->items_model->array_from_post(array('region'));
        // update root category
        // $where = array('client_id' => $data['client_id'], 'lead_name' => $data['lead_name']);
        $where = array('region' => $data['region']);
        // duplicate value check in DB
        if (!empty($id)) { // if id exist in db update data
            $leads_id = array($table_id.' !=' => $id);
        } else { // if id is not exist then set id as null
            $leads_id = null;
        }

        // check whether this input data already exist or not
        $check_leads = $this->items_model->check_update($table, $where, $table_id);
        if (!empty($check_leads)) { // if input data already exist show error alert
            // massage for user
            $type = 'error';
            $msg = "<strong style='color:#000'>" . $data['region'] . '</strong>  ' . lang('already_exist');
        } else { // save and update query
           
            $return_id = $this->items_model->save($data, $id);
            if (!empty($id)) {
                $id = $id;
                $action = 'activity_update_region';
                $msg = 'Region Updated';
            } else {
                $id = $return_id;
                $action = 'activity_save_region';
                $msg = 'Region Saved';
            }
			
            save_custom_field(5, $id);
            
			$activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'region',
                'module_field_id' => $id,
                'activity' => $action,
                'icon' => 'fa-circle-o',
                'value1' => $data['region']
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);
            // messages for user
            $type = "success";
			// $this->create_vicidial_region($id);
			// exit;
        }
        $message = $msg;
        set_message($type, $message);
        redirect('admin/regions');
    }

    public
    function delete_region($id)
    {
        $can_delete = $this->items_model->can_action('tbl_regions', 'delete', array('id' => $id));
        // if (!empty($can_delete)) {
            $leads_info = $this->items_model->check_by(array('id' => $id), 'tbl_regions');
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'regions',
                'module_field_id' => $id,
                'activity' => 'activity_regions_deleted',
                'icon' => 'fa-circle-o',
                'value1' => $leads_info->campaign_name
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);


            $this->items_model->_table_name = 'tbl_regions';
            $this->items_model->_primary_key = 'id';
            $this->items_model->delete($id);

            $type = 'success';
            $message = 'Regions Deleted';
            set_message($type, $message);
        // } else {
            // set_message('error', lang('there_in_no_value'));
        // }
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public 
	function create_vicidial_regionsn($region_id) {
		$campaign = $this->db->where('id',$region_id)->get('tbl_regions')->row();
		
		if(!empty($campaign)){
		$server = config_item('vicidial_server'); //'156.38.193.158';
		$api_user = '6665';
		$api_pass = '159357pocALOCapi';
		
		$url ="http://$server/vicidial/non_agent_api.php?source=crm&function=add_crm_campaign&user=$api_user&pass=$api_pass&campaign_id={$campaign->campaign_id}&campaign_name=".str_replace(' ','+',$campaign->campaign_name);
		$this->send_to_vicidial($url);
		}
	}
	
	public 
	function send_to_vicidial($url){
		$ch = curl_init(); 
	
        // set url 
        curl_setopt($ch, CURLOPT_URL, $url); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch); 
		
		print_r($output);
	}
	
	public function update_webform($campaign_id){
		$campaign = $this->db->where('id',$campaign_id)->get('tbl_campaigns')->row();
		$lists = $this->db->where('campaign_id',$campaign_id)->get('lead_list')->result();
		
		// Update Webform
		$server = config_item('vicidial_server'); //'156.38.193.158';
		$api_user = '6665';
		$api_pass = '159357pocALOCapi';
		
		foreach($lists as $list){
			$url = "http://$server/vicidial/non_agent_api.php?source=crm&user=$api_user&pass=$api_pass&function=update_list&list_id={$list->id}&campaign_id=".$campaign->campaign_id."&web_form_address=".urlencode(base_url('admin/campaigns/campaign_questions/'.$campaign_id));
			$this->send_to_vicidial($url);
		}
	}
	
	
}
