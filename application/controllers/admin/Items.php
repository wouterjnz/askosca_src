<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Items extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('items_model');
        $this->load->model('stock_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

	public function print_barcode($id)
	{
		if (!empty($id)) {
            $data['active'] = 2;
            $data['items_info'] = $this->items_model->check_by(array('saved_items_id' => $id), 'tbl_saved_items');
        } else {
            $data['active'] = 1;
        }
		// print_r($data);
		// exit;
		// $data['subview'] = $this->load->view('admin/items/items_print', $data, TRUE);
        // $this->load->view('admin/_layout_main', $data); //page load
        $this->load->view('admin/items/items_print', $data); //page load
	}
	
    public function items_list($id = NULL) {
        $data['title'] = lang('all_items');
        if (!empty($id)) {
            $data['active'] = 2;
            $data['items_info'] = $this->items_model->check_by(array('saved_items_id' => $id), 'tbl_saved_items');
        } else {
            $data['active'] = 1;
        }
	
	
	$this->stock_model->_table_name = "tbl_stock_category"; //table name
        $this->stock_model->_order_by = "stock_category_id";
        $all_cate_info = $this->stock_model->get();
        // get all category info and designation info
        foreach ($all_cate_info as $v_cate_info) {
            $data['all_category_info'][$v_cate_info->stock_category] = $this->stock_model->get_sub_category_by_id($v_cate_info->stock_category_id);
        }

        $all_stock_info = $this->stock_model->get_all_stock_info();
        foreach ($all_stock_info as $v_stock_info) {
            $data['all_stock_info'][$v_stock_info->stock_category][$v_stock_info->stock_sub_category] = $this->stock_model->get_all_stock_info($v_stock_info->stock_sub_category_id);
        }
		
		
        $data['subview'] = $this->load->view('admin/items/items_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function saved_items($id = NULL) {
        
    if (isset($_POST['action']) && $_POST['action'] == 'Transfer') {
		$this->transfer_items($id);
		exit;
	}
	
	spl_autoload_register(function($class) {
		// project-specific namespace prefix
		$prefix = 'Picqer\\Barcode\\';

		// base directory for the namespace prefix
		$base_dir = './plugins/barcode-generator/src/';

		// does the class use the namespace prefix?
		$len = strlen($prefix);
		if (strncmp($prefix, $class, $len) !== 0) {
			// no, move to the next registered autoloader
			return;
		}

		// get the relative class name
		$relative_class = substr($class, $len);

		// replace the namespace prefix with the base directory, replace namespace
		// separators with directory separators in the relative class name, append
		// with .php
		$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

		// if the file exists, require it
		if (file_exists($file)) {
			require $file;
		}
	});
		include('./plugins/barcode-generator/src/BarcodeGeneratorJPG.php');
		// require_once('plugins/barcode-generator/src/BarcodeGeneratorJPG.php');
		// echo $this->load->library('barcode-generator/src/BarcodeGeneratorJPG');
		// exit;
        $this->items_model->_table_name = 'tbl_saved_items';
        $this->items_model->_primary_key = 'saved_items_id';
		
        $data = $this->items_model->array_from_post(array('item_name', 'item_desc', 'unit_cost', 'quantity', 'item_tax_rate','warehouse','purchase_date','stock_sub_category_id','supplier_id','supplier_invoice_no'));
		
        // update root category
        $where = array('item_name' => $data['item_name']);
        // duplicate value check in DB
        if (!empty($id)) { // if id exist in db update data
            $saved_items_id = array('saved_items_id !=' => $id);
        } else { // if id is not exist then set id as null
            $saved_items_id = null;
        }

        // check whether this input data already exist or not
       // $check_items = $this->items_model->check_update('tbl_saved_items', $where, $saved_items_id);
        //$check_items = $this->items_model->check_update('tbl_saved_items', $where, array('warehouse = ' => $data['warehouse']));
        $check_items = 0;
        if (!empty($check_items)) {	
			// if input data already exist show error alert
            // massage for user
            $type = 'error';
            $msg = "<strong style='color:#000'>" . $data['item_name'] . '</strong>  ' . lang('already_exist');
        } else { // save and update query                        
            $sub_total = $data['unit_cost'] * $data['quantity'];
            $data['item_tax_total'] = ($data['item_tax_rate'] / 100) * $sub_total;
            $data['total_cost'] = $sub_total + $data['item_tax_total'];
            $return_id = $this->items_model->save($data, $id);
			if(empty($id)){
				$this->insert_barcode($return_id, $this->input->post('barcode'));
			}
            if (!empty($id)) {
                $id = $id;
                $action = 'activity_update_items';
                $msg = lang('update_items');
            } else {
                $id = $return_id;
                $action = 'activity_save_items';
                $msg = lang('save_items');
            }
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'items',
                'module_field_id' => $id,
                'activity' => $action,
                'icon' => 'fa-circle-o',
                'value1' => $data['item_name']
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);
            // messages for user
            $type = "success";
        }
        $message = $msg;
        set_message($type, $message);
        redirect('admin/items/items_list');
    }
    
    
    public function transfer_items($id = null){
		
		spl_autoload_register(function($class) {
		// project-specific namespace prefix
		$prefix = 'Picqer\\Barcode\\';

		// base directory for the namespace prefix
		$base_dir = './plugins/barcode-generator/src/';

		// does the class use the namespace prefix?
		$len = strlen($prefix);
		if (strncmp($prefix, $class, $len) !== 0) {
			// no, move to the next registered autoloader
			return;
		}

		// get the relative class name
		$relative_class = substr($class, $len);

		// replace the namespace prefix with the base directory, replace namespace
		// separators with directory separators in the relative class name, append
		// with .php
		$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

		// if the file exists, require it
		if (file_exists($file)) {
			require $file;
		}
	});
		include('./plugins/barcode-generator/src/BarcodeGeneratorJPG.php');
		
		 $this->items_model->_table_name = 'tbl_saved_items';
        $this->items_model->_primary_key = 'saved_items_id';
		
        $data = $this->items_model->array_from_post(array('item_name', 'item_desc', 'unit_cost', 'quantity', 'item_tax_rate','warehouse','purchase_date','stock_sub_category_id','supplier_id','supplier_invoice_no'));
		
        // update root category
        $where = array('item_name' => $data['item_name']);
        // duplicate value check in DB
        if (!empty($id)) { // if id exist in db update data
            $saved_items_id = array('saved_items_id !=' => $id);
        } else { // if id is not exist then set id as null
            $saved_items_id = null;
        }
	
        // check whether this input data already exist or not
        $check_items = $this->items_model->check_update('tbl_saved_items', $where, array('warehouse = ' => $data['warehouse']));
		
		        if (!empty($check_items)) {	
			$new_quantity = $data['quantity'];
		// echo "CHECKED";
		// exit;
			// if input data already exist show error alert
            // massage for user
            // $type = 'error';
            // $msg = "<strong style='color:#000'>" . $data['item_name'] . '</strong>  ' . lang('already_exist');
			$quantity = $check_items[0]->quantity;
			$data['quantity'] = $data['quantity'] + $quantity; 
			// print_r($data);
			// exit;
			$sub_total = $data['unit_cost'] * $data['quantity'];
            $data['item_tax_total'] = ($data['item_tax_rate'] / 100) * $sub_total;
            $data['total_cost'] = $sub_total + $data['item_tax_total'];
			$this->items_model->save($data, $check_items[0]->saved_items_id);
			
			
			
			 if (!empty($id)) {
				// update old items quanity after the transfer.
			
				unset($data);
				$old_quantity = $this->items_model->array_from_post(array('old_quantity'));  
				$calc_quantity = ($old_quantity['old_quantity'] - $new_quantity);	 
				
				$data['quantity'] = $calc_quantity;
				
				$this->items_model->save($data, $id);
            }
			
			$id = $check_items[0]->saved_items_id;
			$action = 'activity_update_transfer_item';
			$msg = 'Items Transferred!';
			
			 $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'items',
                'module_field_id' => $id,
                'activity' => $action,
                'icon' => 'fa-circle-o',
                'value1' => $data['item_name']
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);
            // messages for user
            $type = "success";
        } else {
			// save and update query                        
			// echo "NEW";
			// exit;
            $sub_total = $data['unit_cost'] * $data['quantity'];
            $data['item_tax_total'] = ($data['item_tax_rate'] / 100) * $sub_total;
            $data['total_cost'] = $sub_total + $data['item_tax_total'];
            $return_id = $this->items_model->save($data, null);
			// $return_id = 0;	
			$this->insert_barcode($return_id, $this->input->post('barcode'));
			
            if (!empty($id)) {
				// update old items quanity after the transfer.
				$new_quantity = $data['quantity'];
				unset($data);
				$old_quantity = $this->items_model->array_from_post(array('old_quantity'));  
				$calc_quantity = ($old_quantity['old_quantity'] - $new_quantity);	 
				
				$data['quantity'] = $calc_quantity;
				// print_r($data);
				$this->items_model->save($data, $id);
            } else {
                $id = $return_id;
                $action = 'activity_new_transfer_item';
                $msg = 'Item Transferred and Saved!';
            }
			
			
			// echo "NEW";
			// exit;
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'items',
                'module_field_id' => $id,
                'activity' => $action,
                'icon' => 'fa-circle-o',
                'value1' => $data['item_name']
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);
            // messages for user
            $type = "success";
        }
        $message = $msg;
        set_message($type, $message);
        redirect('admin/items/items_list');
	}
    
    public function delete_items($id) {
        $items_info = $this->items_model->check_by(array('saved_items_id' => $id), 'tbl_saved_items');
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'items',
            'module_field_id' => $id,
            'activity' => 'activity_items_deleted',
            'icon' => 'fa-circle-o',
            'value1' => $items_info->item_name
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        $this->items_model->_table_name = 'tbl_saved_items';
        $this->items_model->_primary_key = 'saved_items_id';
        $this->items_model->delete($id);
        $type = 'success';
        $message = lang('items_deleted');
        set_message($type, $message);
        redirect('admin/items/items_list');
    }

	public function insert_barcode($return_id, $input_string)
	{
		// $hash = hash('crc32b', $return_id); // Generate a hash value of pid to a 8 char string
		$hash = $input_string;
		if(empty($hash)){
			$hash = hash('crc32b', $return_id);
		}
		
		$generator = new Picqer\Barcode\BarcodeGeneratorJPG();
		$str_base64 = base64_encode($generator->getBarcode($hash, $generator::TYPE_CODE_128, 1));
		// $out = '<img src="data:image/jpg;base64,'. $str_base64 .'">';
		// echo 'id: ' . $lastId;
		// echo 'bcode: ' . $str_base64;
		// exit;
		$barcode_string = $input_string;
		if(empty($input_string)){
			$barcode_string = $return_id;
		}
		$this->db->where('saved_items_id', $return_id);
		$this->db->update('tbl_saved_items', ['barcode' => $str_base64, 'barcode_string' => $hash]);
	}
	
	public function initialize_barcode()
	{
		spl_autoload_register(function($class) {
		// project-specific namespace prefix
		$prefix = 'Picqer\\Barcode\\';

		// base directory for the namespace prefix
		$base_dir = './plugins/barcode-generator/src/';

		// does the class use the namespace prefix?
		$len = strlen($prefix);
		if (strncmp($prefix, $class, $len) !== 0) {
			// no, move to the next registered autoloader
			return;
		}

		// get the relative class name
		$relative_class = substr($class, $len);

		// replace the namespace prefix with the base directory, replace namespace
		// separators with directory separators in the relative class name, append
		// with .php
		$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

		// if the file exists, require it
		if (file_exists($file)) {
			require $file;
		}
	});
		include('./plugins/barcode-generator/src/BarcodeGeneratorJPG.php');
	}
	
	public function generate_barcodes()
	{
		$this->initialize_barcode();
		$this->db->where('barcode', null);
		$query = $this->db->get('tbl_saved_items')->result_array();
	   
		foreach($query as $q){
			$this->insert_barcode($q['saved_items_id'], '');
		}
		
	}
	
}
