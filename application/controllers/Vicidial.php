<?php

/**
 * Description of Vicidial components
 *
 * @author Jamie
 */
class Vicidial extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->load->model('tickets_model');
		// 0823353713
    }
    
	public function create_libertech_werform(){
			$this->db->query("CREATE TABLE IF NOT EXISTS libertech_webform (
			id int(9) unsigned NOT NULL AUTO_INCREMENT,
			title VARCHAR(10) DEFAULT NULL,
			surname VARCHAR(30) DEFAULT NULL,
			full_name VARCHAR(40) DEFAULT NULL,
			date_of_birth VARCHAR(20) DEFAULT NULL,
			language VARCHAR(20) DEFAULT NULL,
			id_number VARCHAR(16) DEFAULT NULL,
			gender VARCHAR(10) DEFAULT NULL,
			occupation VARCHAR(40) DEFAULT  NULL,
			
			postal_address VARCHAR(30) DEFAULT NULL,
			postal_box_no VARCHAR(30) DEFAULT NULL,
			postal_area VARCHAR(30) DEFAULT  NULL,
			postal_suburb VARCHAr(30) DEFAULT NULL,
			postal_town VARCHAR(30) DEFAULt NULL,
			postal_code VARCHAR(30) DEFAULT NULL,
			residential_address VARCHAR(30) DEFAULT NULL,
			street_no VARCHAR(30) DEFAULT NULL,
			street_name VARCHAR(30) DEFAULT NULL,
			estate VARCHAR(30) DEFAULT NULL,
			unit_no VARCHAR(30) DEFAULT NULL,
			suburb VARCHAR(30) DEFAULT NULL,
			town VARCHAR(30) DEFAULT NULL,
			residential_code VARCHAR(30) DEFAULT NULL,
			
			cell_no VARCHAR(20) DEFAULT NULL,
			tel_no VARCHAR(20) DEFAULT NULL,
			work_no VARCHAR(20) DEFAULT NULL,
			email VARCHAR(20) DEFAULT NULL,
			
			product_name VARCHAR(30) DEFAULT NULL,
			product_amount VARCHAR(30) DEFAULT NULL,
			product_period VARCHAR(10) DEFAULT NULL,
			
			account_holder VARCHAR(30) DEFAULT NULL,
			bank_name VARCHAR(30) DEFAULT NULL,
			branch_code VARCHAR(30) DEFAULT NULL,
			account_type VARCHAR(30) DEFAULT NULL,
			account_number VARCHAR(30) DEFAULT NULL,
			debit_day VARCHAR(30) NULL,
			
			call_recording_reference TEXT DEFAULT NULL,
			correspondence VARCHAR(30) DEFAULT NULL,
			marketing VARCHAR(30) DEFAULT NULL,
			agent_information TEXT DEFAULT NULL,
			
			date_joined VARCHAR(20)  DEFAULT NULL,
			campaign_code VARCHAR(30)  DEFAULT NULL,
			agent_name VARCHAR(20)  DEFAULT NULL,
			dt_order_date VARCHAR(15) DEFAULT NULL,
		    PRIMARY KEY (`id`)
			
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			");
	}
	
	public function create_debtco_werform(){
			$this->db->query("CREATE TABLE IF NOT EXISTS debtco_webform (
			id int(9) unsigned NOT NULL AUTO_INCREMENT,
			title VARCHAR(10) DEFAULT NULL,
			full_name VARCHAR(40) DEFAULT NULL,
			surname VARCHAR(30) DEFAULT NULL,
			id_number VARCHAR(16) DEFAULT NULL,
			email VARCHAR(50) DEFAULT NULL,
			client_address VARCHAR(70) DEFAULT NULL,
			next_of_kin_name VARCHAR(30) DEFAULT NULL,
			next_of_kin_number VARCHAR(30) DEFAULT NULL,
			relationship_status VARCHAR(50) DEFAULT NULL,
			
			work_place VARCHAR(30) DEFAULT NULL,
			work_address VARCHAR(60) DEFAULT NULL,
			work_number VARCHAR(30) DEFAULT NULL,
			
			account_holder VARCHAR(30) DEFAULT NULL,
			bank_name VARCHAR(30) DEFAULT NULL,
			branch_code VARCHAR(30) DEFAULT NULL,
			account_type VARCHAR(30) DEFAULT NULL,
			account_number VARCHAR(30) DEFAULT NULL,
			debit_day VARCHAR(30) NULL,
			salary_date VARCHAR(30) NULL,
			client_deduct_consent VARCHAR(30) NULL,
			
			call_recording_reference TEXT DEFAULT NULL,
			
			gross_salary VARCHAR(10) DEFAULT NULL,
			net_salary VARCHAR(10) DEFAULT NULL,
			first_payment VARCHAR(10) DEFAULT NULL,
			second_payment VARCHAR(10) DEFAULT NULL,
			third_month VARCHAR(10) DEFAULT NULL,
			debtreview_consent VARCHAR(10) DEFAULT NULL,
			estimation_period VARCHAR(10) DEFAULT NULL,
			
			lead_id int(9) DEFAULT NULL,
				
		    PRIMARY KEY (`id`)
			
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			");
	}
	
	public function webform($id = 0){
		if ($id > 0) {
			$data = $this->db->where('id', $id)->get('libertech_webform')->result_array();
			$data = $data[0];
		}else{
			// $data['title'] = $_GET['title']?: '';
			// $data['cell_no'] = $_GET['phone_number']?: '';
			// $data['full_name'] = $_GET['first_name']?: '';
			// $data['surname'] = $_GET['last_name']?: '';
			// $data['gender'] = $_GET['gender']?: '';
			// $data['date_of_birth'] = $_GET['date_of_birth']?: '';
		}
		$data['title'] = 'Webform';
		$data['form_title'] = 'Webform';
		// echo "<pre/>";
		// print_r($data);
		// exit;
        $subview  = 'webform.php';
		
        $data['subview'] = $this->load->view('frontend/vicidial/webform_debtco', $data, TRUE);
        $this->load->view('frontend/_layout_main', $data); //page load
	}
    
	public function save_debtco_webform(){
		// $products = $this->input->post(['product_name','product_amount','product_period','sage_account_id']);
		
	
		$fields = $this->input->post();
		unset($fields['button']);
		// unset($fields['product_name']);
		// unset($fields['product_amount']);
		// unset($fields['product_period']);
		// unset($fields['sage_account_id']);
		
		$all_fields = array();
		foreach($fields as $field => $key){
			$is_array = '';
			$all_fields[$field] = $key;
			if(is_array($key)){
				unset($all_fields[$field]);
				foreach($key as $f){
					$is_array .= "'".$f."',";
				}
				$all_fields[$field] = $is_array;
			}
		}
		
		$query = $this->db->insert('debtco_webform', $all_fields);
		/*
		$insert_id = $this->db->insert_id();
		
		$data['name'] = $all_fields['full_name'];
		$data['email'] = $all_fields['email'];
		$data['phone'] = $all_fields['tel_no'];
		$data['mobile'] = $all_fields['cell_no'];
		$data['address'] = $all_fields[] ;
		$data['city'] = $all_fields['town'];
		$data['zipcode'] = $all_fields['residential_code'];
		$data['language'] = $all_fields['language'];
		$data['client_status'] = 1;
		$data['form_id'] = $insert_id;
		*/
		if($query){
			$type = "success";
			$message = 'Form has been saved';
			set_message($type, $message);
			redirect('vicidial/webform'); //redirect page	redirect
		}else{
			$type = "error";
			$message = 'an Error occured';
			set_message($type, $message);
			redirect('vicidial/webform'); //redirect page	redirect
		}				   
	}
	
	public function save_libertech_webform(){
		$products = $this->input->post(['product_name','product_amount','product_period','sage_account_id']);
		
	
		$fields = $this->input->post();
		unset($fields['button']);
		unset($fields['product_name']);
		unset($fields['product_amount']);
		unset($fields['product_period']);
		unset($fields['sage_account_id']);
		
		$all_fields = array();
		foreach($fields as $field => $key){
			$is_array = '';
			$all_fields[$field] = $key;
			if(is_array($key)){
				unset($all_fields[$field]);
				foreach($key as $f){
					$is_array .= "'".$f."',";
				}
				$all_fields[$field] = $is_array;
			}
		}
		
		
		$query = $this->db->insert('libertech_webform', $all_fields);
		$insert_id = $this->db->insert_id();
		
		$data['name'] = $all_fields['full_name'];
		$data['email'] = $all_fields['email'];
		$data['phone'] = $all_fields['tel_no'];
		$data['mobile'] = $all_fields['cell_no'];
		$data['address'] = $all_fields['street_no'] . ' ' . $all_fields['street_no'] . ' ' . $all_fields['suburb'] . ' ' . $all_fields['town'] . ' ' . $all_fields['residential_code'] ;
		$data['city'] = $all_fields['town'];
		$data['zipcode'] = $all_fields['residential_code'];
		$data['language'] = $all_fields['language'];
		$data['client_status'] = 1;
		$data['form_id'] = $insert_id;
		$create_client = $this->db->insert('tbl_client', $data);
		$client_id = $this->db->insert_id();	
		foreach ($products as $product => $v) {
			foreach ($v as $p => $l) {
				$all_products[$p][$product]  = $l;
				$all_products[$p]['form_id'] = $insert_id;
				$all_products[$p]['client_id'] = $client_id;
			}
		}
		
		
		foreach($all_products as $product){
			$this->db->insert('libertech_webform_products', $product);
		}
		
		if($query){
			$type = "success";
			$message = 'Form has been saved';
			set_message($type, $message);
			// redirect('vicidial/webform'); //redirect page	redirect
		}else{
			$type = "warning";
			$message = 'an Error occured';
			set_message($type, $message);
			// redirect('vicidial/webform'); //redirect page	redirect
		}				   
	}
	
	public function liberty_form_display(){
		$data['title'] = 'Webform';
        $subview  = 'liberty_webform_display.php';

        $data['subview'] = $this->load->view('admin/vicidial/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
	}
	
    public function index() {
        $data['title'] = 'Agent Login';
        $subview  = 'agent_login.php';

        $data['subview'] = $this->load->view('admin/vicidial/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }   
	
	
}