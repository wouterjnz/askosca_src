<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jobcards extends Client_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('tickets_model');
        $this->load->model('items_model');
        $this->load->model('stock_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }
	
	public function save_signature($id = null){
		$data_uri = $this->input->post('signature_image');
		$encoded_image = explode(",", $data_uri)[1];
		// $decoded_image = base64_decode($encoded_image);
		//saving signature to root directory.
		// file_put_contents("signature.png", $decoded_image);
		$this->db->where('id', $id);
		$saved = $this->db->update('tbl_jobcards', array('signed'=> 1, 'signature' => $encoded_image));
		
		if ($saved) {
			$type = 'success';
			$message = 'signature saved';
			echo json_encode(['success' => true, 'message' => 'signature saved']);
		} else {
				$type = 'error';
				$message = 'unable to save signature';
				echo json_encode(['success' => false, 'message' => 'unable to save signature']);
		};
		  set_message($type, $message);
	}
	
    public function jobcard_list($id = NULL) {
        $data['title'] = lang('all_items');
        if (!empty($id)) {
            $data['active'] = 2;
            $data['items_info'] = $this->items_model->check_by(array('id' => $id), 'tbl_jobcards');
        } else {
            $data['active'] = 1;
        }
		// $data['breadcrumbs'] = '';
		$data['breadcrumbs'] = 'Jobcards';
		
        $data['subview'] = $this->load->view('client/jobcards/jobcard_list', $data, TRUE);
		$this->load->view('client/_layout_main', $data); //page load
    }

    public function saved_jobcards($id = NULL) {

        $this->items_model->_table_name = 'tbl_jobcards';
        $this->items_model->_primary_key = 'id';
		
        $data = $this->items_model->array_from_post(array('staff_id', 'client_id', 'start_time', 'end_time', 'description'));
		
        // update root category
        
		$where = array('description' => $data['description']);
        // duplicate value check in DB
        if (!empty($id)) { // if id exist in db update data
            $saved_items_id = array('id !=' => $id);
        } else { // if id is not exist then set id as null
            $saved_items_id = null;
        }
		
		
        // check whether this input data already exist or not
        $check_items = $this->items_model->check_update('tbl_jobcards', $where, $saved_items_id);
        if (!empty($check_items)) {	
			// if input data already exist show error alert
            // massage for user
            $type = 'error';
            $msg = "<strong style='color:#000'>" . $data['description'] . '</strong>  ' . lang('already_exist');
        } else { // save and update query                        
            
            $return_id = $this->items_model->save($data, $id);
			
            if (!empty($id)) {
               $id = $id;
                $action = 'activity_update_jobcard';
                $msg = 'Jobcart updated successfully.';
            } else {
                $id = $return_id;
                $action = 'activity_save_jobcard';
                $msg = 'Jobcard created successfully.';
            }
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'items',
                'module_field_id' => $id,
                'activity' => $action,
                'icon' => 'fa-circle-o',
                'value1' => $data['item_name']
            );
            $this->items_model->_table_name = 'tbl_activities';
            $this->items_model->_primary_key = 'activities_id';
            $this->items_model->save($activity);
            // messages for user
            $type = "success";
        }
        $message = $msg;
        set_message($type, $message);
        redirect('client/jobcards/jobcard_list');
    }

    public function delete_items($id) {
        $items_info = $this->items_model->check_by(array('saved_items_id' => $id), 'tbl_saved_items');
        $activity = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'items',
            'module_field_id' => $id,
            'activity' => 'activity_items_deleted',
            'icon' => 'fa-circle-o',
            'value1' => $items_info->item_name
        );
        $this->items_model->_table_name = 'tbl_activities';
        $this->items_model->_primary_key = 'activities_id';
        $this->items_model->save($activity);
        $this->items_model->_table_name = 'tbl_saved_items';
        $this->items_model->_primary_key = 'saved_items_id';
        $this->items_model->delete($id);
        $type = 'success';
        $message = lang('items_deleted');
        set_message($type, $message);
        redirect('client/items/items_list');
    }

	public function save_reply($id)
	{
		$userId = $this->session->userdata('user_id');
		$reply = '';
		if(!empty($this->input->post('reply'))){
			$reply = $this->input->post('reply');
		}
		$data = ['jobcard_id' => $id,  'user_id' => $userId, 'reply' => $reply];
		if($this->db->insert('tbl_jobcard_replies', $data)){
			 // messages for user
            $type = "success";
            $message = 'Reply Added';
            set_message($type, $message);
            
		}else{
			 $type = "danger";
            $message = 'Unable to add Reply.  Please try again later.';
            set_message($type, $message);
		}
		redirect('client/jobcards/jobcard_list/'.$id);
	}
}
