<style>
    
body { padding:0; margin:0; background: #FFF; }

div.scroll_list { height: 400px; width: 140px; overflow: scroll; }
div.text_input { overflow: auto; font-size: 10px;  font-family: sans-serif; }
.body_text { font-size: 13px;  font-family: sans-serif; }
.queue_text_red { font-size: 12px;  font-family: sans-serif; font-weight: bold; color: red; }
.queue_text { font-size: 12px;  font-family: sans-serif; color: black; text-decoration:none; }
.preview_text { font-size: 13px;  font-family: sans-serif; background: #CCFFCC; }
.preview_text_red { font-size: 13px;  font-family: sans-serif; background: #FFCCCC; }
.body_small { font-size: 11px;  font-family: sans-serif; }
.body_small_bold { font-size: 11px;  font-family: sans-serif; font-weight: bold; }
.body_tiny { font-size: 10px;  font-family: sans-serif; }
.log_text { font-size: 11px;  font-family: monospace; }
.log_text_red { font-size: 11px;  font-family: monospace; font-weight: bold; background: #FF3333; }
.log_title { font-size: 12px;  font-family: monospace; font-weight: bold; }
.sd_text { font-size: 16px;  font-family: sans-serif; font-weight: bold; }
.sh_text { font-size: 14px;  font-family: sans-serif; font-weight: bold; }
.sh_text_white { font-size: 14px;  font-family: sans-serif; font-weight: bold; color: white;}
.sb_text { font-size: 12px;  font-family: sans-serif; }
.sk_text { font-size: 11px;  font-family: sans-serif; }
.skb_text { font-size: 13px;  font-family: sans-serif; font-weight: bold; }
.ON_conf { font-size: 11px;  font-family: monospace; color: black; background: #FFFF99; }
.OFF_conf { font-size: 11px;  font-family: monospace; color: black; background: #FFCC77; }
.cust_form { font-family: sans-serif; font-size: 10px; overflow: hidden; }
.cust_form_text { font-family: sans-serif; font-size: 10px; overflow: auto; }
.loading_text { font-family: sans-serif; font-size: 20px; overflow: auto; }

.blink {
-webkit-animation: blink .75s linear infinite;
-moz-animation: blink .75s linear infinite;
-ms-animation: blink .75s linear infinite;
-o-animation: blink .75s linear infinite;
animation: blink .75s linear infinite;
}

@-webkit-keyframes blink {
0% { opacity: 1; }
50% { opacity: 1; }
50.01% { opacity: 0; }
100% { opacity: 0; }
}

@-moz-keyframes blink {
0% { opacity: 1; }
50% { opacity: 1; }
50.01% { opacity: 0; }
100% { opacity: 0; }
}

@-ms-keyframes blink {
0% { opacity: 1; }
50% { opacity: 1; }
50.01% { opacity: 0; }
100% { opacity: 0; }
}

@-o-keyframes blink {
0% { opacity: 1; }
50% { opacity: 1; }
50.01% { opacity: 0; }
100% { opacity: 0; }
}

@keyframes blink {
0% { opacity: 1; }
50% { opacity: 1; }
50.01% { opacity: 0; }
100% { opacity: 0; }
}
input.red_btn{
   font-family: Arial, Sans-Serif;
   font-size: 10px;
   color:#FFFFFF;
   font-weight:bold;
   background-color:#990000;
   border:2px solid;
   border-top-color:#FFCCCC;
   border-left-color:#FFCCCC;
   border-right-color:#330000;
   border-bottom-color:#330000;
}
input.green_btn{
   font-family: Arial, Sans-Serif;
   font-size: 10px;
   color:#FFFFFF;
   font-weight:bold;
   background-color:#009900;
   border:2px solid;
   border-top-color:#CCFFCC;
   border-left-color:#CCFFCC;
   border-right-color:#003300;
   border-bottom-color:#003300;
}
input.blue_btn{
   font-family: Arial, Sans-Serif;
   font-size: 10px;
   color:#FFFFFF;
   font-weight:bold;
   background-color:#000099;
   border:2px solid;
   border-top-color:#CCCCFF;
   border-left-color:#CCCCFF;
   border-right-color:#000033;
   border-bottom-color:#000033;
}
input.form_field {
    font-family: Arial, Sans-Serif;
    font-size: 10px;
    margin-bottom: 3px;
    padding: 2px;
    border: solid 1px #000066;
}
textarea.form_field {
    font-family: Arial, Sans-Serif;
    font-size: 10px;
    margin-bottom: 3px;
    padding: 2px;
    border: solid 1px #000066;
}
select.form_field {
    font-family: Arial, Sans-Serif;
    font-size: 10px;
    margin-bottom: 3px;
    padding: 2px;
    border: solid 1px #000066;
} 
table {
    background-color: #D9E6FE;
}
</style>

<div class="panel panel-custom">
    <div class="panel-heading">
        <div class="panel-title"><?= $title; ?></div>
    </div>
    <div class="panel-body">

            <center>
			<form action='http://<?php echo $server; ?>/agc/vicidial.php' method='post' target='_blank' id='vicidial_form'>
			<table width='460px' style="padding: 10px 10px" bgcolor='#D9E6FE' cellpadding="3" cellspacing="0">
				
				<tr>
				    <td bgcolor="#015B91"><img src="<?php echo base_url() ?>/uploads/vicidial_admin_web_logo.png" height="45" width="170"/></td>
				    <td bgcolor="#015B91">Login<br/><br/></td>
				</tr>
				<tr>
				    <td>&nbsp;</td>
				    				    <td></td>
				</tr>
				<tr>
					<td align="right">Phone Login: &nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td><input type='text' name='phone_login' value='<?php echo $vicidial->agent_user;?>' /></td>
				</tr>
				<tr><td align="right">Phone Password: &nbsp;&nbsp;&nbsp;&nbsp;</td><td>
				<input type='text' name='phone_pass' value='<?php echo $vicidial->agent_pass;?>' /></td>
				</tr><tr><td align="right">User Login: &nbsp;&nbsp;&nbsp;&nbsp;</td><td>
				<input type='text' name='VD_login' value='<?php echo $vicidial->agent_user;?>' /></td>
				</tr><tr><td align="right">User Pass: &nbsp;&nbsp;&nbsp;&nbsp;</td><td>
				<input type='text' name='VD_pass' value='<?php echo $vicidial->agent_pass;?>' /></td>
				<!-- </tr><tr><td align="right">Campaign: &nbsp;&nbsp;&nbsp;&nbsp;</td>
				 <td><select name='VD_campaign' id='VD_campaign'>
					<option value='fghjtyui'>fghjtyui - fghjtyui</option>
				</select></td> 
				</tr> -->
				<tr>
				<td colspan="2" align="center"><br/><input type='submit' value='Login' class="btn btn-primary btn-sm"  style="margin-bottom: 5px" /></td>
				</tr>
				</table>
			</form>
</div>
</div>