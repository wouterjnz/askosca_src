<?= message_box('success'); ?>
<?= message_box('error'); ?>
<?php
$all_bug_info = $this->client_model->get_permission('tbl_bug');
$total_bugs = 0;
if (!empty($all_bug_info)) {
    foreach ($all_bug_info as $v_bugs) {
        if (!empty($v_bugs)) {
            $profile = $this->db->where(array('user_id' => $v_bugs->reporter))->get('tbl_account_details')->row();
            if ($profile->company == $client_details->client_id) {
                $total_bugs += count($v_bugs->bug_id);
            }
        }
    }
}
$recently_paid = $this->db
    ->where('paid_by', $client_details->client_id)
    ->order_by('created_date', 'desc')
    ->get('tbl_payments')
    ->result();
$all_tickets_info = $this->client_model->get_permission('tbl_tickets');
$total_tickets = 0;
if (!empty($all_tickets_info)) {
    foreach ($all_tickets_info as $v_tickets_info) {
        if (!empty($v_tickets_info)) {
            $profile_info = $this->db->where(array('user_id' => $v_tickets_info->reporter))->get('tbl_account_details')->row();
            if(!empty($profile_info)){
            if ($profile_info->company == $client_details->client_id) {
                $total_tickets += count($v_tickets_info->tickets_id);
            }
            }
        }
    }
}
$all_project = $this->db->where('client_id', $client_details->client_id)->get('tbl_project')->result();
$client_outstanding = $this->invoice_model->client_outstanding($client_details->client_id);
$client_payments = $this->invoice_model->get_sum('tbl_payments', 'amount', $array = array('paid_by' => $client_details->client_id));
$client_payable = $client_payments + $client_outstanding;
$client_currency = $this->invoice_model->client_currency_sambol($client_details->client_id);
if (!empty($client_currency)) {
    $cur = $client_currency->symbol;
} else {
    $currency = $this->db->where(array('code' => config_item('default_currency')))->get('tbl_currencies')->row();
    $cur = $currency->symbol;
}
if ($client_payable > 0 AND $client_payments > 0) {
    $perc_paid = round(($client_payments / $client_payable) * 100, 1);
    if ($perc_paid > 100) {
        $perc_paid = '100';
    }
} else {
    $perc_paid = 0;
}
$client_transactions = $this->db->where('paid_by', $client_details->client_id)->get('tbl_transactions')->result();
?>
<div class="row">
    <div class="col-md-3">
        <div class="panel widget mb0 b0">
            <div class="row-table row-flush">
                <div class="col-xs-4 bg-info text-center">
                    <em class="fa fa-money fa-2x"></em>
                </div>
                <div class="col-xs-8">
                    <div class="text-center">
                        <h4 class="mb-sm"><?php
                            if (!empty($client_payments)) {
                                echo display_money($client_payments, $cur);
                            } else {
                                echo '0.00';
                            }
                            ?></h4>
                        <p class="mb0 text-muted"><?= lang('paid_amount') ?></p>
                        <a href="<?= base_url() ?>admin/invoice/all_payments"
                           class="small-box-footer"><?= lang('more_info') ?> <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel widget mb0 b0">
            <div class="row-table row-flush">
                <div class="col-xs-4 bg-danger text-center">
                    <em class="fa fa-usd fa-2x"></em>
                </div>
                <div class="col-xs-8">
                    <div class="text-center">
                        <h4 class="mb-sm"><?php
                            if ($client_outstanding > 0) {
                                echo display_money($client_outstanding, $cur);
                            } else {
                                echo '0.00';
                            }
                            ?></h4>
                        <p class="mb0 text-muted"><?= lang('due_amount') ?></p>
                        <a href="<?= base_url() ?>admin/invoice/manage_invoice"
                           class="small-box-footer"><?= lang('more_info') ?>
                            <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel widget mb0 b0">
            <div class="row-table row-flush">
                <div class="col-xs-4 bg-inverse text-center">
                    <em class="fa fa-usd fa-2x"></em>
                </div>
                <div class="col-xs-8">
                    <div class="text-center">
                        <h4 class="mb-sm">
                            <?php
                            if ($client_payable > 0) {
                                echo display_money($client_payable, $cur);
                            } else {
                                echo '0.00';
                            }
                            ?></h4>
                        <p class="mb0 text-muted"><?= lang('invoice_amount') ?></p>
                        <a href="<?= base_url() ?>admin/invoice/manage_invoice"
                           class="small-box-footer"><?= lang('more_info') ?>
                            <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel widget mb0 b0">
            <div class="row-table row-flush">
                <div class="col-xs-4 bg-purple text-center">
                    <em class="fa fa-usd fa-2x"></em>
                </div>
                <div class="col-xs-8">
                    <div class="text-center">
                        <h4 class="mb-sm">
                            <?= $perc_paid ?>%</h4>
                        <p class="mb0 text-muted"><?= lang('paid') . ' ' . lang('percentage') ?></p>
                        <a href="<?= base_url() ?>admin/invoice/all_payments"
                           class="small-box-footer"><?= lang('more_info') ?>
                            <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-lg">
    <div class="col-sm-3">
        <ul class="nav nav-pills nav-stacked navbar-custom-nav">
             <li class="">
                <a href="#step_details" data-toggle="tab" aria-expanded="true"><?= 'Steps'; ?></a>
            </li>
            
            <li class="<?= (empty($company) ? 'active' : null) ?>"><a href="#task_details" data-toggle="tab"
                                                                      aria-expanded="true"><?= lang('details') ?></a>
            </li>
            <li class="<?= (!empty($company) ? 'active' : null) ?>"><a href="#contacts" data-toggle="tab"
                                                                       aria-expanded="false"><?= lang('contacts') ?>
                    <strong
                        class="pull-right"><?= (!empty($client_contacts) ? count($client_contacts) : null) ?></strong></a>
            </li>
            <!--
            <li class=""><a href="#invoices" data-toggle="tab" aria-expanded="false"><?= lang('invoices') ?><strong
                        class="pull-right"><?= (!empty($client_invoices) ? count($client_invoices) : null) ?></strong></a>
            </li>
            <li class=""><a href="#estimates" data-toggle="tab" aria-expanded="false"><?= lang('estimates') ?><strong
                        class="pull-right"><?= (!empty($client_estimates) ? count($client_estimates) : null) ?></strong></a>
            </li>
            <li class=""><a href="#payments" data-toggle="tab" aria-expanded="false"><?= lang('payments') ?><strong
                        class="pull-right"><?= (!empty($recently_paid) ? count($recently_paid) : null) ?></strong></a>
            </li>
            <li class=""><a href="#transaction" data-toggle="tab" aria-expanded="false"><?= lang('transactions') ?>
                    <strong
                        class="pull-right"><?= (!empty($client_transactions) ? count($client_transactions) : null) ?></strong></a>
            </li>
            -->
            <!-- <li class=""><a href="#projects" data-toggle="tab" aria-expanded="false"><?= lang('project') ?><strong
                        class="pull-right"><?= (!empty($all_project) ? count($all_project) : null) ?></strong></a></li> -->
            <li class=""><a href="#ticket" data-toggle="tab" aria-expanded="false"><?= lang('tickets') ?><strong
                        class="pull-right"><?= (!empty($total_tickets) ? count($total_tickets) : null) ?></strong></a>
            </li>
            <!-- <li class=""><a href="#bugs" data-toggle="tab" aria-expanded="false"><?= lang('bugs') ?><strong
                        class="pull-right"><?= (!empty($total_bugs) ? count($total_bugs) : null) ?></strong></a></li> -->
        	<li class=""><a href="#recordings" data-toggle="tab" aria-expanded="false"><?= 'Recordings'; ?><strong
			class="pull-right"><?= (!empty($callRecordsFiles) ? count($callRecordsFiles) : null) ?></strong></a></li>
		    <li class=""><a href="#script" data-toggle="tab" aria-expanded="false"><?= 'Script'; ?><strong
			class="pull-right"></strong></a></li>
        </ul>
    </div>
    <div class="col-sm-9">
        <div class="tab-content" style="border: 0;padding:0;">
            <!-- Task Details tab Starts -->
            <div class="tab-pane <?= (empty($company) ? 'active' : null) ?>" id="task_details"
                 style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title"><strong><?= $client_details->name ?> - <?= lang('details') ?> </strong>
                            <div class="pull-right">
                                <?php
                                if ($client_details->leads_id != 0) {
                                    echo lang('converted_from')
                                    ?>
                                    <a href="<?= base_url() ?>admin/leads/leads_details/<?= $client_details->leads_id ?>"><?= lang('leads') ?></a>
                                <?php }
                                ?>
                                <a href="<?php echo base_url() ?>admin/client/manage_client/<?= $client_details->client_id ?>"
                                   class="btn-xs "><i class="fa fa-edit"></i> <?= lang('edit') ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <!-- Details START -->
                        <div class="col-md-6">
                            <div class="group">
                                <h4 class="subdiv text-muted"><?= lang('contact_details') ?></h4>
                                <div class="row inline-fields">
                                    <div class="col-md-4"><?= lang('name').':'; ?></div>
                                    <div class="col-md-6"><?= $client_details->name ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4"><?= lang('contact_person').':'; ?></div>
                                    <div class="col-md-6">
                                        <?php
                                        if ($client_details->primary_contact != 0) {
                                            $contacts = $client_details->primary_contact;
                                        } else {
                                            $contacts = NULL;
                                        }
                                        $primary_contact = $this->client_model->check_by(array('account_details_id' => $contacts), 'tbl_account_details');
                                        if ($primary_contact) {
                                            echo $primary_contact->fullname;
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4"><?= lang('email').':'; ?></div>
                                    <div class="col-md-6"><?= $client_details->email ?></div>
                                </div>
                            </div>

                            <div class="row inline-fields">
                                <div class="col-md-4"><?= lang('city').':'; ?></div>
                                <div class="col-md-6"><?= $client_details->city ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= lang('country').':'; ?></div>
                                <div class="col-md-6 text-success"><?= $client_details->country ?></div>
                            </div>
                            
                            
                            
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= 'NCR:' ?></div>
                                <div class="col-md-6"><?= $client_details->ncr ?></div>
                            </div>
                            
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= 'ID number:' ?></div>
                                <div class="col-md-6"><?= $client_details->id_number ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= 'Inhouse D/O:' ?></div>
                                <div class="col-md-6"><?= $client_details->inhouse_do ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= 'DC:' ?></div>
                                <div class="col-md-6"><?= trim($client_details->dc) ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= '1st D/O:' ?></div>
                                <div class="col-md-6"><?= $client_details->first_do ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= 'Status:' ?></div>
                                <div class="col-md-6"><?= $client_details->status1 ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= 'Legal:' ?></div>
                                <div class="col-md-6"><?= trim($client_details->legal) ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= '2nd D/O:' ?></div>
                                <div class="col-md-6"><?= $client_details->second_do ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= 'Status:' ?></div>
                                <div class="col-md-6"><?= $client_details->status2 ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= 'DC or Legal:' ?></div>
                                <div class="col-md-6"><?= $client_details->dc_or_legal ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= '3rd D/O:' ?></div>
                                <div class="col-md-6"><?= $client_details->third_do ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= 'Status:' ?></div>
                                <div class="col-md-6"><?= $client_details->status3 ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4"><?= 'Notes:' ?></div>
                                <div class="col-md-6"><?= $client_details->notes ?></div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-lg">
                            <div class="group">
                                <div class="row" style="margin-top: 5px">
                                    <div class="rec-pay col-md-12">
                                        <h4 class="subdiv text-muted"><?= lang('received_amount') ?></h4>
                                        <h3 class="amount text-danger cursor-pointer"><strong>
                                                <?php
                                                $get_curency = $this->client_model->check_by(array('client_id' => $client_details->client_id), 'tbl_client');
                                                $curency = $this->client_model->check_by(array('code' => $get_curency->currency), 'tbl_currencies');
                                                ?><?= display_money($this->client_model->client_paid($client_details->client_id), $curency->symbol); ?>
                                            </strong></h3>
                                        <div class="row inline-fields">
                                            <div class="col-md-4"><?= lang('address') ?></div>
                                            <div class="col-md-6"><?= $client_details->address ?></div>
                                        </div>
                                        <div class="row inline-fields">
                                            <div class="col-md-4"><?= lang('phone') ?></div>
                                            <div class="col-md-6"><a
                                                    href="tel:<?= $client_details->phone ?>"><?= $client_details->phone ?></a>
                                            </div>
                                        </div>
                                        <div class="row inline-fields">
                                            <div class="col-md-4"><?= lang('website') ?></div>
                                            <div class="col-md-6"><a href="<?= $client_details->website ?>"
                                                                     class="text-info"
                                                                     target="_blank"><?= $client_details->website ?></a>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center block mt">
                            <div style="display: inline-block">
                                <div id="easypie3" data-percent="<?= $perc_paid ?>" class="easypie-chart">
                                    <span class="h2"><?= $perc_paid ?>%</span>
                                    <div class="easypie-text"><?= lang('paid') ?></div>
                                </div>
                            </div>
                        </div>

                        <!-- Details END -->
                    </div>
                    <div class="panel-footer">
                        <span><?= lang('invoice_amount') ?>: <strong
                                class="label label-primary">
                                <?= display_money($client_payable, $curency->symbol); ?>
                            </strong></span>
                        <span class="text-danger pull-right">
                            <?= lang('outstanding') ?>
                            :<strong
                                class="label label-danger"> <?= display_money($client_outstanding, $curency->symbol) ?></strong>
                        </span>
                    </div>
                </div>
            </div>
	       <!--            *************** steps tab start ************-->
            <div class="tab-pane" id="step_details" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>
                                <?= 'Steps'; ?>
                            </strong>
                        </div>
                    </div>
                    <div class="panel-body">
					<select class="form-control" name="step_selected" onchange="set_client_step(this.value, '<?php echo $client_details->client_id; ?>')">
						<option value="">--</option>
						<?php $step_query = $this->db->order_by('order_status','ASC')->get('vicidial_client_steps')->result();
						foreach($step_query as $step){ ?>
							<option value="<?php echo $step->id; ?>" <?php echo ($client_details->step_id == $step->id)? 'selected':''; ?>><?php echo  $step->order_status . ' ' . $step->stepname; ?></option>
						<?php } ?>	
					</select>
	
						  <div class="success" id="step_saved">
                            </div>
                            
                    <br/>
                    <br/>
                    <h4>Step History</h4>
                    <table class="table table-striped">
                        <tr>
                            <td><strong>Step</strong></td>
                            <td><strong>Date Changed</strong></td>
                            <td><strong>User</strong></td>
                        </tr>
                        <?php
                        $step_history = $this->db->query('SELECT * FROM vicidial_step_history vh 
                                                        LEFT JOIN vicidial_client_steps vc ON vc.id = vh.step_id
                                                        LEFT JOIN tbl_users u ON u.user_id = vh.user_id WHERE client_id = '.$client_details->client_id)->result();
                        if(!empty($step_history)){
                        foreach($step_history as $step){
                        ?>
                        <tr>
                            <td><?php echo $step->stepname ; ?></td>
                            <td><?php echo $step->timestamp ; ?></td>
                            <td><?php echo $step->username ; ?></td>
                        </tr>
                        <?php } } ?>
                    </table>
                    </div>
                    <div class="panel-footer">
                     
                    </div>
                </section>
            </div>
            <!--            *************** contact tab start ************-->
            <div class="tab-pane <?= (!empty($company) ? 'active' : null) ?>" id="contacts" style="position: relative;">
                <?php if (!empty($company)): ?>
                    <?php include_once 'asset/admin-ajax.php'; ?>
                    <?php
                    $eeror_message = $this->session->userdata('error');

                    if (!empty($eeror_message)):foreach ($eeror_message as $key => $message):
                        ?>
                        <div class="alert alert-danger">
                            <?php echo $message; ?>
                        </div>
                        <?php
                    endforeach;
                    endif;
                    $this->session->unset_userdata('error');
                    ?>
                    <form role="form" enctype="multipart/form-data" id="form"
                          action="<?php echo base_url(); ?>admin/client/save_contact/<?php
                          if (!empty($account_details)) {
                              echo $account_details->user_id;
                          }
                          ?>" method="post" class="form-horizontal  ">
                        <div class="panel panel-custom">
                            <!-- Default panel contents -->
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <?= lang('add_contact') ?>.
                                    <a href="<?= base_url() ?>admin/client/client_details/<?= $client_details->client_id ?>"
                                       class="btn-sm pull-right">Return to Details</a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <input type="hidden" name="r_url"
                                       value="<?= base_url() ?>admin/client/client_details/<?= $company ?>">
                                <input type="hidden" name="company" value="<?= $company ?>">
                                <input type="hidden" name="role_id" value="2">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"><?= lang('full_name') ?> <span
                                            class="text-danger"> *</span></label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control" value="<?php
                                        if (!empty($account_details)) {
                                            echo $account_details->fullname;
                                        }
                                        ?>" placeholder="E.g John Doe" name="fullname" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"><?= lang('email') ?><span
                                            class="text-danger"> *</span></label>
                                    <div class="col-lg-5">
                                        <input class="form-control" id='email' type="email" value="<?php
                                        if (!empty($user_info)) {
                                            echo $user_info->email;
                                        }
                                        ?>" placeholder="me@domin.com" name="email" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"><?= lang('phone') ?> </label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control" value="<?php
                                        if (!empty($account_details)) {
                                            echo $account_details->phone;
                                        }
                                        ?>" name="phone" placeholder="+52 782 983 434">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"><?= lang('mobile') ?> <span
                                            class="text-danger"> *</span></label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control" value="<?php
                                        if (!empty($account_details)) {
                                            echo $account_details->mobile;
                                        }
                                        ?>" name="mobile" placeholder="+8801723611125">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"><?= lang('skype_id') ?> </label>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control" value="<?php
                                        if (!empty($account_details)) {
                                            echo $account_details->skype;
                                        }
                                        ?>" name="skype" placeholder="john">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"><?= lang('language') ?></label>
                                    <div class="col-lg-5">
                                        <select name="language" class="form-control">
                                            <?php foreach ($languages as $lang) : ?>
                                                <option value="<?= $lang->name ?>"<?php
                                                if (!empty($account_details->language) && $account_details->language == $lang->name) {
                                                    echo 'selected="selected"';
                                                } else {
                                                    echo($this->config->item('language') == $lang->name ? ' selected="selected"' : '');
                                                }
                                                ?>><?= ucfirst($lang->name) ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"><?= lang('locale') ?></label>
                                    <div class="col-lg-5">
                                        <select class="  form-control" name="locale">
                                            <?php foreach ($locales as $loc) : ?>
                                                <option lang="<?= $loc->code ?>"
                                                        value="<?= $loc->locale ?>"<?= ($this->config->item('locale') == $loc->locale ? ' selected="selected"' : '') ?>><?= $loc->name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <?php if (empty($account_details)): ?>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label"><?= lang('username') ?> <span
                                                class="text-danger">*</span></label>
                                        <div class="col-lg-5">
                                            <input class="form-control" id='username' type="text"
                                                   value="<?= set_value('username') ?>"
                                                   onchange="check_user_name(this.value)" placeholder="johndoe"
                                                   name="username" required>
                                            <div class="required" id="username_result"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label"><?= lang('password') ?> <span
                                                class="text-danger"> *</span></label>
                                        <div class="col-lg-5">
                                            <input type="password" class="form-control" id="password"
                                                   value="<?= set_value('password') ?>" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label"><?= lang('confirm_password') ?> <span
                                                class="text-danger"> *</span></label>
                                        <div class="col-lg-5">
                                            <input type="password" class="form-control"
                                                   value="<?= set_value('confirm_password') ?>" name="confirm_password">
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-5">
                                        <button type="submit" id="sbtn"
                                                class="btn btn-primary"><?= lang('add_contact') ?></button>
                                    </div>

                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                    </form>
                <?php else: ?>
                    <section class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong><?= lang('contacts') ?></strong>
                                <a href="<?= base_url() ?>admin/client/client_details/<?= $client_details->client_id ?>/add_contacts"
                                   class="btn-sm pull-right"><?= lang('add_contact') ?></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><?= lang('full_name') ?></th>
                                    <th><?= lang('email') ?></th>
                                    <th><?= lang('phone') ?> </th>
                                    <th><?= lang('mobile') ?> </th>
                                    <th><?= lang('skype_id') ?></th>
                                    <th class="col-date"><?= lang('last_login') ?> </th>
                                    <th><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($client_contacts)) {
                                    foreach ($client_contacts as $key => $contact) {
                                        ?>
                                        <tr>
                                            <td><?= $contact->fullname ?></td>
                                            <td class="text-info"><?= $contact->email ?> </td>
                                            <td><a href="tel:<?= $contact->phone ?>"><?= $contact->phone ?></a></td>
                                            <td><a href="tel:<?= $contact->mobile ?>"><?= $contact->mobile ?></a></td>
                                            <td><a href="skype:<?= $contact->skype ?>?call"><?= $contact->skype ?></a>
                                            </td>
                                            <?php
                                            if ($contact->last_login == '0000-00-00 00:00:00') {
                                                $login_time = "-";
                                            } else {
                                                $login_time = strftime(config_item('date_format') . " %H:%M:%S", strtotime($contact->last_login));
                                            }
                                            ?>
                                            <td><?= $login_time ?> </td>
                                            <td>
                                                <a href="<?= base_url() ?>admin/client/make_primary/<?= $contact->user_id ?>/<?= $client_details->client_id ?>"
                                                   data-toggle="tooltip" class="btn <?php
                                                if ($client_details->primary_contact == $contact->user_id) {
                                                    echo "btn-success";
                                                } else {
                                                    echo "btn-default";
                                                }
                                                ?> btn-xs " title="<?= lang('primary_contact') ?>">
                                                    <i class="fa fa-chain"></i> </a>
                                                <a href="<?= base_url() ?>admin/client/client_details/<?= $client_details->client_id . '/add_contacts/' . $contact->user_id ?>"
                                                   class="btn btn-primary btn-xs" title="<?= lang('edit') ?>">
                                                    <i class="fa fa-edit"></i> </a>
                                                <a href="<?= base_url() ?>admin/client/delete_contacts/<?= $client_details->client_id . '/' . $contact->user_id ?>"
                                                   class="btn btn-danger btn-xs" title="<?= lang('delete') ?>">
                                                    <i class="fa fa-trash-o"></i> </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>


                                </tbody>
                            </table>
                        </div>
                    </section>
                <?php endif ?>

            </div>
               <!--            *************** Recordings tab start ************-->
            <div class="tab-pane" id="recordings" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                           <strong> <?= 'Recordings'; ?></strong>
                        </div>
                    </div>
                    <div class="panel-body">
                    <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Recording</th>
                                    <th><?= 'link'; ?></th>
                                </tr>
                                </thead>
                                <tbody>
								<?php if(!empty($callRecordsFiles)){ foreach($callRecordsFiles as $recordings){
								?>
								<tr>
								    <td >
								        <?php 
										$rname = explode("/",$recordings);
										//$rlink = explode(" ", $rname[5]);
										//$rlink2  =explode(" ", $rlink[0]);
										//print_r($rlink);
										// echo $rlink2[0]; //$rname[5];
										// print_r($rname);
										if(isset($rname[5])){
										$rlink = substr($rname[5],0, 34);
									    ?>
								        <audio controls style="width: 400px">
								            <source src="<?= substr($recordings, 0 , 72); ?>" type="audio/mpeg">
								        </audio>
								    </td>
									<td><a href="<?php echo substr($recordings, 0 , 72); ?>" target="_blank">
									<?php 
										//$rname = explode("/",$recordings);
										//$rlink = explode(" ", $rname[5]);
										//$rlink2  =explode(" ", $rlink[0]);
										//print_r($rlink);
										// echo $rlink2[0]; //$rname[5];
										//echo substr($rname[5],0, 34);	
											echo $rlink;
										}
											?>
									
									</a></td>
								</tr>
								<?php } } ?>
								</tbody>
					</table>
                    </div>
                  
                </section>
            </div>
            
            <!--            *************** invoice tab start ************-->
            <div class="tab-pane" id="invoices" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>
                                <?= lang('invoices') ?>
                            </strong>
                            <a href="<?= base_url() ?>admin/invoice/manage_invoice/create_invoice"
                               class="btn-sm pull-right"><?= lang('new_invoice') ?></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th><?= lang('reference_no') ?></th>
                                <th><?= lang('date_issued') ?></th>
                                <th><?= lang('due_date') ?> </th>
                                <th class="col-currency"><?= lang('amount') ?> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            setlocale(LC_ALL, config_item('locale') . ".UTF-8");
                            $total_invoice = 0;
                            if (!empty($client_invoices)) {
                                foreach ($client_invoices as $key => $invoice) {
                                    $total_invoice += $this->client_model->invoice_payable($invoice->invoices_id);
                                    ?>
                                    <tr>
                                        <td><a class="text-info"
                                               href="<?= base_url() ?>admin/invoice/manage_invoice/invoice_details/<?= $invoice->invoices_id ?>"><?= $invoice->reference_no ?></a>
                                        </td>
                                        <td><?= strftime(config_item('date_format'), strtotime($invoice->date_saved)); ?> </td>
                                        <td><?= strftime(config_item('date_format'), strtotime($invoice->due_date)); ?> </td>
                                        <td>
                                            <?= display_money($this->client_model->invoice_payable($invoice->invoices_id), $cur); ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <strong><?= lang('invoice') . ' ' . lang('amount') ?>:</strong> <strong
                            class="label label-success">
                            <?php
                            echo display_money($total_invoice, $cur);
                            ?>
                        </strong>
                    </div>
                </section>
            </div>
            <!--            *************** invoice tab start ************-->
            <div class="tab-pane" id="estimates" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>
                                <?= lang('estimates') ?>
                            </strong>
                            <a href="<?= base_url() ?>admin/estimates/index/edit_estimates/"
                               class="btn-sm pull-right"><?= lang('new_estimate') ?></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th><?= lang('reference_no') ?></th>
                                <th><?= lang('date_issued') ?></th>
                                <th><?= lang('due_date') ?> </th>
                                <th class="col-currency"><?= lang('amount') ?> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            setlocale(LC_ALL, config_item('locale') . ".UTF-8");
                            $total_estimate = 0;
                            if (!empty($client_estimates)) {
                                foreach ($client_estimates as $key => $estimate) {
                                    $total_estimate += $this->estimates_model->estimate_calculation('estimate_amount', $estimate->estimates_id);
                                    ?>
                                    <tr>
                                        <td><a class="text-info"
                                               href="<?= base_url() ?>admin/estimates/index/estimates_details//<?= $estimate->estimates_id ?>"><?= $estimate->reference_no ?></a>
                                        </td>
                                        <td><?= strftime(config_item('date_format'), strtotime($estimate->date_saved)); ?> </td>
                                        <td><?= strftime(config_item('date_format'), strtotime($estimate->due_date)); ?> </td>
                                        <td>
                                            <?php echo display_money($this->estimates_model->estimate_calculation('estimate_amount', $estimate->estimates_id), $cur); ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <strong><?= lang('estimate') . ' ' . lang('amount') ?>:</strong> <strong
                            class="label label-success">
                            <?= display_money($total_estimate, $cur); ?>
                        </strong>
                    </div>
                </section>
            </div>
            <!--            *************** invoice tab start ************-->
            <div class="tab-pane" id="payments" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <?= lang('payments') ?></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><?= lang('payment_date') ?></th>
                                    <th><?= lang('invoice_date') ?></th>
                                    <th><?= lang('invoice') ?></th>
                                    <th><?= lang('amount') ?></th>
                                    <th><?= lang('payment_method') ?></th>
                                    <th><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                $total_amount = 0;
                                if (!empty($recently_paid)) {
                                    foreach ($recently_paid as $key => $v_paid) {
                                        $invoice_info = $this->db->where(array('invoices_id' => $v_paid->invoices_id))->get('tbl_invoices')->row();
                                        $payment_method = $this->db->where(array('payment_methods_id' => $v_paid->payment_method))->get('tbl_payment_methods')->row();

                                        if ($v_paid->payment_method == '1') {
                                            $label = 'success';
                                        } elseif ($v_paid->payment_method == '2') {
                                            $label = 'danger';
                                        } else {
                                            $label = 'dark';
                                        }
                                        $total_amount += $v_paid->amount;
                                        ?>
                                        <tr>
                                            <td>
                                                <a href="<?= base_url() ?>admin/invoice/manage_invoice/payments_details/<?= $v_paid->payments_id ?>"> <?= strftime(config_item('date_format'), strtotime($v_paid->payment_date)); ?></a>
                                            </td>
                                            <td><?= strftime(config_item('date_format'), strtotime($invoice_info->date_saved)) ?></td>
                                            <td><a class="text-info"
                                                   href="<?= base_url() ?>admin/invoice/manage_invoice/invoice_details/<?= $v_paid->invoices_id ?>"><?= $invoice_info->reference_no; ?></a>
                                            </td>
                                            <?php $currency = $this->invoice_model->client_currency_sambol($invoice_info->client_id); ?>
                                            <td><?= display_money($v_paid->amount, $currency->symbol) ?></td>
                                            <td><span
                                                    class="label label-<?= $label ?>"><?= $payment_method->method_name ?></span>
                                            </td>
                                            <td>
                                                <?= btn_edit('admin/invoice/all_payments/' . $v_paid->payments_id) ?>
                                                <?= btn_view('admin/invoice/manage_invoice/payments_details/' . $v_paid->payments_id) ?>
                                                <?= btn_delete('admin/invoice/delete/delete_payment/' . $v_paid->payments_id) ?>
                                                <a data-toggle="tooltip" data-placement="top"
                                                   href="<?= base_url() ?>admin/invoice/send_payment/<?= $v_paid->payments_id . '/' . $v_paid->amount ?>"
                                                   title="<?= lang('send_email') ?>"
                                                   class="btn btn-xs btn-success">
                                                    <i class="fa fa-envelope"></i> </a>
                                            </td>
                                        </tr>

                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <strong><?= lang('paid_amount') ?>:</strong> <strong class="label label-success">
                            <?= display_money($total_amount, $cur); ?>
                        </strong>
                    </div>
                </section>
            </div>
            <!--            *************** Transactions tab start ************-->
            <div class="tab-pane" id="transaction" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <?= lang('transactions') ?>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th><?= lang('date') ?></th>
                                <th><?= lang('account') ?></th>
                                <th><?= lang('type') ?> </th>
                                <th><?= lang('amount') ?> </th>
                                <th><?= lang('action') ?> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total_income = 0;
                            $total_expense = 0;
                            $curency = $this->client_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
                            if (!empty($client_transactions)):foreach ($client_transactions as $v_transactions) :
                                $account_info = $this->client_model->check_by(array('account_id' => $v_transactions->account_id), 'tbl_accounts');
                                ?>
                                <tr>
                                    <td><?= strftime(config_item('date_format'), strtotime($v_transactions->date)); ?></td>
                                    <td><?= $account_info->account_name ?></td>
                                    <td><?= $v_transactions->type ?></td>
                                    <td><?= display_money($v_transactions->amount, $curency->symbol); ?></td>
                                    <td>
                                        <?php

                                        if ($v_transactions->type == 'Income') {
                                            $total_income += $v_transactions->amount;
                                            ?>
                                            <?= btn_edit('admin/transactions/deposit/' . $v_transactions->transactions_id) ?>
                                            <?= btn_delete('admin/transactions/delete_deposit/' . $v_transactions->transactions_id) ?>
                                            <?php
                                        } else {
                                            $total_expense += $v_transactions->amount;
                                            ?>
                                            <?= btn_edit('admin/transactions/expense/' . $v_transactions->transactions_id) ?>
                                            <?= btn_delete('admin/transactions/delete_expense/' . $v_transactions->transactions_id) ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                                ?>

                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <small><strong><?= lang('total_income') ?>:</strong><strong
                                class="label label-success"><?= display_money($total_income, $curency->symbol); ?></strong>
                        </small>
                        <small class="text-danger pull-right">
                            <strong><?= lang('total_expense') ?>:</strong>
                            <strong
                                class="label label-danger"><?= display_money($total_expense, $curency->symbol); ?></strong>
                        </small>
                    </div>
                </section>
            </div>
            <!--            *************** Project tab start ************-->
            <div class="tab-pane" id="projects" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <?= lang('project') ?>
                            <a href="<?= base_url() ?>admin/projects/index/"
                               class="btn-sm pull-right"><?= lang('new_project') ?></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><?= lang('project_name') ?></th>
                                    <th><?= lang('end_date') ?></th>
                                    <th><?= lang('status') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($all_project)):foreach ($all_project as $v_project):
                                    ?>
                                    <tr>
                                        <td><a class="text-info"
                                               href="<?= base_url() ?>admin/projects/project_details/<?= $v_project->project_id ?>"><?= $v_project->project_name ?></a>
                                            <?php if (time() > strtotime($v_project->end_date) AND $v_project->progress < 100) { ?>
                                                <span
                                                    class="label label-danger pull-right"><?= lang('overdue') ?></span>
                                            <?php } ?>

                                            <div class="progress progress-xs progress-striped active">
                                                <div
                                                    class="progress-bar progress-bar-<?php echo ($v_project->progress >= 100) ? 'success' : 'primary'; ?>"
                                                    data-toggle="tooltip"
                                                    data-original-title="<?= $v_project->progress ?>%"
                                                    style="width: <?= $v_project->progress; ?>%"></div>
                                            </div>

                                        </td>
                                        <td><?= strftime(config_item('date_format'), strtotime($v_project->end_date)) ?></td>

                                        <td><?php
                                            if (!empty($v_project->project_status)) {
                                                if ($v_project->project_status == 'completed') {
                                                    $status = "<span class='label label-success'>" . lang($v_project->project_status) . "</span>";
                                                } elseif ($v_project->project_status == 'in_progress') {
                                                    $status = "<span class='label label-primary'>" . lang($v_project->project_status) . "</span>";
                                                } elseif ($v_project->project_status == 'cancel') {
                                                    $status = "<span class='label label-danger'>" . lang($v_project->project_status) . "</span>";
                                                } else {
                                                    $status = "<span class='label label-warning'>" . lang($v_project->project_status) . "</span>";
                                                }
                                                echo $status;
                                            }
                                            ?>      </td>
                                    </tr>
                                    <?php
                                endforeach;
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            <!--            *************** Tickets tab start ************-->
            <div class="tab-pane" id="ticket" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <?= lang('tickets') ?>
                            <a href="<?= base_url() ?>admin/tickets/index/edit_tickets/"
                               class="btn-sm pull-right"><?= lang('new_ticket') ?></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><?= lang('subject') ?></th>
                                    <th class="col-date"><?= lang('date') ?></th>
                                    <?php if ($this->session->userdata('user_type') == '1') { ?>
                                        <th><?= lang('reporter') ?></th>
                                    <?php } ?>
                                    <th><?= lang('status') ?></th>
                                    <th><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($all_tickets_info)) {
                                    foreach ($all_tickets_info as $v_tickets_info) {
                                        $profile_info = $this->db->where(array('user_id' => $v_tickets_info->reporter))->get('tbl_account_details')->row();
                                       
                                        if (!empty($profile_info) && $profile_info->company == $client_details->client_id) {
                                            if ($v_tickets_info->status == 'open') {
                                                $s_label = 'danger';
                                            } elseif ($v_tickets_info->status == 'closed') {
                                                $s_label = 'success';
                                            } else {
                                                $s_label = 'default';
                                            }
                                            ?>
                                            <tr>
                                                <td><a class="text-info"
                                                       href="<?= base_url() ?>admin/tickets/index/tickets_details/<?= $v_tickets_info->tickets_id ?>"><?= $v_tickets_info->subject ?></a>
                                                </td>
                                                <td><?= strftime(config_item('date_format'), strtotime($v_tickets_info->created)); ?></td>
                                                <?php if ($this->session->userdata('user_type') == '1') { ?>

                                                    <td>
                                                        <a class="pull-left recect_task  ">
                                                            <?php if (!empty($profile_info)) {
                                                                ?>
                                                                <img style="width: 30px;margin-left: 18px;
                                                         height: 29px;
                                                         border: 1px solid #aaa;"
                                                                     src="<?= base_url() . $profile_info->avatar ?>"
                                                                     class="img-circle">
                                                            <?php } ?>

                                                            <?=
                                                            ($profile_info->fullname)
                                                            ?>
                                                        </a>
                                                    </td>

                                                <?php } ?>
                                                <?php
                                                if ($v_tickets_info->status == 'in_progress') {
                                                    $status = 'In Progress';
                                                } else {
                                                    $status = $v_tickets_info->status;
                                                }
                                                ?>
                                                <td><span
                                                        class="label label-<?= $s_label ?>"><?= ucfirst($status) ?></span>
                                                </td>
                                                <td>
                                                    <?= btn_edit('admin/tickets/index/edit_tickets/' . $v_tickets_info->tickets_id) ?>
                                                    <?= btn_delete('admin/tickets/delete/delete_tickets/' . $v_tickets_info->tickets_id) ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            <!--            *************** Bugs tab start ************-->
            <div class="tab-pane" id="bugs" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <?= lang('bugs') ?>
                            <a href="<?= base_url() ?>admin/bugs/index/"
                               class="btn-sm pull-right"><?= lang('new_bugs') ?></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><?= lang('bug_title') ?></th>
                                    <th><?= lang('status') ?></th>
                                    <th><?= lang('priority') ?></th>
                                    <?php if ($this->session->userdata('user_type') == '1') { ?>
                                        <th><?= lang('reporter') ?></th>
                                    <?php } ?>
                                    <th><?= lang('assigned_to') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($all_bug_info)) {
                                    foreach ($all_bug_info as $v_bugs) {
                                        $profile = $this->db->where(array('user_id' => $v_bugs->reporter))->get('tbl_account_details')->row();
                                        if ($profile->company == $client_details->client_id) {
                                            $total_bugs += count($v_bugs->bug_id);
                                            $reporter = $this->db->where('user_id', $v_bugs->reporter)->get('tbl_users')->row();
                                            if ($reporter->role_id == '1') {
                                                $badge = 'danger';
                                            } elseif ($reporter->role_id == '2') {
                                                $badge = 'info';
                                            } else {
                                                $badge = 'primary';
                                            }

                                            if ($v_bugs->bug_status == 'unconfirmed') {
                                                $label = 'warning';
                                            } elseif ($v_bugs->bug_status == 'confirmed') {
                                                $label = 'info';
                                            } elseif ($v_bugs->bug_status == 'in_progress') {
                                                $label = 'primary';
                                            } elseif ($v_bugs->bug_status == 'resolved') {
                                                $label = 'purple';
                                            } else {
                                                $label = 'success';
                                            }
                                            ?>
                                            <tr>
                                                <td><a class="text-info" style="<?php
                                                    if ($v_bugs->bug_status == 'resolve') {
                                                        echo 'text-decoration: line-through;';
                                                    }
                                                    ?>"
                                                       href="<?= base_url() ?>admin/bugs/view_bug_details/<?= $v_bugs->bug_id ?>"><?php echo $v_bugs->bug_title; ?></a>
                                                </td>
                                                </td>
                                                <td>
                                                    <span
                                                        class="label label-<?= $label ?>"><?= lang("$v_bugs->bug_status") ?></span>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($v_bugs->priority == 'High') {
                                                        $plabel = 'danger';
                                                    } elseif ($v_bugs->priority == 'Medium') {
                                                        $plabel = 'info';
                                                    } else {
                                                        $plabel = 'primary';
                                                    }
                                                    ?>
                                                    <span
                                                        class="badge btn-<?= $plabel ?>"><?= ucfirst($v_bugs->priority) ?></span>
                                                </td>
                                                <td>
                                                    <span
                                                        class="badge btn-<?= $badge ?> "><?= $reporter->username ?></span>
                                                </td>
                                                <td>
                                                    <?php

                                                    if ($v_bugs->permission != 'all') {
                                                        $get_permission = json_decode($v_bugs->permission);

                                                        if (!empty($get_permission)) :
                                                            foreach ($get_permission as $permission => $v_permission) :
                                                                $user_info = $this->db->where(array('user_id' => $permission))->get('tbl_users')->row();
                                                                if ($user_info->role_id == 1) {
                                                                    $label = 'circle-danger';
                                                                } else {
                                                                    $label = 'circle-success';
                                                                }
                                                                $profile_info = $this->db->where(array('user_id' => $permission))->get('tbl_account_details')->row();
                                                                ?>

                                                                <a href="#" data-toggle="tooltip" data-placement="top"
                                                                   title="<?= $profile_info->fullname ?>"><img
                                                                        src="<?= base_url() . $profile_info->avatar ?>"
                                                                        class="img-circle img-xs" alt="">
                                                <span style="margin: 0px 0 8px -10px;"
                                                      class="circle <?= $label ?>  circle-lg"></span>
                                                                </a>

                                                                <?php
                                                            endforeach;
                                                        endif;
                                                    } else { ?>
                                                        <strong><?= lang('everyone') ?></strong>
                                                        <i
                                                            title="<?= lang('permission_for_all') ?>"
                                                            class="fa fa-question-circle" data-toggle="tooltip"
                                                            data-placement="top"></i>
                                                        <?php
                                                    }
                                                    ?>
                                                    <?php if ($this->session->userdata('user_type') == 1) { ?>
                                                        <span data-placement="top" data-toggle="tooltip"
                                                              title="<?= lang('add_more') ?>">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/bugs/update_users/<?= $v_bugs->bug_id ?>"
                                               class="text-default ml"><i class="fa fa-plus"></i></a>
                                                </span>
                                                    <?php } ?>


                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            
            
            
            
            
                <!--            *************** Script tab start ************-->
            <div class="tab-pane" id="script" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                           <strong> <?= 'Script'; ?></strong>
                        </div>
                    </div>
                    <div class="panel-body">
                   <center>
            <?php 
            $script = $this->db->where('lead_id',$client_details->lead_id)->get('debtco_webform')->row();
            if(!empty($script)){
            ?>
			<form action="<?php  echo base_url('admin/vicidial/update_debtco_webform'); ?>" method='post' id='vicidial_form'>
			
			<table class="table table-bordered">
				<tr>
						<td width="400px"><strong>Gross Salary per month: </strong></td>
						<td><input type="text" name="gross_salary" class="form-control" value="<?php echo $script->gross_salary ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td width="400px"><strong>Net Salary per month:</strong></td>
						<td><input type="text" name="net_salary" class="form-control" value="<?php  echo $script->net_salary ?: ''; ?>" /></td>
				</tr>
				
				<tr>
					<td width="400px"><strong>First Paymet is DC fee:</strong></td>
					<td><select name="first_payment" class="form-control">
					<option value="">--</option>
					<option value="Ja"  <?php echo ($script->first_payment == 'Ja') ? 'selected': ''; ?> >Ja / Yes</option>
					<option value="Nee" <?php echo ($script->first_payment == 'Nee') ? 'selected': ''; ?> >Nee / No</option>
				</select></td>
				</tr>
				<tr>
						<td width="400px"><strong>Second payment is legal fee:</strong></td>
						<td><select name="second_payment" class="form-control">
					<option value="">--</option>
					<option value="Ja" <?php echo ($script->first_payment == 'Ja') ? 'selected': ''; ?> >Ja / Yes</option>
					<option value="Nee" <?php echo ($script->first_payment == 'Nee') ? 'selected': ''; ?> >Nee / No</option>
				</select></td>
				</tr>
				<tr>
						<td width="400px"><strong>Third Month (Creditors received payment):</strong></td>
						<td>
							<select name="third_month" class="form-control">
							<option value="">--</option>
							<option value="Ja" <?php echo ($script->third_month == 'Ja') ? 'selected': ''; ?> >Ja / Yes</option>
							<option value="Nee" <?php echo ($script->third_month == 'Nee') ? 'selected': ''; ?> >Nee / No</option>
							</select>
						</td>
				</tr>
				<tr>
						<td width="400px"><strong>Clients consents to not being able to use old accounts<br/> or open new accounts once under debt review:</strong></td>
						<td><select name="debtreview_consent" class="form-control">
					<option value="">--</option>
					<option value="Ja" <?php echo ($script->debtreview_consent == 'Ja') ? 'selected': ''; ?> >Ja / Yes</option>
					<option value="Nee" <?php echo ($script->debtreview_consent == 'Nee') ? 'selected': ''; ?> >Nee / No</option>
				</select></td>
				</tr>
				<tr>
						<td width="400px"><strong>No estimation of the period under debt review can be given<br/> - once court order is granted we will have an exact period.</strong></td>
						<td><select name="estimation_period" class="form-control">
					<option value="">--</option>
					<option value="Ja" <?php echo ($script->estimation_period == 'Ja') ? 'selected': ''; ?>>Ja / Yes</option>
					<option value="Nee" <?php echo ($script->estimation_period == 'Nee') ? 'selected': ''; ?>>Nee / No</option>
				</select></td>
				</tr>
			
			</table>
			
			<br/><br/>
			
			<table class="table table-bordered">
				<tr>
						<td colspan="2" align="center"><strong>WERK BESONDERHERE / EMPLOYMENT DETAILS </strong></td>
				</tr>
				<tr>
						<td ><strong>Place of Work:</strong></td>
						<td><input type="text" name="work_place" class="form-control" value="<?php  echo $script->work_place ?: ''; ?>" /></td>
				</tr>
					<tr>
						<td><strong>Work Address:</strong></td>
						<td><input type="text" name="work_address" class="form-control" value="<?php  echo $script->work_address ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Contact details:</strong></td>
						<td><input type="text" name="work_number" class="form-control" value="<?php echo $script->work_number ?: ''; ?>" /></td>
				</tr>
			</table>
			
			<br/><br/>
			
			<table class="table table-bordered">
				<tr>
						<td colspan="4" align="center"><strong>KLIENT BESONDERHERE / CLIENT DETAILS </strong></td>
				</tr>
				<tr>
						<td><strong>Titel / Title: </strong></td>
						<td colspan="3"><input type="text" name="title" class="form-control" value="<?php echo $script->title ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Volle name / <br/> Full Names:</strong></td>
						<td colspan="3"><input type="text" name="full_name" class="form-control" required value="<?php echo $script->full_name ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Van / Surname:</strong></td>
						<td colspan="3"><input type="text" name="surname" class="form-control" required value="<?php echo $script->surname ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>ID-nommer / <br/> ID Number:</strong></td>
						<td colspan="3"><input type="text" name="id_number" class="form-control" value="<?php echo $script->id_number ?: '' ?>" /></td>
				</tr>
				<tr>
						<td><strong>E-posadres / email:</strong></td>
						<td colspan="3"> <input type="email" name="email" class="form-control" required value="<?php echo $script->email ?: ''; ?>" /></td>
				</tr>
				<tr>
					<td><strong>Client Address:</strong></td>
					<td colspan="3"><input type="text" name="client_address" class="form-control" value="<?php echo $script->client_address ?: ''; ?>" /></td>
				</tr>
				<tr>
					<td><strong>Next of Kin Name:</strong></td>
					<td><input type="text" name="next_of_kin_name" class="form-control" value="<?php  echo $script->next_of_kin_name ?: ''; ?>" /></td>
					<td><strong>Next of Kin Number:</strong></td>
					<td><input type="text" name="next_of_kin_number" class="form-control" value="<?php echo $script->next_of_kin_number ?: ''; ?>" /></td>
				</tr>
				<tr>
					<td><strong>Relationship Status:</strong></td>
					<td colspan="3">
						<select name="relationship_status" class="form-control">
							<option value="">--</option>
							<option value="Single" <?php echo ($script->relationship_status == 'Single') ? 'selected': ''; ?> >Single</option>
							<option value="Widowed" <?php echo ($script->relationship_status == 'Widowed') ? 'selected': ''; ?> >Widowed</option>
							<option value="Married" <?php echo ($script->relationship_status == 'Married') ? 'selected': ''; ?> >Married</option>
							<option value="Separated " <?php echo ($script->relationship_status == 'Separated') ? 'selected': ''; ?> >Separated </option>
							<option value="Divorced " <?php echo ($script->relationship_status == 'Divorced') ? 'selected': ''; ?> >Divorced </option>
							<option value="Married In Community of Property" <?php echo ($script->relationship_status == 'Married In Community of Property') ? 'selected': ''; ?> >Married In Community of Property</option>
							<option value="Married Out of Community of Property" <?php echo ($script->relationship_status == 'Married Out of Community of Property') ? 'selected': ''; ?> >Married Out of Community of Property</option>
						</select>
					</td>
				</tr>
			</table>
			
			<br/><br/>
			
			<table class="table table-bordered">
			<tr>
				<td>
				<div class="form-group row">
				<div class="col-md-4"><strong>Oproep opname verwysing / Call recording reference:</strong></div> <div class="col-md-8"><input type="text" name="call_recording_reference" class="form-control"  value="<?php  echo $script->call_recording_reference ?: ''; ?>" /></div>
				</div>
				</div>
		</td>
			</tr>
			</table>
		
			<br/><br/>
		
			<table class="table table-bordered">
				<tr>
						<td colspan="2" align="center"><strong>BANK BESONDERHERE / BANK DETAILS </strong></td>
				</tr>
				<!-- <tr><td colspan="2"></td><td></td></tr>-->
				<tr>
						<td><strong>Rekeninghouer / Account holder:</strong> </td>
						<td><input type="text" name="account_holder" class="form-control" value="<?php  echo $script->account_holder ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Banknaam / Bank name:</strong></td>
						<td><input type="text" name="bank_name" class="form-control" value="<?php  echo $script->bank_name ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Takkode / Branch code:</strong></td>
						<td><input type="text" name="branch_code" class="form-control" value="<?php  echo $script->branch_code ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Tipe rekening / Type of account:</strong></td>
						<td>
						<select name="account_type" class="form-control">
								<option value="">--</option>
								<option value="Cheque" <?php echo ($script->account_type == 'Cheque') ? 'selected': ''; ?>>Cheque</option>
								<option value="Savings" <?php echo ($script->account_type == 'Savings') ? 'selected': ''; ?>>Savings</option>
							</select>
						</td>
				</tr>
				<tr>
						<td><strong>Rekeningnommer / Account Number:</strong></td>
						<td><input type="text" name="account_number" class="form-control" value="<?php  echo $script->account_number ?: ''; ?>" /></td>
				</tr>
				<tr>
				    	<td><strong>Dag waarop debietorder ingevorder moet word /  <br/> Day on which debit order should be processed</strong></td>
				<td><select name="debit_day" class="form-control">
							<option value="">--</option>
							<?php for($i=0; $i <= 31; $i++){ ?>
							<option value="<?php echo $i ?>" <?php echo ($script->debit_day == $i)? 'selected': ''; ?>><?php echo $i ?></option>
							<?php } ?>
							</select></td>
				</tr>
				<tr>
					<td><strong>Datum waarop salaris betaal word /  <br/> Date when the salary is paid into the account</strong></td>
					<td>
					<div class="input-group">
						<input type="text" name="salary_date" value="<?php echo $script->salary_date ?: ''; ?>" class="form-control datepicker" > 
							<div class="input-group-addon"> 
								<a href="#"><i class="fa fa-calendar"></i></a>
                            </div>
                        </div>
					</td>
				</tr>
				<tr>
					<td><strong>Die kliënt gee toestemming om aftrekkings te maak. /<br/> The client gives consent to deduct from above account.</strong></td>
					<td>
						<select name="client_deduct_consent" class="form-control">
							<option value="">--</option>
							<option value="Ja" <?php echo ($script->client_deduct_consent == 'Ja') ? 'selected': ''; ?>>Ja / Yes</option>
							<option value="Nee" <?php echo ($script->client_deduct_consent == 'Nee') ? 'selected': ''; ?>>Nee / No</option>
						</select>
					</td>
				</tr>
			</table>
			<br/><br/>
			
	
			
			<table class="table table-bordered">
				<tr rowspan="4">
					<td colspan="4"><input type="hidden" name="lead_id" value="<?php echo $script->id; ?>" /><input type="submit" class="btn btn-primary pull-right" /></td>
				</tr>
			</table>
				
			
			   
			</form>
			<?php } ?>
                    </div>
                  
                </section>
            </div>
            
            
        </div>
    </div>
</div>

<script>
    function set_client_step(stepId, clientId){
	$.ajax({
		url: '<?php echo base_url('admin/client/update_step'); ?>/' + clientId + '/'+ stepId,
		success: function(data){
			console.log(data);
			if(data == '1'){
				//$('#step_saved').removeClass('Hidden');
				$('#step_saved').html('Step has been Updated');
			}
		}
	});
}
</script>
