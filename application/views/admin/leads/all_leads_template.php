<?= message_box('success'); ?>
<?= message_box('error'); ?>


<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage"
                                                            data-toggle="tab"><?= lang('all_leads') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#create"
                                                            data-toggle="tab"><?= lang('new_leads') ?></a></li>
        <li><a style="background-color: #1797be;color: #ffffff"
               href="<?= base_url() ?>admin/leads/import_leads"><?= lang('import_leads') ?></a></li>  
	
    </ul>
    <div class="tab-content bg-white">
        <!--  general -->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">

            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= 'Name' ?></th>
                        <th><?= 'Surname' ?></th>
                        <th><?= 'Gender' ?></th>
                        <th><?= 'Product' ?></th>
                        <th><?= 'Policy' ?></th>
                        <th><?= 'Email' ?></th>
						<th><?= lang('lead_status') ?></th>
                        <th class="col-options no-sort"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$table_id = 'id'; // 'leads_id;'
					$table = 'campaign_leads'; // 'tbl_leads
                    if (!empty($all_leads)):foreach ($all_leads as $v_leads):
                            ?>
                            <tr>
                                <td><?= $v_leads->first_name ?></td>
                                <td><?= $v_leads->last_name ?></td>
                                <td><?= $v_leads->gender ?></td>
                                <td><?= $v_leads->product ?></td>
                                <td><?= $v_leads->policy_number ?></td>
                                <td><?= $v_leads->email ?></td>
                                <td><?php
                                    if (!empty($v_leads->lead_status_id)) {
                                        $lead_status = $this->db->where('lead_status_id', $v_leads->lead_status_id)->get('tbl_lead_status')->row();

                                       echo "<span class='label label-success'>" . $lead_status->lead_status . "</span>";
                                        // $status = "<span class='label label-success'>" . lang($lead_status->lead_type) . "</span>";
                                        
                                        // echo $status . ' ' . $lead_status->lead_status;
                                    }
                                    ?>     </td>
                                <td>
                                        <?= btn_edit('admin/leads/index_leads/' . $v_leads->list_id .'/'.$v_leads->$table_id) ?>
                                        <?php // btn_delete('admin/leads/delete_leads/' . $v_leads->$table_id) ?>
                                </td>
                            </tr>
                            <?php
                    endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
            <form role="form" enctype="multipart/form-data" id="form"
                  action="<?php echo base_url(); ?>admin/leads/saved_leads_template/<?php
                  if (!empty($leads_info)) {
                      echo $leads_info->list_id . '/' . $leads_info->$table_id;
                  }
                  ?>" method="post" class="form-horizontal  ">

                <div class="panel-body">
					<div class="form-group">
						<label class="col-lg-2 control-label"><?= 'Title' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->title;
                            }
                            ?>" name="title" >
                        </div>
						
						<label class="col-lg-2 control-label"><?= 'Gender' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->gender;
                            }
                            ?>" name="gender" >
                        </div>
						
						
					</div>
                    <div class="form-group">
						
						<label class="col-lg-2 control-label"><?= 'Name' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->first_name;
                            }
                            ?>" name="first_name" >
                        </div>
						
						<label class="col-lg-2 control-label"><?= 'Surname' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->last_name;
                            }
                            ?>" name="last_name" >
                        </div>
						
                    </div>
					
					<div class="form-group">
						<label class="col-lg-2 control-label"><?= 'D.O.B' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->date_of_birth;
                            }
                            ?>" name="date_of_birth" >
                        </div>
						
						<label class="col-lg-2 control-label"><?= 'Age' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->age;
                            }
                            ?>" name="age" >
                        </div>
						
						
					</div>
					
				
					<div class="form-group">
							
						<label class="col-lg-2 control-label"><?= 'Effective Date' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->effective_date;
                            }
                            ?>" name="effective_date" >
                        </div>
						
						
						<label class="col-lg-2 control-label"><?= 'Policy Number' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->policy_number;
                            }
                            ?>" name="policy_number">
                        </div>
						
                    </div>
					
					<div class="form-group">
							
						<label class="col-lg-2 control-label"><?= 'Policy Status' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->policy_status;
                            }
                            ?>" name="policy_status" >
                        </div>
						
						
						<label class="col-lg-2 control-label"><?= 'Policy Status (payment history)' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->policy_status_per_history;
                            }
                            ?>" name="policy_status_per_history">
                        </div>
						
                    </div>
					
					
					<div class="form-group">
						<label class="col-lg-2 control-label"><?= 'Broker_channel' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->broker_channel;
                            }
                            ?>" name="broker_channel" >
                        </div>
						
						
						<label class="col-lg-2 control-label"><?= 'Product' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->product;
                            }
                            ?>" name="product">
                        </div>
                    </div>
					
					<div class="form-group">
						<label class="col-lg-2 control-label"><?= 'Inception Date' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->inception_date;
                            }
                            ?>" name="inception_date" >
                        </div>
						
						
						<label class="col-lg-2 control-label"><?= 'Relationship' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->relationship;
                            }
                            ?>" name="relationship">
                        </div>
                    </div>
					
					 <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('email') ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->email;
                            }
                            ?>" name="email">
                        </div>
						
						<label class="col-lg-2 control-label"><?= 'ID Number' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->id_number;
                            }
                            ?>" name="id_number">
                        </div>
						
						</div>
					
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Work Number' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->work_number;
                            }
                            ?>" name="work_number" >
                        </div>
						
						<label class="col-lg-2 control-label"><?= 'Cell Number' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->cell_number;
                            }
                            ?>" name="cell_number" >
                        </div>
						
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('address') ?> 1<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address1;
                            }
                            ?>" name="address1" >
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('address') ?> 2<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address2;
                            }
                            ?>" name="address2" >
                        </div>

                    </div>
                    <!-- End discount Fields -->
                     <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('address') ?> 3<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address3;
                            }
                            ?>" name="address3" >
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('address') ?> 4<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address4;
                            }
                            ?>" name="address4" >
                        </div>

                    </div>
					
					 <div class="form-group">
                        <label class="col-lg-2 control-label">Postal Code<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->postal_code;
                            }
                            ?>" name="postal_code" >
                        </div>
                        <label class="col-lg-2 control-label">Agent Code<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->agent_code;
                            }
                            ?>" name="agent_code" >
                        </div>
                    </div>
					
				<div class="form-group">
                        <label class="col-lg-2 control-label">Payment Method<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->payment_method;
                            }
                            ?>" name="payment_method" >
                        </div>
						
                        <label class="col-lg-2 control-label">Deduction Day<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->deduction_day;
                            }
                            ?>" name="deduction_day" >
                        </div>
                    </div>

				<hr/>
				
					<div class="form-group">
                        <label class="col-lg-2 control-label">Account Holder<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->debit_order_account_holder;
                            }
                            ?>" name="debit_order_account_holder" >
                        </div>
						
                        <label class="col-lg-2 control-label">Account Number<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->debit_order_account_number;
                            }
                            ?>" name="debit_order_account_number" >
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="col-lg-2 control-label">Bank Name<span
                                class="text-danger"></span></label>
                        <div class="col-lg-2">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->debit_order_bank_name;
                            }
                            ?>" name="debit_order_bank_name" >
                        </div>
						
                        <label class="col-lg-2 control-label">Branch Code<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->debit_order_branch_code;
                            }
                            ?>" name="debit_order_branch_code" >
                        </div>
						
						<label class="col-lg-2 control-label">Account Type<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->debit_order_account_type;
                            }
                            ?>" name="debit_order_account_type" >
                        </div>
						
                    </div>
					
					<hr/>
					<hr/>
					<div class="form-group">
                        <label class="col-lg-2 control-label">Sum Insured Amount<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->sum_assured_amount;
                            }
                            ?>" name="sum_assured_amount" >
                        </div>
						
                        <label class="col-lg-2 control-label">Total Premium<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->total_premium;
                            }
                            ?>" name="total_premium" >
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="col-lg-2 control-label">Cover Amount<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->cover_amount;
                            }
                            ?>" name="cover_amount" >
                        </div>
						
                        <label class="col-lg-2 control-label">Total Cover<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->total_cover;
                            }
                            ?>" name="total_cover" >
                        </div>
                    </div>
					
					<hr/>
					
					
                    <div class="form-group">
					
                        <label class="col-lg-2 control-label"><?= lang('lead_status') ?> </label>
                        <div class="col-lg-4">
                            <select name="lead_status_id" class="form-control select_box" style="width: 100%"
                                    >
                                <?php

                                if (!empty($status_info)) {
                                    foreach ($status_info as $type => $v_leads_status) {
                                        if (!empty($v_leads_status)) {
                                            ?>
                                            <optgroup label="<?= lang($type) ?>">
                                                <?php foreach ($v_leads_status as $v_l_status) { ?>
                                                    <option
                                                            value="<?= $v_l_status->lead_status_id ?>" <?php
                                                    if (!empty($leads_info->lead_status_id)) {
                                                        echo $v_l_status->lead_status_id == $leads_info->lead_status_id ? 'selected' : '';
                                                    }
                                                    ?>><?= $v_l_status->lead_status ?></option>
                                                <?php } ?>
                                            </optgroup>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
							
								<label class="col-lg-2 control-label"></label>
                        <div class="col-lg-4">
						</div>
						
                        </div>
					</div>
				
				
                    <div class="form-group" id="border-none">
                        <label class="col-lg-2 control-label"><?='comments' ?> </label>
                        <div class="col-lg-8">
                            <textarea name="comments" class="form-control textarea"><?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->comments;
                                }
                                ?></textarea>
                        </div>
                    </div>
                    <?php
                    if (!empty($leads_info)) {
                        $leads_id = $leads_info->$table_id;
                    } else {
                        $leads_id = null;
                    }
                    ?>
					
				
									
									
                    <?= custom_form_Fields(5, $leads_id, true); ?>

                    <div class="form-group" id="border-none">
                        <label for="field-1" class="col-sm-2 control-label"><?= lang('assigned_to') ?> <span
                                class="required"></span></label>
                        <div class="col-sm-9">
                            <div class="checkbox c-radio needsclick">
                                <label class="needsclick">
                                    <input id="" <?php
                                    if (!empty($leads_info->permission) && $leads_info->permission == 'all') {
                                        echo 'checked';
                                    } elseif (empty($leads_info)) {
                                        echo 'checked';
                                    }
                                    ?> type="radio" name="permission" value="everyone">
                                    <span class="fa fa-circle"></span><?= lang('everyone') ?>
                                    <i title="<?= lang('permission_for_all') ?>"
                                       class="fa fa-question-circle" data-toggle="tooltip"
                                       data-placement="top"></i>
                                </label>
                            </div>
                            <div class="checkbox c-radio needsclick">
                                <label class="needsclick">
                                    <input id="" <?php
                                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                        echo 'checked';
                                    }
                                    ?> type="radio" name="permission" value="custom_permission"
                                    >
                                    <span class="fa fa-circle"></span><?= lang('custom_permission') ?> <i
                                        title="<?= lang('permission_for_customization') ?>"
                                        class="fa fa-question-circle" data-toggle="tooltip"
                                        data-placement="top"></i>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group <?php
                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                        echo 'show';
                    }
                    ?>" id="permission_user_1">
                        <label for="field-1"
                               class="col-sm-2 control-label"><?= lang('select') . ' ' . lang('users') ?>
                            <span
                                class="required"></span></label>
                        <div class="col-sm-9">
                            <?php
                            if (!empty($assign_user)) {
                                foreach ($assign_user as $key => $v_user) {

                                    if ($v_user->role_id == 1) {
                                        $disable = true;
                                        $role = '<strong class="badge btn-danger">' . lang('admin') . '</strong>';
                                    } else {
                                        $disable = false;
                                        $role = '<strong class="badge btn-primary">' . lang('staff') . '</strong>';
                                    }

                                    ?>
                                    <div class="checkbox c-checkbox needsclick">
                                        <label class="needsclick">
                                            <input type="checkbox"
                                                <?php
                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            echo 'checked';
                                                        }
                                                    }

                                                }
                                                ?>
                                                   value="<?= $v_user->user_id ?>"
                                                   name="assigned_to[]"
                                                   class="needsclick">
                                                        <span
                                                            class="fa fa-check"></span><?= $v_user->username . ' ' . $role ?>
                                        </label>

                                    </div>
                                    <div class="action_1 p
                                                <?php

                                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                        $get_permission = json_decode($leads_info->permission);

                                        foreach ($get_permission as $user_id => $v_permission) {
                                            if ($user_id == $v_user->user_id) {
                                                echo 'show';
                                            }
                                        }

                                    }
                                    ?>
                                                " id="action_1<?= $v_user->user_id ?>">
                                        <label class="checkbox-inline c-checkbox">
                                            <input id="<?= $v_user->user_id ?>" checked type="checkbox"
                                                   name="action_1<?= $v_user->user_id ?>[]"
                                                   disabled
                                                   value="view">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('view') ?>
                                        </label>
                                        <label class="checkbox-inline c-checkbox">
                                            <input <?php if (!empty($disable)) {
                                                echo 'disabled' . ' ' . 'checked';
                                            } ?> id="<?= $v_user->user_id ?>"
                                                <?php

                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);

                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            if (in_array('edit', $v_permission)) {
                                                                echo 'checked';
                                                            };

                                                        }
                                                    }

                                                }
                                                ?>
                                                 type="checkbox"
                                                 value="edit" name="action_<?= $v_user->user_id ?>[]">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('edit') ?>
                                        </label>
                                        <label class="checkbox-inline c-checkbox">
                                            <input <?php if (!empty($disable)) {
                                                echo 'disabled' . ' ' . 'checked';
                                            } ?> id="<?= $v_user->user_id ?>"
                                                <?php

                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            if (in_array('delete', $v_permission)) {
                                                                echo 'checked';
                                                            };
                                                        }
                                                    }

                                                }
                                                ?>
                                                 name="action_<?= $v_user->user_id ?>[]"
                                                 type="checkbox"
                                                 value="delete">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('delete') ?>
                                        </label>
                                        <input id="<?= $v_user->user_id ?>" type="hidden"
                                               name="action_<?= $v_user->user_id ?>[]" value="view">

                                    </div>


                                    <?php
                                }
                            }
                            ?>


                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                        <?php if (empty($leads_info->converted_client_id) || $leads_info->converted_client_id == 0) { ?>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-sm btn-primary"><?= lang('updates') ?></button>
                            </div>
                        <?php } ?>
                    </div>
            </form>
        </div>
    </div>
</div>