<?= message_box('success'); ?>
<?= message_box('error'); ?>


<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage"
                                                            data-toggle="tab"><del><?= lang('all_leads') ?></del></a></li>
    </ul>
    <div class="tab-content bg-white">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">

            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><del><?= 'List ID'; ?></del></th>
                        <th><del><?= 'Created' ?></del></th>
                         <th><del><?= 'Called / Total'; ?></del></th>
                        <th><del><?= 'Action'; ?></del></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$table_id = 'id';
					$table = 'lead_list_archived';
                    if (!empty($all_leads_archived)):foreach ($all_leads_archived as $v_leads):
                            ?>
                            <tr>
                                <td>
                                     <a href="<?= base_url() ?>admin/leads/index_leads_archived//<?= $v_leads->$table_id ?>"><del><?= $v_leads->id ?></del></a>
                                </td>
                                <td><del><?= $v_leads->added ?></del></td>
                                <td><del><?= $v_leads->dispositioned .' /  '. $v_leads->total_leads; ?></del></td>
                                <td>
                                    <a href="<?php echo base_url('admin/leads/export_leads_archived/'.$v_leads->id);?>" class="btn btn-primary btn-xs">Export List</a>
                                </td>
                            </tr>
                            <?php
                    endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>