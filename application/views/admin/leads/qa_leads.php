<?= message_box('success'); ?>
<?= message_box('error'); ?>

<?php 
$margin = 'margin-bottom:15px';
$report_campaign_id = ($campaign_id)?$campaign_id:0;

//echo $date_from. ' | ';
//echo $date_to;
 if($date_from < '2018-12-03 23:59:59'){
	$total = $this->db->where(array('lead_status_id' => 26))->count_all_results('view_campaign_leads');
	$total_approved = $this->db->where(array('lead_status_id' => 26, 'qa_status' => 1))->count_all_results('view_campaign_leads'); 
	$total_pending = $this->db->where(array('lead_status_id' => 26, 'qa_status' => 2))->count_all_results('view_campaign_leads');
	$total_dnp = $this->db->where(array('lead_status_id' => 26, 'qa_status' => 3))->count_all_results('view_campaign_leads');
	$total_dnq = $this->db->where(array('lead_status_id' => 26, 'qa_status' => 4))->count_all_results('view_campaign_leads');
	$unattended = $this->db->where(array('lead_status_id' => 26, 'qa_status' => 0))->count_all_results('view_campaign_leads'); 
 }else{
	$total = $this->db->where(array('lead_status_id' => 26,  'dispo_date >= ' => $date_from, 'dispo_date <= ' => $date_to))->count_all_results('view_campaign_leads');
	$total_approved = $this->db->where(array('lead_status_id' => 26, 'qa_status' => 1,  'dispo_date >= ' => $date_from, 'dispo_date <= ' => $date_to))->count_all_results('view_campaign_leads'); 
	$total_pending = $this->db->where(array('lead_status_id' => 26, 'qa_status' => 2,  'dispo_date >= ' => $date_from, 'dispo_date <= ' => $date_to))->count_all_results('view_campaign_leads');
	$total_dnp = $this->db->where(array('lead_status_id' => 26, 'qa_status' => 3,  'dispo_date >= ' => $date_from, 'dispo_date <= ' => $date_to))->count_all_results('view_campaign_leads');
	$total_dnq = $this->db->where(array('lead_status_id' => 26, 'qa_status' => 4,  'dispo_date >= ' => $date_from, 'dispo_date <= ' => $date_to))->count_all_results('view_campaign_leads');
	$unattended = $this->db->where(array('lead_status_id' => 26, 'qa_status' => 0,  'dispo_date >= ' => $date_from, 'dispo_date <= ' => $date_to))->count_all_results('view_campaign_leads');
 }
?>
 
<div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
	<div class="col-md-4">
		<div class="row row-table pv-lg">
			<div class="col-xs-6">
				<p class="m0 lead"><?= ($total) ?></p>
				<p class="m0">
					<small><?= 'Total Interested in Queue' ?></small>
				</p>
			</div>
			<div class="col-xs-6 ">
				<p class="m0 lead"><?= ($total_approved) ?></p>
				<p class="m0">
					<small><?= 'Total Passed' ?></small>
				</p>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="row row-table pv-lg">
			<div class="col-xs-6">
				<p class="m0 lead"><?= ($total_pending) ?></p>
				<p class="m0">
					<small><?= 'Total Pending' ?></small>
				</p>
			</div>
			<div class="col-xs-6 ">
				<p class="m0 lead"><?= ($total_dnp) ?></p>
				<p class="m0">
					<small><?= 'Total Does Pass' ?></small>
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="row row-table pv-lg">
		    			    <div class="col-xs-6 ">
				<p class="m0 lead"><?= ($total_dnq) ?></p>
				<p class="m0">
					<small><?= 'Total DNQ' ?></small>
				</p>
			</div>
			<div class="col-xs-6">

				<p class="m0 lead"><?= $unattended ?></p>
				<p class="m0">
					<small><?= 'Not Attended' ?></small>
				</p>
			</div>
		</div>
	</div>
</div>

<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage"
                                                            data-toggle="tab"><?= lang('all_leads') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#create"
                                                            data-toggle="tab"><?= lang('new_leads') ?></a></li>
      <!--  <li><a style="background-color: #1797be;color: #ffffff"
               href="<?= base_url() ?>admin/leads/import_leads"><?= lang('import_leads') ?></a></li>   -->
	
    </ul>
    <div class="tab-content bg-white">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            
        <div class="form-group row">
       
		<a style="margin-left: 5px"
				   href="<?php echo base_url('admin/leads/analysis_report/qa/'.$report_campaign_id.'/'.urlencode($date_from).'/'.urlencode($date_to)); ?>" id="hrefUrl"
				   data-toggle="tooltip" data-placement="top" title="" data-original-title="Report"
				   class="btn btn-xs btn-success pull-right">
					<i class="fa fa-file-pdf-o"></i>
				</a>
				
			<form method="post" action="<?php echo base_url('admin/leads/sale_leads');?>" id="daterange_form">
		
                <input name="daterange" type="text" class="form-control pull-right" value="<?php echo $date_from . ' - ' . $date_to; ?>" id="daterange" style="width: 200px">
            
                <select style="width: 250px" name="campaign_id" onchange="daterange_form.submit()">
                    <option value="0"> Select Campaign </option>
                    <?php if ($campaigns){ foreach($campaigns as $campaign) { ?>
                        <option value="<?php echo $campaign->id ;?>"><?php echo $campaign->campaign_name ;?></option>
                    <?php }  } ?>
                </select>

        	</form>
		</div>
		
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= 'Company name' ?></th>
                        <th><?= 'Full name' ?></th>
                        <th><?= lang('email') ?></th>
                        <th><?= lang('lead_status') ?></th>
                        <th><?= 'listID' ?></th>
                        <th><?= 'Campaign Name' ?></th>
                        <th><?= 'QA status' ?></th>
                        <th><?= 'username' ?></th>
                        <!--<th><?= 'Alternative No.' ?></th>-->
                        <th><?= 'Source' ?></th>
						<th><?= 'DateTime' ?></th>
                        <th class="col-options no-sort"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$table_id = 'id'; // 'leads_id;'
					$table = 'view_campaign_leads'; // 'tbl_leads
                    if (!empty($all_leads)):foreach ($all_leads as $v_leads):
                            ?>
                            <tr>
                                <td>
                                     <a href="<?= base_url() ?>admin/campaigns/campaign_questions/<?php echo $v_leads->campaign_id; ?>?entry_list_id=<?= $v_leads->$table_id ?>" target="_blank"><?= $v_leads->company_name ?></a> 
                                </td>
                                <td><?= $v_leads->first_name ?> <?= $v_leads->last_name ?></td>
                                <td><?= $v_leads->email ?></td>
                                <td><?php
                                    if (!empty($v_leads->lead_status_id)) {
                                        $lead_status = $this->db->where('lead_status_id', $v_leads->lead_status_id)->get('tbl_lead_status')->row();

                                       echo "<span class='label label-success'>" . $lead_status->lead_status . "</span>";
                                        // $status = "<span class='label label-success'>" . lang($lead_status->lead_type) . "</span>";
                                        
                                        // echo $status . ' ' . $lead_status->lead_status;
                                    }
                                    ?>     </td>
									
								<td><?php echo $v_leads->list_id; ?></td>
								<td><?php echo $v_leads->campaign_name; ?></td>
								<td>
								<?php if($v_leads->qa_status ==1){  ?>
									Passed
								<?php }elseif($v_leads->qa_status ==2){ ?>
									Pending
								<?php }elseif($v_leads->qa_status ==3){ ?>
									Does Not Pass
								<?php }elseif($v_leads->qa_status ==4){ ?>
									Does Not Qualify
								<?php } ?>
								</td>
								<td><?php echo $v_leads->username; ?></td>
								<!--<td><?php echo $v_leads->time_called; ?></td>-->
								<td><?php echo $v_leads->lead_source; ?></td>
								<td><?php echo $v_leads->updated; ?></td>
                                <td>
                                        <?= btn_edit('admin/campaigns/campaign_questions/'.$v_leads->campaign_id.'?entry_list_id='.$v_leads->$table_id) ?>
                                        <?php // btn_delete('admin/leads/delete_leads/' . $v_leads->$table_id) ?>
                                </td>
                            </tr>
                            <?php
                    endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
            <form role="form" enctype="multipart/form-data" id="form"
                  action="<?php echo base_url(); ?>admin/leads/saved_leads/<?php
                  if (!empty($leads_info)) {
                      echo $leads_info->list_id . '/' . $leads_info->$table_id;
                  }
                  ?>" method="post" class="form-horizontal  ">

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'company_name' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->company_name;
                            }
                            ?>" name="company_name" required="">
                        </div>
						
						<label class="col-lg-2 control-label"><?= 'first_name' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->first_name;
                            }
                            ?>" name="first_name" required="">
                        </div>
						<!--
                        <label class="col-lg-2 control-label"><?= lang('select') ?> </label>
                        <div class="col-lg-4">
                            <select name="client_id" class="form-control select_box" style="width: 100%" >
                                <option value=""><?= lang('select_client') ?></option>
                                <?php
                                $all_client = $this->db->get('tbl_client')->result();
                                if (!empty($all_client)) {
                                    foreach ($all_client as $v_client) {
                                        ?>
                                        <option value="<?= $v_client->client_id ?>" <?php
                                        if (!empty($leads_info) && $leads_info->client_id == $v_client->client_id) {
                                            echo 'selected';
                                        }
                                        ?>><?= $v_client->name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
						-->
                    </div>
                    <div class="form-group">
					
                        <label class="col-lg-2 control-label"><?= lang('lead_status') ?> </label>
                        <div class="col-lg-4">
                            <select name="lead_status_id" class="form-control select_box" style="width: 100%"
                                    required="">
                                <?php

                                if (!empty($status_info)) {
                                    foreach ($status_info as $type => $v_leads_status) {
                                        if (!empty($v_leads_status)) {
                                            ?>
                                            <optgroup label="<?= lang($type) ?>">
                                                <?php foreach ($v_leads_status as $v_l_status) { ?>
                                                    <option
                                                            value="<?= $v_l_status->lead_status_id ?>" <?php
                                                    if (!empty($leads_info->lead_status_id)) {
                                                        echo $v_l_status->lead_status_id == $leads_info->lead_status_id ? 'selected' : '';
                                                    }
                                                    ?>><?= $v_l_status->lead_status ?></option>
                                                <?php } ?>
                                            </optgroup>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
							
								<label class="col-lg-2 control-label"></label>
                        <div class="col-lg-4">
						</div>
						
                        </div>
					</div>
					<div class="form-group">
							<label class="col-lg-2 control-label"><?= 'last_name' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->last_name;
                            }
                            ?>" name="last_name" required="">
                        </div>
						
							<label class="col-lg-2 control-label"><?= 'jobtitle' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->jobtitle;
                            }
                            ?>" name="jobtitle" required="">
                        </div>
						
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('email') ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->email;
                            }
                            ?>" name="email">
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('phone') ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->phone_number;
                            }
                            ?>" name="phone_number" required="">
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('address') ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address1;
                            }
                            ?>" name="address1" required="">
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('city') ?><span
                                class="text-danger"> *</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->city;
                            }
                            ?>" name="city" required="">
                        </div>

                    </div>
                    <!-- End discount Fields -->
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'zip' ?><span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-4">
                            <input type="text" min="0" required="" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->zip;
                            }
                            ?>" name="zip"/>
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('country') ?> </label>
                        <div class="col-lg-4">
                            <textarea name="country" class="form-control"><?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->country;
                                }
                                ?></textarea>
                        </div>


                    </div>
                    
                              <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Warm Lead Indicator' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->warm_lead_indicator;
                            }
                            ?>" name="warm_lead_indicator">
                    </div>
                      
                        <label class="col-lg-2 control-label"><?= 'linkedIn Link' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->linkedin_link;
                            }
                            ?>" name="linkedin_link">
                    </div>
                        </div>
					<!-- 
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'alternate_number' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->alternate_number;
                            }
                            ?>" name="alternate_number">
                        </div>
                        <label class="col-lg-2 control-label"><?= 'alternate_dm' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->alternate_dm;
                            }
                            ?>" name="alternate_dm">
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'country' ?></label>
                        <div class="col-lg-4">
                            <select name="country" class="form-control person select_box" style="width: 100%">
                                <optgroup label="Default Country">
                                    <?php if (!empty($leads_info->alternate_project)) { ?>
                                        <option value="<?= $leads_info->alternate_project ?>"><?= $leads_info->alternate_project ?></option>
                                    <?php } else { ?>
                                        <option
                                            value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                                    <?php } ?>
                                </optgroup>
                                <optgroup label="<?= lang('other_countries') ?>">
                                    <?php
                                    $countries = $this->db->get('tbl_countries')->result();
                                    if (!empty($countries)): foreach ($countries as $country):
                                        ?>
                                        <option value="<?= $country->value ?>"><?= $country->value ?></option>
                                        <?php
                                    endforeach;
                                    endif;
                                    ?>
                                </optgroup>
                            </select>
                        </div>
                        <label class="col-lg-2 control-label"><?= 'outcome' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->outcome;
                            }
                            ?>" name="outcome">
                        </div>


                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'date_worked' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->date_worked;
                            }
                            ?>" name="date_worked">
                        </div>
                        <label class="col-lg-2 control-label"><?= 'time_worked' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->time_worked;
                            }
                            ?>" name="time_worked">
                        </div>

                    </div>
					-->
                    <div class="form-group" id="border-none">
                        <label class="col-lg-2 control-label"><?='comments' ?> </label>
                        <div class="col-lg-8">
                            <textarea name="comments" class="form-control textarea"><?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->comments;
                                }
                                ?></textarea>
                        </div>
                    </div>
                    <?php
                    if (!empty($leads_info)) {
                        $leads_id = $leads_info->$table_id;
                    } else {
                        $leads_id = null;
                    }
                    ?>
					
				
									
									
                    <?= custom_form_Fields(5, $leads_id, true); ?>

                    <div class="form-group" id="border-none">
                        <label for="field-1" class="col-sm-2 control-label"><?= lang('assigned_to') ?> <span
                                class="required">*</span></label>
                        <div class="col-sm-9">
                            <div class="checkbox c-radio needsclick">
                                <label class="needsclick">
                                    <input id="" <?php
                                    if (!empty($leads_info->permission) && $leads_info->permission == 'all') {
                                        echo 'checked';
                                    } elseif (empty($leads_info)) {
                                        echo 'checked';
                                    }
                                    ?> type="radio" name="permission" value="everyone">
                                    <span class="fa fa-circle"></span><?= lang('everyone') ?>
                                    <i title="<?= lang('permission_for_all') ?>"
                                       class="fa fa-question-circle" data-toggle="tooltip"
                                       data-placement="top"></i>
                                </label>
                            </div>
                            <div class="checkbox c-radio needsclick">
                                <label class="needsclick">
                                    <input id="" <?php
                                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                        echo 'checked';
                                    }
                                    ?> type="radio" name="permission" value="custom_permission"
                                    >
                                    <span class="fa fa-circle"></span><?= lang('custom_permission') ?> <i
                                        title="<?= lang('permission_for_customization') ?>"
                                        class="fa fa-question-circle" data-toggle="tooltip"
                                        data-placement="top"></i>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group <?php
                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                        echo 'show';
                    }
                    ?>" id="permission_user_1">
                        <label for="field-1"
                               class="col-sm-2 control-label"><?= lang('select') . ' ' . lang('users') ?>
                            <span
                                class="required">*</span></label>
                        <div class="col-sm-9">
                            <?php
                            if (!empty($assign_user)) {
                                foreach ($assign_user as $key => $v_user) {

                                    if ($v_user->role_id == 1) {
                                        $disable = true;
                                        $role = '<strong class="badge btn-danger">' . lang('admin') . '</strong>';
                                    } else {
                                        $disable = false;
                                        $role = '<strong class="badge btn-primary">' . lang('staff') . '</strong>';
                                    }

                                    ?>
                                    <div class="checkbox c-checkbox needsclick">
                                        <label class="needsclick">
                                            <input type="checkbox"
                                                <?php
                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            echo 'checked';
                                                        }
                                                    }

                                                }
                                                ?>
                                                   value="<?= $v_user->user_id ?>"
                                                   name="assigned_to[]"
                                                   class="needsclick">
                                                        <span
                                                            class="fa fa-check"></span><?= $v_user->username . ' ' . $role ?>
                                        </label>

                                    </div>
                                    <div class="action_1 p
                                                <?php

                                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                        $get_permission = json_decode($leads_info->permission);

                                        foreach ($get_permission as $user_id => $v_permission) {
                                            if ($user_id == $v_user->user_id) {
                                                echo 'show';
                                            }
                                        }

                                    }
                                    ?>
                                                " id="action_1<?= $v_user->user_id ?>">
                                        <label class="checkbox-inline c-checkbox">
                                            <input id="<?= $v_user->user_id ?>" checked type="checkbox"
                                                   name="action_1<?= $v_user->user_id ?>[]"
                                                   disabled
                                                   value="view">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('view') ?>
                                        </label>
                                        <label class="checkbox-inline c-checkbox">
                                            <input <?php if (!empty($disable)) {
                                                echo 'disabled' . ' ' . 'checked';
                                            } ?> id="<?= $v_user->user_id ?>"
                                                <?php

                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);

                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            if (in_array('edit', $v_permission)) {
                                                                echo 'checked';
                                                            };

                                                        }
                                                    }

                                                }
                                                ?>
                                                 type="checkbox"
                                                 value="edit" name="action_<?= $v_user->user_id ?>[]">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('edit') ?>
                                        </label>
                                        <label class="checkbox-inline c-checkbox">
                                            <input <?php if (!empty($disable)) {
                                                echo 'disabled' . ' ' . 'checked';
                                            } ?> id="<?= $v_user->user_id ?>"
                                                <?php

                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            if (in_array('delete', $v_permission)) {
                                                                echo 'checked';
                                                            };
                                                        }
                                                    }

                                                }
                                                ?>
                                                 name="action_<?= $v_user->user_id ?>[]"
                                                 type="checkbox"
                                                 value="delete">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('delete') ?>
                                        </label>
                                        <input id="<?= $v_user->user_id ?>" type="hidden"
                                               name="action_<?= $v_user->user_id ?>[]" value="view">

                                    </div>


                                    <?php
                                }
                            }
                            ?>


                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                        <?php if (empty($leads_info->converted_client_id) || $leads_info->converted_client_id == 0) { ?>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-sm btn-primary"><?= lang('updates') ?></button>
                            </div>
                        <?php } ?>
                    </div>
            </form>
        </div>
    </div>
</div>



<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
$('input[name="daterange"]').daterangepicker({
	 opens: 'left',
	 drops: 'down',
    locale: {
      format: 'YYYY-MM-DD'
    }
});

$('#daterange').on('apply.daterangepicker', function(ev, picker) {
    //alert ('hello');
	var dates = $('#daterange').val();
	$('#daterange_form').submit();
});
</script>
	
	