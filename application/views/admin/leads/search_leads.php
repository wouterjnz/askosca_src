<?= message_box('success'); ?>
<?= message_box('error'); ?>


<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= lang('all_leads') ?></a></li>
    </ul>
    <div class="tab-content bg-white">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
			<form action="<?php echo base_url('admin/leads/search_lead');?>" method="POST">
				<input type="text" class="form-control" name="lead_id" placeholder="lead ID" />
			</form>
            <div class="table-responsive">
                <table class="table table-striped " id="" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= 'leadID' ?></th>
                        <th><?= 'listID' ?></th>
                        <th><?= lang('lead_name') ?></th>
                        <th><?= 'JobTitle' ?></th>
                        <th><?= 'First name' ?></th>
                        <th><?= 'Last name' ?></th>
                        <th><?= 'Source' ?></th>
                        <th><?= lang('email') ?></th>
                        <th><?= lang('phone') ?></th>
                        <th><?= lang('lead_status') ?></th>
                        <th class="col-options no-sort"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$table_id = 'id'; // 'leads_id;'
					$table = 'campaign_leads'; // 'tbl_leads
                    if (!empty($all_leads)):foreach ($all_leads as $v_leads):
                            ?>
                            <tr>
                                <td><?php echo $v_leads->id; ?></td>
                                <td><?php echo $v_leads->list_id; ?></td>
                                <td>
                                     <a href="<?= base_url() ?>admin/campaigns/campaign_questions/<?php echo $v_leads->campaign_id; ?>?entry_list_id=<?= $v_leads->$table_id ?>"><?= $v_leads->company_name ?></a> 
                                </td>
                                <td><?= $v_leads->jobtitle ?></td>
                                <td><?= $v_leads->first_name ?></td>
                                <td><?= $v_leads->last_name ?></td>
                                <td><?= $v_leads->lead_source ?></td>
                                <td><?= $v_leads->email ?></td>
                                <td><?= $v_leads->phone_number ?></td>
                                <td><?php
                                    if (!empty($v_leads->lead_status_id)) {
                                        $lead_status = $this->db->where('lead_status_id', $v_leads->lead_status_id)->get('tbl_lead_status')->row();

                                       echo "<span class='label label-success'>" . $lead_status->lead_status . "</span>";
                                        // $status = "<span class='label label-success'>" . lang($lead_status->lead_type) . "</span>";
                                        
                                        // echo $status . ' ' . $lead_status->lead_status;
                                    }
                                    ?>     </td>
                                <td>
                                        <?= btn_edit('admin/leads/index_leads/' . $v_leads->list_id .'/'.$v_leads->$table_id) ?>
                                        <?php //btn_delete('admin/leads/delete_leads/' . $v_leads->$table_id) ?>
                                </td>
                            </tr>
                            <?php
                    endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
		
    </div>
</div>

 <script>
	function view_comments(id){
		$('#prev_comments').load('/admin/leads/get_comments/'+id, function(response) {
			$('#prev_comments').innerHTML = response;
		});
	}
</script> 
    