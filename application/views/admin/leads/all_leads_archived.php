<?= message_box('success'); ?>
<?= message_box('error'); ?>


<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage"
                                                            data-toggle="tab"><del><?= lang('all_leads') ?></del></a></li>
    </ul>
    <div class="tab-content bg-white">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">

            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><del><?= 'leadID' ?></del></th>
                        <th><del><?= 'listID' ?></del></th>
                        <th><del><?= lang('lead_name') ?></del></th>
                        <th><del><?= 'JobTitle' ?></del></th>
                        <th><del><?= 'First name' ?></del></th>
                        <th><del><?= 'Last name' ?></del></th>
                        <th><del><?= 'Source' ?></del></th>
                        <th><del><?= lang('email') ?></del></th>
                        <th><del><?= lang('phone') ?></del></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$table_id = 'id'; // 'leads_id;'
					$table = 'campaign_leads_archived'; // 'tbl_leads
                    if (!empty($all_leads_archived)):foreach ($all_leads_archived as $v_leads):
                            ?>
                            <tr>
                                <td><del><?php echo $v_leads->id; ?></td>
                                <td><del><?php echo $v_leads->list_id; ?></td>
                                <td>
                                    <del><?= $v_leads->company_name ?></del>
                                </td>
                                <td><del><?= $v_leads->jobtitle ?></del></td>
                                <td><del><?= $v_leads->first_name ?></del></td>
                                <td><del><?= $v_leads->last_name ?></del></td>
                                <td><del><?= $v_leads->lead_source ?></del></td>
                                <td><del><?= $v_leads->email ?></del></td>
                                <td><del><?= $v_leads->phone_number ?></del></td>
                            </tr>
                            <?php
                    endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

 <script>
	function view_comments(id){
		$('#prev_comments').load('/admin/leads/get_comments/'+id, function(response) {
			$('#prev_comments').innerHTML = response;
		});
	}
</script>
