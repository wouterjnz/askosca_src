<?= message_box('success'); ?>
<?= message_box('error'); ?>

<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage"
                                                            data-toggle="tab"><?= lang('all_leads') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#create"
                                                            data-toggle="tab"><?= lang('new_leads') ?></a></li>
        <li class="<?= $active == 3 ? 'active' : ''; ?>"><a href="#journey" data-toggle="tab"><?= lang('customer_journey') ?></a></li>
         <?php if($this->session->userdata('user_type') != 4){ ?>
        <li><a style="background-color: #1797be;color: #ffffff"
               href="<?= base_url() ?>admin/leads/import_leads"><?= lang('import_leads') ?></a></li>
	    <?php } ?>
    </ul>
    <div class="tab-content bg-white">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">

            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= 'leadID' ?></th>
                        <th><?= 'listID' ?></th>
                        <th><?= lang('lead_name') ?></th>
                        <th><?= 'JobTitle' ?></th>
                        <th><?= 'First name' ?></th>
                        <th><?= 'Last name' ?></th>
                        <th><?= 'Source' ?></th>
                        <th><?= lang('email') ?></th>
                        <th><?= lang('phone') ?></th>
                        <th><?= lang('lead_status') ?></th>
                        <th class="col-options no-sort"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$table_id = 'id'; // 'leads_id;'
					$table = 'campaign_leads'; // 'tbl_leads
                    if (!empty($all_leads)):foreach ($all_leads as $v_leads):
                            ?>
                            <tr>
                                <td><?php echo $v_leads->id; ?></td>
                                <td><?php echo $v_leads->list_id; ?></td>
                                <td>
                                     <a href="<?= base_url() ?>admin/campaigns/campaign_questions/<?php echo $v_leads->campaign_id; ?>?entry_list_id=<?= $v_leads->$table_id ?>"><?= $v_leads->company_name ?></a>
                                </td>
                                <td><?= $v_leads->jobtitle ?></td>
                                <td><?= $v_leads->first_name ?></td>
                                <td><?= $v_leads->last_name ?></td>
                                <td><?= $v_leads->lead_source ?></td>
                                <td><?= $v_leads->email ?></td>
                                <td><?= $v_leads->phone_number ?></td>
                                <td><?php
                                    if (!empty($v_leads->lead_status_id)) {
                                        $lead_status = $this->db->where('lead_status_id', $v_leads->lead_status_id)->get('tbl_lead_status')->row();

                                       echo "<span class='label label-success'>" . $lead_status->lead_status . "</span>";
                                        // $status = "<span class='label label-success'>" . lang($lead_status->lead_type) . "</span>";

                                        // echo $status . ' ' . $lead_status->lead_status;
                                    }
                                    ?>     </td>
                                <td>
                                        <?= btn_edit('admin/leads/index_leads/' . $v_leads->list_id .'/'.$v_leads->$table_id) ?>
                                        <?php //btn_delete('admin/leads/delete_leads/' . $v_leads->$table_id) ?>
                                </td>
                            </tr>
                            <?php
                    endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
            <form role="form" enctype="multipart/form-data" id="form"
                  action="<?php echo base_url(); ?>admin/leads/saved_leads/<?php
                  if (!empty($leads_info)) {
                      echo $leads_info->list_id . '/' . $leads_info->$table_id;
                  }
                  ?>" method="post" class="form-horizontal  ">

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Company Name' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->company_name;
                            }
                            ?>" name="company_name" required="">
                        </div>

						<label class="col-lg-2 control-label"><?= 'First Name' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->first_name;
                            }
                            ?>" name="first_name" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('lead_status') ?> </label>
                        <div class="col-lg-4">
                            <select name="lead_status_id" class="form-control select_box" style="width: 100%"
                                    required="">
                                <?php

                                if (!empty($status_info)) {
                                    foreach ($status_info as $type => $v_leads_status) {
                                        if (!empty($v_leads_status)) {
                                            ?>
                                            <optgroup label="<?= lang($type) ?>">
                                                <?php foreach ($v_leads_status as $v_l_status) { ?>
                                                    <option
                                                            value="<?= $v_l_status->lead_status_id ?>" <?php
                                                    if (!empty($leads_info->lead_status_id)) {
                                                        echo $v_l_status->lead_status_id == $leads_info->lead_status_id ? 'selected' : '';
                                                    }
                                                    ?>><?= $v_l_status->lead_status ?></option>
                                                <?php } ?>
                                            </optgroup>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>
						<label class="col-lg-2 control-label"></label>
                        <div class="col-lg-4">
						</div>
					</div>
					<div class="form-group">
							<label class="col-lg-2 control-label"><?= 'Last Name' ?> <span
                                class="text-danger">*</span></label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="<?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->last_name;
                                }
                                ?>" name="last_name" required="">
                            </div>

							<label class="col-lg-2 control-label"><?= 'Job Title' ?> <span
                                class="text-danger">*</span></label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" value="<?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->jobtitle;
                                }
                                ?>" name="jobtitle" required="">
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('email') ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->email;
                            }
                            ?>" name="email">
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('phone') ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->phone_number;
                            }
                            ?>" name="phone_number" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('address') ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address1;
                            }
                            ?>" name="address1" required="">
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('city') ?><span
                                class="text-danger"> *</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->city;
                            }
                            ?>" name="city" required="">
                        </div>
                    </div>
                    <!-- End discount Fields -->
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Zip' ?><span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-4">
                            <input type="text" min="0" required="" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->zip;
                            }
                            ?>" name="zip"/>
                        </div>


                        <label class="col-lg-2 control-label"><?= lang('country') ?> </label>
                       <div class="col-lg-4">
                            <select name="country" class="form-control person select_box" style="width: 100%">
                                <optgroup label="Default Country">
                                    <?php if (!empty($leads_info->alternate_project)) { ?>
                                        <option value="<?= $leads_info->alternate_project ?>"><?= $leads_info->alternate_project ?></option>
                                    <?php } else { ?>
                                        <option
                                            value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                                    <?php } ?>
                                </optgroup>
                                <optgroup label="<?= lang('other_countries') ?>">
                                    <?php
                                    $countries = $this->db->get('tbl_countries')->result();
                                    if (!empty($countries)): foreach ($countries as $country):
                                        ?>
                                        <option value="<?= $country->value ?>"><?= $country->value ?></option>
                                        <?php
                                    endforeach;
                                    endif;
                                    ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Company Size' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->company_size;
                            }
                            ?>" name="company_size">
                        </div>


                        <label class="col-lg-2 control-label"><?= 'Industry Sector' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->industry_sector;
                            }
                            ?>" name="industry_sector">
                        </div>
                    </div>

                    <div class="form-group">
                          <label class="col-lg-2 control-label"><?= 'Warm Lead Indicator' ?></label>
                          <div class="col-lg-4">
                              <input type="text" class="form-control" value="<?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->warm_lead_indicator;
                                }
                                ?>" name="warm_lead_indicator">
                            </div>
                        </div>


                        <label class="col-lg-2 control-label"><?= 'LinkedIn Link' ?></label>
                        <div class="col-lg-4">
                                <input type="text" class="form-control" value="<?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->linkedin_link;
                                }
                                ?>" name="linkedin_link">
                        </div>
                    </div>

                    <div class="form-group" id="border-none">
                        <label class="col-lg-2 control-label"><?='Comments' ?> </label>
                            <div class="col-lg-8">
                                <textarea name="comments" class="form-control textarea"><?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->comments;
                                }
                                ?></textarea>
                                <div id="prev_comments"></div>
					            <a onclick="view_comments('<?php echo $leads_info->id;?>')" class="btn btn-info">View Previous comments</a>
                            </div>
                        <?php
                            if (!empty($leads_info)) {
                                $leads_id = $leads_info->$table_id;
                            } else {
                                $leads_id = null;
                            }
                        ?>
                    </div>
                    <div class="form-group">
                            <div class="col-lg-4">
                            <?= custom_form_Fields(5, $leads_id, true); ?>
                        </div>
                    </div>
                    <div class="form-group" id="border-none">
                        <label for="field-1" class="col-sm-2 control-label"><?= lang('assigned_to') ?> <span
                                class="required">*</span></label>
                        <div class="col-lg-4">
                            <div class="checkbox c-radio needsclick">
                                <label class="needsclick">
                                    <input id="" <?php
                                    if (!empty($leads_info->permission) && $leads_info->permission == 'all') {
                                        echo 'checked';
                                    } elseif (empty($leads_info)) {
                                        echo 'checked';
                                    }
                                    ?> type="radio" name="permission" value="everyone">
                                    <span class="fa fa-circle"></span><?= lang('everyone') ?>
                                    <i title="<?= lang('permission_for_all') ?>"
                                       class="fa fa-question-circle" data-toggle="tooltip"
                                       data-placement="top"></i>
                                </label>
                            </div>
                            <div class="checkbox c-radio needsclick">
                                <label class="needsclick">
                                    <input id="" <?php
                                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                        echo 'checked';
                                    }
                                    ?> type="radio" name="permission" value="custom_permission"
                                    >
                                    <span class="fa fa-circle"></span><?= lang('custom_permission') ?> <i
                                        title="<?= lang('permission_for_customization') ?>"
                                        class="fa fa-question-circle" data-toggle="tooltip"
                                        data-placement="top"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php
                        if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                            echo 'show';
                        }
                        ?>" id="permission_user_1">
                            <label for="field-1"
                               class="col-sm-2 control-label"><?= lang('select') . ' ' . lang('users') ?>
                            <span
                                class="required">*</span></label>
                        <div class="col-sm-9">
                            <?php
                            if (!empty($assign_user)) {
                                foreach ($assign_user as $key => $v_user) {

                                    if ($v_user->role_id == 1) {
                                        $disable = true;
                                        $role = '<strong class="badge btn-danger">' . lang('admin') . '</strong>';
                                    } else {
                                        $disable = false;
                                        $role = '<strong class="badge btn-primary">' . lang('staff') . '</strong>';
                                    }

                                    ?>
                                    <div class="checkbox c-checkbox needsclick">
                                        <label class="needsclick">
                                            <input type="checkbox"
                                                <?php
                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            echo 'checked';
                                                        }
                                                    }

                                                }
                                                ?>
                                                   value="<?= $v_user->user_id ?>"
                                                   name="assigned_to[]"
                                                   class="needsclick">
                                                        <span
                                                            class="fa fa-check"></span><?= $v_user->username . ' ' . $role ?>
                                        </label>

                                    </div>
                                    <div class="action_1 p
                                        <?php

                                        if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                            $get_permission = json_decode($leads_info->permission);

                                            foreach ($get_permission as $user_id => $v_permission) {
                                                if ($user_id == $v_user->user_id) {
                                                    echo 'show';
                                                }
                                            }

                                        }
                                        ?>
                                                " id="action_1<?= $v_user->user_id ?>">
                                        <label class="checkbox-inline c-checkbox">
                                            <input id="<?= $v_user->user_id ?>" checked type="checkbox"
                                                   name="action_1<?= $v_user->user_id ?>[]"
                                                   disabled
                                                   value="view">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('view') ?>
                                        </label>
                                        <label class="checkbox-inline c-checkbox">
                                            <input <?php if (!empty($disable)) {
                                                echo 'disabled' . ' ' . 'checked';
                                            } ?> id="<?= $v_user->user_id ?>"
                                                <?php

                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);

                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            if (in_array('edit', $v_permission)) {
                                                                echo 'checked';
                                                            };

                                                        }
                                                    }

                                                }
                                                ?>
                                                 type="checkbox"
                                                 value="edit" name="action_<?= $v_user->user_id ?>[]">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('edit') ?>
                                        </label>
                                        <label class="checkbox-inline c-checkbox">
                                            <input <?php if (!empty($disable)) {
                                                echo 'disabled' . ' ' . 'checked';
                                            } ?> id="<?= $v_user->user_id ?>"
                                                <?php

                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            if (in_array('delete', $v_permission)) {
                                                                echo 'checked';
                                                            };
                                                        }
                                                    }

                                                }
                                                ?>
                                                 name="action_<?= $v_user->user_id ?>[]"
                                                 type="checkbox"
                                                 value="delete">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('delete') ?>
                                        </label>
                                        <input id="<?= $v_user->user_id ?>" type="hidden"
                                               name="action_<?= $v_user->user_id ?>[]" value="view">

                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                        <?php if (empty($leads_info->converted_client_id) || $leads_info->converted_client_id == 0) { ?>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-sm btn-primary"><?= lang('updates') ?></button>
                            </div>
                        <?php } ?>
                    </div>
            </form>
        </div>
        <div class="tab-pane <?= $active == 3 ? 'active' : ''; ?>" id="journey">
                <div class="panel-body">
                    <div class="table-responsive table-sm">
                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th colspan="10">
                                        <form role="form" id="journey_form" class="form-inline" action="<?php echo base_url();?>admin/leads/index_leads//<?php
                                            if (!empty($list_id)) {
                                                echo $list_id;
                                                }
                                        ?>" method="post">
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <input type="text" name="search" class="form-control col-lg-12" id="find-journey" placeholder="Type email to search" value="<?= (isset($search)) ? $search : ""?>">
                                                </div>
                                            </div>
                                            <button type="submit" name="journey" value="3" class="btn btn-primary mb-2">Find Journey</button>
                                        </form>
                                    </th>
                                </tr>
                                <tr>
                                    <th><?= Action ?></th>
                                    <th><?= lang('lead_status') ?></th>
                                    <th><?= 'Comments' ?></th>
                                    <th><?= WLI ?></th>
                                    <th width="15%"><?= 'Campaign' ?></th>
                                    <th><?= lang('email') ?></th>
                                    <th width="10%"><?= 'Company Name' ?></th>
                                    <th><?= 'First Name' ?></th>
                                    <th><?= 'Last Name' ?></th>
                                    <th width="6%"><?= lang('phone') ?></th>
                                    <th width="5%"><?= 'Date' ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if (isset($journies)) {
                                            foreach($journies as $journey) {
                                                if($journey->field_name == 'comments') {
                                                    $field = '<span class="label label-info">';
                                                    $field .= 'COMMENTS';
                                                    $field .= '</span>';
                                                } else {
                                                    $field = '<span class="label label-success">';
                                                    $field .=  'DISPO';
                                                    $field .= '</span>';
                                                }

                                    ?>
                                    <tr>
                                            <td><small><?= $field ?></small></td>
                                            <td><small><?= $journey->lead_status?></small></td>
                                            <td><small><?= strip_tags($journey->comments);?></small></td>
                                            <td><small><?= $journey->warm_lead_indicator?></small></td>
                                            <td><small><?= $journey->campaign_name?></small></td>
                                            <td><small><?= $journey->email?></small></td>
                                            <td><small><?= $journey->company_name?></small></td>
                                            <td><small><?= $journey->first_name?></small></td>
                                            <td><small><?= $journey->last_name?></small></td>
                                            <td><small><?= $journey->phone_number ?></small></td>
                                            <td><small><?= date('Y-m-d', strtotime($journey->updated));?></small></td>
                                        <?php } ?>
                                        <?php } ?>
                                    </tr>
                                </tbody>
                            </table>
                </div>
            </div>
        </div>
        <div class="tab-pane <?= $active == 4 ? 'active' : ''; ?>" id="imports">

        </div>
    </div>
</div>

 <script>
	function view_comments(id){
		$('#prev_comments').load('/admin/leads/get_comments/'+id, function(response) {
			$('#prev_comments').innerHTML = response;
		});
	}
</script>
