<?= message_box('success'); ?>
<?= message_box('error'); ?>
<div class="panel panel-custom">
    <header class="panel-heading">
        <div class="panel-title"><strong>Form</strong>
        </div>
    </header>
    <div class="panel-body">
		<?php if(!empty($questions)){ $action = 'update_form'; }else{ $action = 'generate_form'; }?>
         <form action="<?php echo base_url("admin/campaigns/$action"); ?>" method="post">
            <div class="panel-body">
                <!--
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">
                        <?= lang('choose_file') ?><span class="required"></span></label>
                    <div class="col-sm-5">
                        <div style="display: inherit;margin-bottom: inherit" class="fileinput fileinput-new"
                             data-provides="fileinput">
                    <span class="btn btn-default btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span>
                        <input type="file" name="upload_file"></span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                               style="float: none;">&times;</a>
                        </div>
                    </div>
                </div>
                -->
               <div class="form-group">
                 <div class="col-sm-5">
                       Question
                    </div>
                    <div class="col-sm-2">
                      Data Type
                    </div>
                    <div class="col-sm-3">
                      Options (comma "," seperated)
                    </div>
                    <div class="col-sm-2">
                       Required
                    </div>
                </div>

                <?php for($i = 1; $i <= 20; $i++){
                //	if(isset($questions[$i-1]->id)){
                ?>
                <div class="form-group row">
                    <div class="col-sm-5">
						<?php if(!empty($questions)){ ?>
							<input type="hidden" name="id_<?php echo $i;?>" value="<?php if(!empty($questions) && isset($questions[$i-1])){ echo $questions[$i-1]->id; } ?>" />
							<input type="hidden" name="number_<?php echo $i;?>" value="<?php if(!empty($questions) && isset($questions[$i-1])){ echo $questions[$i-1]->question_number; } ?>" />
                        <?php } ?>
						<textarea name="question_<?php echo $i;?>" class="form-control"><?php if(!empty($questions) && isset($questions[$i-1])){ echo $questions[$i-1]->question_question; } ?></textarea>
                    </div>
                    <div class="col-sm-2">
                        <select name="type_<?php echo $i;?>" class="form-control">
                            <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_type) == 'short text'){  echo 'selected'; }} ?>>Short Text</option>
                            <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_type) == 'long text'){  echo 'selected'; }} ?>>Long Text</option>
                            <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_type) == 'radio'){  echo 'selected'; }} ?>>Radio</option>
                            <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_type) == 'checkbox'){  echo 'selected'; }} ?>>checkbox</option>
                            <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_type) == 'select box'){  echo 'selected'; }} ?>>select box</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input name="options_<?php echo $i;?>" class="form-control" value="<?php if(!empty($questions) && isset($questions[$i-1])){ echo $questions[$i-1]->question_options; } ?>" />
                    </div>
                    <div class="col-sm-2">
                        <select name="required_<?php echo $i;?>" class="form-control">
                            <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_required) == 'no'){  echo 'selected'; }} ?>>no</option>
                            <option <?php if(!empty($questions) && isset($questions[$i-1])){ if(strtolower($questions[$i-1]->question_required) == 'yes'){  echo 'selected'; }} ?>>yes</option>
                        </select>
                    </div>
                </div>
                <?php } //} ?>
               <input type="hidden" name="campaign_id" value="<?php echo $campaign_id; ?>" />




                </div>


                <div class="form-group">
                    <label class="col-lg-3 control-label"></label>
                    <div class="col-lg-6">
						<?php if(!empty($questions)){ ?>
						<button type="submit" value="submit" name="submit" class="btn btn-sm btn-primary"></i> <?= 'Update'; ?></button>
						<?php }else{ ?>
                        <button type="submit" value="submit" name="submit" class="btn btn-sm btn-primary"></i> <?= 'Submit'; ?></button>
						<?php } ?>
                    </div>
                </div>
            </div>
             </form>
    </div>
</div>
