<?= message_box('success'); ?>
<?= message_box('error'); ?>
<?php
	$list_id_query = $this->db->where('id', $lead_id)->get('campaign_leads')->row('list_id');
?>
<div class="panel panel-custom">
    <header class="panel-heading">
        <div class="panel-title">
			<strong><?= '[LEAD ID: '.$lead_id.'] | [List ID: ' .$list_id_query.'] | <u>[Campaign: ' .$campaign_name.']</u>';?></strong>

			<?php if($this->session->userdata('user_type') != 5){ ?>
<a href="<?php echo base_url("admin/campaigns/add_referral/$id/$lead_id");?>" class="btn btn-primary pull-right" style="margin-left: 5px" target="_blank">Add Referral</a>

<?php } ?>
<a data-target="#updateQuestions" data-toggle="modal" class="btn btn-info pull-right">Question Form</a>
	<?php if($this->session->userdata('user_type') == 5){ ?>
				  <div class="pull-right">
                                   <div class="btn-group">
                                            <button class="btn btn-success dropdown-toggle" style="margin-right: 5px"
                                                    data-toggle="dropdown">
                                                <?= lang('change') ?>
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu animated zoomIn">
                                                <li>
										<a href="<?= base_url() ?>admin/campaigns/change_qa_status/<?php echo $lead_id; ?>/1">Passed</a>
									</li>
									<li>
										<a href="<?= base_url() ?>admin/campaigns/change_qa_status/<?php echo $lead_id; ?>/2">Pending</a>
									</li>
									<li>
										<a href="<?= base_url() ?>admin/campaigns/change_qa_status/<?php echo $lead_id; ?>/3">Does Not Pass</a>
									</li>
									<li>
										<a href="<?= base_url() ?>admin/campaigns/change_qa_status/<?php echo $lead_id; ?>/4">Does Not Qualify </a>
									</li>
                                            </ul>
                                        </div>
                                </div>



				<?php } ?>
        </div>
    </header>
    <div class="panel-body">

            <div class="panel-body">
    <form action="<?php echo base_url('admin/campaigns/save_questions/'.$id); ?>" method="post">
<div id="updateQuestions" class="modal fade" style=" background-color: rgba(0, 0, 0, 0.6);">
	<div class="modal-dialog modal-lg">

		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				 <h4>Questions</h4>
			</div>

			<div class="modal-body">
		   <?php
			$i = 1;
			$div = '<table class="table table-bordered">';
		   foreach ($questions as $dat) {
			// $div = '';
			 $input = '';
			$required = '';
			$op = '<option value="">--select--</option>';
			if (!empty($dat['question_question'])) {
				$div .= '<tr><td width="400px"><strong>'.$dat['question_number'].')  '.$dat['question_question'].': </strong></td>';


				if(!empty($dat['question_options'])){
					$options = explode(",",$dat['question_options']);
					if($dat['question_type'] == 'select box'){
						foreach ($options as $option) {

							$selected = '';
							 if(!empty($answers)){
								foreach($answers as $answ){
									if($dat["id"] == $answ->question_id){
										if($answ->answer == $option){
											$selected = "selected";
										}
									}
								}
							}

							$op .= '<option value="'.$option.'" '.$selected.'>'.$option.'</option>';
						}
					}
				}
				if($dat['question_required'] == 'yes'){
					$required = ' required';
				}
				if(strtolower($dat['question_type']) == 'short text'){
					$input = '<input class="form-control" name="{names}" value="{textinput}" {required} ></input><!--<input type="text"  name="answer_'.$i.'_{answer_id}" value="{answer_id}" />-->';
				}
				if(strtolower($dat['question_type']) == 'long text'){
					$input = '<textarea class="form-control" name="{names}" {required} >{textinput}</textarea>';
				}
				if(strtolower($dat['question_type']) == 'select box'){
						$input = '<select class="form-control" name="{names}" {required} >{options}</select>';
				}
				if(strtolower($dat['question_type']) == 'checkbox'){
						$check_options = explode(",",$dat['question_options']);
						foreach($check_options as $co){
							$checked = '';
							 if(!empty($answers)){
								foreach($answers as $answ){
									if($dat["id"] == $answ->question_id){
										if($answ->answer == $co){
											$checked = "checked";
										}
									}
								}
							}
							$input .= '<label>'.$co.'</label>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="" name="{names}[]" {required} value="'.$co.'" 	'.$checked.' /><br/>';
						}
				}
				if(strtolower($dat['question_type']) == 'radio'){
						$radio_options = explode(",",$dat['question_options']);
						foreach($radio_options as $ro){
							$checked = '';
							 if(!empty($answers)){
								foreach($answers as $answ){
									if($dat["id"] == $answ->question_id){
										if($answ->answer == $ro){
											$checked = "checked";
										}
									}
								}
							}

							$input .= '<label>'.$ro.'</label>&nbsp;&nbsp;&nbsp;<input type="radio" class="" name="{names}" {required} value="'.$ro.'"  '.$checked.' /><br/>';
						}
				}
				// $input .= '<input type="hidden" name="question_id" value="'.$dat['id'].'" />';
				$form = str_replace('{textinput}','{textinput}',$input);
				$form2 = str_replace('{required}',$required,$form);
				$form3 = str_replace('{options}',$op,$form2);
				$form4 = str_replace('{names}','question_'.$i.'_'.$dat['id'],$form3);
				$form6 = $form4;

				if(!empty($answers)){
					foreach($answers as $answ){
						if($dat['id'] == $answ->question_id){
							$form6 = str_replace('{textinput}',$answ->answer,$form6);
							$form6 = str_replace('question_'.$i.'_'.$dat['id'],'question_'.$i.'_'.$dat['id'].'_'.$answ->id,$form6);
							// if(strtolower($dat['question_type']) == 'radio'){
								// $form6 = str_replace('{selected}','checked', $form6);
							// }
							// if(strtolower($dat['question_type']) == 'checkbox' && in_array($answ->answer, $check_options)){
								// $form6 = str_replace('{selected}','checked', $form6);
							// }
						}
					}
				}
				$form6 = str_replace('{textinput}','',$form6);
				// $form6 = str_replace('{answer_id}',0,$form6);
				$div .= '<td>'.$form6.'</td>
				</tr>';

			   }
			$i++;

		}
          $div .= '</table>';
		  echo $div;
?>


				<input type="hidden" name="lead_id" value="<?php echo $lead_id; ?>" />
				<input type="hidden" name="save_method" value="<?php if(!empty($answers)) { echo 'update'; } ?>" />
				<table class="table table-bordered">
				<tr rowspan="2">
					<td colspan="2"><input type="submit" class="btn btn-primary pull-right" /></td>
				</tr>
			</table>
			</div>
			</div>
			</div>
			</div>
</form>

			<!--  LEAD INFO -->
		<?php
		$table_id = 'id'; // 'leads_id;'
		$table = 'campaign_leads'; // 'tbl_leads
		?>
<?php if($template < 2){?>
 <form role="form" enctype="multipart/form-data" id="form"
                  action="<?php echo base_url(); ?>admin/leads/saved_leads/<?php
                  if (!empty($leads_info)) {
                      echo $leads_info->list_id . '/' . $leads_info->$table_id;
                  }
                  ?>" method="post" class="form-horizontal  ">
            	<input type="hidden" name="entry_list_id" value="<?php echo isset($_GET['entry_list_id'])?$_GET['entry_list_id']:0;?>" />
				<input type="hidden" name="referral_id" value="<?php echo isset($_GET['referral_id'])?$_GET['referral_id']:0;?>" />
                <div class="panel-body">
                    <?php
                    if(isset($duplicate)) {
                        if ($duplicate == true) {
                           echo '<div class="alert alert-warning alert-dismissible">';
                           echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>';
                           echo '<strong>Duplicate lead not saved.</strong> Referral lead already exist (see below).';
                           echo '</div>';
                        }
                    }
                    ?>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Company Name' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->company_name;
                            }
                            ?>" name="company_name" required="">
                        </div>

						<label class="col-lg-2 control-label"><?= 'First Name' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo isset($_GET['referral_id'])?'':$leads_info->first_name;
                            }
                            ?>" name="first_name" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('lead_status') ?> </label>
                        <div class="col-lg-4">
                            <select name="lead_status_id" class="form-control select_box" style="width: 100%"
                                    required="">
                                    <option value=""> __ </option>
                                <?php

                                if (!empty($status_info)) {
                                    foreach ($status_info as $type => $v_leads_status) {
                                        if (!empty($v_leads_status)) {
                                            ?>
                                            <optgroup label="<?= lang($type) ?>">
                                                <?php foreach ($v_leads_status as $v_l_status) { ?>
                                                    <option
                                                            value="<?= $v_l_status->lead_status_id ?>" <?php
                                                    if (!empty($leads_info->lead_status_id)) {
                                                        echo $v_l_status->lead_status_id == $leads_info->lead_status_id ? 'selected' : '';
                                                    }
                                                    if (empty($leads_info->lead_status_id)) {
                                                        echo $v_l_status->lead_status_id == 10 ? 'selected' : '';
                                                    }
                                                    ?>><?= $v_l_status->lead_status ?></option>
                                                <?php } ?>
                                            </optgroup>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
						</div>
                        <label class="col-lg-2 control-label">Source</label>
                        <div class="col-lg-4">
                                <input type="text" class="form-control" value="<?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->lead_source;
                                }
                                ?>" name="lead_source" >
                        </div>
					</div>
					<div class="form-group">
							<label class="col-lg-2 control-label"><?= 'Last Name' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo isset($_GET['referral_id'])?'':$leads_info->last_name;
                            }
                            ?>" name="last_name" required="">
                        </div>

							<label class="col-lg-2 control-label"><?= 'Job Title' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->jobtitle;
                            }
                            ?>" name="jobtitle" required="">
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('email') ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo isset($_GET['referral_id'])?'':$leads_info->email;
                            }
                            ?>" name="email">
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('phone') ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo isset($_GET['referral_id'])?'':$leads_info->phone_number;
                            }
                            ?>" name="phone_number" required="">
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('address') ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address1;
                            }
                            ?>" name="address1" required="">
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('city') ?><span
                                class="text-danger"> *</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->city;
                            }
                            ?>" name="city" required="">
                        </div>

                    </div>

                    <!-- End discount Fields -->
                    <div class="form-group">
                         <label class="col-lg-2 control-label"><?= 'State' ?><span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-4">
                            <input type="text" required="" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->state;
                            }
                            ?>" name="state"/>
                        </div>

                        <label class="col-lg-2 control-label"><?= 'Zip' ?><span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-4">
                            <input type="text" min="0" required="" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->zip;
                            }
                            ?>" name="zip"/>
                        </div>
                    </div>

                     <div class="form-group">
                           <label class="col-lg-2 control-label"><?= lang('country') ?> </label>
                        <div class="col-lg-4">
                             <select name="country" class="form-control person select_box" style="width: 100%">
                                <optgroup label="Default Country">
                                    <?php if (!empty($leads_info->country)) { ?>
                                        <option value="<?= $leads_info->country ?>"><?= $leads_info->country ?></option>
                                    <?php } else { ?>
                                        <option
                                            value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                                    <?php } ?>
                                </optgroup>
                                <optgroup label="<?= lang('other_countries') ?>">
                                    <?php
                                    $countries = $this->db->get('tbl_countries')->result();
                                    if (!empty($countries)): foreach ($countries as $country):
                                        ?>
                                        <option value="<?= $country->value ?>"><?= $country->value ?></option>
                                        <?php
                                    endforeach;
                                    endif;
                                    ?>
                                </optgroup>
                            </select>
                        </div>

                        <label class="col-lg-2 control-label"><?= 'Company Size' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->company_size;
                            }
                            ?>" name="company_size">
                        </div>


                     </div>

                  <div class="form-group">
                       <label class="col-lg-2 control-label"><?= 'Industry Sector' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->industry_sector;
                            }
                            ?>" name="industry_sector">
                        </div>

                        <label class="col-lg-2 control-label"><?= 'Warm Lead Indicator' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->warm_lead_indicator;
                            }
                            ?>" name="warm_lead_indicator">
                    </div>


                        </div>
					<div class="form-group">
					    <label class="col-lg-2 control-label"><?= 'LinkedIn Link' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->linkedin_link;
                            }
                            ?>" name="linkedin_link">
                        </div>
                        <label class="col-lg-2 control-label"><?= 'Pool Id' ?></label>
                      <div class="col-lg-4">
                           <input type="text" class="form-control" value="<?php
                          if (!empty($leads_info)) {
                              echo $leads_info->pool_id;
                          }
                          ?>" name="pool_id" readonly>
                      </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Campaign Code' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->campaign_code;
                            }
                            ?>" name="campaign_code" readonly>
                        </div>
                        <label class="col-lg-2 control-label"><?= 'Alternate Number #1' ?></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->alternate_number;
                            }
                            ?>" name="alternate_number">
                            <input type="hidden" readonly name="starttime" class="form-control" value="<?php echo date('Y-m-d H:i:s') ?>" />
                        </div>
                    </div>

                    <div class="form-group">
					    <label class="col-lg-2 control-label"><?= 'Alternate Number #2' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->alternative_number_a;
                            }
                            ?>" name="alternative_number_a">
                        </div>
                        <label class="col-lg-2 control-label"><?= 'Alternate Number #3' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->alternative_number_b;
                            }
                            ?>" name="alternative_number_b">
                        </div>
                    </div>

                    <div class="form-group">
					      <label class="col-lg-2 control-label"><?= 'Alternate Number #4' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->alternative_number_c;
                            }
                            ?>" name="alternative_number_c">
                        </div>
                        <label class="col-lg-2 control-label"><?= 'Alternate Number #5'  ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->alternative_number_d;
                            }
                            ?>" name="alternative_number_d">
                        </div>
                    </div>

				<!--
                     <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Alternative Number' ?></label>
                        <div class="col-lg-4">
                             <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->time_called;
                            }
                            ?>" name="time_called">
                        </div>
                    </div>
                    -->
                   <!--
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'alternate_number' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->alternate_number;
                            }
                            ?>" name="alternate_number">
                        </div>
                        <label class="col-lg-2 control-label"><?= 'alternate_dm' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->alternate_dm;
                            }
                            ?>" name="alternate_dm">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'country' ?></label>
                        <div class="col-lg-4">
                            <select name="country" class="form-control person select_box" style="width: 100%">
                                <optgroup label="Default Country">
                                    <?php if (!empty($leads_info->alternate_project)) { ?>
                                        <option value="<?= $leads_info->alternate_project ?>"><?= $leads_info->alternate_project ?></option>
                                    <?php } else { ?>
                                        <option
                                            value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                                    <?php } ?>
                                </optgroup>
                                <optgroup label="<?= lang('other_countries') ?>">
                                    <?php
                                    $countries = $this->db->get('tbl_countries')->result();
                                    if (!empty($countries)): foreach ($countries as $country):
                                        ?>
                                        <option value="<?= $country->value ?>"><?= $country->value ?></option>
                                        <?php
                                    endforeach;
                                    endif;
                                    ?>
                                </optgroup>
                            </select>
                        </div>
                        <label class="col-lg-2 control-label"><?= 'outcome' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->outcome;
                            }
                            ?>" name="outcome">
                        </div>


                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'date_worked' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->date_worked;
                            }
                            ?>" name="date_worked">
                        </div>
                        <label class="col-lg-2 control-label"><?= 'time_worked' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->time_worked;
                            }
                            ?>" name="time_worked">
                        </div>

                    </div>
                    -->
                    <div class="form-group" id="border-none">
                        <label class="col-lg-2 control-label"><?='Comments' ?> </label>
                        <div class="col-lg-8">
                            <textarea name="comments" class="form-control textarea"><?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->comments;
                                }
                                ?></textarea>
                                <div id="prev_comments"></div>
					            <a onclick="view_comments('<?php echo $leads_info->id;?>')" class="btn btn-info">View Previous comments</a>
                        </div>
                    </div>
                    <?php
                    if (!empty($leads_info)) {
                        $leads_id = $leads_info->$table_id;
                    } else {
                        $leads_id = null;
                    }
                    ?>
                    <?= custom_form_Fields(5, $leads_id, true); ?>

                    <div class="form-group" id="border-none">
                        <label for="field-1" class="col-sm-2 control-label"><?= lang('assigned_to') ?> <span
                                class="required">*</span></label>
                        <div class="col-sm-9">
                            <div class="checkbox c-radio needsclick">
                                <label class="needsclick">
                                    <input id="" <?php
                                    if (!empty($leads_info->permission) && $leads_info->permission == 'all') {
                                        echo 'checked';
                                    } elseif (empty($leads_info)) {
                                        echo 'checked';
                                    }
                                    ?> type="radio" name="permission" value="everyone">
                                    <span class="fa fa-circle"></span><?= lang('everyone') ?>
                                    <i title="<?= lang('permission_for_all') ?>"
                                       class="fa fa-question-circle" data-toggle="tooltip"
                                       data-placement="top"></i>
                                </label>
                            </div>
                            <div class="checkbox c-radio needsclick">
                                <label class="needsclick">
                                    <input id="" <?php
                                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                        echo 'checked';
                                    }
                                    ?> type="radio" name="permission" value="custom_permission"
                                    >
                                    <span class="fa fa-circle"></span><?= lang('custom_permission') ?> <i
                                        title="<?= lang('permission_for_customization') ?>"
                                        class="fa fa-question-circle" data-toggle="tooltip"
                                        data-placement="top"></i>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group <?php
                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                        echo 'show';
                    }
                    ?>" id="permission_user_1">
                        <label for="field-1"
                               class="col-sm-2 control-label"><?= lang('select') . ' ' . lang('users') ?>
                            <span
                                class="required">*</span></label>
                        <div class="col-sm-9">
                            <?php
                            if (!empty($assign_user)) {
                                foreach ($assign_user as $key => $v_user) {

                                    if ($v_user->role_id == 1) {
                                        $disable = true;
                                        $role = '<strong class="badge btn-danger">' . lang('admin') . '</strong>';
                                    } else {
                                        $disable = false;
                                        $role = '<strong class="badge btn-primary">' . lang('staff') . '</strong>';
                                    }

                                    ?>
                                    <div class="checkbox c-checkbox needsclick">
                                        <label class="needsclick">
                                            <input type="checkbox"
                                                <?php
                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    if(isset($get_permission[0])){
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            echo 'checked';
                                                        }
                                                    }
                                                }
                                                }
                                                ?>
                                                   value="<?= $v_user->user_id ?>"
                                                   name="assigned_to[]"
                                                   class="needsclick">
                                                        <span
                                                            class="fa fa-check"></span><?= $v_user->username . ' ' . $role ?>
                                        </label>

                                    </div>
                                    <div class="action_1 p
                                                <?php

                                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                        $get_permission = json_decode($leads_info->permission);
                                        if(isset($get_permission[0])){
                                        foreach ($get_permission as $user_id => $v_permission) {
                                            if ($user_id == $v_user->user_id) {
                                                echo 'show';
                                            }
                                        }
                                    }
                                    }
                                    ?>
                                                " id="action_1<?= $v_user->user_id ?>">
                                        <label class="checkbox-inline c-checkbox">
                                            <input id="<?= $v_user->user_id ?>" checked type="checkbox"
                                                   name="action_1<?= $v_user->user_id ?>[]"
                                                   disabled
                                                   value="view">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('view') ?>
                                        </label>
                                        <label class="checkbox-inline c-checkbox">
                                            <input <?php if (!empty($disable)) {
                                                echo 'disabled' . ' ' . 'checked';
                                            } ?> id="<?= $v_user->user_id ?>"
                                                <?php

                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    if(isset($get_permission[0])){
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            if (in_array('edit', $v_permission)) {
                                                                echo 'checked';
                                                            };

                                                        }
                                                    }
                                                    }

                                                }
                                                ?>
                                                 type="checkbox"
                                                 value="edit" name="action_<?= $v_user->user_id ?>[]">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('edit') ?>
                                        </label>
                                        <label class="checkbox-inline c-checkbox">
                                            <input <?php if (!empty($disable)) {
                                                echo 'disabled' . ' ' . 'checked';
                                            } ?> id="<?= $v_user->user_id ?>"
                                                <?php

                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    if(isset($get_permission[0])){
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            if (in_array('delete', $v_permission)) {
                                                                echo 'checked';
                                                            };
                                                        }
                                                    }
                                                    }

                                                }
                                                ?>
                                                 name="action_<?= $v_user->user_id ?>[]"
                                                 type="checkbox"
                                                 value="delete">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('delete') ?>
                                        </label>
                                        <input id="<?= $v_user->user_id ?>" type="hidden"
                                               name="action_<?= $v_user->user_id ?>[]" value="view">

                                    </div>


                                    <?php
                                }
                            }
                            ?>


                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                        <?php if (empty($leads_info->converted_client_id) || $leads_info->converted_client_id == 0) { ?>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-sm btn-primary"><?= lang('update') ?></button>
                            </div>
                        <?php } ?>
                    </div>
            </form>
<?php } else{ ?>
          <form role="form" enctype="multipart/form-data" id="form"
                  action="<?php echo base_url(); ?>admin/leads/saved_leads_template/<?php
                  if (!empty($leads_info)) {
                      echo $leads_info->list_id . '/' . $leads_info->$table_id;
                  }
                  ?>" method="post" class="form-horizontal  ">

                <div class="panel-body">
					<div class="form-group">
						<label class="col-lg-2 control-label"><?= 'Title' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->title;
                            }
                            ?>" name="title" >
                        </div>

						<label class="col-lg-2 control-label"><?= 'Gender' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->gender;
                            }
                            ?>" name="gender" >
                        </div>


					</div>
                    <div class="form-group">

						<label class="col-lg-2 control-label"><?= 'Name' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->first_name;
                            }
                            ?>" name="first_name" >
                        </div>

						<label class="col-lg-2 control-label"><?= 'Surname' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->last_name;
                            }
                            ?>" name="last_name" >
                        </div>

                    </div>

					<div class="form-group">
						<label class="col-lg-2 control-label"><?= 'D.O.B' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->date_of_birth;
                            }
                            ?>" name="date_of_birth" >
                        </div>

						<label class="col-lg-2 control-label"><?= 'Age' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->age;
                            }
                            ?>" name="age" >
                        </div>


					</div>


					<div class="form-group">

						<label class="col-lg-2 control-label"><?= 'Effective Date' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->effective_date;
                            }
                            ?>" name="effective_date" >
                        </div>


						<label class="col-lg-2 control-label"><?= 'Policy Number' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->policy_number;
                            }
                            ?>" name="policy_number">
                        </div>

                    </div>

					<div class="form-group">

						<label class="col-lg-2 control-label"><?= 'Policy Status' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->policy_status;
                            }
                            ?>" name="policy_status" >
                        </div>


						<label class="col-lg-2 control-label"><?= 'Policy Status (payment history)' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->policy_status_per_history;
                            }
                            ?>" name="policy_status_per_history">
                        </div>

                    </div>


					<div class="form-group">
						<label class="col-lg-2 control-label"><?= 'Broker_channel' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->broker_channel;
                            }
                            ?>" name="broker_channel" >
                        </div>


						<label class="col-lg-2 control-label"><?= 'Product' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->product;
                            }
                            ?>" name="product">
                        </div>
                    </div>

					<div class="form-group">
						<label class="col-lg-2 control-label"><?= 'Inception Date' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->inception_date;
                            }
                            ?>" name="inception_date" >
                        </div>


						<label class="col-lg-2 control-label"><?= 'Relationship' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->relationship;
                            }
                            ?>" name="relationship">
                        </div>
                    </div>

					 <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('email') ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->email;
                            }
                            ?>" name="email">
                        </div>

						<label class="col-lg-2 control-label"><?= 'ID Number' ?> </label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->id_number;
                            }
                            ?>" name="id_number">
                        </div>

						</div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Work Number' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->work_number;
                            }
                            ?>" name="work_number" >
                        </div>

						<label class="col-lg-2 control-label"><?= 'Cell Number' ?> <span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->cell_number;
                            }
                            ?>" name="cell_number" >
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('address') ?> 1<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address1;
                            }
                            ?>" name="address1" >
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('address') ?> 2<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address2;
                            }
                            ?>" name="address2" >
                        </div>

                    </div>
                    <!-- End discount Fields -->
                     <div class="form-group">
                        <label class="col-lg-2 control-label"><?= lang('address') ?> 3<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address3;
                            }
                            ?>" name="address3" >
                        </div>
                        <label class="col-lg-2 control-label"><?= lang('address') ?> 4<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->address4;
                            }
                            ?>" name="address4" >
                        </div>

                    </div>

					 <div class="form-group">
                        <label class="col-lg-2 control-label">Postal Code<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->postal_code;
                            }
                            ?>" name="postal_code" >
                        </div>
                        <label class="col-lg-2 control-label">Agent Code<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->agent_code;
                            }
                            ?>" name="agent_code" >
                        </div>
                    </div>

				<div class="form-group">
                        <label class="col-lg-2 control-label">Payment Method<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->payment_method;
                            }
                            ?>" name="payment_method" >
                        </div>

                        <label class="col-lg-2 control-label">Deduction Day<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->deduction_day;
                            }
                            ?>" name="deduction_day" >
                        </div>
                    </div>

				<hr/>

					<div class="form-group">
                        <label class="col-lg-2 control-label">Account Holder<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->debit_order_account_holder;
                            }
                            ?>" name="debit_order_account_holder" >
                        </div>

                        <label class="col-lg-2 control-label">Account Number<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->debit_order_account_number;
                            }
                            ?>" name="debit_order_account_number" >
                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-lg-2 control-label">Bank Name<span
                                class="text-danger"></span></label>
                        <div class="col-lg-2">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->debit_order_bank_name;
                            }
                            ?>" name="debit_order_bank_name" >
                        </div>

                        <label class="col-lg-2 control-label">Branch Code<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->debit_order_branch_code;
                            }
                            ?>" name="debit_order_branch_code" >
                        </div>

						<label class="col-lg-2 control-label">Account Type<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->debit_order_account_type;
                            }
                            ?>" name="debit_order_account_type" >
                        </div>

                    </div>

					<hr/>
					<hr/>
					<div class="form-group">
                        <label class="col-lg-2 control-label">Sum Insured Amount<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->sum_assured_amount;
                            }
                            ?>" name="sum_assured_amount" >
                        </div>

                        <label class="col-lg-2 control-label">Total Premium<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->total_premium;
                            }
                            ?>" name="total_premium" >
                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-lg-2 control-label">Cover Amount<span
                                class="text-danger"></span></label>
                        <div class="col-lg-4">
                            <input type="address1" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->cover_amount;
                            }
                            ?>" name="cover_amount" >
                        </div>

                        <label class="col-lg-2 control-label">Total Cover<span
                                class="text-danger"> </span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->total_cover;
                            }
                            ?>" name="total_cover" >
                        </div>
                    </div>

					<hr/>


                    <div class="form-group">

                        <label class="col-lg-2 control-label"><?= lang('lead_status') ?> </label>
                        <div class="col-lg-4">
                            <select name="lead_status_id" class="form-control select_box" style="width: 100%"
                                    >
                                <?php

                                if (!empty($status_info)) {
                                    foreach ($status_info as $type => $v_leads_status) {
                                        if (!empty($v_leads_status)) {
                                            ?>
                                            <optgroup label="<?= lang($type) ?>">
                                                <?php foreach ($v_leads_status as $v_l_status) { ?>
                                                    <option
                                                            value="<?= $v_l_status->lead_status_id ?>" <?php
                                                    if (!empty($leads_info->lead_status_id)) {
                                                        echo $v_l_status->lead_status_id == $leads_info->lead_status_id ? 'selected' : '';
                                                    }
                                                    ?>><?= $v_l_status->lead_status ?></option>
                                                <?php } ?>
                                            </optgroup>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>

								<label class="col-lg-2 control-label"></label>
                        <div class="col-lg-4">
						</div>

                        </div>
					</div>


                    <div class="form-group" id="border-none">
                        <label class="col-lg-2 control-label"><?='comments' ?> </label>
                        <div class="col-lg-8">
                            <textarea name="comments" class="form-control textarea"><?php
                                if (!empty($leads_info)) {
                                    echo $leads_info->comments;
                                }
                                ?></textarea>
                        </div>
                    </div>
                    <?php
                    if (!empty($leads_info)) {
                        $leads_id = $leads_info->$table_id;
                    } else {
                        $leads_id = null;
                    }
                    ?>




                    <?= custom_form_Fields(5, $leads_id, true); ?>

                    <div class="form-group" id="border-none">
                        <label for="field-1" class="col-sm-2 control-label"><?= lang('assigned_to') ?> <span
                                class="required"></span></label>
                        <div class="col-sm-9">
                            <div class="checkbox c-radio needsclick">
                                <label class="needsclick">
                                    <input id="" <?php
                                    if (!empty($leads_info->permission) && $leads_info->permission == 'all') {
                                        echo 'checked';
                                    } elseif (empty($leads_info)) {
                                        echo 'checked';
                                    }
                                    ?> type="radio" name="permission" value="everyone">
                                    <span class="fa fa-circle"></span><?= lang('everyone') ?>
                                    <i title="<?= lang('permission_for_all') ?>"
                                       class="fa fa-question-circle" data-toggle="tooltip"
                                       data-placement="top"></i>
                                </label>
                            </div>
                            <div class="checkbox c-radio needsclick">
                                <label class="needsclick">
                                    <input id="" <?php
                                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                        echo 'checked';
                                    }
                                    ?> type="radio" name="permission" value="custom_permission"
                                    >
                                    <span class="fa fa-circle"></span><?= lang('custom_permission') ?> <i
                                        title="<?= lang('permission_for_customization') ?>"
                                        class="fa fa-question-circle" data-toggle="tooltip"
                                        data-placement="top"></i>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group <?php
                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                        echo 'show';
                    }
                    ?>" id="permission_user_1">
                        <label for="field-1"
                               class="col-sm-2 control-label"><?= lang('select') . ' ' . lang('users') ?>
                            <span
                                class="required"></span></label>
                        <div class="col-sm-9">
                            <?php
                            if (!empty($assign_user)) {
                                foreach ($assign_user as $key => $v_user) {

                                    if ($v_user->role_id == 1) {
                                        $disable = true;
                                        $role = '<strong class="badge btn-danger">' . lang('admin') . '</strong>';
                                    } else {
                                        $disable = false;
                                        $role = '<strong class="badge btn-primary">' . lang('staff') . '</strong>';
                                    }

                                    ?>
                                    <div class="checkbox c-checkbox needsclick">
                                        <label class="needsclick">
                                            <input type="checkbox"
                                                <?php
                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            echo 'checked';
                                                        }
                                                    }

                                                }
                                                ?>
                                                   value="<?= $v_user->user_id ?>"
                                                   name="assigned_to[]"
                                                   class="needsclick">
                                                        <span
                                                            class="fa fa-check"></span><?= $v_user->username . ' ' . $role ?>
                                        </label>

                                    </div>
                                    <div class="action_1 p
                                                <?php

                                    if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                        $get_permission = json_decode($leads_info->permission);

                                        foreach ($get_permission as $user_id => $v_permission) {
                                            if ($user_id == $v_user->user_id) {
                                                echo 'show';
                                            }
                                        }

                                    }
                                    ?>
                                                " id="action_1<?= $v_user->user_id ?>">
                                        <label class="checkbox-inline c-checkbox">
                                            <input id="<?= $v_user->user_id ?>" checked type="checkbox"
                                                   name="action_1<?= $v_user->user_id ?>[]"
                                                   disabled
                                                   value="view">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('view') ?>
                                        </label>
                                        <label class="checkbox-inline c-checkbox">
                                            <input <?php if (!empty($disable)) {
                                                echo 'disabled' . ' ' . 'checked';
                                            } ?> id="<?= $v_user->user_id ?>"
                                                <?php

                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);

                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            if (in_array('edit', $v_permission)) {
                                                                echo 'checked';
                                                            };

                                                        }
                                                    }

                                                }
                                                ?>
                                                 type="checkbox"
                                                 value="edit" name="action_<?= $v_user->user_id ?>[]">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('edit') ?>
                                        </label>
                                        <label class="checkbox-inline c-checkbox">
                                            <input <?php if (!empty($disable)) {
                                                echo 'disabled' . ' ' . 'checked';
                                            } ?> id="<?= $v_user->user_id ?>"
                                                <?php

                                                if (!empty($leads_info->permission) && $leads_info->permission != 'all') {
                                                    $get_permission = json_decode($leads_info->permission);
                                                    foreach ($get_permission as $user_id => $v_permission) {
                                                        if ($user_id == $v_user->user_id) {
                                                            if (in_array('delete', $v_permission)) {
                                                                echo 'checked';
                                                            };
                                                        }
                                                    }

                                                }
                                                ?>
                                                 name="action_<?= $v_user->user_id ?>[]"
                                                 type="checkbox"
                                                 value="delete">
                                                        <span
                                                            class="fa fa-check"></span><?= lang('can') . ' ' . lang('delete') ?>
                                        </label>
                                        <input id="<?= $v_user->user_id ?>" type="hidden"
                                               name="action_<?= $v_user->user_id ?>[]" value="view">

                                    </div>


                                    <?php
                                }
                            }
                            ?>


                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                        <?php if (empty($leads_info->converted_client_id) || $leads_info->converted_client_id == 0) { ?>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-sm btn-primary"><?= lang('updates') ?></button>
                            </div>
                        <?php } ?>
                    </div>
            </form>
<?php } ?>
        </div>
		<!-- END OF LEAD INFO -->


			 </div>
            </div>
    </div>




<?php /* ?>
<div class="panel panel-custom">
    <header class="panel-heading">
        <div class="panel-title">
			<strong><?= 'Recordings' ?></strong>
        </div>
    </header>
    <div class="panel-body">

            <div class="panel-body">
		<h5>Recordings</h5/>
		 <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Recording</th>
                                    <th><?= 'link'; ?></th>
                                </tr>
                                </thead>
                                <tbody>
								<?php if(!empty($callRecordsFiles)){ foreach($callRecordsFiles as $recordings){
								?>
								<tr>
								    <td >
								        <?php
										$rname = explode("/",$recordings);
										//$rlink = explode(" ", $rname[5]);
										//$rlink2  =explode(" ", $rlink[0]);
										//print_r($rlink);
										// echo $rlink2[0]; //$rname[5];
										// print_r($rname);
										if(isset($rname[5])){
										$rlink = substr($rname[5],0, 35);
									    ?>
								        <audio controls style="width: 400px">
								            <source src="<?= substr($recordings, 0 , 73); ?>" type="audio/mpeg">
								        </audio>
								    </td>
									<td><a href="<?php echo substr($recordings, 0 , 73); ?>" target="_blank">
									<?php
										//$rname = explode("/",$recordings);
										//$rlink = explode(" ", $rname[5]);
										//$rlink2  =explode(" ", $rlink[0]);
										//print_r($rlink);
										// echo $rlink2[0]; //$rname[5];
										//echo substr($rname[5],0, 34);
											echo $rlink;
										}
											?>

									</a></td>
								</tr>
								<?php } } ?>
								</tbody>
					</table>
</div>
</div>
</div>
<?php */ ?>

</div>


 <script>
	function view_comments(id){
		$('#prev_comments').load('/admin/leads/get_comments/'+id, function(response) {
			$('#prev_comments').innerHTML = response;
		});
	}
</script>
