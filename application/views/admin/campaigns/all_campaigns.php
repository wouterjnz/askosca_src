<?= message_box('success'); ?>
<?= message_box('error'); ?>


<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage" data-toggle="tab"><?= 'All Campaigns' ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#create" data-toggle="tab"><?= 'New Campaign' ?></a></li>
        <li class="<?= $active == 3 ? 'active' : ''; ?>"><a href="#archived" data-toggle="tab"><del><?= 'Archived Campaigns' ?></del></a></li>
    </ul>
    <div class="tab-content bg-white">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">

            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="hidden"><?= 'ID' ?></th>
                        <th><?= 'Campaign ID' ?></th>
                        <th><?= 'Campaign Name' ?></th>
                        <th class="col-options no-sort"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$table_id = 'id'; // 'leads_id;'
					$table = 'tbl_campaigns'; // 'tbl_leads
                    if (!empty($all_leads)):foreach ($all_leads as $v_leads):
                            ?>
                            <tr>
                                 <td class="hidden">
                                    <?= $v_leads->id ?>
								</td>
                                <td>
                                     <a href="<?= base_url() ?>admin/leads/campaign_list/<?= $v_leads->$table_id ?>"><?= $v_leads->campaign_id ?></a>
								</td>
								<td>
                                     <?= $v_leads->campaign_name ?>
                                </td>
                                   <td>
										<?php $questions_exists = $this->db->where('campaign_id',$v_leads->id)->get('campaign_questions')->row(); ?>
										<?php if($questions_exists){ ?>
										<?= btn_view('admin/campaigns/campaign_questions/' . $v_leads->$table_id) ?>
										<?php }else{ ?>
                                        <?= btn_create('admin/campaigns/create_campaign_questions/' . $v_leads->$table_id, 'Questions') ?>
										<?php } ?>
                                        <?= btn_edit('admin/campaigns/index/' . $v_leads->$table_id) ?>
                                        <?= btn_delete_archived('admin/campaigns/delete_campaign/' . $v_leads->$table_id) ?>
                                </td>
                            </tr>
                            <?php
                        // }
                    endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
            <form role="form" enctype="multipart/form-data" id="form"
                  action="<?php echo base_url(); ?>admin/campaigns/saved_campaign/<?php
                  if (!empty($leads_info)) {
                      echo $leads_info->$table_id;
                  }
                  ?>" method="post" class="form-horizontal  ">

                <div class="panel-body">

				  <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Region'; ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <?php
								$regions = $this->db->get('tbl_regions')->result()
							?>
							<select name="region_id" class="form-control">
							<option value="">--</option>
							<?php
							if($regions){
								foreach($regions as $region) {
							?>
							<option value="<?php echo $region->id; ?>"  <?php if (!empty($leads_info)  && $leads_info->region_id == $region->id) { echo 'selected'; } ?>><?php echo $region->region; ?></option>
							<?php }} ?>
							</select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Campaign ID' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->campaign_id;
                            }
                            ?>" placeholder="8 characters max" name="campaign_id" minlength="2" maxlength="8" required="">
                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Campaign Name' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->campaign_name;
                            }
                            ?>" name="campaign_name" minlength="6" maxlength="40"required="">
                        </div>
                    </div>

					 <div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Campaign Description'; ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->campaign_description;
                            }
                            ?>" name="campaign_description" maxlength="200">
                        </div>
                    </div>



					<div class="form-group hidden">

						<label class="col-lg-2 control-label"><?= 'Type' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($leads_info)) {
                                echo $leads_info->type;
                            }else{
							echo 'Vicidial';
							}
                            ?>" name="type" required="">
                        </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                        <?php if (empty($leads_info->converted_client_id) || $leads_info->converted_client_id == 0) { ?>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-sm btn-primary"><?= 'Submit' ?></button>
                            </div>
                        <?php } ?>
                    </div>
            </form>
        </div>
        <div class="tab-pane <?= $active == 3 ? 'active' : ''; ?>" id="archived">
        <div class="table-responsive">
                <table class="table table-striped DataTables_archived " id="DataTables_archive" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="hidden"><?= 'ID' ?></th>
                        <th><del><?= 'Campaign ID' ?></del></th>
                        <th><del><?= 'Campaign Name' ?></del></th>
                        <th class="col-options no-sort"><del><?= lang('action') ?></del></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$table_id = 'id'; // 'leads_id;'
					$table = 'tbl_campaigns'; // 'tbl_leads
                    if (!empty($all_leads_archived)):foreach ($all_leads_archived as $v_leads):
                            ?>
                            <tr>
                                 <td class="hidden">
                                    <?= $v_leads->id ?>
								</td>
                                <td>
                                     <del><a href="<?= base_url() ?>admin/leads/campaign_list_archived/<?= $v_leads->$table_id ?>"><?= $v_leads->campaign_id ?></a></del>
								</td>
								<td>
                                     <del><?= $v_leads->campaign_name ?></del>
                                </td>
                                   <td>
                                     <?= btn_delete_permanent('admin/campaigns/delete_campaign_permanent/' . $v_leads->$table_id) ?>
                                </td>
                            </tr>
                            <?php
                        // }
                    endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>