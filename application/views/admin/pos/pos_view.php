 <style>
    .myTdiv { height: 350px; overflow: scroll; line-height: 3px}
</style>
<div class="panel">
	<div class="panel-body">
		
		<div class="row">
			<div class="col-md-5">
				<input type="hidden" value="<?= $warehouse_id; ?>" name="warehouse_id" />
				<div class="myTdiv">
				<table class="table" id="selectedProducts">
						<thead>
							<th><span class="fa fa-trash"></span></th>
							<th>Product</th>
							<th>Qty</th>
							<th>Price</th>
						</thead>
					
					<tbody>
					<tr>
					
					</tr>
				</tbody>
					
				</table>
			</div>	
				<table class="table">
					<tr>
						<td><strong>Total Items<strong></td>
						<td id="totalItems">0</td>
						<td><strong>Total<strong></td>
						<td id="totalAmount">0.00</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><strong>VAT<strong></td>
						<td id="vatTotal">0.00</td>
					</tr>
					<tr>
						<td><strong>Discount<strong></td>
						<td></td>
						<td></td>
						<td id="discountTotal">0.00</td>
					</tr>
					<tr>
						<td><strong>Total Payable<strong></td>
						<td></td>
						<td></td>
						<td id="totalPayable">0.00</td>
					</tr>
				</table>
			<div class="form-group">
			<button class="btn btn-warning" onclick="voidSale()">CANCEL</button>	
			<button class="btn btn-info">PRINT</button>	
			<!--<button class="btn btn-danger">SUSPEND</button>-->
			<button class="btn btn-success" onclick="finishPayment()">PAYMENT</button>	
			<!--<button class="btn btn-success" onclick="pop()">POP</button>	-->
			</div>
			</div>
		
		<div class="col-md-7">
			<h4>
				Products &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="text" name="scannedProduct" onkeypress="scannedProduct(event)" placeholder=" scan product.." autofocus style="background-color: #e6e600; height: 32px; color: #F00; font-size: 14px" /> 
			<div class="pull-right">
				<select class="form-control required" required="" style="width: 250px; background-color: #e6e600; font-size: 14px" name="client_id" id="clientSelected">
					<option value="-"><?= lang('select') . ' ' . lang('client') ?></option>
					<?php
					if (!empty($all_client)) {
						foreach ($all_client as $v_client) {
							if ($v_client->client_status == 1) {
								$status = lang('person');
							} else {
								$status = lang('company');
							}
							?>
							<option value="<?= $v_client->client_id ?>"
								<?php
								//if (!empty($client_id)) {
								//	echo $client_id == $v_client->client_id ? 'selected' : null;
								//}
								echo ($v_client->client_id == 319)? 'selected': '';
								?>
							><?= ucfirst($v_client->name) . ' <small>' . $status . '</small>' ?></option>
							<?php
						}
					}
					?>
				</select>
			
			</div></h4>
			<hr/>
			            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= lang('qty') ?></th>
                        <th><?= lang('item_name') ?></th>
                        <th><?= lang('description') ?></th>
                        <th><?= lang('unit_price') ?></th>
                        <th class="hidden"><?= 'Barcode';  ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$this->db->where('warehouse',$warehouse_id);
                    $warehouse_items = $this->db->get('tbl_saved_items')->result();

                    $currency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row();
                    $total_balance = 0;
                    foreach ($warehouse_items as $v_items):
                        ?>
                        <tr onclick="getProduct('<?= $v_items->saved_items_id ?>', '<?= $v_items->quantity ?>', '<?= $v_items->item_name ?>', '<?= $v_items->unit_cost ?>')">
                            <td><?= $v_items->quantity ?></td>
                            <td><?= $v_items->item_name ?></td>
                            <td><?= $v_items->item_desc ?></td>
                            <td><?=
                                display_money($v_items->unit_cost, $currency->symbol);
                                ?></td>
								<td class="hidden"><?= $v_items->barcode_string; ?></td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>
		</div>
		</div>
	</div>
</div>

<div id="confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Finish Sale</h4>
                </div>
                <div class="modal-body">
					
					<div class="form-group">
					<div class="col-md-4">
						<label>Total Items</label>
						</div>
						<div class="col-md-8">
						<input id="totalItemsf" name="totalItems" value="" readonly style="background-color: #e6e600" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<label>Total Payable</label>
						</div>
						<div class="col-md-8">
							<input id="totalPayablef" name="totalPayableAmount" value="" readonly style="background-color: #e6e600" />
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-4">
						<label>Paid</label>
					</div>
					<div class="col-md-8">
						<input id="paidVal" name="paid" value="0" onchange="giveChange()" />
					</div>
					</div>
					<div class="form-group">
					<div class="col-md-4">
						<label>Change</label>
					</div>
					<div class="col-md-8">
						<input id="changeVal" name="change" value="0" readonly style="background-color: #e6e600" />
					</div>
					</div>
					
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary" onclick="pop()">Finish</button>
                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>


<script type='text/javascript' src='<?php echo base_url() ?>assets/js/DecimalFormat.js'></script>

<script>
	function getProduct(id, qty, item, cost) {
		qty = 1;
		var cost = cost / 1.14;
		var inc = id + (new Date()).getTime();
		
		var numberbox = '<select type="text" name="selectedQty[]" id="SP'+inc+'"  onchange="updateQuantity(this, '+id+', '+cost+','+qty+','+inc+')"><option value="'+qty+'">'+qty+'</option>';
		for (var i=1; i < 100; i++) {
			numberbox += '<option value="'+i+'">'+i+'</option>';
		}
		numberbox += '</select>';
	
		$('#selectedProducts tbody').append('<tr id="selectedr'+inc+'"><td><input type="hidden" name="selectedPriceId[]" value="'+id+'" /><span id="span'+inc+'" class="fa fa-trash" onclick="removeProd('+inc+', '+id+','+cost+','+qty+')"></span></td><td>'+ item +'<input type="hidden" name="selectedPrice[]" value="'+cost+'" /></td><td>'+numberbox+'</td><td>'+cost+'</td></tr>');

		var totalItems = $('#totalItems').text();
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		var totalPayable = $('#totalPayable').text();
		
		var df = new DecimalFormat("#0.00#");
		
		$('#totalItems').text(parseFloat(totalItems) + 1);
		$('#totalAmount').text(df.format(parseFloat(totalAmount) + parseFloat(cost)));
		$('#vatTotal').text(df.format((parseFloat(vatTotal) + (parseFloat(cost) * 1.14)) - parseFloat(cost)));
		$('#totalPayable').text(df.format(parseFloat(totalPayable) + (parseFloat(cost) * 1.14)));
	}
	
	function removeProd(inc, id, cost, qty){
		//var cost  = cost * 1.14;
		var cost = cost * qty;
		 $('#selectedr'+inc).remove();
		var totalItems = $('#totalItems').text();
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		var totalPayable = $('#totalPayable').text();
		var df = new DecimalFormat("#0.00#");
		
		var calcVat = df.format(parseFloat(cost * 1.14));
		var vOnly = df.format(calcVat - parseFloat(cost));
		var newVat = df.format(parseFloat(vatTotal) - vOnly);
		
		$('#totalItems').text(totalItems - qty);
		$('#totalAmount').text(df.format(totalAmount - cost));
		$('#vatTotal').text(df.format(newVat));
		$('#totalPayable').text(df.format(totalPayable - calcVat));
	}
	
	function voidSale()
	{
		//Clear sales information
		$('#totalItems').text(0);
		$('#totalAmount').text(0.00);
		$('#vatTotal').text(0.00);
		$('#totalPayable').text(0.00);
		
		//Clear table contents
		$('#selectedProducts').find("tr:gt(1)").remove();
	}
	
	function finishPayment()
	{
		$('#confirm').modal({
			backdrop: 'static',
			show: true
		});
		
		var totalPayable = $('#totalPayable').text();
		var totalItems = $('#totalItems').text();
		var df = new DecimalFormat("#0.00#");
		
		document.getElementById('totalPayablef').value = df.format(totalPayable);
		document.getElementById('totalItemsf').value = totalItems;
	}
	
	function updateQuantity1(elem, id, cost, oldQty){
		var qty = Number($(elem).val());
		var costPrice = 0;
		var vatPrice = costPrice * 0.14;
		var totalPrice = costPrice +vatPrice;	
	}
	
	function updateQuantity(elem, id, cost, oldQty, inc)
	{
		 // var cost = cost  * 1.14;
		var qty = Number($(elem).val());
		var df = new DecimalFormat("#0.00#");
		
		var nqty = Number(qty);
		
		var ncost = (cost * nqty);
		var vcost = (ncost) * 1.14;
		//console.log(vcost);
		
		var oldTotalPayable= function(cost, oldQty) {
			var oldc = cost;
			var oldt =  cost * oldQty;
			var oldv = oldt  * 1.14;
			return df.format(parseFloat(oldv));
		};
		
		var oldTotalAmount = function(cost, oldQty) {
			var oldc = cost;
			var oldt =  cost * oldQty;
			var oldv = oldt * 1.14;
			return df.format(parseFloat(oldv));
		};
		
		var oldVatPayable= function(cost, oldQty) {
			var oldc = cost;
			var oldt =  cost * oldQty;
			var oldv = oldt * 0.14;
			return df.format(parseFloat(oldv));
		};
		
		oldTotalP = oldTotalPayable(cost, oldQty);
		oldVat = oldVatPayable(cost, oldQty);
		
		// FETCH TOTALS FROM SCREEN
		var totalItems = $('#totalItems').text();
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		var totalPayable = $('#totalPayable').text();
		
		var diff = Number(qty) - Number(oldQty);
		var diffcost = Number(diff) * parseFloat(cost);
		var tamount = parseFloat(totalAmount) + parseFloat(diffcost);
		var tvat = parseFloat(vatTotal) + parseFloat(oldVat);	
			
		$('#totalItems').text((Number(totalItems) + diff));
		$('#totalAmount').text(df.format(tamount));
		$('#vatTotal').text(df.format(tamount * 0.14));
		$('#totalPayable').text(df.format((parseFloat(totalPayable) - oldTotalP) + vcost));
		
		$('#SP'+inc).attr('Onchange','updateQuantity(this, '+id+','+cost+','+qty+','+inc+')');
		$('#span'+inc).attr('Onclick','removeProd('+inc+', '+id+','+cost+','+qty+')');
	}

	function pop() {
	    
	    if($('#clientSelected').val() == "-"){
			alert('Please Select Client');
			return;
		}
		
		var df = new DecimalFormat("#0.00#");
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		
		var prods= [];
			
		// $('input[name="selectedPriceId[]"]')[0].value;
		var ids = $('input[name="selectedPriceId[]"]');
		var idp = $('input[name="selectedPrice[]"]');
		var idq = $('select[name="selectedQty[]"]');
		for (i=0; i < ids.length;  i++) {
			prods[i] = [];
			// prods[i]['id'] = ids[i].value;
			// prods[i]['price'] = idp[i].value;
			// prods[i]['qty'] = idq[i].value;
			
			prods[i].push(ids[i].value);
			prods[i].push(idp[i].value);
			prods[i].push(idq[i].value);
		}
		// console.log(prods);
	  var form_data = {
		  'items[]': prods,
		  'totalAmount': df.format(parseFloat(totalAmount)),
		  'totalVat': df.format(parseFloat(vatTotal)),
		  'totalItemsf': $('#totalItemsf').val(),
		  'totalPayablef': $('#totalPayablef').val(),
		  'paidVal': $('#paidVal').val(),
		  'changeVal': $('#changeVal').val(),
		  'client_id': $('select[name="client_id"]').val(),
		  'warehouse_id': $('input[name="warehouse_id"]').val(),
	  }
	  
	$.ajax({
            type: "POST",
            url: "<?php echo base_url('admin/pos/save_items');?>",
            data: form_data,
		dataType : 'json',
		// contentType: "application/json",
		error: function(jqXHR, data, message) {
                // swal('Error!', 'Error ' + jqXHR.status +': ' + message, 'error');
				// $('#processingModal').modal('hide');
            }
        })
		.done(function (data) {
			console.log(data);
			if (data.success == false) {
				 // $('#processingModal').modal('hide');
				// swal('Error!', 'Error :' + data.message, 'error');
			} else {
				window.location.href = "<?php echo base_url('admin/pos/receipt');?>/"+data.result;
				// $('#processingModal').modal('hide');
				// swal("Done", data.message, "success");
			}
        });
	}
			
	function giveChange(){
		var df = new DecimalFormat("#0.00#");
		var totalPayable = $('#totalPayable').text();
		var paidVal = $('#paidVal').val();
		
		var changeAmount  = parseFloat(paidVal) - parseFloat(totalPayable);
		$('#changeVal').val(df.format(changeAmount));
	}
	
	function scannedProduct(e)
	{
		if(e.keyCode === 13){
			 e.preventDefault(); // Ensure it is only this code that rusn

			//console.log("Enter was pressed was presses");
		var value = e.target.value;
        
		$.ajax({
            type: "GET",
            url: "<?php echo base_url('admin/pos/get_item');?>/"+value,
		// contentType: "application/json",
		error: function(jqXHR, data, message) {
                // swal('Error!', 'Error ' + jqXHR.status +': ' + message, 'error');
				// $('#processingModal').modal('hide');
            }
        })
		.done(function (data) {
		//	console.log(data);
			if (data.success == false) {
				 // $('#processingModal').modal('hide');
				// swal('Error!', 'Error :' + data.message, 'error');
			} else {
				//	window.location.href = "<?php echo base_url('admin/pos/receipt');?>/"+data.result;
				// $('#processingModal').modal('hide');
				data = JSON.parse(data);
				if(data.success == 'success'){
					getProduct(data.saved_items_id, 1, data.item_name, data.unit_cost);
				}
			}
        });
		
		}
	}
</script>