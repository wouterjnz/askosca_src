 <style>
    .myTdiv { height: 350px; overflow: scroll; line-height: 3px}
</style>
<div class="panel">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-5">
				<div class="myTdiv">
				<table class="table" id="selectedProducts">
						<thead>
							<th><span class="fa fa-trash"></span></th>
							<th>Product</th>
							<th>Qty</th>
							<th>Price</th>
						</thead>
					
					<tbody>
					<tr>
					
					</tr>
				</tbody>
					
				</table>
			</div>	
				<table class="table">
					<tr>
						<td><strong>Total Items<strong></td>
						<td id="totalItems">0</td>
						<td><strong>Total<strong></td>
						<td id="totalAmount">0.00</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><strong>VAT<strong></td>
						<td id="vatTotal">0.00</td>
					</tr>
					<tr>
						<td><strong>Discount<strong></td>
						<td></td>
						<td></td>
						<td id="discountTotal">0.00</td>
					</tr>
					<tr>
						<td><strong>Total Payable<strong></td>
						<td></td>
						<td></td>
						<td id="totalPayable">0.00</td>
					</tr>
				</table>
			<div class="form-group">
			<button class="btn btn-warning" onclick="voidSale()">CANCEL</button>	
			<button class="btn btn-info">PRINT</button>	
			<button class="btn btn-danger">SUSPEND</button>
			<button class="btn btn-success" onclick="finishPayment()">PAYMENT</button>	
			</div>
			</div>
		
		<div class="col-md-7">
			<h4>Products</h4>
			<hr/>
			            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= lang('qty') ?></th>
                        <th><?= lang('item_name') ?></th>
                        <th><?= lang('description') ?></th>
                        <th><?= lang('unit_price') ?></th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $all_items = $this->db->get('tbl_saved_items')->result();

                    $currency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row();
                    $total_balance = 0;
                    foreach ($all_items as $v_items):
                        ?>
                        <tr onclick="getProduct('<?= $v_items->saved_items_id ?>', '<?= $v_items->quantity ?>', '<?= $v_items->item_name ?>', '<?= $v_items->unit_cost ?>')">
                            <td><?= $v_items->quantity ?></td>
                            <td><?= $v_items->item_name ?></td>
                            <td><?= $v_items->item_desc ?></td>
                            <td><?=
                                display_money($v_items->unit_cost, $currency->symbol);
                                ?></td>
                     
                        </tr>
                        <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>
		</div>
		
		</div>
	</div>
</div>

<div id="confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Finish Sale</h4>
                </div>
                <div class="modal-body">
					
					<div class="form-group">
						<div class="col-md-4">
							<label>Total Payable</label>
						</div>
						<div class="col-md-8">
							<input id="totalPayablef" name="totalPayableAmount" value="" readonly style="background-color: #EEE" />
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-4">
						<label>Total Items</label>
						</div>
						<div class="col-md-8">
						<input id="totalItemsf" name="totalItems" value="" readonly style="background-color: #EEE" />
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-4">
						<label>Paid</label>
					</div>
					<div class="col-md-8">
						<input id="paidVal" name="paid" value="0" onchange="giveChange()" />
					</div>
					</div>
					<div class="form-group">
					<div class="col-md-4">
						<label>Change</label>
					</div>
					<div class="col-md-8">
						<input id="changeVal" name="change" value="0" readonly style="background-color: #EEE" />
					</div>
					</div>
					
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary" id="finished">Finish</button>
                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>


<script type='text/javascript' src='<?php echo base_url() ?>assets/js/DecimalFormat.js'></script>

<script>
	function getProduct(id, qty, item, cost){
		// console.log('item: '+ qty + item + cost);
		qty = 1;
		var numberbox = '<select type="text" id="'+id+'"  onchange="updateQuantity(this, '+id+', '+cost+')"><option value="'+qty+'">'+qty+'</option>';
		for(var i=0; i < 100; i++){
			numberbox += '<option value="'+i+'">'+i+'</option>';
		}
		numberbox += '</select>';
		$('#selectedProducts tbody').append('<tr id="selectedr'+id+'"><td><span class="fa fa-trash" onclick="removeProd('+id+','+cost+')"></span></td><td>'+ item +'</td><td>'+numberbox+'</td><td>'+cost+'</td></tr>');
		var totalItems = $('#totalItems').text();
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		var totalPayable = $('#totalPayable').text();
		// var df = new DecimalFormat("R #0,000.00");
		var df = new DecimalFormat("#0.00#");
		// console.log(totalPayable);
		$('#totalItems').text(parseFloat(totalItems) + 1);
		$('#totalAmount').text(df.format(parseFloat(totalAmount) + parseFloat(cost)));
		$('#vatTotal').text(df.format((parseFloat(vatTotal) + (parseFloat(cost) * 1.14)) - parseFloat(cost)));
		$('#totalPayable').text(df.format(parseFloat(totalPayable) + (parseFloat(cost) * 1.14)));
		
		// console.log($('#totalPayable').text());
	}
	
	function removeProd(id, cost){
		// console.log('remove');
		 $('#selectedr'+id).remove();
		var totalItems = $('#totalItems').text();
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		var totalPayable = $('#totalPayable').text();
		var df = new DecimalFormat("#0.00#");
		
		var calcVat = df.format(parseFloat(cost * 1.14));
		var vOnly = df.format(calcVat - parseFloat(cost));
		var newVat = df.format(parseFloat(vatTotal) - vOnly);
		
		// console.log('SV: '+ df.format(parseFloat(vatTotal)));
		// console.log('CV: ' + df.format(parseFloat(calcVat)));
		// console.log('V: ' + df.format((calcVat - parseFloat(cost))));
		// console.log('NV: ' + df.format(parseFloat(newVat)));
		// console.log('NO: ' + vOnly);
		
		$('#totalItems').text(totalItems - 1);
		$('#totalAmount').text(df.format(totalAmount - cost));
		$('#vatTotal').text(df.format(newVat));
		$('#totalPayable').text(df.format(totalPayable - calcVat));
		
		// console.log('totalPayable: '+totalPayable);
		// console.log('cost: '+ (parseFloat(cost) * 1.14));
		// console.log('deducted: '+parseFloat(vatTotal) - (parseFloat(cost) * 1.14));
	}
	
	
	function voidSale()
	{
		//Clear sales information
		$('#totalItems').text(0);
		$('#totalAmount').text(0.00);
		$('#vatTotal').text(0.00);
		$('#totalPayable').text(0.00);
		
		//Clear table contents
		$('#selectedProducts').find("tr:gt(1)").remove();
	}
	
	function finishPayment()
	{
		$('#confirm').modal({
			backdrop: 'static',
			show: true
		});
		// $('#confirm').modal('show');
		
		var totalPayable = $('#totalPayable').text();
		var totalItems = $('#totalItems').text();
		var df = new DecimalFormat("#0.00#");
		
		document.getElementById('totalPayablef').value = df.format(totalPayable);
		document.getElementById('totalItemsf').value = totalItems;
	}
	
	function updateQuantity(elem, id, cost)
	{
		var qty = Number($(elem).val());
		// console.log('id: ' + id);
		// console.log('cost: ' + cost);
		// console.log('price: ' + (qty * cost));
		var df = new DecimalFormat("#0.00#");
		
		var nqty = qty;
		if(qty == 1){
			nqty = 1;
		}else{
			nqty = qty -1;
		}
		
		var ncost = (cost * nqty);
		var vcost = (cost * nqty) * 1.14;
		
		var totalItems = $('#totalItems').text();
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		var totalPayable = $('#totalPayable').text();
		
		
		$('#totalItems').text(parseFloat(totalItems) + nqty);
		$('#totalAmount').text(df.format(parseFloat(totalAmount) + parseFloat(cost)));
		$('#vatTotal').text(df.format((parseFloat(vatTotal) + vcost) - parseFloat(ncost)));
		$('#totalPayable').text(df.format(parseFloat(totalPayable) + vcost));
	}
	
	function giveChange(){
		var df = new DecimalFormat("#0.00#");
		var totalPayable = $('#totalPayable').text();
		var paidVal = $('#paidVal').val();
		
		var changeAmount  = parseFloat(paidVal) - parseFloat(totalPayable);
		$('#changeVal').val(df.format(changeAmount));
	}
</script>