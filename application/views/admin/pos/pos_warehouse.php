 <style>
    .myTdiv { height: 350px; overflow: scroll; line-height: 3px}
</style>
<div class="panel">
	<div class="panel-body">
		
			
			<div class="form-group">
                    <div class="col-lg-4 control-label">
					</div>
                    <div class="col-lg-4">
					<h4 style="text-align: center">Select Warehouse</h4>
                        <select name="warehouse" class="form-control" required="" onchange="gotopos(this.value)">
						<option value="">--</option>
						   <?php
                    $warehouses = $this->db->get('tbl_warehouse')->result();
					// print_r($warehouses);
					// exit;
                    if($warehouses):
					// echo "<option>jamie</option>";
					
					foreach ($warehouses as $whouse):
                        ?>
						<option  value="<?= $whouse->id; ?>" <?php      if (!empty($items_info)){ if($whouse->id == $items_info->warehouse){ echo 'selected'; }}	?>   >
							<?= $whouse->warehouse; ?> 
						</option>
						<?php 
						endforeach;
					
						endif;
						?>
						</select>
                    </div>
					<div class="col-lg-4 control-label">
					</div
                </div>
		</div>
	</div>
</div>

<div class="panel">
	<div class="panel-body">
		<div class="form-group">
			<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
				<h4 style="text-align: center">POS Transactions</h4>
				<table  class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th class="hidden"><strong>Date</strong></th>
						<th><strong>Client</strong></th>
						<th><strong>Total</strong></th>
						<th><strong>Date</strong></th>
						<th><strong>Action</strong></th>
					</tr>
				</thead>
				<tbody>
				<?php
					$this->db->select('tbl_pos.*, tbl_client.name');
					$this->db->join('tbl_client','tbl_client.client_id = tbl_pos.clientId','left');
					$this->db->order_by('tbl_pos.transactionDate','DESC');
					$pos = $this->db->get('tbl_pos')->result();
					if($pos):
					foreach ($pos as $p):
				?>
					<tr>
						<td class="hidden"><?= $p->transactionDate; ?></td>
						<td><?= (ucfirst($p->name))?: 'UNKNOWN'; ?></td>
						<td><?= $p->totalAmount; ?></td>
						<td><?= $p->transactionDate; ?></td>
						<td><a href="<?= base_url('admin/pos/receipt/'.$p->id); ?>" target="_blank" class="btn btn-primary bt-xs">View Receipt</a></td>
					</tr>	
				<?php 
						endforeach;
					endif;
				?>			
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>



<script type='text/javascript' src='<?php echo base_url() ?>assets/js/DecimalFormat.js'></script>

<script>
	function getProduct(id, qty, item, cost){
		qty = 1;
		var numberbox = '<select type="text" name="selectedQty[]" id="SP'+id+'"  onchange="updateQuantity(this, '+id+', '+cost+','+qty+')"><option value="'+qty+'">'+qty+'</option>';
		for(var i=1; i < 100; i++){
			numberbox += '<option value="'+i+'">'+i+'</option>';
		}
		numberbox += '</select>';
		$('#selectedProducts tbody').append('<tr id="selectedr'+id+'"><td><input type="hidden" name="selectedPriceId[]" value="'+id+'" /><span class="fa fa-trash" onclick="removeProd('+id+','+cost+','+qty+')"></span></td><td>'+ item +'<input type="hidden" name="selectedPrice[]" value="'+cost+'" /></td><td>'+numberbox+'</td><td>'+cost+'</td></tr>');
		var totalItems = $('#totalItems').text();
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		var totalPayable = $('#totalPayable').text();
		// var df = new DecimalFormat("R #0,000.00");
		var df = new DecimalFormat("#0.00#");
		// console.log(totalPayable);
		$('#totalItems').text(parseFloat(totalItems) + 1);
		$('#totalAmount').text(df.format(parseFloat(totalAmount) + parseFloat(cost)));
		$('#vatTotal').text(df.format((parseFloat(vatTotal) + (parseFloat(cost) * 1.14)) - parseFloat(cost)));
		$('#totalPayable').text(df.format(parseFloat(totalPayable) + (parseFloat(cost) * 1.14)));
		
	}
	
	function removeProd(id, cost){
		 $('#selectedr'+id).remove();
		var totalItems = $('#totalItems').text();
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		var totalPayable = $('#totalPayable').text();
		var df = new DecimalFormat("#0.00#");
		
		var calcVat = df.format(parseFloat(cost * 1.14));
		var vOnly = df.format(calcVat - parseFloat(cost));
		var newVat = df.format(parseFloat(vatTotal) - vOnly);
		
		$('#totalItems').text(totalItems - 1);
		$('#totalAmount').text(df.format(totalAmount - cost));
		$('#vatTotal').text(df.format(newVat));
		$('#totalPayable').text(df.format(totalPayable - calcVat));
	}
	
	
	function voidSale()
	{
		//Clear sales information
		$('#totalItems').text(0);
		$('#totalAmount').text(0.00);
		$('#vatTotal').text(0.00);
		$('#totalPayable').text(0.00);
		
		//Clear table contents
		$('#selectedProducts').find("tr:gt(1)").remove();
	}
	
	function finishPayment()
	{
		$('#confirm').modal({
			backdrop: 'static',
			show: true
		});
		
		var totalPayable = $('#totalPayable').text();
		var totalItems = $('#totalItems').text();
		var df = new DecimalFormat("#0.00#");
		
		document.getElementById('totalPayablef').value = df.format(totalPayable);
		document.getElementById('totalItemsf').value = totalItems;
	}
	
	
	function updateQuantity1(elem, id, cost, oldQty){
		var qty = Number($(elem).val());
		var costPrice = 0;
		var vatPrice = costPrice*0.14;
		var totalPrice = costPrice +vatPrice;	
	}
	
	
	function updateQuantity(elem, id, cost, oldQty)
	{
		var qty = Number($(elem).val());
		var df = new DecimalFormat("#0.00#");
		
		var nqty = Number(qty);
		
		var ncost = (cost * nqty);
		var vcost = (ncost) * 1.14;
		console.log(vcost);
		
		var oldTotalPayable= function(cost, oldQty) {
			var oldc = cost;
			var oldt =  cost * oldQty;
			var oldv = oldt * 1.14;
			return df.format(parseFloat(oldv));
		};
		
		var oldTotalAmount = function(cost, oldQty) {
			var oldc = cost;
			var oldt =  cost * oldQty;
			var oldv = oldt * 1.14;
			return df.format(parseFloat(oldv));
		};
		
		var oldVatPayable= function(cost, oldQty) {
			var oldc = cost;
			var oldt =  cost * oldQty;
			var oldv = oldt * 0.14;
			return df.format(parseFloat(oldv));
		};
		
		oldTotalP = oldTotalPayable(cost, oldQty);
		oldVat = oldVatPayable(cost, oldQty);
		
		// FETCH TOTALS FROM SCREEN
		var totalItems = $('#totalItems').text();
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		var totalPayable = $('#totalPayable').text();
		
		var diff = Number(qty) - Number(oldQty);
		var diffcost = Number(diff) * parseFloat(cost);
		var tamount = parseFloat(totalAmount) + parseFloat(diffcost);
		var tvat = parseFloat(vatTotal) + parseFloat(oldVat);	
			
		$('#totalItems').text((Number(totalItems) + diff));
		$('#totalAmount').text(df.format(tamount));
		$('#vatTotal').text(df.format(tamount*0.14));
		$('#totalPayable').text(df.format((parseFloat(totalPayable) - oldTotalP) + vcost));
		
		$('#SP'+id).attr('Onchange','updateQuantity(this, '+id+','+cost+','+qty+')');
	}
	

	
	function pop() {
		var df = new DecimalFormat("#0.00#");
		var totalAmount = $('#totalAmount').text();
		var vatTotal = $('#vatTotal').text();
		
		var prods= [];
			
		// $('input[name="selectedPriceId[]"]')[0].value;
		var ids = $('input[name="selectedPriceId[]"]');
		var idp = $('input[name="selectedPrice[]"]');
		var idq = $('select[name="selectedQty[]"]');
		for (i=0; i < ids.length;  i++) {
			prods[i] = [];
			// prods[i]['id'] = ids[i].value;
			// prods[i]['price'] = idp[i].value;
			// prods[i]['qty'] = idq[i].value;
			
			prods[i].push(ids[i].value);
			prods[i].push(idp[i].value);
			prods[i].push(idq[i].value);
		}
		// console.log(prods);
	  var form_data = {
		  'items[]': prods,
		  'totalAmount': df.format(parseFloat(totalAmount)),
		  'totalVat': df.format(parseFloat(vatTotal)),
		  'totalItemsf': $('#totalItemsf').val(),
		  'totalPayablef': $('#totalPayablef').val(),
		  'paidVal': $('#paidVal').val(),
		  'changeVal': $('#changeVal').val(),
		  'client_id': $('select[name="client_id"]').val(),
	  }
	  
	$.ajax({
            type: "POST",
            url: "<?php echo base_url('admin/pos/save_items');?>",
            data: form_data,
		dataType : 'json',
		// contentType: "application/json",
		error: function(jqXHR, data, message) {
                // swal('Error!', 'Error ' + jqXHR.status +': ' + message, 'error');
				// $('#processingModal').modal('hide');
            }
        })
		.done(function (data) {
			console.log(data);
			if (data.success == false) {
				 // $('#processingModal').modal('hide');
				// swal('Error!', 'Error :' + data.message, 'error');
			} else {
				window.location.href = "<?php echo base_url('admin/pos/receipt');?>/"+data.result;
				// $('#processingModal').modal('hide');
				// swal("Done", data.message, "success");
			}
        });
	}
	
	
		
	function giveChange(){
		var df = new DecimalFormat("#0.00#");
		var totalPayable = $('#totalPayable').text();
		var paidVal = $('#paidVal').val();
		
		var changeAmount  = parseFloat(paidVal) - parseFloat(totalPayable);
		$('#changeVal').val(df.format(changeAmount));
	}
	
	function scannedProduct(e)
	{
		if(e.keyCode === 13){
			 e.preventDefault(); // Ensure it is only this code that rusn

			//console.log("Enter was pressed was presses");
		var value = e.target.value;
        
		$.ajax({
            type: "GET",
            url: "<?php echo base_url('admin/pos/get_item');?>/"+value,
		// contentType: "application/json",
		error: function(jqXHR, data, message) {
                // swal('Error!', 'Error ' + jqXHR.status +': ' + message, 'error');
				// $('#processingModal').modal('hide');
            }
        })
		.done(function (data) {
		//	console.log(data);
			if (data.success == false) {
				 // $('#processingModal').modal('hide');
				// swal('Error!', 'Error :' + data.message, 'error');
			} else {
				//	window.location.href = "<?php echo base_url('admin/pos/receipt');?>/"+data.result;
				// $('#processingModal').modal('hide');
				data = JSON.parse(data);
				if(data.success == 'success'){
					getProduct(data.saved_items_id, 1, data.item_name, data.unit_cost);
				}
			}
        });
		
		}
	}
	
	function gotopos(value){
		//console.log(value);
		window.location.href = "<?php echo base_url('admin/pos/index/');?>"+value;
	}
</script>