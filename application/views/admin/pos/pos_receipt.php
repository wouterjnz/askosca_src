<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $page_title." ".$this->lang->line("Receipt").""; ?></title>
<style type="text/css" media="all">
body { text-align:center; color:#000; font-family: Arial, Helvetica, sans-serif; font-size:12px; }
#wrapper {  width: 280px; margin: 0 auto; }
#wrapper img { max-width: 250px; width: auto; }

h3 { margin: 5px 0; }
.left { width:60%; float:left; text-align:left; margin-bottom: 3px; }
.right { width:40%; float:right; text-align:right; margin-bottom: 3px; }
.table, .totals { width: 100%; margin:10px 0; }
.table th { border-bottom: 1px solid #000; }
.table td { padding:0; }
.totals td { width: 24%; padding:0; }
.table td:nth-child(2) { overflow:hidden; }

@media print {
	#buttons { display: none; }
	#wrapper { max-width: 300px; width: 100%; margin: 0 auto; font-size:8px; }
	#wrapper img { max-width:250px; width: 80%; }
}


</style>
</head>

<body>
<div id="wrapper">

<?php 
$ref = ""; 
$refs = "0";
$posId = $pos->id;
// $posId = 11;
// $posId = 1125;
for($i=0; $i <= 4; $i++){
	if(strlen($posId) < $i){
		$ref .= "$refs";
	}
}
// echo 'count: ' . strlen($posId);
// echo "<br/> ref: ". $ref.$posId;
?>

<img src="<?php echo $this->config->base_url(); ?>assets/img/logo-coloured.png" />
<h3 style="text-transform:uppercase;"><?php echo 'IT Store'; //$this->config->item('company_name');?></h3>
	<?php
	
	echo "<p style=\"text-transform:capitalize;\">". $this->config->item('company_address').", ".$this->config->item('company_city').", ".$this->config->item('company_zip_code') .", ".$this->config->item('company_country')."</p>"; 
	// Franschoek
	echo "<p style=\"text-transform:capitalize;\">Franschhoek Centre, Shop 7, 4 Main Road, Franchshoek</p>"; 
	//Paarl
	echo "<p style=\"text-transform:capitalize;\">311 Main Road, Paarl</p>"; 
	echo "<span class=\"left\">Vat No: ".$this->config->item('company_vat')."</span>";
	echo "<span class=\"left\">Reference no : PS-".$ref.$posId."</span> 
	<span class=\"right\">Tel : ".$all_client[0]->phone."</span>"; 
	echo '<div style="clear:both;"></div>';
	echo "<span class=\"left\">Customer : ". $all_client[0]->name."</span> 
	<span class=\"right\">Date: ".date('Y-m-d', strtotime($pos->transactionDate))."</span>"; 
	
	?>
	<?php /*echo "<p style=\"text-transform:capitalize;\">".$all_client[0]->address.", ".$all_client[0]->city.", ".$all_client[0]->zipcode.", ".$all_client[0]->country."</p>"; 
	echo "<span class=\"left\">Reference no : PS-".$ref.$posId."</span> 
	<span class=\"right\">Tel : ".$all_client[0]->phone."</span>"; 
	echo '<div style="clear:both;"></div>';
	echo "<span class=\"left\">Customer : ". $all_client[0]->name."</span> 
	<span class=\"right\">Date: ".date('Y-m-d', strtotime($pos->transactionDate))."</span>"; 
	*/
	?>
    <div style="clear:both;"></div>
<table class="table" cellspacing="0"  border="0">
	<thead>
	<tr>
		<th style="text-align:center; width:30px;">#</th>
		<th style="text-align:left; width:180px;">Description</th>
		<th>Qty</th>
		<th>Price</th>
		<th>Total</th>
	</tr>	
	</thead>
	<tbody>
	<?php $r = 1; foreach($items as $item){ ?>
	<tr>
		<td style="text-align:center; width:30px;"><?php echo $r; ?></td>
		<td style="text-align:left; width:180px;"><?php echo $item->item_name; ?></td>
		<td style="text-align:center; width:50px;"><?php echo $item->qty; ?></td>
		<td style="text-align:right; width:55px; "><?php echo $item->price; ?></td>
		<td style="text-align:right; width:65px;"><?php echo $item->price * $item->qty; ?></td>
	</tr>
	<?php $r++; } ?>
	</tbody>
</table>

<table class="table" cellspacing="0"  border="0">
	<tbody>
		<tr>
			<td tyle="text-align:left;">Total Items</td>
			<td style="text-align:right; padding-right:1.5%; border-right: 1px solid #999;font-weight:bold;"><?php echo $pos->totalItems; ?></td>
			<td style="text-align:left; padding-left:1.5%;">Total</td>
			<td style="text-align:right;font-weight:bold;"><?php echo $pos->totalAmount; ?></td>
		</tr>
	</tbody>
</table>

<table class="table" cellspacing="0"  border="0">
	<tbody>
		<tr>
			<td style="text-align:left; font-weight:bold; border-top:1px solid #000; padding-top:10px;">Vat @ 14%</td>
			<td style="border-top:1px solid #000; padding-top:10px; text-align:right; font-weight:bold;"><?php echo $pos->totalPayable - $pos->totalAmount; ?></td>
		</tr>
		<tr>
			<td style="text-align:left; font-weight:bold; padding-top:10px;">Total Payable</td>
			<td style="padding-top:10px; text-align:right; font-weight:bold;"><?php echo $pos->totalPayable; ?></td>
		</tr>
		<tr>
			<td style="text-align:left; font-weight:bold; padding-top:5px;">Paid</td>
			<td style="padding-top:5px; text-align:right; font-weight:bold;"><?php echo $pos->totalPaid; ?></td>
		</tr>
		<tr>
			<td style="text-align:left; font-weight:bold; padding-top:5px;">Change</td>
			<td style="padding-top:5px; text-align:right; font-weight:bold;"><?php echo $pos->totalChange; ?></td>
		</tr>
	</tbody>
</table>
<div id="buttons" style="padding-top:10px; text-transform:uppercase;">
<button type="button" onClick="window.print();return false;" style="width:100%; cursor:pointer; font-size:12px; background-color:#FFA93C; color:#000; text-align: center; border:1px solid #FFA93C; padding: 10px 1px; font-weight:bold;"><?php echo $this->lang->line("print"); ?></button></span>
<div style="clear:both;"></div>
	
<a href="<?php echo base_url('admin/pos/index/'.$pos->warehouse_id);?>" style="width:98%; display: block; font-size:12px; text-decoration: none; text-align:center; color:#FFF; background-color:#007FFF; border:2px solid #007FFF; padding: 10px 1px; margin: 5px auto 10px auto; font-weight:bold;">New Sale</a>

</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){ $('#email').click( function(){
var email = prompt("<?php echo $this->config->item('company_email'); ?>","<?php echo $all_client[0]->email; ?>");
if (email!=null){
  $.ajax({
		type: "post",
		url: "index.php?module=pos&view=email_receipt",
		data: { <?php echo $this->security->get_csrf_token_name(); ?>: "<?php echo $this->security->get_csrf_hash(); ?>", email: email, id: <?php echo $posId; ?> },
		dataType: "json",
		success: function(data) {
			   alert(data.msg);
		  },
	  error: function(){
		  alert('<?php echo $this->lang->line('ajax_request_failed'); ?>');
		  return false;
	  }
	});
}
return false;
}); });
$(window).load(function() { window.print(); });
</script>
</body>
</html>
