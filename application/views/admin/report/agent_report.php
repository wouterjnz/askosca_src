<?php if (!empty($report)): ?>
    <div id="printReport">
        <div class="show_print">
            <div style="width: 100%; border-bottom: 2px solid black;">
                <table style="width: 100%; vertical-align: middle;">
                    <tr>
                        <td style="width: 50px; border: 0px;">
                            <img style="width: 50px;height: 50px;margin-bottom: 5px;"
                                 src="<?= base_url() . config_item('company_logo') ?>" alt="" class="img-circle"/>
                        </td>

                        <td style="border: 0px;">
                            <p style="margin-left: 10px; font: 14px lighter;"><?= config_item('company_name') ?></p>
                        </td>

                    </tr>
                </table>
            </div>
            <br/>
        </div>
        <div class="panel panel-custom">
            <!-- Default panel contents -->
            <div class="panel-heading">
                <div class="panel-title">
                    <strong><?= lang('Agent Report') ?></strong>

                    <?php
                    if (!empty($all_transaction_info)):
                        ?>
                        <div class="pull-right hidden-print">
                            <a href="<?php echo base_url() ?>admin/report/account_statement_pdf/<?= $account_id . '/' . date('Y-m-d', strtotime($start_date)) . '/' . date('Y-m-d', strtotime($end_date)) . '/' . $transaction_type ?>"
                               class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top"
                               title="<?= lang('pdf') ?>"><?= lang('pdf') ?></a>
                            <a onclick="print_sales_report('printReport')" class="btn btn-xs btn-danger"
                               data-toggle="tooltip" data-placement="top"
                               title="<?= lang('print') ?>"><?= lang('print') ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="width: 15%"><?= lang('date') ?></th>
                        <th style="width: 15%"><?= lang('account') ?></th>
                        <th><?= lang('type') ?></th>
                        <th><?= lang('notes') ?></th>
                        <th><?= lang('amount') ?></th>
                        <th><?= lang('credit') ?></th>
                        <th><?= lang('debit') ?></th>
                        <th><?= lang('balance') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $total_amount = 0;
                    $total_debit = 0;
                    $total_credit = 0;
                    $total_balance = 0;
                    $curency = $this->report_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
                    if (!empty($all_transaction_info)): foreach ($all_transaction_info as $v_transaction) :
                        $account_info = $this->report_model->check_by(array('account_id' => $v_transaction->account_id), 'tbl_accounts');
                        ?>
                        <tr class="custom-tr custom-font-print">
                            <td><?= strftime(config_item('date_format'), strtotime($v_transaction->date)); ?></td>
                            <td class="vertical-td"><?= $account_info->account_name ?></td>
                            <td class="vertical-td"><?= lang($v_transaction->type) ?> </td>
                            <td class="vertical-td"><?= $v_transaction->notes ?></td>
                            <td><?= display_money($v_transaction->amount, $curency->symbol) ?></td>
                            <td><?= display_money($v_transaction->credit, $curency->symbol) ?></td>
                            <td><?= display_money($v_transaction->debit, $curency->symbol) ?></td>
                            <td><?= display_money($v_transaction->total_balance, $curency->symbol) ?></td>
                        </tr>
                        <?php
                        $total_amount += $v_transaction->amount;
                        $total_debit += $v_transaction->debit;
                        $total_credit += $v_transaction->credit;
                        $total_balance += $v_transaction->total_balance;
                        ?>
                    <?php endforeach; ?>
                        <tr class="custom-color-with-td">
                            <td style="text-align: right;" colspan="4"><strong><?= lang('total') ?>:</strong></td>
                            <td><strong><?= display_money($total_amount, $curency->symbol) ?></strong></td>
                            <td><strong><?= display_money($total_credit, $curency->symbol) ?></strong></td>
                            <td><strong><?= display_money($total_debit, $curency->symbol) ?></strong></td>
                            <td><strong><?= display_money($total_credit - $total_debit, $curency->symbol) ?></strong></td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <script type="text/javascript">

        function print_sales_report(printReport) {
            var printContents = document.getElementById(printReport).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }

    </script>

<?php endif; ?>
<form role="form" enctype="multipart/form-data" id="form"
      action="<?php echo base_url(); ?>admin/report/agent_report_xls" method="post" class="form-horizontal  ">
<section class="panel panel-custom">
	<div class="panel-heading">
		<div class="panel-title">
			<?= 'Agent Report'; ?>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
				<div class="form-group row">
					<label class="col-lg-2 control-label"><?= 'Select Agent'; ?> </label>
					<div class="col-lg-4">
						<select name="agent_id" class="form-control select_box" style="width: 100%" id="client_id">
							<option value="">--</option>
							<option value="all">All</option>
							<?php
							$all_client = $this->db->where('role_id',4)->get('tbl_users')->result();
							if (!empty($all_client)) {
								foreach ($all_client as $v_client) {
									?>
										<option value="<?= $v_client->user_id ?>" <?php
										if (!empty($leads_info) && $leads_info->user_id == $v_client->user_id) {
											echo 'selected';
										}
										?>><?= $v_client->username ?></option>
										<?php
									}
								}
							?>
						</select>
					</div>
					
						<label class="col-lg-2 control-label"><?= 'Type'; ?> </label>
					<div class="col-lg-4">
						<select name="type" class="form-control select_box" style="width: 100%" id="client_id">
							<option value="">--</option>
							<option value="1">Leads</option>
							<option value="4">Leads By Campaign</option>
							<option value="2">Clock In/Out</option>
							<option value="3">Agent Pipeline leads</option>
						</select>
					</div>
					
				</div>
				<div class="form-group row">
					<label class="col-lg-2 control-label"><?= 'Date From'; ?> </label>
					<div class="col-lg-4">
					  <div class="input-group">
							<input class="form-control datepicker" type="text"
								   value="<?php echo date('Y-m-d'); ?>"
								   name="date_from" id="date_from"
								   data-date-format="<?= config_item('date_picker_format'); ?>">
							<div class="input-group-addon">
								<a href="#"><i class="fa fa-calendar"></i></a>
							</div>
						</div>
					</div>
					<label class="col-lg-2 control-label"><?= 'Date To'; ?> </label>
					<div class="col-lg-4">
					  <div class="input-group">
							<input class="form-control datepicker" type="text"
								   value="<?php echo date('Y-m-d'); ?>"
								   name="date_to" id="date_to"
								   data-date-format="<?= config_item('date_picker_format'); ?>">
							<div class="input-group-addon">
								<a href="#"><i class="fa fa-calendar"></i></a>
							</div>
						</div>
					</div>
				</div>
			   <div class="form-group row">
					<div class="col-lg-12">
						<!-- <button onclick="load_div()" id="sbtn" class="btn btn-primary pull-right"><?= 'submit'; ?></button> -->
						<input type="submit" class="btn btn-primary pull-right" value="<?= 'submit'; ?>" />
					</div>
				</div>
		</div>
	</div>
</section>	
</form>
<!--
<div class="panel">
	<div class="panel-body">
	  <a style="margin-right: 5px"
		   href="" id="hrefUrl"
           data-toggle="tooltip" data-placement="top" title="" data-original-title="PDF"
           class="btn btn-xs btn-success pull-right">
            <i class="fa fa-file-pdf-o"></i>
        </a>
		<div class="hidden" id="image_loader"><center><img src="<?php echo base_url() ."uploads/ajax-loader.gif"; ?>" /></center></div>	
		<div id="report_data"></div>
	</div>
</div>
-->
<script>
	function load_div() {
	   	$('#report_data').html('');
	    $('#image_loader').removeClass('hidden');
		var date_from	= $('#date_from').val();
		var date_to	= $('#date_to').val();
		var client_id		= $('#client_id').val();
		$('#report_data').load('invoice_statement/'+client_id+'/'+date_from+'/'+date_to, function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success")
             $('#image_loader').addClass('hidden');
        if(statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
         });
    	$("#hrefUrl").attr("href", '<?php echo base_url('admin/report/invoice_statement')?>/'+client_id+'/'+date_from+'/'+date_to+'/download');
	}
	function get_pdf() {
		var date_from	= $('#date_from').val();
		var date_to	= $('#date_to').val();
		var client_id		= $('#client_id').val();
		var domain_only = $('#domain_only').prop('checked');
		$.ajax({
			type: "GET",
			url: '<?php echo base_url('admin/report/invoice_statement')?>/'+client_id+'/'+date_from+'/'+date_to+'/download',
			error: function(jqXHR, data, message) {
                // swal('Error!', 'Error ' + jqXHR.status +': ' + message, 'error');
				// $('#processingModal').modal('hide');
            }
		});
	}
</script>