<div class="panel">
<div class="panel-body">
  <a style="margin-right: 5px"
         onclick="get_pdf()"
           data-toggle="tooltip" data-placement="top" title="" data-original-title="PDF"
           class="btn btn-xs btn-success pull-right">
            <i class="fa fa-file-pdf-o"></i>
        </a>
	<table class="table table-striped" cellspacing="0" width="100%">
	<tr>
		<td>Created</td>
		<td>Ticket Code</td>
		<td>Subject</td>
		<td>Status</td>
		<td>Time Spent</td>
		<td>Last Reply</td>
		<td>Last Reply Date</td>
	</tr>
<?php 
$total_time = 0;
foreach($ticket_info as $ticket){ 
$query = $this->db->limit(1)->order_by('time', 'DESC')->where('tickets_id', $ticket->tickets_id)->get('tbl_tickets_replies')->row();
?>
<tr>
		<td><?php echo $ticket->created; ?></td>
		<td><?php echo $ticket->ticket_code; ?></td>
		<td><?php echo $ticket->subject ;?></td>
		<td><?php echo $ticket->status ;?></td>
		<td><?php if (isset($query->time_spent)){ $ticket_time = str_replace('mins','',$query->time_spent); echo $query->time_spent; $total_time += $ticket_time; } ?></td>
		<td><?php echo isset($query->body)? $query->body : 'NA' ;?></td>
		<td><?php echo isset($query->time)? $query->time : 'NA'; ?></td>
</tr>
<?php } ?>
<tr>
    <td colspan="4">Total Time Spent: </td>
    <td><?php echo $total_time . 'mins'; ?></td>
    <td></td>
    <td></td>
</tr>
	</table>
</div>
</div>