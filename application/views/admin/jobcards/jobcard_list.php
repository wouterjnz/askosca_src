<?= message_box('success'); ?>
<?= message_box('error'); ?>
<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="<?php echo base_url('admin/jobcards/jobcard_list');?>"
                                                            ><?= 'All Jobcards'; ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#create"
                                                            data-toggle="tab"><?= 'New Jobcards'; ?></a></li>
    </ul>
    <div class="tab-content bg-white">
	
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= 'Client'; ?></th>
                        <th><?= lang('user') ?></th>
                        <th><?= lang('start_time') ?></th>
                        <th><?= lang('end_time') ?></th>
                        <th><?= lang('description') ?></th>
                        <th><?= 'Signed'; ?></th>
                        <th class="col-options no-sort"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $jobcards = $this->db->get('tbl_jobcards')->result();
					
                    foreach ($jobcards as $jobcard):
                        ?>
                        <tr>
                            <td>
								<?php
								$this->db->where('client_id',$jobcard->client_id);
								$client = $this->db->get('tbl_client')->result();
								echo ($client)? $client[0]->name: 'NA'; ?>
							</td>
                            <td>
								<?php
								$this->db->where('user_id',$jobcard->staff_id);
								$staff = $this->db->get('tbl_users')->result();
								echo ($staff)? $staff[0]->username: 'NA'; ?>
							</td>
                            <td><?= $jobcard->start_time ?></td>
                            <td><?= $jobcard->end_time ?></td>
                            <td><?= $jobcard->description; ?></td>
                            <td><?= ($jobcard->signed)? '<small style="font-size:10px;padding:2px;" class="label label-success">&nbsp;&nbsp;&nbsp;Y&nbsp;&nbsp;&nbsp;&nbsp;</small>': '<small style="font-size:10px;padding:2px;" class="label label-warning">&nbsp;&nbsp;&nbsp;N&nbsp;&nbsp;&nbsp;</small>'; ?></td>
                            <td>
                                <?= btn_edit('admin/jobcards/jobcard_list/' . $jobcard->id) ?>
                                <?= btn_delete('admin/jobcards/delete_jobcard/' . $jobcard->id) ?>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
		<div class="col-md-7 col-sm-7">
            <form role="form" data-parsley-validate="" novalidate="" enctype="multipart/form-data" id="form"
                  action="<?php echo base_url(); ?>admin/jobcards/saved_jobcards/<?php
                  if (!empty($items_info)) {
                      echo $items_info->id;
                  }
                  ?>" method="post" class="form-horizontal  ">
				<div class="form-group">
                    <label class="col-lg-3 control-label"><?= 'Staff'; ?> <span
                            class="text-danger"></span></label>
                    <div class="col-lg-7">
                        <select name="staff_id" class="form-control" required="">
							<option value="">--</option>
						   <?php
							$users = $this->db->get('tbl_users')->result();
							if($users):
								foreach ($users as $user):
							?>
						<option value="<?= $user->user_id; ?>" <?php      if (!empty($items_info)){ if($user->user_id == $items_info->staff_id){ echo 'selected'; }}	?>>
							<?= $user->username; ?>
						</option>
						<?php 
							endforeach;
						endif;
						?>
						</select>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-3 control-label"><?= 'Client'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-7">
                        <select name="client_id" class="form-control" required="">
						<option value="">--</option>
					<?php
                    $clients = $this->db->get('tbl_client')->result();
                    if($clients):
				    foreach ($clients as $client):
                    ?>
						<option value="<?= $client->client_id; ?>" <?php      if (!empty($items_info)){ if($client->client_id == $items_info->client_id){ echo 'selected'; }}	?>>
							<?= $client->name; ?>
						</option>
					<?php 
					endforeach;
					endif;
					?>
						</select>
                    </div>
                </div>
					
					
					<div class="form-group">
                        <label for="field-1" class="control-label col-sm-3 "><?= 'Start Date'; ?><span class="required">*</span></label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input required type="text" class="form-control   datepicker" name="start_time" value="<?php
                                if (!empty($items_info->start_time)) {
                                    echo $items_info->start_time;
                                }
                                ?>" data-format="yyyy/mm/dd">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
				
					<div class="form-group">
                        <label for="field-1" class="control-label col-sm-3 "><?= 'End Date'; ?><span class="required">*</span></label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input required type="text" class="form-control   datepicker" name="end_time" value="<?php
                                if (!empty($items_info->end_time)) {
                                    echo $items_info->end_time;
                                }
                                ?>" data-format="yyyy/mm/dd">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
					
                <div class="form-group terms">
                    <label class="col-lg-3 control-label"><?= lang('description') ?> </label>
                    <div class="col-lg-7">
                        <textarea name="description" class="form-control"><?php
                            if (!empty($items_info->description)) {
                                echo $items_info->description;
                            }
                            ?></textarea>
                    </div>
                </div>
            <?php if(is_numeric($this->uri->segment(4))){ ?>
				
				<?php 
					$hidden = '';
					if($items_info->signed){ 
					 $hidden = 'hidden';
				?>
				<div class="form-group" id="signedSignature">
				<label class="col-lg-3 control-label"><?= 'Signed'; ?> </label>
                    <div class="col-lg-5">
					<?php echo  '<img src="data:image/jpg;base64,'. $items_info->signature .'">';?>
					</div>
				</div>
				<?php } ?>
				
				<div class="form-group terms <?php echo $hidden; ?>" id="signature">
                    <label class="col-lg-3 control-label"><?= 'Signature'; ?> </label>
                    <div class="col-lg-7">
                       <div class="wrapper">
						<center><mute>SIGN HERE</mute></center>
						<canvas id="signature-pad" class="signature-pad" width=400 height=200></canvas>
					</div>
					<div>
						<a id="save" class="btn btn-primary">Save</a>
						<a id="clear" class="btn btn-primary">Clear</a>
					</div>
                    </div>
                </div>
				
				
			<?php } ?>	
                <div class="form-group">
                    <label class="col-lg-3 control-label"></label>
                    <div class="col-lg-7">
                        <button type="submit" class="btn btn-sm btn-primary"><?= lang('update') ?></button>
					<?php if(!empty($items_info->signed)){ ?>                      
					  <button type="button" class="btn btn-sm btn-primary" onclick="resign()" id="resignbtn"><?= 'Re-sign'; ?></button>
					<?php } ?>
                    </div>
                </div>
            </form>
		 </div>	
			  <?php if(is_numeric($this->uri->segment(4))){ ?>
		<button data-toggle="collapse" data-target="#topic-reply" class="btn btn-primary mb mt"
				aria-expanded="true"><?= 'Reply Jobcard'; ?>
		</button>
			  <?php } ?>
			  
	
			  
		 <?php if(is_numeric($this->uri->segment(4))){ ?>
		<div class="col-md-4 col-sm-4" >
			<div id="topic-reply" class="collapse" aria-expanded="true">
				<form method="post" enctype="multipart/form-data"
					  action="<?= base_url() ?>admin/jobcards/save_reply/<?= $items_info->id ?>">
					<div class="form-group col-sm-12">
					<textarea class="form-control no-border" name="reply" rows="3" placeholder=" #reply "></textarea>
					</div>
					<?php /*
					<div id="add_new">
						<div class="form-group">
							<div class="col-sm-8">
								<div class="fileinput fileinput-new" data-provides="fileinput">

									<span class="btn btn-default btn-file"><span
											class="fileinput-new"><?= lang('select_file') ?></span>
												<span class="fileinput-exists"><?= lang('change') ?></span>
												<input type="file" name="attachment[]">
											</span>
									<span class="fileinput-filename"></span>
									<a href="#" class="close fileinput-exists" data-dismiss="fileinput"
									   style="float: none;">&times;</a>
								</div>
								<div id="msg_pdf" style="color: #e11221"></div>
							</div>
							<div class="col-sm-4">
								<strong><a href="javascript:void(0);" id="add_more" class="addCF "><i
											class="fa fa-plus"></i>&nbsp;<?= lang('add_more') ?>
									</a></strong>
							</div>
						</div>
					</div>
					*/ ?>
					<div class="form-group panel-footer ">

						<button class="btn btn-info pull-right btn-sm mt"
								type="submit"><?= lang('submit') ?></button>
					</div>
				</form>
			</div>
		</div>			
			
	<?php 
			if(!empty($items_info->id)){
			$this->db->order_by('date_added','desc');
				$this->db->where('jobcard_id',$items_info->id);
			$this->db->join('tbl_users','tbl_users.user_id = tbl_jobcard_replies.user_id','left');
			$replies = $this->db->get('tbl_jobcard_replies')->result();
			}
			if($replies){
			foreach($replies as $reply){
				$label = '<small style="font-size:10px;padding:2px;" class="label label-default"> Unknown </small>';
				if ($reply->role_id == 1) {
					$label = '<small style="font-size:10px;padding:2px;" class="label label-danger ">' . lang('admin') . '</small>';
				}elseif($reply->role_id == 3) {
					$label = '<small style="font-size:10px;padding:2px;" class="label label-primary">' . lang('staff') . '</small>';
				} elseif($reply->role_id == 2) {
					$label = '<small style="font-size:10px;padding:2px;" class="label label-info">' . lang('client') . '</small>';
				}
				$today = time();
				$reply_day = strtotime($reply->date_added);
									
			?>
		<div class="col-md-4 col-sm-4" >
			<div class="form-group row">
			 <small class="text-muted pull-right"><i
					class="fa fa-clock-o"></i> <?= $this->tickets_model->get_time_different($today, $reply_day) ?> <?= lang('ago') ?>
				<?php if ($reply->user_id == $this->session->userdata('user_id')) { ?>
					<?= btn_delete('admin/jobcards/delete_ticket_reply/' . $reply->id); ?>
				<?php } ?></small>
			<a href="#" class="name">
				<?= ($reply->username)? $reply->username .' ' . $label : ' NA ' .' ' . $label;  ?>
			</a>
			<?php echo '<br/> ' . nl2br($reply->reply);?>
			</div>
		</div>
		<?php
			} }
		?>	  
		  <?php } ?>			
		<?php for($i=0; $i < 25; $i++){
			echo "<br/>";
		 } ?>
    </div>
    </div>
</div>

<script src="<?php echo base_url('plugins/signature_pad-master/src/signature_pad.js');?>"></script>
<script>
/*
var canvas = document.querySelector("canvas");

var signaturePad = new SignaturePad(canvas);

// Returns signature image as data URL (see https://mdn.io/todataurl for the list of possible paramters)
signaturePad.toDataURL(); // save image as PNG
signaturePad.toDataURL("image/jpeg"); // save image as JPEG

// Draws signature image from data URL
signaturePad.fromDataURL("data:image/png;base64,iVBORw0K...");

// Clears the canvas
signaturePad.clear();

// Returns true if canvas is empty, otherwise returns false
signaturePad.isEmpty();

// Unbinds all event handlers
signaturePad.off();

// Rebinds all event handlers
signaturePad.on();

var signaturePad = new SignaturePad(canvas, {
    minWidth: 5,
    maxWidth: 10,
    penColor: "rgb(66, 133, 244)"
});

var signaturePad = new SignaturePad(canvas);
signaturePad.minWidth = 5;
signaturePad.maxWidth = 10;
signaturePad.penColor = "rgb(66, 133, 244)";

function resizeCanvas() {
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
    signaturePad.clear(); // otherwise isEmpty() might return incorrect value
}

window.addEventListener("resize", resizeCanvas);
resizeCanvas();
*/

/*
var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
  backgroundColor: 'rgba(50, 50, 50, 50)',
  penColor: 'rgb(0, 0, 0)'
});
var saveButton = document.getElementById('save');
var cancelButton = document.getElementById('clear');

saveButton.addEventListener('click', function (event) {
  var signature_data = signaturePad.toDataURL('image/png');

  $.ajax({
	type: "POST",
	url: "<?php echo site_url('jobcart/jobcarts/save_signature'); ?>",
	data: {'signature_image': signature_data},
  })
  .done(function (data) {
			if (data.success == false) {
				console.log(data);
				 // $('#processingModal').modal('hide');
				// swal('Error!', 'Error :' + data.message, 'error');
			} else {
				// $('#processingModal').modal('hide');
				// swal("Done", data.message, "success");
			}
        });;
  // var d = 'signature';
  // $(this).attr("href", data).attr("download", "file-" + d + ".png");
});

cancelButton.addEventListener('click', function (event) {
  signaturePad.clear();
});
  */
  
var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
  backgroundColor: 'rgba(200, 200, 200, 25)',
  penColor: 'rgb(0, 0, 0)'
});
var saveButton = document.getElementById('save');
var cancelButton = document.getElementById('clear');

saveButton.addEventListener('click', function (event) {
  var signature_data = signaturePad.toDataURL('image/png');

  $.ajax({
	type: "POST",
	url: "<?php echo base_url('admin/jobcards/save_signature/'); ?>/"+<?php echo $items_info->id; ?>,
	data: {'signature_image': signature_data},
  })
  .done(function (data) {
			data = JSON.parse(data);
			if (data.success == false) {
				// console.log('fail');
				 // $('#processingModal').modal('hide');
				// swal('Error!', 'Error :' + data.message, 'error');
			} else {
				// console.log('success');
				location.reload();
				// $('#processingModal').modal('hide');
				// swal("Done", data.message, "success");
			}
        });
  // var d = 'signature';
  // $(this).attr("href", data).attr("download", "file-" + d + ".png");
});

cancelButton.addEventListener('click', function (event) {
  signaturePad.clear();
});
function resign()
{
	$('#signedSignature').addClass('hidden');
	$('#signature').removeClass('hidden');
	$('#resignbtn').addClass('hidden');
}
</script>