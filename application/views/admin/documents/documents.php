<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"/>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


<div class="panel panel-custom">
    <div class="panel-heading">
        <div class="panel-title"><?= 'Documents'; ?></div>
    </div>
    <div class="panel-body">
		<ul>
			<li><a href="<?php echo base_url();?>uploads/documents/faulty_returns.pdf" target="_blank">Faulty Returns</a></li>
			<li><a href="<?php echo base_url();?>uploads/documents/Cellphone_Repair_Job_Card.pdf" target="_blank">Cellphone Repair Job Card</a></li>
			<li><a href="<?php echo base_url();?>uploads/documents/Delivery_note.pdf" target="_blank">Delivery Note</a></li>
			<li><a href="<?php echo base_url();?>uploads/documents/Stock_Taking_Form.pdf" target="_blank">Stock Taking Form</a></li>
			<li><a href="<?php echo base_url();?>uploads/documents/Manual_Job_Card.pdf" target="_blank">Manual Job Card </a></li>
			<li><a href="<?php echo base_url();?>uploads/documents/Installation_job_card.pdf" target="_blank">Installation Job Card </a></li>
			<li><a href="#jobcard" data-toggle="tab" onclick="activate_tab('jobcard')">Manual Job Card (Capture)</a></li>
			<li><a href="#installation"  data-toggle="tab" onclick="activate_tab('installation')">Installation Job Card (Capture)</a></li>
		</ul>
    </div>
</div>

<?php if(!isset($active)){ $active = 1;} ?>
<div class="nav-tabs-custom">
		<!-- Tabs within a box -->
		<ul class="nav nav-tabs">
			<li class="<?= $active == 1 ? 'active' : ''; ?>" id="jobcard_li"><a href="#jobcard" data-toggle="tab"><?= 'Manual Job Card'; ?></a></li>
			<li class="<?= $active == 2 ? 'active' : ''; ?>" id="installation_li"><a href="#installation" data-toggle="tab"><?= 'Installation Job Card'; ?></a></li>
			<li class="<?= $active == 3 ? 'active' : ''; ?>" id="jobcard_lis"><a href="#jobcard_list" data-toggle="tab"><?= 'All Manual Job Cards'; ?></a></li>
			<li class="<?= $active == 4 ? 'active' : ''; ?>" id="installation_lis"><a href="#installation_list" data-toggle="tab"><?= 'ALL Installation Job Cards'; ?></a></li>
		</ul>
	
	<div class="tab-content bg-white">

	  <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="jobcard">
	  <?php if(!empty($all_manualjobcard)){ $form_url = 'update/tbl_docs_manualjobcard/'.$all_manualjobcard->id ; }else{ $form_url = 'save/tbl_docs_manualjobcard'; } ?>
	  <form action="<?php echo base_url('admin/documents/'.$form_url);?>" method="post">
            <div class="table-responsive">
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Client / Company Name'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
						<select class="form-control select_box" required style="width: 100%" name="client">
                           <option value="-"><?= lang('select') . ' ' . lang('client') ?></option>
							<?php
								if (!empty($all_client)) {
									foreach ($all_client as $v_client) {
										if ($v_client->client_status == 1) {
											$status = lang('person');
										} else {
											$status = lang('company');
										}
										if (!empty($project_info->client_id)) {
											$client_id = $project_info->client_id;
										} elseif ($invoice_info->client_id) {
											$client_id = $invoice_info->client_id;
										}
										?>
										<option value="<?= $v_client->client_id ?>"
											<?php
											if (!empty($all_manualjobcard)) {
												echo $all_manualjobcard->client == $v_client->client_id ? 'selected' : null;
											}
											?>
										><?= ucfirst($v_client->name) . ' <small>' . $status . '</small>' ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'ID / Registration Number'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->id_reg_number; } ?>" name="id_reg_number" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Primary Contact Person'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->primary_contact; } ?>" name="primary_contact" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Cell phone'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->cellphone; } ?>" name="cellphone" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Email'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->email; } ?>" name="email" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Address'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->address; } ?>" name="address" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Hardware Model'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->hardware_model; } ?>" name="hardware_model" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Operating System'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->operating_system; } ?>" name="operating_system" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Fault / Service Description'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->fault_service_description; } ?>" name="fault_service_description" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'OS Activation code (if applicable)'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->os_activation_code; } ?>" name="os_activation_code" placeholder="" >
                    </div>
                </div>
		
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Accessories'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->accessories; } ?>" name="accessories" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Book-in Date'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
						<div class="input-group">
							<input type="text" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" name="book_in_date" data-date-format="<?= config_item('date_picker_format'); ?>" />
						   <div class="input-group-addon">
                                <a href="#"><i class="fa fa-calendar"></i></a>
                            </div>
						</div>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Client Signature'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="" name="client_signature_in" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Fault Diagnosis'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->fault_description; } ?>" name="fault_description" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Hardware / Software Required'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard->barcode)){ echo $all_manualjobcard->barcode; } ?>" name="barcode" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Technician Notes'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->technician_notes; } ?>" name="technician_notes" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Book-out Date'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                      <div class="input-group">
							<input type="text" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" name="bookout_date" data-date-format="<?= config_item('date_picker_format'); ?>" />
						   <div class="input-group-addon">
                                <a href="#"><i class="fa fa-calendar"></i></a>
                            </div>
						</div>
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Client Signature'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="" name="client_signature_out" placeholder="" >
                    </div>
                </div>
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Fault / Service Description'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="<?php if(!empty($all_manualjobcard)){ echo $all_manualjobcard->fault_description; } ?>" name="fault_description" placeholder="" >
                    </div>
                </div>
			</div>
			<input type="submit" name="submit" class="btn btn-primary" value="<?php if(!empty($all_manualjobcard)){ echo 'Update'; }else{ echo 'Submit'; } ?>"/>
			</form>
		</div>
		
		 <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="installation">
		   <?php if(!empty($all_installation)){ $form_url = 'update/tbl_docs_installation/'.$all_installation->id.'/1'; }else{ $form_url = 'save/tbl_docs_installation/1'; } ?>
		  <form action="<?php echo base_url('admin/documents/'.$form_url);?>" method="post">
            <div class="table-responsive">
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Customer'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                          <select class="form-control select_box" required style="width: 100%" name="customer">
                           <option value="-"><?= lang('select') . ' ' . lang('client') ?></option>
                                        <?php
                                        if (!empty($all_client)) {
                                            foreach ($all_client as $v_client) {
                                                if ($v_client->client_status == 1) {
                                                    $status = lang('person');
                                                } else {
                                                    $status = lang('company');
                                                }
                                                if (!empty($project_info->client_id)) {
                                                    $client_id = $project_info->client_id;
                                                } elseif ($invoice_info->client_id) {
                                                    $client_id = $invoice_info->client_id;
                                                }
                                                ?>
                                                <option value="<?= $v_client->client_id ?>"
                                                    <?php
                                                    if (!empty($all_installation)) {
                                                        echo $all_installation->customer == $v_client->client_id ? 'selected' : null;
                                                    }
                                                    ?>
                                                ><?= ucfirst($v_client->name) . ' <small>' . $status . '</small>' ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
					<label class="col-lg-3 control-label"><?= 'Job Number '; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->job_number; } ?>" name="job_number" placeholder="" >
                    </div>
                </div>
				
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Address '; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->address_line1; } ?>" name="address_line1" placeholder="" >
                    </div>
					<label class="col-lg-3 control-label"><?= 'Date Received  '; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
						<div class="input-group">
							<input type="text" class="form-control datepicker" data-value="<?php if(!empty($all_installation)){ echo $all_installation->date_received; } ?>" name="date_received" data-date-format="<?= config_item('date_picker_format'); ?>" />
						   <div class="input-group-addon">
                                <a href="#"><i class="fa fa-calendar"></i></a>
                            </div>
						</div>
                    </div>
                </div>
				
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= ''; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->address_line2; } ?>" name="address_line2" placeholder="" >
                    </div>
					<label class="col-lg-3 control-label"><?= 'Date to be completed '; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
						<div class="input-group">
							<input type="text" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" name="due_date" data-date-format="<?= config_item('date_picker_format'); ?>" />
						   <div class="input-group-addon">
                                <a href="#"><i class="fa fa-calendar"></i></a>
                            </div>
						</div>
                    </div>
                </div>
				
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= ''; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->address_line3; } ?>" name="address_line3" placeholder="" >
                    </div>
					<label class="col-lg-3 control-label"><?= 'Instruction taken by :'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->taken_by; } ?>" name="taken_by" placeholder="" >
                    </div>
                </div>
				
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Contact'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->contact; } ?>" name="contact" placeholder="" >
                    </div>
					<label class="col-lg-3 control-label"><?= 'Job done by' ; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->job_done_by; } ?>" name="job_done_by" placeholder="" >
                    </div>
                </div>
				
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Tel. No'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->tel_no; } ?>" name="tel_no" placeholder="" >
                    </div>
					<label class="col-lg-3 control-label"><?= 'Time started'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->time_started; } ?>" name="time_started" placeholder="" >
                    </div>
                </div>
				
				<div class="form-group row">
                    <label class="col-lg-3 control-label"><?= 'Fax No'; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->fax_no; } ?>" name="fax_no" placeholder="" >
                    </div>
					<label class="col-lg-3 control-label"><?= 'Time completed '; ?> <span class="text-danger"></span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control" value="<?php if(!empty($all_installation)){ echo $all_installation->time_completed; } ?>" name="time_completed" placeholder="" >
                    </div>
                </div>
				
				<div class="form-group row">
				  <div class="col-lg-12">
				<h4>Instructions</h4>
				</div>
				  <div class="col-lg-12">
				<textarea name="instructions" rows="5" class="form-control"></textarea>
				</div>
				</div>
				<div class="form-group row">
					<table class="table table-bordered">
						<tr>
							<td colspan="4" align="center">Material</td>
							<td colspan="4" align="center">Labour Costs</td>
						</tr>
						<tr>
							<td>Description</td>
							<td>Quantity</td>
							<td>Unit Price</td>
							<td>Total</td>
							<td>Description</td>
							<td>Quantity</td>
							<td>Unit Price</td>
							<td>Total</td>
						</tr>
						<?php 
							if (!empty($all_installation)) {
								$query = $this->db->query("SELECT * FROM tbl_docs_installation_data WHERE installation_id = '" . $all_installation->id . "'")->result(); 
								foreach ($query as $q) {
								$m_vat = $q->m_vat;
								$l_vat = $q->l_vat;
								$m_per = $q->m_vat_percentage;
								$l_per = $q->l_vat_percentage;
								$m_grand = $q->m_grand_total;
								$l_grand = $q->l_grand_total;
						?>
						<tr>
							<td><input type="text" name="id[]" value="<?php echo $q->id ;?>" class="form-control hidden"/>
							<input type="text" name="m_desc[]" value="<?php echo $q->m_desc ;?>" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" value="<?php echo $q->m_qty ;?>" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" value="<?php echo $q->m_unitprice ;?>" class="form-control"/></td>
							<td><input type="text" name="m_total[]" value="<?php echo $q->m_total ;?>" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" value="<?php echo $q->l_desc ;?>"  class="form-control"/></td>
							<td><input type="text" name="l_qty[]" value="<?php echo $q->l_qty ;?>" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" value="<?php echo $q->l_unitprice ;?>" class="form-control"/></td>
							<td><input type="text" name="l_total[]" value="<?php echo $q->l_total ;?>" class="form-control"/></td>
						</tr>
						<?php } ?>
						<tr>
							<td></td>
							<td></td>
							<td>Vat</td>
							<td><input type="text" name="material_vat" value="<?php if (!empty($all_installation)) { echo $all_installation->material_vat; } ?>" class="form-control"/></td>
							<td></td>
							<td></td>
							<td>Vat</td>
							<td><input type="text" name="labour_vat" value="<?php if (!empty($all_installation)) { echo $all_installation->labour_vat; } ?>" class="form-control"/></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>Vat @</td>
							<td><input type="text" name="material_vat_at" value="<?php if (!empty($all_installation)) { echo $all_installation->material_vat_at; } ?>" class="form-control"/></td>
							<td></td>
							<td></td>
							<td>Vat @</td>
							<td><input type="text" name="labour_vat_at"value="<?php if (!empty($all_installation)) { echo $all_installation->labour_vat_at; } ?>"  class="form-control"/></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>Total Due</td>
							<td><input type="text" name="material_total_due" value="<?php if (!empty($all_installation)) { echo $all_installation->material_total_due; } ?>" class="form-control"/></td>
							<td></td>
							<td></td>
							<td>Total Due</td>
							<td><input type="text" name="labour_total_due" value="<?php if (!empty($all_installation)) { echo $all_installation->labour_total_due; } ?>"class="form-control"/></td>
						</tr>
						<?php	}else{  ?>
						<tr>
							<td><input type="text" name="m_desc[]" value="" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="m_total[]" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" class="form-control"/></td>
							<td><input type="text" name="l_qty[]" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="l_total[]" class="form-control"/></td>
						</tr>
						<tr>
							<td><input type="text" name="m_desc[]" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="m_total[]" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" class="form-control"/></td>
							<td><input type="text" name="l_qty[]" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="l_total[]" class="form-control"/></td>
						</tr>
						<tr>
							<td><input type="text" name="m_desc[]" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="m_total[]" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" class="form-control"/></td>
							<td><input type="text" name="l_qty[]" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="l_total[]" class="form-control"/></td>
						</tr>
						<tr>
							<td><input type="text" name="m_desc[]" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="m_total[]" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" class="form-control"/></td>
							<td><input type="text" name="l_qty[]" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="l_total[]" class="form-control"/></td>
						</tr>
						<tr>
							<td><input type="text" name="m_desc[]" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="m_total[]" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" class="form-control"/></td>
							<td><input type="text" name="l_qty[]" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="l_total[]" class="form-control"/></td>
						</tr>
						<tr>
							<td><input type="text" name="m_desc[]" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="m_total[]" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" class="form-control"/></td>
							<td><input type="text" name="l_qty[]" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="l_total[]" class="form-control"/></td>
						</tr>
						<tr>
							<td><input type="text" name="m_desc[]" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="m_total[]" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" class="form-control"/></td>
							<td><input type="text" name="l_qty[]" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="l_total[]" class="form-control"/></td>
						</tr>
						<tr>
							<td><input type="text" name="m_desc[]" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="m_total[]" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" class="form-control"/></td>
							<td><input type="text" name="l_qty[]" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="l_total[]" class="form-control"/></td>
						</tr>
						<tr>
							<td><input type="text" name="m_desc[]" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="m_total[]" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" class="form-control"/></td>
							<td><input type="text" name="l_qty[]" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="l_total[]" class="form-control"/></td>
						</tr>
						<tr>
							<td><input type="text" name="m_desc[]" class="form-control"/></td>
							<td><input type="text" name="m_qty[]" class="form-control"/></td>
							<td><input type="text" name="m_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="m_total[]" class="form-control"/></td>
							<td><input type="text" name="l_desc[]" class="form-control"/></td>
							<td><input type="text" name="l_qty[]" class="form-control"/></td>
							<td><input type="text" name="l_unitprice[]" class="form-control"/></td>
							<td><input type="text" name="l_total[]" class="form-control"/></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>Vat</td>
							<td><input type="text" name="material_vat" class="form-control"/></td>
							<td></td>
							<td></td>
							<td>Vat</td>
							<td><input type="text" name="labour_vat"class="form-control"/></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>Vat @</td>
							<td><input type="text" name="material_vat_at" class="form-control"/></td>
							<td></td>
							<td></td>
							<td>Vat @</td>
							<td><input type="text" name="labour_vat_at" class="form-control"/></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>Total Due</td>
							<td><input type="text" name="material_total_due" class="form-control"/></td>
							<td></td>
							<td></td>
							<td>Total Due</td>
							<td><input type="text" name="labour_total_due" class="form-control"/></td>
						</tr>
						<?php } ?>
					</table>
					<input type="text" name="total_job_cost" class="form-control" placeholder="Total Cost of job R" />
				</div>
			</div>
			<input type="submit" name="submit" class="btn btn-primary" value="<?php if(!empty($all_installation)){ echo 'Update'; }else{ echo 'Submit'; } ?>"/>
			</form>
		</div>
	
		<div class="tab-pane <?= $active == 3 ? 'active' : ''; ?>" id="jobcard_list">
            <div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<th>Customer Name</th>
					<th>ID / Reg No</th>
					<th>Contact Person</th>
					<th>Email</th>
					<th>Fault Diagnosis</th>
					<th>Book-in Date</th>
					<th></th>
				</thead>
				<body>
					<?php $query = $this->db->query("SELECT mj.*, c.name as client FROM tbl_docs_manualjobcard mj LEFT JOIN tbl_client c ON c.client_id = mj.client")->result(); 
					foreach ($query as $q) {
					?>					
					<tr>
						<td><?= $q->client; ?></td>
						<td><?= $q->id_reg_number; ?></td>
						<td><?= $q->primary_contact; ?></td>
						<td><?= $q->email; ?></td>
						<td><?= $q->fault_service_description; ?></td>
						<td><?= $q->book_in_date; ?></td>
						<td> <?php echo btn_edit('admin/documents/edit_form/manualjobcard/' . $q->id . '/1') ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			</div>
		</div>
		
		<div class="tab-pane <?= $active == 4 ? 'active' : ''; ?>" id="installation_list">
            <div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<th>Customer Name</th>
					<th>Contact Person</th>
					<th>Job Number</th>
					<th>Date Received</th>
					<th>Due Date</th>
					<th>Instructions</th>
					<th></th>
				</thead>
				<body>
					<?php $query = $this->db->query("SELECT di.*, c.name as customer FROM tbl_docs_installation di LEFT JOIN tbl_client c ON c.client_id = di.customer")->result(); 
					foreach ($query as $q) {
					?>					
					<tr>
						<td><?= $q->customer; ?></td>
						<td><?= $q->contact; ?></td>
						<td><?= $q->job_number; ?></td>
						<td><?= $q->date_received; ?></td>
						<td><?= $q->due_date; ?></td>
						<td><?= $q->instructions; ?></td>
						<td> <?php echo btn_edit('admin/documents/edit_form/installation/' . $q->id .'/2') ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			</div>
		</div>
		
	</div>
	
</div>

<script>
	function activate_tab(tab) {
		if (tab === 'installation') {
			$('#jobcard_li').removeClass('active');					
		} else {
			$('#installation_li').removeClass('active');					
		}
		$('#'+tab+'_li').addClass('active');
	}
</script>
