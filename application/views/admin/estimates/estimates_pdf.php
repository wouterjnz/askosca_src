<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= lang('estimate') ?></title>
    <style type="text/css">
        @font-face {
            font-family: "Source Sans Pro", sans-serif;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #0087C3;
            text-decoration: none;
        }

        body {
            color: #555555;
            background: #FFFFFF;
            font-size: 14px;
            font-family: "Source Sans Pro", sans-serif;
        }

        header {

            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        #logo {
            float: left;
        }

        #company {
            float: right;
            text-align: right;
        }

        #details {
            margin-bottom: 50px;
        }

        #client {
            padding-left: 6px;
            border-left: 6px solid #0087C3;
            float: left;
        }

        #client .to {
            color: #777777;
        }

        h2.name {
            font-size: 1em;
            font-weight: normal;
            margin: 0;
        }

        #invoice {
            float: right;
            text-align: right;
        }

        #invoice h1 {
            color: #0087C3;
            font-size: 1.5em;
            line-height: 1em;
            font-weight: normal;
        }

        #invoice .date {
            font-size: 1.1em;
            color: #777777;
        }

        table {
            width: 100%;
            border-spacing: 0;
        }

        table.items {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 10px;
        }
        table.items th,
        table.items td {
            padding: 8px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
        }

        table.items th {
            white-space: nowrap;
            font-weight: normal;
        }

        table.items td {
            text-align: right;
        }

        table.items td h3 {
            /* color: #57B223;*/
            color: #555555;
            font-size: 1em;
            font-weight: normal;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        table.items .no {
            background: #DDDDDD;
        }

        table.items .desc {
            text-align: left;
        }

        table.items .unit {
            background: #DDDDDD;
        }

        table.items .qty {
        }

        table.items .total {
            background: #DDDDDD;
        }

        table.items td.unit,
        table.items td.qty,
        table.items td.total {
            font-size: 1.2em;
        }

        table.items tbody tr:last-child td {
            border: none;
        }

        table.items tfoot td {
            padding: 10px 20px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        table.items tfoot tr:first-child td {
            border-top: none;
        }

        table.items tfoot tr:last-child td {
            /* color: #57B223; */
            color: #555555;
            font-size: 1.4em;
            /* border-top: 1px solid #57B223; */
            border-top: 1px solid #555555; 

        }

        table.items tfoot tr td:first-child {
            border: none;
        }

        #thanks {
            font-size: 1.5em;
            margin-bottom: 20px;
        }

        #notices {
            padding-left: 6px;
            border-left: 6px solid #0087C3;
        }

        #notices .notice {
            font-size: 1em;
            color: #777;
        }

        footer {
             color: #777777;
            width: 100%;
            height: 40px;
            position: absolute;
            bottom: 15px;
            border-top: 1px solid #AAAAAA;
            padding: 4px 0;
            text-align: center;
        }

    </style>
</head>
<body>

<?php
$client_info = $this->estimates_model->check_by(array('client_id' => $estimates_info->client_id), 'tbl_client');

$client_lang = $client_info->language;
unset($this->lang->is_loaded[5]);
$language_info = $this->lang->load('sales_lang', $client_lang, TRUE, FALSE, '', TRUE);
$currency = $this->estimates_model->client_currency_sambol($estimates_info->client_id);
if ($client_info->client_status == 1) {
    $status = lang('person');
} else {
    $status = lang('company');
}
?>
<table class="clearfix">
    <tr>
        <td width="500px">
            <div id="logo" style="margin-top: 8px;">
                <img style=" height: 60px;" src="<?= base_url() . '/uploads/logo-inline-black.png';  //base_url() . config_item('invoice_logo') ?>">
				<?php for($i=0;$i< 15; $i++){ 
				echo '&nbsp;';
				} ?>
               <!-- <img style=" height: 50px;" src="<?= base_url() . '/uploads/logo-lg.png';  //base_url() . config_item('invoice_logo') ?>"> -->
            </div>
        </td>
        <td>
            <div id="company">
            <strong style=" color: #0087C3;">Banking Details</strong> <br/>
		        Name: : JNZ Group (Pty) Ltd<br/>
                Bank: FNB<br/>
                Branch: 201210<br/>
                Account No: 62463713274<br/>
                Type: Cheque Account
            </div>
           
        </td>
    </tr>
</table>


<table id="details" class="clearfix">
    <tr>
        <td>
            <div id="client">
                <h2 class="name"><?= $client_info->name . ' ' . $status . ' ' ?></h2>
                <div class="address"><?= $client_info->address ?></div>
                <div class="address">
                    <?php if(!empty($client_info->city)){ ?>
                        <?= $client_info->city ?>, 
                    <?php } ?>
                    <?php if(!empty($client_info->zipcode)){ ?>
                        <?= $client_info->zipcode ?>,
                    <?php } ?>
                    <?php if(!empty($client_info->country)){ ?>
                        <?= $client_info->country ?>
                    <?php } ?>
                </div>
                <div class="address"><?= $client_info->phone ?></div>
                 <div class="address">Vat No: <?= $client_info->vat ?></div>
                <div class="email"><a href="mailto:<?= $client_info->email ?>"><?= $client_info->email ?></a></div>
            </div>
        </td>
        <td>
            <div id="invoice">
                <h1><?= $estimates_info->reference_no ?></h1>
                <div class="date"><?= $language_info['estimate_date'] ?>
                    :<?= strftime(config_item('date_format'), strtotime($estimates_info->date_saved)); ?></div>
                <div class="date"><?= $language_info['valid_until'] ?>
                    :<?= strftime(config_item('date_format'), strtotime($estimates_info->due_date)); ?></div>
                <div class="date"><?= $language_info['estimate_status'] ?>: <?= $estimates_info->status ?></div>
                  <?php if(!empty($client_info->sales_person)){ ?>
                    <div class="date">Sales Person: <?= $client_info->sales_person; ?></div>
                <?php } ?>
            </div>
        </td>
    </tr>
</table>

<table class="items" border="0" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th class="unit"><?= $language_info['qty'] ?></th>
        <th class="desc"><?= $language_info['item_name'] ?></th>
        <th class="unit"><?= $language_info['unit_price'] ?></th>
        <th class="qty"><?= $language_info['tax_rate'] ?></th>
        <th class="unit"><?= $language_info['tax'] ?></th>
        <th class="qty"><?= $language_info['total'] ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $estimates_items = $this->estimates_model->ordered_items_by_id($estimates_info->estimates_id);

    if (!empty($estimates_items)) :
        foreach ($estimates_items as $key => $v_item) :
            $item_name = $v_item->item_name;
            ?>
            <tr>
                <td class="unit"><?= $v_item->quantity ?></td>
                <td class="desc"><h3><?= $item_name ?></h3><?= nl2br($v_item->item_desc) ?></td>
                <td class="unit"><?= display_money($v_item->unit_cost, $currency->symbol) ?></td>
                <td class="qty"><?= $v_item->item_tax_rate ?>%</td>
                <td class="unit"><?= display_money($v_item->item_tax_total, $currency->symbol) ?></td>
                <td class="qty"><?= display_money($v_item->total_cost, $currency->symbol) ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endif ?>

    </tbody>
    <tfoot>
    <tr>
        <td colspan="3"></td>
        <td colspan="2"><?= $language_info['sub_total'] ?></td>
        <td><?= display_money($this->estimates_model->estimate_calculation('estimate_cost', $estimates_info->estimates_id), $currency->symbol) ?></td>
    </tr>
    <?php if ($estimates_info->tax > 0.00): ?>
        <tr>
            <td colspan="3"></td>
            <td colspan="2"><?= $language_info['tax'] ?> (<?php echo $estimates_info->tax; ?>%)</td>
            <td><?= display_money($this->estimates_model->estimate_calculation('tax', $estimates_info->estimates_id), $currency->symbol) ?></td>
        </tr>
    <?php endif ?>

    <?php if ($estimates_info->discount > 0): ?>
        <tr>
            <td colspan="3"></td>
            <td colspan="2"><?= $language_info['discount'] ?>(<?php echo $estimates_info->discount; ?>%)</td>
            <td> <?= display_money($this->estimates_model->estimate_calculation('discount', $estimates_info->estimates_id), $currency->symbol) ?></td>
        </tr>
    <?php endif ?>
    <tr>
        <td colspan="3"></td>
        <td colspan="2"><?= $language_info['total'] ?></td>
        <td><?= display_money($this->estimates_model->estimate_calculation('estimate_amount', $estimates_info->estimates_id), $currency->symbol) ?></td>
    </tr>
    </tfoot>
</table>
<div id="thanks"><?= lang('thanks') ?>!</div>
<div id="notices">
    <div class="notice"><?= $estimates_info->notes ?></div>
</div>
<footer>
    <div style="font-size: 11px">* Please note that this quote is only valid for 48 hours and is subject to the exchange rate.  E&OE (Errors & Omissions are excluded).</div>
<table class="clearfix">
   <tr>
		<td width="200px">
		<strong>Head Office<!-- <?= (config_item('company_legal_name_' . $client_lang) ? config_item('company_legal_name_' . $client_lang) : config_item('company_legal_name')) ?>--></strong>

                <div><?= (config_item('company_address_' . $client_lang) ? config_item('company_address_' . $client_lang) : config_item('company_address')) ?></div>

                <div><?= (config_item('company_city_' . $client_lang) ? config_item('company_city_' . $client_lang) : config_item('company_city')) ?></div>
                <div><?= (config_item('company_country_' . $client_lang) ? config_item('company_country_' . $client_lang) : config_item('company_country')) ?></div>
                <div> <?= config_item('company_phone') ?></div>
                <!--<div><a href="mailto:<?= config_item('company_email') ?>"><?= config_item('company_email') ?></a></div>-->
		

		
		</td>
		<td width="200px">
	
		<strong>Paarl</strong><br/>
           311 Main Road<br/>
           Paarl<br/>
           087 700 0321
		   <br/>
		   &nbsp;
        </td>
        <td width="200px">
            <strong>Franschhoek</strong><br/>
		   Franschhoek Centre<br/>
		   Shop 7<br/>
		   4 Main Road<br/>
		   087 700 1100
        </td>
        <td width="225px">
	
		<strong>Stellenbosch</strong><br/>
           Innovation centre 1<br/>
           Meson Close<br/>
          Technopark <br/>
		   &nbsp;
        </td>
    </tr>
</table>
</footer>
</body>
</html>
