<?= message_box('success'); ?>
<?= message_box('error'); ?>
<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage"
                                                            data-toggle="tab"><?= 'All Warehouses'; ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#create"
                                                            data-toggle="tab"><?= 'New Warehouse';?></a></li>
    </ul>
    <div class="tab-content bg-white">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">

            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= 'Name'; ?></th>
                        <th><?= 'Address'; ?></th>
                        <th class="col-options no-sort"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $all_items = $this->db->get('tbl_warehouse')->result();
                    foreach ($all_items as $v_items):
                        ?>
                        <tr>
							
                            <td><?= $v_items->warehouse; ?></td>
                            <td><?= $v_items->address; ?></td>
                            <td>
                                <?= btn_edit('admin/warehouse/index/' . $v_items->id) ?>
                                <?= btn_delete('admin/warehouse/delete_warehouse/' . $v_items->id) ?>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
            <form role="form" data-parsley-validate="" novalidate="" enctype="multipart/form-data" id="form"
                  action="<?php echo base_url(); ?>admin/warehouse/saved_warehouse/<?php
                  if (!empty($warehouse_info)) {
                      echo $warehouse_info->id;
                  }
                  ?>" method="post" class="form-horizontal  ">
				  
				  
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= 'Warehouse'; ?> <span
                            class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" data-parsley-type="text" class="form-control" value="<?php
                        if (!empty($warehouse_info)) {
                            echo $warehouse_info->warehouse;
                        }
                        ?>" name="warehouse" required="">
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= 'Address'; ?> <span
                            class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" data-parsley-type="text" class="form-control" value="<?php
                        if (!empty($warehouse_info)) {
                            echo $warehouse_info->address;
                        }
                        ?>" name="address" required="">
                    </div>

                </div>
               
                <div class="form-group">
                    <label class="col-lg-3 control-label"></label>
                    <div class="col-lg-5">
                        <button type="submit" class="btn btn-sm btn-primary"><?= lang('update') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>