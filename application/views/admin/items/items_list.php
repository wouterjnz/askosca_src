<?= message_box('success'); ?>
<?= message_box('error'); ?>
<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage"
                                                            data-toggle="tab"><?= lang('all_items') ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#create"
                                                            data-toggle="tab"><?= lang('new_items') ?></a></li>
    </ul>
    <div class="tab-content bg-white">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
        	<?php
						$warehouse = $this->db->get('tbl_warehouse')->result();
						//print_r($warehouse); 
		    ?>
			<select class="form-control pull-right" placeholder="Warehouse" onchange="warehouse(this.value);return false;" style="background-color: #E9EF1D; color:##7FB3D5; width: 250px">
				<option value="">Select Warehouse</option>
				<option value="">All Warehouses</option>
					<?php
					if(!empty($warehouse)){
						foreach($warehouse as $whouse){
					?>
					<option value="<?= $whouse->id;?>" <?php if(isset($_GET['id']) && $_GET['id'] == $whouse->id){ echo 'selected'; } ?> ><?= $whouse->warehouse;?></option>
					<?php } } ?>
			</select>
            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= 'Barcode' ?></th>
                        <th><?= lang('qty') ?></th>
                        <th><?= lang('item_name') ?></th>
                      
                         <th><?= 'Warehouse'; ?></th>
                        <th><?= lang('unit_price') ?></th>
                        <th class="col-options no-sort"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                   $this->db->select('tbl_saved_items.*, tbl_warehouse.warehouse');
                    if (isset($_GET['id'])) {
                        if (!empty($_GET['id'])) {
					    	$this->db->where('tbl_saved_items.warehouse',$_GET['id']);
                        }
					}
					 
					$this->db->join('tbl_warehouse','tbl_warehouse.id = tbl_saved_items.warehouse','left');
                    $all_items = $this->db->get('tbl_saved_items')->result();

                    $currency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row();
                    $total_balance = 0;
                    foreach ($all_items as $v_items):
                        ?>
                        <tr>
							<td><?php echo '<img src="data:image/jpg;base64,'. $v_items->barcode .'">';?></td>
                            <td><?= $v_items->quantity ?></td>
                            <td><?= $v_items->item_name ?></td>
                
                            <td><?= $v_items->warehouse; ?></td>
                            <td><?=
                                display_money($v_items->unit_cost, $currency->symbol);
                                ?>
							</td>
                            <td>
                                <?= btn_edit('admin/items/items_list/' . $v_items->saved_items_id) ?>
                                <?= btn_delete('admin/items/delete_items/' . $v_items->saved_items_id) ?>
								<a href="<?php echo base_url('admin/items/print_barcode/'.$v_items->saved_items_id);?>" class="btn btn-warning btn-xs" target="_blank"><i class="fa fa-print"></i></a>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
            <form role="form" data-parsley-validate="" novalidate="" enctype="multipart/form-data" id="form"
                  action="<?php echo base_url(); ?>admin/items/saved_items/<?php
                  if (!empty($items_info)) {
                      echo $items_info->saved_items_id;
                  }
                  ?>" method="post" class="form-horizontal  ">
				  
				  
				  
				  
				<div class="form-group">
                    <label class="col-lg-3 control-label"><?= 'Warehouse'; ?> <span
                            class="text-danger"></span></label>
                    <div class="col-lg-5">
                        <select name="warehouse" class="form-control" required="">
						<option value="">--</option>
						   <?php
                    $warehouses = $this->db->get('tbl_warehouse')->result();
					// print_r($warehouses);
					// exit;
                    if($warehouses):
					// echo "<option>jamie</option>";
					
					foreach ($warehouses as $whouse):
                        ?>
						<option value="<?= $whouse->id; ?>" <?php      if (!empty($items_info)){ if($whouse->id == $items_info->warehouse){ echo 'selected'; }}	?>>
							<?= $whouse->warehouse; ?>
						</option>
						<?php 
						endforeach;
					
						endif;
						?>
						</select>
                    </div>
                </div>
				
				<div class="form-group">
                    <label class="col-lg-3 control-label"><?= 'Supplier'; ?> <span
                            class="text-danger"></span></label>
                    <div class="col-lg-5">
                        <select name="supplier_id" class="form-control" required="">
						<option value="">--</option>
						   <?php
                    $suppliers = $this->db->get('tbl_supplier')->result();
					
                    if($suppliers):
					foreach ($suppliers as $supplier):
                        ?>
						<option value="<?= $supplier->supplier_id; ?>" <?php      if (!empty($items_info)){ if($supplier->supplier_id == $items_info->supplier_id){ echo 'selected'; }}	?>>
							<?= $supplier->name; ?>
						</option>
						<?php 
						endforeach;
					
						endif;
						?>
						</select>
                    </div>
                </div>
				
				<div class="form-group">
                    <label class="col-lg-3 control-label"><?= 'Supplier Invoice #'; ?> <span
                            class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" class="form-control" value="<?php
                        if (!empty($items_info)) {
                            echo $items_info->supplier_invoice_no;
                        }
                        ?>" name="supplier_invoice_no">
                    </div>

                </div>
				
				 <div class="form-group ">
                        <label class="control-label col-sm-3"><?= lang('stock_category')?><span class="required">*</span></label>
                        <div class="col-sm-5">

                            <select name="stock_sub_category_id" style="width: 100%" class="form-control select_box">
                                <option value=""><?= lang('select').' '.lang('stock_category')?></option>
                                <?php 
								
							
								if (!empty($all_category_info)): foreach ($all_category_info as $cate_name => $v_category_info) : ?>
                                    <?php if (!empty($v_category_info)):
                                        if(!empty($cate_name)){
                                            $cate_name=$cate_name;
                                        }else{
                                            $cate_name=lang('undefined_category');
                                        }
                                        ?>
                                        <optgroup label="<?php echo $cate_name ; ?>">
                                            <?php foreach ($v_category_info as $sub_category) :
                                                if (!empty($sub_category->stock_sub_category)) {
                                                    ?>
                                                    <option value="<?php echo $sub_category->stock_sub_category_id; ?>"
                                                        <?php
                                                        if (!empty($items_info->stock_sub_category_id)) {
                                                            echo $sub_category->stock_sub_category_id == $items_info->stock_sub_category_id ? 'selected' : '';
                                                        }
                                                        ?>><?php echo $sub_category->stock_sub_category ?></option>
                                                    <?php
                                                }
                                            endforeach;
                                            ?>
                                        </optgroup>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
					
					
					
					    <div class="form-group">
                        <label for="field-1" class="control-label col-sm-3 "><?= lang('buying_date')?><span class="">*</span></label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input  type="text" class="form-control   datepicker" name="purchase_date" value="<?php
                                if (!empty($items_info->purchase_date)) {
                                    echo $items_info->purchase_date;
                                }
                                ?>" data-format="yyyy/mm/dd">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
				
				<hr/>
				<div class="form-group">
                    <label class="col-lg-3 control-label"><?= 'Barcode'; ?> <span
                            class="text-danger"></span></label>
                    <div class="col-lg-5">
                        <input type="text" class="form-control" value="<?php
                        if (!empty($items_info)) {
                            echo $items_info->barcode_string;
                        }
                        ?>" name="barcode" placeholder="leave empty to autogenerate" >
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= lang('item_name') ?> <span
                            class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" class="form-control" value="<?php
                        if (!empty($items_info)) {
                            echo $items_info->item_name;
                        }
                        ?>" name="item_name" required="">
                    </div>

                </div>
                <!-- End discount Fields -->
                <div class="form-group terms">
                    <label class="col-lg-3 control-label"><?= lang('description') ?> </label>
                    <div class="col-lg-5">
                        <textarea name="item_desc" class="form-control"><?php
                            if (!empty($items_info)) {
                                echo $items_info->item_desc;
                            }
                            ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= lang('unit_price') ?> <span
                            class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="text" data-parsley-type="number" class="form-control" value="<?php
                        if (!empty($items_info)) {
                            echo $items_info->unit_cost;
                        }
                        ?>" name="unit_cost" required="">
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= lang('quantity') ?> <span
                            class="text-danger">*</span></label>
                    <div class="col-lg-5">
                        <input type="hidden" name="old_quantity" value="<?php
                        if (!empty($items_info)) {
                            echo $items_info->quantity;
                        }
                        ?>" />
                        <input type="text" data-parsley-type="number" class="form-control" value="<?php
                        if (!empty($items_info)) {
                            echo $items_info->quantity;
                        }
                        ?>" name="quantity" required="">
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><?= lang('tax_rate') ?> </label>
                    <div class="col-lg-5">
                        <select name="item_tax_rate" class="form-control">
                            <option value="0.00"><?= lang('none') ?></option>
                            <?php
                            $tax_rates = $this->db->get('tbl_tax_rates')->result();
                            if (!empty($tax_rates)) {
                                foreach ($tax_rates as $v_tax) {
                                    ?>
                                    <option value="<?= $v_tax->tax_rate_percent ?>" <?php
                                    if (!empty($item_info) && $item_info->item_tax_rate == $v_tax->tax_rate_percent) {
                                        echo 'selected';
                                    }
                                    ?>><?= $v_tax->tax_rate_name ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"></label>
                    <div class="col-lg-5">
                          <input type="submit" class="btn btn-sm btn-primary" name="action" value="Update" />
                        <input type="submit" class="btn btn-sm btn-primary" name="action" value="Transfer"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
function warehouse(id){
	location.href = "<?php echo base_url('admin/items/items_list?id=');?>"+id;
}
</script>