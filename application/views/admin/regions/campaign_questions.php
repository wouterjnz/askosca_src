<?= message_box('success'); ?>
<?= message_box('error'); ?>
<div class="panel panel-custom">
    <header class="panel-heading">
        <div class="panel-title"><strong><?= lang('import') . ' ' . lang('client') ?></strong>
            <div class="pull-right hidden-print">
                <div class="pull-right "><a href="<?php echo base_url() ?>assets/sample/client_sample.xlsx"
                                            class="btn btn-primary"><i
                            class="fa fa-download"> <?= lang('download_sample') ?></i></a>
                </div>
            </div>

        </div>
    </header>
    <div class="panel-body">
         <form action="<?php echo base_url('admin/campaigns/generate_form'); ?>" method="post">
            <div class="panel-body">
                <!--
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">
                        <?= lang('choose_file') ?><span class="required"></span></label>
                    <div class="col-sm-5">
                        <div style="display: inherit;margin-bottom: inherit" class="fileinput fileinput-new"
                             data-provides="fileinput">
                    <span class="btn btn-default btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span>
                        <input type="file" name="upload_file"></span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                               style="float: none;">&times;</a>
                        </div>
                    </div>
                </div>
                -->
               <div class="form-group">
                 <div class="col-sm-5">
                       Question
                    </div>
                    <div class="col-sm-2">
                      Data Type
                    </div>
                    <div class="col-sm-3">
                      Options
                    </div>
                    <div class="col-sm-2">
                       Required
                    </div>
                </div>

                <?php for($i = 1; $i <= 10; $i++){ ?>
                <div class="form-group row">
                    <div class="col-sm-5">
                        <textarea name="question_<?php echo $i;?>" class="form-control"></textarea>
                    </div>
                    <div class="col-sm-2">
                        <select name="type_<?php echo $i;?>" class="form-control">
                            <option>Short Text</option>
                            <option>Long Text</option>
                            <option>Radio</option>
                            <option>checkbox</option>
                            <option>select box</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input name="options_<?php echo $i;?>" class="form-control" />
                    </div>
                    <div class="col-sm-2">
                        <select name="required_<?php echo $i;?>" class="form-control">
                            <option>yes</option>
                            <option>no</option>
                        </select>
                    </div>
                </div>
                <?php } ?>
               <input type="hidden" name="campaign_id" value="<?php echo $campaign_id; ?>" />



                </div>


                <div class="form-group">
                    <label class="col-lg-3 control-label"></label>
                    <div class="col-lg-6">
                        <button type="submit" value="submit" name="submit" class="btn btn-sm btn-primary"></i> <?= 'Submit'; ?></button>
                    </div>
                </div>
            </div>
             </form>
    </div>
</div>
