<?= message_box('success'); ?>
<?= message_box('error'); ?>
<div class="panel panel-custom">
    <header class="panel-heading">
        <div class="panel-title">
			<strong><?= 'Form' ?></strong>    
        </div>
    </header>
    <div class="panel-body">
         <form action="<?php echo base_url('admin/campaigns/save_questions/'.$id); ?>" method="post">
            <div class="panel-body">
                <!--
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">
                        <?= lang('choose_file') ?><span class="required"></span></label>
                    <div class="col-sm-5">
                        <div style="display: inherit;margin-bottom: inherit" class="fileinput fileinput-new"
                             data-provides="fileinput">
                    <span class="btn btn-default btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span>
                        <input type="file" name="upload_file"></span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                               style="float: none;">&times;</a>
                        </div>
                    </div>
                </div>
                -->

		   <?php 
		   $i = 1;
			$div = '<table class="table table-bordered">';
		   foreach ($questions as $dat) {
			// $div = '';
			   $input = '';
			$required = '';
			$op = '<option value="">--select--</option>';
			if (!empty($dat['question_question'])) {
				$div .= '<tr><td width="400px"><strong>'.$dat['question_number'].')  '.$dat['question_question'].': </strong></td>';
				// $div .=
					// "<div class=\"row form-group\">
						// <div class=\"col-sm-4\">
							// ".$dat['question_number'].'. '.$dat['question_question']."
						// </div>
					// <div class=\"col-sm-8\">";
				
				if(!empty($dat['question_options'])){	
					$options = explode(",",$dat['question_options']);
					if($dat['question_type'] == 'select box'){
						foreach ($options as $option) {
							$op .= '<option value="'.$option.'">'.$option.'</option>';
						}
					}
				}		
				if($dat['question_required'] == 'yes'){
					$required = ' required';
				}
				if(strtolower($dat['question_type']) == 'short text'){
					$input = '<input class="form-control" name="{names}" value="{textinput}" {required} ></input>';
				}
				if(strtolower($dat['question_type']) == 'long text'){
					$input = '<textarea class="form-control" name="{names}" {required} >{textinput}</textarea>';
				}
				if(strtolower($dat['question_type']) == 'select box'){
						$input = '<select class="form-control" name="{names}" {required} >{options}</select>';
				}
				if(strtolower($dat['question_type']) == 'checkbox'){
						$check_options = explode(",",$dat['question_options']);
						foreach($check_options as $co){
							$input .= '<label>'.$co.'</label>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="" name="{names}[]" {required} value="'.$co.'" /><br/>';
						}
				}
				if(strtolower($dat['question_type']) == 'radio'){
						$radio_options = explode(",",$dat['question_options']);
						foreach($radio_options as $ro){
							$input .= '<label>'.$ro.'</label>&nbsp;&nbsp;&nbsp;<input type="radio" class="" name="{names}" {required} value="'.$ro.'" /><br/>';
						}
				}
				// $input .= '<input type="hidden" name="question_id" value="'.$dat['id'].'" />';
				$form = str_replace('{textinput}',"",$input);
				$form2 = str_replace('{required}',$required,$form);
				$form3 = str_replace('{options}',$op,$form2);
				$form4 = str_replace('{names}','question_'.$i.'_'.$dat['id'],$form3);
					
				$div .= '<td>'.$form4.'</td>
				</tr>';
				// $div .= $form4 . "</div>
				// </div>";
				// echo $div;
			   }
			$i++;
			
		}
          $div .= '</table>';
		  echo $div;
?>
               
               
               
<!--
                <div class="form-group">
                    <label class="col-lg-3 control-label"></label>
                    <div class="col-lg-6">
                        <button type="submit" value="submit" name="submit" class="btn btn-sm btn-primary"></i> <?= 'Submit'; ?></button>
                    </div>
                </div>
				-->
				<table class="table table-bordered">
				<tr rowspan="2">
					<td colspan="2"><input type="submit" class="btn btn-primary pull-right" /></td>
				</tr>
			</table>
			 </div>
            </div>
             </form>
    </div>
</div>
