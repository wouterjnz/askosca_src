<?= message_box('success'); ?>
<?= message_box('error'); ?>


<div class="nav-tabs-custom">
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs">
        <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage"
                                                            data-toggle="tab"><?= 'All Regions' ?></a></li>
        <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#create"
                                                            data-toggle="tab"><?= 'New Regions' ?></a></li>
        
    </ul>
    <div class="tab-content bg-white">
        <!-- ************** general *************-->
        <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">

            <div class="table-responsive">
                <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= 'Region' ?></th>
                        <th class="col-options no-sort"><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$table_id = 'id'; 
					$table = 'tbl_regions'; 
                    if (!empty($all_regions)):foreach ($all_regions as $v_region):
                            ?>
                            <tr>
                                <td>
                                     <a href="<?= base_url() ?>admin/regions/index/<?= $v_region->$table_id ?>"><?= $v_region->region ?></a> 
								</td>
                                   <td>
                                        <?= btn_edit('admin/regions/index/' . $v_region->$table_id) ?>
                                        <?= btn_delete('admin/regions/delete_region/' . $v_region->$table_id) ?>
                                </td>
                            </tr>
                            <?php
                        // }
                    endforeach;
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
            <form role="form" enctype="multipart/form-data" id="form"
                  action="<?php echo base_url(); ?>admin/regions/saved_region/<?php
                  if (!empty($region_info)) {
                      echo $region_info->$table_id;
                  }
                  ?>" method="post" class="form-horizontal  ">

                <div class="panel-body">
				
					<div class="form-group">
                        <label class="col-lg-2 control-label"><?= 'Region' ?> <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php
                            if (!empty($region_info)) {
                                echo $region_info->region;
                            }
                            ?>" name="region" maxlength="40"required="">
                        </div>
                    </div>
					
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-sm btn-primary"><?= 'Submit' ?></button>
                            </div>
                    </div>
            </form>
        </div>
    </div>
</div>