<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
 <link id="autoloaded-stylesheet" rel="stylesheet"
          href="http://libertech.jnzsoftware.co.za/assets/css/bg-info-dark.css">
<script>
    function startTime() {
        var time = new Date();
        var date = time.getDate();
        var month = time.getMonth() + 1;
        var years = time.getFullYear();
        var hr = time.getHours();
        var hour = time.getHours();
        var min = time.getMinutes();
        var minn = time.getMinutes();
        var sec = time.getSeconds();
        var secc = time.getSeconds();
        if (date <= 9) {
            var dates = "0" + date;
        } else {
            dates = date;
        }
        if (month <= 9) {
            var months = "0" + month;
        } else {
            months = month;
        }
        var ampm = " PM "
        if (hr < 12) {
            ampm = " AM "
        }
        if (hr > 12) {
            hr -= 12
        }
        if (hr < 10) {
            hr = " " + hr
        }
        if (min < 10) {
            min = "0" + min
        }
        if (sec < 10) {
            sec = "0" + sec
        }
        document.getElementById('date').value = years + "-" + months + "-" + dates;
        document.getElementById('clock_time').value = hour + ":" + minn + ":" + secc;
        document.getElementById('txt').innerHTML = hr + ":" + min + ":" + sec + ampm;
        var t = setTimeout(function () {
            startTime()
        }, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }
        ;  // add zero in front of numbers < 10
        return i;
    }
</script>

<?php 
$my_lead_id = 0;
if(isset($_GET['lead_id'])){
    $my_lead_id = $_GET['lead_id'];
} ?>
<div class="panel panel-custom">
    <div class="panel-heading">
        <div class="panel-title"><?= $form_title?: 'Form'; ?> <a href="<?php echo base_url('vicidial/webform?lead_id='.$my_lead_id);?>" class="btn btn-primary btn-xs pull-right" target="_blank">Add Referral</a> </div>
    </div>
    <div class="panel-body">

            <center>
			<form action="<?php  echo base_url('vicidial/save_debtco_webform'); ?>" method='post' id='vicidial_form'>
			
			<table class="table table-bordered">
				<tr>
						<td width="400px"><strong>Gross Salary per month: </strong></td>
						<td><input type="text" name="gross_salary" class="form-control" value="<?php // echo $title ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td width="400px"><strong>Net Salary per month:</strong></td>
						<td><input type="text" name="net_salary" class="form-control" value="<?php // echo $surname ?: ''; ?>" /></td>
				</tr>
				
				<tr>
						<td width="400px"><strong>First Paymet is DC fee:</strong></td>
						<td><select name="first_payment" class="form-control">
					<option value="">--</option>
					<option value="Ja">Ja / Yes</option>
					<option value="Nee">Nee / No</option>
				</select></td>
				</tr>
				<tr>
						<td width="400px"><strong>Second payment is legal fee:</strong></td>
						<td><select name="second_payment" class="form-control">
					<option value="">--</option>
					<option value="Ja">Ja / Yes</option>
					<option value="Nee">Nee / No</option>
				</select></td>
				</tr>
				<tr>
						<td width="400px"><strong>Third Month (Creditors received payment):</strong></td>
						<td>
							<select name="third_month" class="form-control">
							<option value="">--</option>
							<option value="Ja">Ja / Yes</option>
							<option value="Nee">Nee / No</option>
							</select>
						</td>
				</tr>
				<tr>
						<td width="400px"><strong>Clients consents to not being able to use old accounts<br/> or open new accounts once under debt review:</strong></td>
						<td><select name="debtreview_consent" class="form-control">
					<option value="">--</option>
					<option value="Ja">Ja / Yes</option>
					<option value="Nee">Nee / No</option>
				</select></td>
				</tr>
				<tr>
						<td width="400px"><strong>No estimation of the period under debt review can be given<br/> - once court order is granted we will have an exact period.</strong></td>
						<td><select name="estimation_period" class="form-control">
					<option value="">--</option>
					<option value="Ja">Ja / Yes</option>
					<option value="Nee">Nee / No</option>
				</select></td>
				</tr>
			
			</table>
			
			<br/><br/>
			
			<table class="table table-bordered">
				<tr>
						<td colspan="2" align="center"><strong>WERK BESONDERHERE / EMPLOYMENT DETAILS </strong></td>
				</tr>
				<tr>
						<td ><strong>Place of Work:</strong></td>
						<td><input type="text" name="work_place" class="form-control" value="<?php // // echo $postal_area ?: ''; ?>" /></td>
				</tr>
					<tr>
						<td><strong>Work Address:</strong></td>
						<td><input type="text" name="work_address" class="form-control" value="<?php // // echo $postal_area ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Contact details:</strong></td>
						<td><input type="text" name="work_number" class="form-control" value="<?php // echo $surname ?: ''; ?>" /></td>
				</tr>
			</table>
			
			<br/><br/>
			
			<table class="table table-bordered">
				<tr>
						<td colspan="4" align="center"><strong>KLIENT BESONDERHERE / CLIENT DETAILS </strong></td>
				</tr>
				<tr>
						<td><strong>Titel / Title: </strong></td>
						<td colspan="3"><input type="text" name="title" class="form-control" value="<?php echo (isset($_GET['title']))? $_GET['title']: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Volle name / <br/> Full Names:</strong></td>
						<td colspan="3"><input type="text" name="full_name" class="form-control" required value="<?php echo (isset($_GET['first_name']))? $_GET['first_name']: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Van / Surname:</strong></td>
						<td colspan="3"><input type="text" name="surname" class="form-control" required value="<?php echo (isset($_GET['last_name']))? $_GET['last_name']: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>ID-nommer / <br/> ID Number:</strong></td>
						<td colspan="3"><input type="text" name="id_number" class="form-control" value="" /></td>
				</tr>
				<tr>
						<td><strong>E-posadres / email:</strong></td>
						<td colspan="3"> <input type="email" name="email" class="form-control" required value="<?php echo (isset($_GET['email']))? $_GET['email']: ''; ?>" /></td>
				</tr>
				<tr>
					<td><strong>Client Address:</strong></td>
					<td colspan="3"><input type="text" name="client_address" class="form-control" value="<?php echo (isset($_GET['address1']))? $_GET['address1']: ''; ?>" /></td>
				</tr>
				<tr>
					<td><strong>Next of Kin Name:</strong></td>
					<td><input type="text" name="next_of_kin_name" class="form-control" value="<?php // // echo $postal_area ?: ''; ?>" /></td>
					<td><strong>Next of Kin Number:</strong></td>
					<td><input type="text" name="next_of_kin_number" class="form-control" value="<?php // // echo $postal_area ?: ''; ?>" /></td>
				</tr>
				<tr>
					<td><strong>Relationship Status:</strong></td>
					<td colspan="3">
						<select name="relationship_status" class="form-control">
							<option value="">--</option>
							<option value="Single">Single</option>
							<option value="Widowed">Widowed</option>
							<option value="Married">Married</option>
							<option value="Separated ">Separated </option>
							<option value="Divorced ">Divorced </option>
							<option value="Married In Community of Property">Married In Community of Property</option>
							<option value="Married Out of Community of Property">Out of Community of Property</option>
						</select>
					</td>
				</tr>
			</table>
			
			<br/><br/>
			
			<table class="table table-bordered">
			<tr>
				<td>
				<div class="form-group row">
				<div class="col-md-4"><strong>Oproep opname verwysing / Call recording reference:</strong></div> <div class="col-md-8"><input type="text" name="call_recording_reference" class="form-control"  value="<?php // echo $call_recording_reference ?: ''; ?>" /></div>
				</div>
				</div>
		</td>
			</tr>
			</table>
		
			<br/><br/>
		
			<table class="table table-bordered">
				<tr>
						<td colspan="2" align="center"><strong>BANK BESONDERHERE / BANK DETAILS </strong></td>
				</tr>
				<!-- <tr><td colspan="2"></td><td></td></tr>-->
				<tr>
						<td><strong>Rekeninghouer / Account holder:</strong> </td>
						<td><input type="text" name="account_holder" class="form-control" value="<?php // echo $account_holder ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Banknaam / Bank name:</strong></td>
						<td><input type="text" name="bank_name" class="form-control" value="<?php // echo $bank_name ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Takkode / Branch code:</strong></td>
						<td><input type="text" name="branch_code" class="form-control" value="<?php // echo $branch_code ?: ''; ?>" /></td>
				</tr>
				<tr>
						<td><strong>Tipe rekening / Type of account:</strong></td>
						<td>
						<select name="account_type" class="form-control">
								<option value="">--</option>
								<option value="Cheque">Cheque</option>
								<option value="Savings">Savings</option>
							</select>
						</td>
				</tr>
				<tr>
						<td><strong>Rekeningnommer / Account Number:</strong></td>
						<td><input type="text" name="account_number" class="form-control" value="<?php // echo $account_number ?: ''; ?>" /></td>
				</tr>
				<tr>
				    	<td><strong>Dag waarop debietorder ingevorder moet word /  <br/> Day on which debit order should be processed</strong></td>
				<td><select name="debit_day" class="form-control">
							<option value="">--</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							</select></td>
				</tr>
				<tr>
					<td><strong>Datum waarop salaris betaal word /  <br/> Date when the salary is paid into the account</strong></td>
					<td>
					<div class="input-group">
						<input type="text" name="salary_date" value="2018-00-00" class="form-control datepicker" > 
							<div class="input-group-addon"> 
								<a href="#"><i class="fa fa-calendar"></i></a>
                            </div>
                        </div>
					</td>
				</tr>
				<tr>
					<td><strong>Die kliënt gee toestemming om aftrekkings te maak. /<br/> The client gives consent to deduct from above account.</strong></td>
					<td>
						<select name="client_deduct_consent" class="form-control">
							<option value="">--</option>
							<option value="Ja">Ja / Yes</option>
							<option value="Nee">Nee / No</option>
						</select>
					</td>
				</tr>
			</table>
			<br/><br/>
			
	
			
			<table class="table table-bordered">
				<tr rowspan="4">
					<td colspan="4"><input type="hidden" name="lead_id" value="<?php echo (isset($_GET['lead_id']))? $_GET['lead_id']: ''; ?>" /><input type="submit" class="btn btn-primary pull-right" /></td>
				</tr>
			</table>
				
			
			   
			</form>
			</center>
	</div>
</div>
