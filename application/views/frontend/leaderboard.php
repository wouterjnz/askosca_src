<style>
table { font-size: 1.2em };
</style>
<h3>LEADERBOARD</h3>
<div class="row">
			
        <div class="col-lg-12">
            <!-- START widget-->
            <div class="panel widget">
                <div class="row row-table row-flush">
          <!--          <div class="panel-body">-->
          <!--              <h4>Outcomes</h4>-->
    			   <!--    	<table class="table table-bordered" style="text-align: center">-->
    						<!--<thead>-->
    						<!--<tr>-->
    						<!--<?php foreach($all_outcomes as $okey => $outcome){ ?>-->
    						<!--	<th style="text-align: center"><?php echo $okey; ?></th>-->
    						<!--<?php } ?>-->
    						<!--</td>-->
    						<!--</thead>-->
    						<!--<tbody>-->
    						<!--	<tr>-->
    						<!--	<?php foreach($all_outcomes as $okey => $outcome){ ?>-->
    						<!--		<td><?php echo $outcome; ?></td>-->
    						<!--	<?php } ?>	-->
    						<!--	</tr>-->
    						<!--</tbody>-->
    				  <!--  </table>-->
		    	   <!-- </div>-->
		    	   
		    	    <div class="panel-body">
                        
    			       	<table class="table table-bordered" style="text-align: center">
    						<thead>
    						<tr>
    						    <?php $ntotal_touched = 0; ?>
						        <?php $ntotal_interested = 0; ?>
    						<?php foreach($all_outcomes as $pkey => $outcome){ ?>
						        <?php if(trim($pkey) !== "INTERESTED"){ $ntotal_touched += $outcome; } ?>
						        <?php if(trim($pkey) == 'INTERESTED'){ $ntotal_interested += $outcome; } ?>
						    <?php } ?>	
								<td>Total Records Touched</td>
								<td>Total Interested</td>
    						</td>
    						</thead>
    						<tbody>
    							<tr>
    								<td><?php echo $ntotal_touched; ?></td>
    								<td><?php echo $ntotal_interested; ?></td>
    							</tr>
    						</tbody>
    				    </table>
		    	    </div>
		    	    
			</div>
	</div>
</div>
</div>



<div class="row">
			
        <div class="col-lg-12">
            <!-- START widget-->
            <div class="panel widget">
                <div class="row row-table row-flush">

                    <div class="panel-body">
                        

						<table class="table table-bordered">
							<thead>
								<th>Username</th>
								<?php foreach($outcomes as $outcome){ ?>
								<th  width="150px"><?php echo $outcome->lead_status; ?></th>
								<?php } ?>
								<th width="80px">Total</th>
								<th width="80px">%</th>
							</thead>
							<tbody>
								<?php foreach ($users as $key => $user) {  ?>
								<tr>
									<td><?php echo $key; ?></td>
											<?php $converstion_rate = 0; $interested = 0; $not_interested = 0; $total = 0; ?>
									<?php foreach ($outcomes as $outcome) { ?>
											<?php $new_outcome = 0; ?>
										<?php foreach ($user as $userkey => $useroutcome) { ?>
											<?php if ($outcome->lead_status_id == $userkey) { ?>
													<?php $new_outcome = $useroutcome;  $total+=$useroutcome; ?>
													<?php if($outcome->lead_status_id == 26){  $interested += $useroutcome; } ?>
													<?php //if($outcome->lead_status_id == 24){  $not_interested += $useroutcome; } ?>
											<?php } ?>
										<?php } ?>
											<td><?php echo $new_outcome; ?></td>
									<?php } ?>
									<td><?php echo $total; ?></td>
									<?php //$conversion_rate = $interested>0? (($not_interested/$interested) * 100):0; ?>
									<?php $conversion_rate = $interested>0? (($interested/$total) * 100):0; ?>
									<td><?php echo number_format($conversion_rate,2)."%"; ?>;</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>


						
						
						
                    </div>
                </div>
            </div>
        </div>

</div>