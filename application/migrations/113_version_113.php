<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Version_112 extends CI_Migration
{
    function __construct()
    {
        parent::__construct();
    }

    public function up()
    {
		$this->db->query("
			CREATE OR REPLACE VIEW view_campaign_leads AS
			 SELECT
				u.*,
				c.campaign_name,
				e.username,
				s.lead_status
             FROM campaign_leads u
			 left join tbl_campaigns c ON c.id = u.campaign_id
             left join tbl_users e ON e.user_id = u.user
             left join tbl_lead_status s ON s.lead_status_id = u.lead_status_id"
			);
			
		$this->db->query("
			CREATE OR REPLACE VIEW quality_assurance_queue AS
			 SELECT
				u.*,
				c.campaign_name,
				e.username,
				s.lead_status
             FROM campaign_leads u
				 left join tbl_campaigns c ON c.id = u.campaign_id
				 left join tbl_users e ON e.user_id = u.user
				 left join tbl_lead_status s ON s.lead_status_id = u.lead_status_id
			 WHERE 
				u.lead_status_id = 6 AND qa_status != 1 and deleted = 0
			 ");
			
			$this->db->query("
			CREATE OR REPLACE VIEW data_admin_queue AS
			 SELECT
				u.*,
				c.campaign_name,
				e.username,
				s.lead_status
             FROM campaign_leads u
				 left join tbl_campaigns c ON c.id = u.campaign_id
				 left join tbl_users e ON e.user_id = u.user
				 left join tbl_lead_status s ON s.lead_status_id = u.lead_status_id
			 WHERE 
				qa_status = 1 and deleted = 0
			 ");
		/*	 
			$this->db->query("
			CREATE OR REPLACE VIEW view_lead_list AS
			 SELECT
				l.*,
				c.campaign_name,
             FROM lead_list l
			 
			LEFT OUTER JOIN 
			   ( SELECT user_id,
						Count(*)  AS all_count                    
				 FROM   `campaign_leads`
					WHERE deleted = 0
				 GROUP  BY list_id
			   ) AS o ON au.id = o.list_id 
		   LEFT OUTER JOIN  
           ( SELECT user_id,
						Count(*)  AS all_count                    
				 FROM   `campaign_leads`
				 WHERE deleted = 0 and list_id = l.id
				 GROUP  BY list_id
			   )  AS j
				ON c.list_id = l.id
			 WHERE 
				deleted = 0
			 ");
		*/
		$this->db->query("
			CREATE OR REPLACE VIEW view_lead_list AS
			SELECT l.*, sum(IF(c.lead_status_id > 0, 1, 0)) as dispositioned, count(c.id) as total_leads
			FROM lead_list l
			JOIN campaign_leads c ON c.list_id = l.id
			GROUP BY l.id"
		);

	   $this->db->query("CREATE TABLE IF NOT EXISTS `list_template` (
        `template_id` int(11) NOT NULL,
        `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
        `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
			 
	   $this->db->query("CREATE TABLE IF NOT EXISTS `list_template_mapping` (
        `mapping_id` int(11) NOT NULL,
        `template_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT 0,
        `map_field` varchar(50) COLLATE utf8_unicode_ci NOT NULL
        `map_colum` varchar(50) COLLATE utf8_unicode_ci NOT NULL
        `map_value` varchar(50) COLLATE utf8_unicode_ci NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
		
		// lion of africa fields
		$this->db->query("ALTER TABLE `campaign_leads` 
		ADD (
		title varchar(10) NULL,
		effective_date varchar(16) NULL,
		policy_number varchar(30) NULL,
		broker_channel varchar(20) NULL,
		product varchar(25) NULL,
		inception_date varchar(16) NULL,
		policy_status varchar(25) NULL,
		policy_status_per_history varchar(25) NULL,
		relationship varchar(20) NULL,
		id_number varchar(20) NULL,
		date_of_birth varchar(10) NULL,
		age varchar(3) NULL,
		gender varchar(10) NULL,
		work_number varchar(25) NULL,
		cell_number varchar(25) NULL,
		address2 varchar(100) NULL,
		address3 varchar(100) NULL,
		address4 varchar(100) NULL,
		postal_code varchar(6) NULL,
		payment_method varchar(20) NULL,
		agent_code varchar(10) NULL,
		deduction_day varchar(2) NULL,
		debit_order_account_holder varchar(35) NULL,
		debit_order_bank_name varchar(25) NULL,
		debit_order_account_number varchar(25) NULL,
		debit_order_branch_code varchar(8) NULL,
		debit_order_account_type varchar(10) NULL,
		sum_assured_amount varchar(10) NULL,
		total_premium varchar(10) NULL,
		cover_amount varchar(10) NULL,
		total_cover varchar(10) NULL
		);");
			
			$this->db->query("ALTER TABLE `lead_list` ADD ( template_id int(11) NOT NULL );");
		
    }
	
}
